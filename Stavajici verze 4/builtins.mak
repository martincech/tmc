#
# Inprise C++Builder - (C) Copyright 1999 by Borland International
#

CC       = c51
AS       = a51
LINK = bl51
OH       = oh51
MAKE     = kmake

.a51.obj:
      $(AS) $< $(AFLAGS)

.c.obj:
      $(CC) $< $(CFLAGS) 

.c51.obj:
      $(CC) $< $(CFLAGS)

.exe.hex:
	$(OH) $&.exe

