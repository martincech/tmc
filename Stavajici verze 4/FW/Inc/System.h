//*****************************************************************************
//
//    System.h - "Operating system" primitives
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __System_H__
   #define __System_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

// Tento hlavickovy soubor obsahuje jen prototypy funkci.
// Jejich implementace je v projektove zavisla
// a je umistena v modulu Main.c projektu

// Uzivatelska uloha potrebuje pozastavit do doby, kdy vznikne
// nejaka udalost :

byte SysWaitEvent( void);
// Cekani na udalost. Vraci udalost

// Uzivatelska uloha vykonava cyklickou cinnost. V cyklu
// musi vyvolavat nasledujici funkci s periodou mensi, nez
// je perioda WatchDogu. Muze reagovat na vracenou udalost
// (napr. ukoncit cinnost po stisknuti Esc). Protoze
// tato funkce vyvolava obvykle obsluhu HW zarizeni, je
// treba zvolit okamzik volani funkce tak, aby nebyla
// v uzivatelske uloze rozpracovana nejaka HW sekvence
// (prepnute chipselecty, apod. Uloha musi uvolnit
// sbernici procesoru pro cinnost operacniho systemu)

byte SysYield( void);
// Prepnuti kontextu na operacni system. Vraci udalost

void SysFlushTimeout( void);
// Nuluje timeout klavesnice. Uzivatel explicitne vola
// SysYield, ale ignoruje klavesnici. Po delsi dobe
// by se mu vratil K_TIMEOUT

void SysDisableTimeout( void);
// Zakaze timeout klavesnice

void SysEnableTimeout( void);
// Povoli timeout klavesnice

void SysSetTimeout( void);
// Nastavi timeout klavesnice

void SysDelay( word ms);
// Zpozdeni v milisekundach

#endif
