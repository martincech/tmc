//*****************************************************************************
//
//    Cooling.c - Ovladani chlazeni
//    Version 1.0
//
//*****************************************************************************

#include "Tmc.h"
#include "Cooling.h"
#include "Din.h"               // Digitalni vstupy
#include "Dout.h"              // Digitalni vystupy
#include "Diesel.h"            // Diesel motor
#include "Alarm.h"             // Indikace poruch
#include "Ain.h"               // Analogove vstupy
#include "Thermo\Thermo.h"     // Prevod teplot

TCooling __xdata__ Cooling;

#ifndef __TMCREMOTE__

// Automat chlazeni
#define COOLING_AUTO_HYSTERESIS TempMk01C(0, 3)         // Natvrdo +-0.3C kolem cilove teploty

// Prepocet teploty na interni format regulatoru :
extern int __xdata__ RequestedTemperature;

// Promenne pro poruchu
static byte __xdata__ FaultCounter = 0; // Pocitadlo trvani poruchy

// Prumerovani tlaku
static dword __xdata__ SuctionPressureSum;
static word  __xdata__ SuctionPressureCount;
static dword __xdata__ DischargePressureSum;
static word  __xdata__ DischargePressureCount;

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void CoolingInit() {
  // Inicializace
  Cooling.Failure = NO;
  CoolingResetDutyCycle();
  CoolingResetPressure();
  Cooling.SuctionPressure   = 0;
  Cooling.DischargePressure = 0;
  FaultCounter = 0;
}

//-----------------------------------------------------------------------------
// Zapnuti chlazeni
//-----------------------------------------------------------------------------

static void CoolingStart() {
  // Zapne chlazeni a vynuluje poruchu
  Cooling.Failure = NO;         // Porucha nemuze byt
  FaultCounter    = 0;          // Pri pristim zapnuti budu opet pocitat od nuly
  Cooling.On = YES;
  DoutSetCooling(YES);
}

//-----------------------------------------------------------------------------
// Vypnuti chlazeni
//-----------------------------------------------------------------------------

void CoolingStop() {
  // Vypne chlazeni a vynuluje poruchu
  Cooling.Failure = NO;         // Porucha nemuze byt
  FaultCounter    = 0;          // Pri pristim zapnuti budu opet pocitat od nuly
  Cooling.On = NO;
  DoutSetCooling(NO);
}

//-----------------------------------------------------------------------------
// Porucha chlazeni
//-----------------------------------------------------------------------------

static void CoolingCheckFailure() {
  // Zjisti poruchu chlazeni
  AlarmCheckFaultCounter(Cooling.On && (DinHeatingCoolingError == DIN_COOLING_FAILURE), &FaultCounter, &Cooling.Failure);
}

//-----------------------------------------------------------------------------
// Vypocet tlaku
//-----------------------------------------------------------------------------

#define PRESSURE_TABLE_COUNT    2
static byte code SuctionAinTable[PRESSURE_TABLE_COUNT] = {0x50, 0xFA};           // Napeti Ain 3 a 9.68V
static byte code SuctionPressureTable[PRESSURE_TABLE_COUNT] = {0, 60};           // Tlak 0 a 6bar
static byte code DischargeAinTable[PRESSURE_TABLE_COUNT] = {0x36, 0xEC};         // Napeti Ain 2.25 a 9.34V
static byte code DischargePressureTable[PRESSURE_TABLE_COUNT] = {0, 240};        // Tlak 0 a 24bar

void CoolingCalcPressure() {
  // Vypocte aktualni hodnoty tlaku sani a vytlaku
  Cooling.SuctionPressure   = AinCalcValue(AinSuctionPressure,   SuctionAinTable,   SuctionPressureTable,   PRESSURE_TABLE_COUNT);
  Cooling.DischargePressure = AinCalcValue(AinDischargePressure, DischargeAinTable, DischargePressureTable, PRESSURE_TABLE_COUNT);
}

//-----------------------------------------------------------------------------
// Automat chlazeni
//-----------------------------------------------------------------------------

static void SetOutput(int Average) {
  if (Average == AUTO_TEMP_ERROR) {
    // Zadna pozadovana sonda nemeri
    CoolingStop();
    return;
  }
  // hystereze
  if (Cooling.On) {
    // chlazeni zapnuto
    if (Average < RequestedTemperature - COOLING_AUTO_HYSTERESIS) {
      // teplota pod nastavenou
      CoolingStop();                // vypnout chlazeni, nulovani poruchy
    }else{
       DoutSetCooling(YES);            // pro jistotu zapnout chlazeni, i kdyz by uz melo byt zapnute
    }
  } else {
    // chlazeni vypnuto
    if (Average > RequestedTemperature + COOLING_AUTO_HYSTERESIS) {
      // teplota nad nastavenou
      CoolingStart();               // zapnout chlazeni, poruchu pocitam od nuly
    } else{
       DoutSetCooling(NO);             // pro jistotu vypnout chlazeni, i kdyz by uz melo byt vypnute
    }

  }//else
  CoolingCheckFailure();            // Kontrola poruchy
}

void CoolingAutoExecute(int Average) {
  // Krok automatickeho regulatoru chlazeni, volat 1x za sekundu. <Average> je prumerna teplota podle lozeni ve vnitrnim formatu
  // Sepnu nebo vypnu kompresor, zaroven kontrola poruchy
  SetOutput(Average);

  // Vypoctu stridu chodu kompresoru
  DutyCycleExecute(&Cooling.DutyCycle, Cooling.On);

  // Vypoctu prumer tlaku pro ukladani

  if (!Cooling.On) {
    return;     // Prumer tlaku pocitam jen pokud je sepnuta spojka
  }

  // Aktualni hodnoty tlaku sani a vytlaku se pocitaji mimo, uz jsou vypoctene
  SuctionPressureSum += Cooling.SuctionPressure;
  SuctionPressureCount++;
  Cooling.SuctionPressureAverage = (dword)(SuctionPressureSum / (dword)SuctionPressureCount);

  DischargePressureSum += Cooling.DischargePressure;
  DischargePressureCount++;
  Cooling.DischargePressureAverage = (dword)(DischargePressureSum / (dword)DischargePressureCount);
}

//-----------------------------------------------------------------------------
// Snulovani stridy
//-----------------------------------------------------------------------------

void CoolingResetDutyCycle() {
  DutyCycleReset(&Cooling.DutyCycle);
}

//-----------------------------------------------------------------------------
// Snulovani prumerovani tlaku
//-----------------------------------------------------------------------------

void CoolingResetPressure() {
  SuctionPressureSum     = 0;
  SuctionPressureCount   = 0;
  DischargePressureSum   = 0;
  DischargePressureCount = 0;
  Cooling.SuctionPressureAverage   = 0;
  Cooling.DischargePressureAverage = 0;
}


#endif // __TMCREMOTE__
