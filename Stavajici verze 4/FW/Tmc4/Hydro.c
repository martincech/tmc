//*****************************************************************************
//
//    Hydro.c - Beh hydromotoru
//    Version 1.0
//
//*****************************************************************************

#include "Hydro.h"
#include "Diesel.h"            // Diesel motor
#include "ElMotor.h"           // Elektromotor
#include "Charging.h"          // Dobijeni
#include "Ain.h"               // Analogove vstupy
#include "Cooling.h"           // Regulace chlazeni
#include "Alarm.h"             // Indikace poruch

byte __xdata__ HydroFailure;    // YES/NO porucha hydromotoru


#ifndef __TMCREMOTE__

#define HYDRO_AV_FAILURE       0x35     // >5V je porucha

// Odezneni poruchy az po nejake dobe
static byte __xdata__ CounterErrorFree;       // Pocitadlo doby po odezneni chyby (LEDka blika, tj. chytnu ji jen nahodne)
#define WAIT_AFTER_ERROR 5                    // Kolik sekund pockat po odezneni chyby, nez se vyhlaseni poruchy zrusi


//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void HydroInit() {
  HydroFailure = NO;
}

//-----------------------------------------------------------------------------
// Chybovy stav
//-----------------------------------------------------------------------------

#define IsFailure() (AinHydroFailure > HYDRO_AV_FAILURE)

//-----------------------------------------------------------------------------
// Nacteni poruchy hydromotoru
//-----------------------------------------------------------------------------

void HydroExecute() {
  // Nacteni poruchy hydromotoru, volat az po nacteni behu dieselu, dobijeni a elektromotoru

  // Pokud nebezi chlazeni, urcite neni porucha, protoze hydropohon nebezi (zapina se prave signalem chlazeni)
  if (!Cooling.On) {
    HydroFailure = NO;
    return;
  }

  // Hydropohon muze bezet (ale nemusi). Kontroluju vyblikavani poruchy.

  if (IsFailure()) {
    // Narazil jsem na vyblikavani poruchy, hydromotor je v poruse
    HydroFailure = YES;
    CounterErrorFree = 0;             // Snuluju pocitadlo odezneni chyby
  } else {
    // Pocitam dobu odezneni poruchy a az pak poruchu zrusim
    CounterErrorFree++;
    if (CounterErrorFree > WAIT_AFTER_ERROR) {
      CounterErrorFree = WAIT_AFTER_ERROR;            // Aby counter nepretekl
      HydroFailure = NO;
    }
  }
  if (HydroFailure) {
    // Chyba se vyskytla bud v tomto kroku, nebo jeste trva z minula, alarm kazdopadne stale trva az do prepnuti rezimu
    AlarmSet();         // Vyhlasim alarm
  }
}

#endif // __TMCREMOTE__
