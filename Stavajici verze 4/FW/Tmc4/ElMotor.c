//*****************************************************************************
//
//    ElMotor.c - Beh elektromotoru
//    Version 1.0
//
//*****************************************************************************

#include "ElMotor.h"
#include "Diesel.h"            // Diesel motor
#include "Charging.h"          // Dobijeni

byte __xdata__ ElMotorOn = NO;   // YES/NO zapnuty elektromotor

#ifndef __TMCREMOTE__

// Beh elektromotoru - delam to stejne jako poruchy
static byte __xdata__ RunningCounter = 0;
#define MIN_RUNNING_TIME 5     // V sekundach

//-----------------------------------------------------------------------------
// Nacteni behu elektromotoru
//-----------------------------------------------------------------------------

void ElMotorExecute() {
  // Nacteni behu elektromotoru, volat az po nacteni behu dieselu a dobijeni
  ElMotorOn = NO;  // Default neni nastartovany
  if (Charging.On && !Diesel.On) {
    RunningCounter++;
    if (RunningCounter > MIN_RUNNING_TIME) {
      // Porucha uz trva dlouho
      RunningCounter = MIN_RUNNING_TIME;   // Aby pocitadlo nepreteklo
      ElMotorOn = YES;
    }//if
  } else {
    RunningCounter = 0;
  }//else
}

#endif // __TMCREMOTE__
