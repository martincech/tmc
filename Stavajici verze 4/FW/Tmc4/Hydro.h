//*****************************************************************************
//
//    Hydro.c - Beh hydromotoru
//    Version 1.0
//
//*****************************************************************************


#ifndef __Hydro_H__
   #define __Hydro_H__

#include "Hardware.h"     // pro podminenou kompilaci

extern byte __xdata__ HydroFailure;    // YES/NO porucha hydromotoru


void HydroInit();
  // Inicializace

void HydroExecute();
  // Nacteni poruchy hydromotoru, volat az po nacteni behu dieselu, dobijeni a elektromotoru

#endif
