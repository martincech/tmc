//*****************************************************************************
//
//    Main.c51       - TMC testovani
//    Version 1.00   (c) VymOs
//
//*****************************************************************************

/*

Verze 4.00.0: vychazi z verze 3.00.5
   - 12.2.2014: Pokud je auto osazene cidlem CO2, cerstvy vzduch se ridi podle CO2 (PID regulator).
   - 24.2.2014: Moznost prepnout jednotky teploty na stupne Celsia nebo Fahrenheita. Vse se interne pocita ve stupnich Celsia, tj.
                regulace zustala beze zmeny, prepocet na Fahrenheity se dela jen pro zobrazeni. Pouze primo zadavane teploty v TMC
                uzivatelem (cilova teplota, meze teploty) se ukladaji primo ve zvolenych jednotkach bez prepoctu. Cilova teplota
                se po zadani ve Fahrenheitech vnitrne prepocte na stupne Celsia ve vnitrnim formatu a regulace probiha jako dosud,
                tam jsem nedelal zadne zmeny.
   - 26.2.2014: Pokud v menu regulatoru zmeni jednotky teploty, nastavi se cilova teplota a max. odchylky nad a pod na defalut hodnoty
                pro dane jednotky (C/F).
   - 26.2.2014: Vlhkost se zobrazovala spatne, pokud byla 100%, zobrazovalo se jako 0%. Nyni omezeno na 99%. Chyba v RedrawHumidity().
   - 3.3.2014: PID regulace zmenena na obecny modul, ktery se pouziva u topeni, CO2 i naporovych klapek.
   - 14.3.2014: Otevirani klapek pri topeni zmeneno na PID regulaci
   - 18.3.2014: Digitalni vystup alarmu byl udelany chybne, po ztlumeni sireny v TMC se vypnul take vystup alarmu, coz je spatne.
                Nyni se alarm indikuje pred digitalni vystup i kdyz je sirena v TMC ztlumena.
   - 18.3.2014: Pokud je alarm a ridic ztlumi sirenu, sirena zustane ztlumena po dobu 5 minut a pokud i nadale trva alarm, sirena se
                zase zapne. Pokud ji ridic ztlumi podruhe behem toho sameho alarmu, zustane uz sirena mlcet naporad. Jakmile alarm
                zmizi nebo sirenu opet rucne povoli, vse se resetuje a pri dalsim alarmu funguje od zacatku.
   - 18.3.2014: Pokud je alarm ztiseny, zobrazuje se na hlavni obrazovce preskrtnuty reproduktor.
   - 18.3.2014: Pokud prave neni alarm, sirenu v TMC nelze ztlumit. Jde ztlumit, az kdyz je aktivni alarm. TMC tak nejde utisit dopredu.
   - 18.3.2014: Derivacni cas Td regulatoru topeni snizen z 80 na 40 sekund, servo topeni zbytecne oscilovalo.
   - 26.3.2014: Zmena struktury Logu, cerstvy vzduch ukladam na jednotky procent, navic se uklada porucha cidla CO2.
   - 28.3.2014: Do TMCR se na prvni pozici posila cislo verze TMC. Vyuzijeme pro pripadnou komunikaci s GPS od jineho vyrobce.
   - 1.4.2014: Delka RasData umele prodlouzena ze 44 na puvodnich 52 bajtu. Pri delce 44 bajtu blbla editace cislic v TMCR, neprisel jsem
               na to, proc. S delkou 52 bajtu to neblbne.

Verze 4.00.1:
   - 28.8.2014: U dodavky na 12V se vyhodnocuje vykon ventilatoru, muze byt rucne 50% (U<10V) nebo 100% (U>10V). Stejne upravy, jako ve verzi 3.00.6.
   - 28.8.2014: U dodavky s bufikem se spatne vyhodnocovala porucha topeni (nikdy se nehlasila, kdyz byl vypnuty jistic).
                Chyba v SwitchPower() a HeatingAutoStart(). Stejne upravy, jako ve verzi 3.00.7.

Verze 4.00.2:
   - 8.10.2014: U dodavky s bufikem se chybne ukladala strida sepnuti topeni jako strida plamene, pricemz tyto dve stridy si neodpovidaji.
                Bufik nema indikaci horeni plamene a kdyz je zapnuty, plamen spina a vypina automaticky. Nyni se uklada strida sepnuti
                bufiku jako vykon topeni a strida plamene je vzdy nulova.

Verze 4.01.0:
   - 28.11.2014: Do komunikace TMC-TMCR pridane nove veliciny pro sledovani GPS: beh motoru vozidla, tlak sani a vytlaku kompresoru, strida
                 horeni plamene topeni 1 a 2, strida chodu kompresoru a teplota vody v topnem okruhu. Nove veliciny se vlezly do nevyuziteho
                 prostoru na konci komunikace, tj. nema vliv na TMCR. Lze pouzivat i se starsi verzi TMCR4.
   - 28.11.2014: Maximalni cilova teplota zvysena z 35C na 39C, aby se dala vozit inkubovana vejce (LSL Rhein Main).
   - 28.11.2014: Default zesileni regulatoru CO2 snizene z 0.060 na 0.020 (v TMC se zobrazuje 2.000). V Agrofertu a ModernHatchi si stezovali,
                 se se napor hodne otevira. Pri zesileni 0.020 se napor otevre na 20% pri vzestupu CO2 z 2000 na 3000ppm.

Verze 4.02.0:
   - 19.1.2015: Pridana kontrola poruchy hydromotoru, zapisuje se do zaznamu, je i v komunikaci TMCR, tj. jde do GPS
   - 20.1.2015: Zcela zrusena podpora GSM modulu
   - 21.1.2015: Perioda vymeny oleje Kuboty zvysena na 350 motohodin

Verze 4.02.1:
   - 7.4.2015: Pokud je vyhlasen alarm, ovlada se navic externi sirena na DVY4. Pokud ztlumi interni reproduktor v TMC, ztlumi se i externi sirena.

Verze 4.02.2:
   - 11.8.2015: Default regulace CO2 zvysena z 2000 na 2500ppm, mez pro zobrazeni chyby zvysena z 3000 na 3500ppm. V lete nestihalo chlazeni,
                klapky byly moc otevrene na 20-30%.
   - 11.8.2015: Zmenena regulace klapek pri chlazeni. Klapky zacinam zavirat uz kdyz jsou venkovni T a T recirkulace shodne. Pri rozdilu 2C
                uz je napor zcela zavreny a dal se reguluje podle CO2. V lete byly problemy, ze i pri okolnich 33C se napor nechtel zavrit.
                Regulace klapek pri topeni zustala beze zmeny.

Verze 4.02.3:
   - 1.2.2016: Pri kazdem prechodu z ventilace na topeni u dodavky na chvili blikala chyba topeni. Chyba ve volani SwitchPower().

Verze 4.02.4:
   - 18.4.2016: Po startu blikala po dobu 5sec porucha PTO, ale TMC nepiskalo. Doplnena HydroInit().

Verze 4.02.5:
   - 19.7.2016: U Whitakeru jsme meli podezreni, ze se TMC rusenim dostane na chvili do nouzoveho rezimu, pak se hned prepne zpet do automatu,
               ale pri tom jednotka vypne ventilaci. Ridici se ventilace sama vypnula 12x za 2 hodiny. Pro jistotu po sepnuti spinace cekam
               4 sekundy, nez nouzovy rezim vezmu na vedomi (stejne jako u poruch). Nakonec to bylo volnym dratem, ale tuto funkci ponecham.
Verze 4.02.6:
   - 5.8.2016: Externi sirena se nyni ovlada tak, ze 3x piskne a pak 10 sekund mlci. Nesnese trvale piskani a casto odchazela.

Verze 4.02.7:
   - 1.10.2016: V menu pribyla dezinfekce skrine a kol, ovlada se zatim pomoci Loga. Je mozne ovladat i z TMCR. Pokud neni Logo nainstalovane, dezinfekce se v menu nezobrazuje.
   - 21.10.2016: Zruseno mereni a zobrazovani tlaku ventilatoru. Analogovy vstup se pouziva na pripravenost Loga pro dezinfekci.
   - 25.10.2016: Zmenen mail na uvodni obrazovce na info@veit.cz

Verze 4.03.0:
   - 10.6.2017: V menu Regulator - Fresh air pribyla definice zakazaneho pasma cerstveho vzduchu kvuli navesu Wimex s Hatchcare, kde jsme meli podezreni, ze na 50% naporu maji kurata problem.
                Behem regulace v automatu se klapky vzdy vyhnou tomuto zakazanemu pasmu, i kdyby byla teplota vysoka nebo vysoke CO2. Predpokladame, ze v tomto pasmu nefunguje dobre ventilace,
                takze se ji vyhneme nehlede na teplotu nebo CO2. Zakazne pasmo lze dat az do kraje (napr. 0-5% nebo 90-100%), pak klapka nedojede az na kraj. Nastavenim obou mezi na stejne
                hodnoty (napr 0-0) se zakazana oblast neuplatni. Rucne jdou klapky nastavit bez omezeni.
   - 10.6.2017: Pokud je porucha teplotnich cidel takova, ze nemuzu zmerit teplotu u kurat, nastavim natvrdo cerstvy vzuch na 100%. Dosud jsem nastavoval 50%, ale v souvislosti s predchozim bodem
                se 50% radeji vyhnu.

Verze 4.03.1:
   - 21.7.2017: Rozhodovaci napeti vykonu ventilace 100/50% u 12V dodavek snizena z 10V na 7V. Kvuli rele ETA jsem stahovali proud ventilatoru z 20 na 19A a pak to hlasilo 50%. Pokud jsou ventialtory
                umele zeslabene na 19A, je treba pouzit tuto verzi. U aut na 24V beze zmeny.

Verze 4.04.0:
   - 3.9.2017: V menu Diesel je nyni mozne vybrat typ motoru Kubota nebo Deutz. Po kazde zmene typu motoru se automaticky nastavi perioda vymeny oleje na 350hod u Kuboty nebo 500hod u Deutze. Periodu
               lze v nasledujicim menu rucne upravit podle potreby.
   - 3.9.2017: Pokud je motor typu Deutz, motohodiny se pocitaji 1:1 nehlede na otacky. U Kuboty zustava puvodni algoritmus, kdy se pri volnobehu pocitaji pomaleji.
   - 3.9.2017: Motor Deutz ma jinou teplotni krivku. Stejne cidlo meri u Deutze mene, nevime proc.

Verze 4.04.1:
   - 17.1.2018: Podlahove klapky lze pomoci USE_FLOOR_FLAPS_CONTROL nastavit tak, ze se bude klapka zobrazovat a kontrolovat jeji porucha, ale nepujde ovladat, tj. nepujde na ni kliknout. To je vhodne
                u dodavek s podlahovou klapkou, ktere byly dosud spatne, protoze se u nich nekontrolovala porucha podlahove klapky. HW verze 1-9 zustavaji beze zmeny, zmenene jsou az od HW verze 10.
   - 17.1.2018: Nove HW verze 10 a 11.

Verze 4.04.2:
   - 23.3.2018: Podlahove klapky Ultralightu se neoteviraji naplno, pouze o cca 45 stupnu, na 6V misto 10V. Pri otevreni se hlasila porucha podlahove klapky. Do HW definice zavedena nova polozka
                FLOOR_FLAP_ULTRALIGHT. Pokud je zvolena, podlahove servo se otevira v rozmezi 2-6V. Naporove servo beze zmeny 2-10V. Zmena pouze u Ultralightu (HW verze 10), ostatni beze zmen. Zmeny ve FAir.c51.
   - 26.3.2018: U Ultralightu se bohuzel podlahove servo na desce 6P krmi stejnym ridicim signalem jako naporova klapka, tj. 2-10V. Zpetny signal se ale zasekne na 6V a dal neroste. Poruchu tedy kontroluju
                pouze do ridiciho signalu 6V, nad 6V musi byt zpetny aspon 6V, ale uz neroste. Toto pujde odstranit, az kdyz se oddeli ridici signal pro naporove klapky a pro podlahovou klapku. Sice tedy z TMC
                krmim podlahovou klapku v rozsahu 2-6V, ale hardwarove se do serva stejne pusti 2-10V. Na Ultralightu je nutne oddelit ridici signaly podlahoveho serva a naporu, standardne byly dosud spojene.
                Pokud se neoddeli, naporova klapka se plne neotevre.

Verze 4.04.3:
   - 10.4.2018: Cekani po zapnuti topeni na vyhodnocovani chyby topeni prodlouzeno z 5 na 10 sekund. Preventivni opatreni, montujeme nova topeni, kde by doba 5sec nemusela stacit.

Verze 4.05.0:
   - 7.12.2017: Teplotni cidlo motoru v pripade Deutz autostart je pouzito pro indikaci chyby motoru
   - 7.12.2017: Chyba chlazeni - zvysen casovac oznameni poruchy chlazeni na 15 sekund
   - 5.2.2018:  Opozdeni signalu chlazeni o 4 sekundy oproti pozadavku - osetreno sepnut spojky kompresoru az do vysokych otacek pri prepnuti ventilace/chlazeni s autostartem

Dalsi komentare presunuty do confluence a gitu
*/



#include "Hardware.h"
#include "System.h"     // "operacni system"
#include "Rd2\PcaTmc.h"     // PCA projektu TMC
#include "Ads7846\Ads7846.h"    // touch screen
#include "Menu.h"              // Zobrazeni
#include "Din.h"               // Digitalni vstupy
#include "Dout.h"              // Digitalni vystupy
#include "Ain.h"               // Analogove vstupy
#include "Aout.h"              // Analogove vystupy
#include "Temp.h"              // Teplomery
#include "Accu.h"              // Napeti a proud akumulatoru
#include "Log.h"               // Ukladani hodnot
#include "Time.h"              // Aktualni datum a cas
#include "Beep.h"              // Repro
#include "FAir.h"              // Regulace cerstrveho vzduchu
#include "Cfg.h"               // Konfigurace
#include "Servo.h"             // Kontrola stavu serv
#include "Alarm.h"             // Indikace poruch
#include "Diesel.h"            // Diesel motor
#include "Charging.h"          // Dobijeni
#include "ElMotor.h"           // Elektromotor
#include "TruckEn.h"           // Motor vozidla
#include "Fan.h"               // Ventilatory
#include "Control.h"           // Automaticke rizeni topeni/chlazeni
#include "Print.h"             // Tisk protokolu
#include "Ras.h"               // Terminalovy server
#include "Disinf.h"            // Dezinfekce


//#define DEBUG_MODE      1       // Debug mod pro hledani problemu s rele, pri kterem se automaticky prepina mezi automatem a ventilaci
#ifdef DEBUG_MODE
byte xdata DebugModeCounter = 0;
#endif // DEBUG_MODE

// odpocet necinnosti :
#define USER_EVENT_TIMEOUT 250    // uzivatel neobsluhuje dele nez ... s

// casy pro blikani :
#define BLINK_ON   25           // cas zapnuti roviny
#define BLINK_OFF  20           // cas vypnuti roviny

// start odpocitavani 1 s :
#define SetTimer1s()  counter1s = 1000 / TIMER0_PERIOD

// Lokalni promenne :
volatile byte counter1s = 0;        // odpocitavani 1 s
volatile bit  timer1s = 0;        // priznak odpocitani 1 s
char counter_blink = BLINK_ON;  // pocitadlo blikani znamenko urcuje smer citani
volatile bit timer_blink = 0;         // priznak blikani
volatile bit blink_on = 1;         // blikani - roznout, zhasnout

TConfig __xdata__ Config;             // buffer konfigurace v externi RAM, Tmc.h

static byte BrightnessCounter = 0;    // Pocitadlo v preruseni
extern byte Brightness;               // Nastaveny podsvit MIN_PODSVIT az MAX_PODSVIT

byte _Timeout = 0;                    // pocitani necinnosti - kvuli tisku protokolu jsem to musel vytahnout ven z fce SysYield()


// ---------------------------------------------------------------------------------------------
// Cekani na napeti po startu
// ---------------------------------------------------------------------------------------------

#define DELAY_LOWBAT          1000      // Cekani po obnove napajeni v milisekundach

static void WaitForVoltage (void) {
   // Po zapnuti jednotky ceka na jeho vztust napajeni, po obnoveni napajeni jeste nejakou dobu cekam
   while (1) {
      if (TmcAccuOk ()) {
         SysDelay (DELAY_LOWBAT);
         if (TmcAccuOk ()) {
            return;                         // Po dobu DELAY_LOWBAT je napajeni v poradku
         }
      }
      WatchDog ();
   }
}

// ---------------------------------------------------------------------------------------------
// Main
// ---------------------------------------------------------------------------------------------

#define TestujPinRychle(Pin)    \
while (EA) {                    \
  Pin = 0;                      \
  WatchDog();                   \
  Pin = 1;                      \
}

#define TestujPinPomalu(Pin)    \
while (EA) {                    \
  Pin = 0;                      \
  SysDelay(1);                  \
  Pin = 1;                      \
  SysDelay(1);                  \
}

void main () {
   // Systemova inicializace : ---------------------------------------------------
   TimingSetup ();              // Nastaveni X2 modu
   EnableXRAM ();               // Povoleni pristupu k vnitrni XRAM
   WDTPRG = WDTPRG_2090;       // start Watch Dogu
   WatchDog ();

   EnableInts ();               // Povoleni preruseni
   Timer0Run (TIMER0_PERIOD);  // spust casovac 0
   Timer1Run (TIMER1_PERIOD);  // spust casovac 1

   DisplayPodsvit (YES);

   // Inicializace modulu : ------------------------------------------------------

   WaitForVoltage ();    // 4.1.2007: Pocka, az vzroste napeti. Pokud to zde neni, pri upgrad na novejsi verzi se neprovede SetDetaults() v CfgLoad(),
                        // protoze indikace korektniho napeti jeste neni spravne nahozena (pripadne tam dojde k oscilaci).
   CfgLoad ();           // Musi byt pred inicializaci zobrazeni (zobrazuje se tam identifikacni cislo)
   MenuInit ();
   TouchInit ();
   DinInit ();
   DoutInit ();
   AinInit ();
   AoutInit ();
   TimeInit ();
   TemperatureInit ();
   AccuInit ();
   FanInit ();
   LogInit ();   // V inicializaci EEPROM je while(ready) => muze zpusobovat reset po zapnuti pri vadne EEPROM
   PcaInit ();
   FAirInit ();
   ServoInit ();
   AlarmInit ();
   DieselInit ();
   DisinfectionInit ();
   ControlInit ();
   PrintInit ();
   RasInit ();


   BeepStartup ();   // Musi byt az po PcaInit()
   SysDelay (3000);

   SetTimer1s ();               // zahajeni odpoctu 1 s

   // Logovani startu a login ridice
   Driver = 0;                  // Start systemu zaprotokoluju s ridicem cislo 0
   TimeRead ();                  // Nacteni aktualniho data a casu - pro zaprotokolovani startu systemu a logovani
   LogWriteProtocol (PRT_START, 0);       // zaprotokolujeme nabeh systemu
   if (Config.Login) {
      Driver = 1;                // Default ridic 1, kdyby dal behem loginu Cancel
      MenuLogin ();               // Nalogovani ridice
      DiagnosticCounter = 0;     // Aby bylo mozne prejit do diagnostickeho rezimu i pokud pouziva logovani
   }

   while (1) {
      switch (SysWaitEvent ()) {
      case K_TOUCH:
         MenuTouch ();
         break;

      case K_BLINK_ON:
         // Blikani 2. rovinou
         MenuBlink (YES);
         break;

      case K_BLINK_OFF:
         // Blikani 2. rovinou
         MenuBlink (NO);
         break;

      case K_REDRAW:
         MenuRedraw ();
         break;

      case K_TIMEOUT:
         break;

      default:
         break;
      }//switch
   }//while
} // main

//-----------------------------------------------------------------------------
// Prepnuti kontextu na "operacni system" viz System.h
//-----------------------------------------------------------------------------

// Kontrola touchpanelu, vola se casteji v SysYield()
#define CheckTouch()    \
  if (TouchGet()) {     \
    _Timeout = 0;       \
    return( K_TOUCH);   \
  }


byte SysYield ()
// Prepnuti kontextu na operacni system. Vraci udalost
{
   WatchDog ();
   CheckTouch ();
   // Terminal
   RasExecute ();                // Volat co nejcasteji, aby nebylo zpozdeni ve zpracovani
   // casovac periodicke cinnosti :
   if (timer1s) {
      // uplynula 1s
      timer1s = 0;              // zrus priznak
      SetTimer1s ();             // novy cyklus
      _Timeout++;               // pocitame necinnost
      if (DiagnosticCounter < MAX_DIAGNOSTIC_COUNTER) {
         DiagnosticCounter++;    // Volba diagnostiky je jeste platna
      }

      // Debug prepinani rezimu
#ifdef DEBUG_MODE
      if (++DebugModeCounter >= 10) {
         DebugModeCounter = 0;
         switch (Control.Mode) {
         case MODE_AUTO:
            ControlSetNewMode (MODE_VENTILATION);
            break;

         case MODE_VENTILATION:
            ControlSetNewMode (MODE_AUTO);
            break;
         }
         Redraw = REDRAW_ALL;
      }
#endif // DEBUG_MODE

      // Cinnost:
      AlarmClear ();             // Nulovani poruch na zacatku kazdeho mericiho cyklu
      TimeRead ();               // Nacteni aktualniho data a casu
      DinRead ();                // Nacteni digitalnich vstupu
      AinRead ();                // Nacteni analogovych vstupu
      TemperatureRead ();        // Nacteni teplot
#ifndef NO_DIESEL
      DieselExecute ();          // Kontrola behu, motohodin a teploty dieselu
#endif
      TruckEngineExecute ();     // Nacteni behu motoru vozidla
      AccuCheck (Diesel.On || ElMotorOn || TruckEngineOn);     // Vypocet napeti a proudu akumulatoru
      ChargingExecute ();        // Kontrola dobijeni
      ElMotorExecute ();         // Kontrola behu elektromotoru, volat az po nacteni behu dieselu a dobijeni
      FanCheck ();               // Kontrola ventilatoru
      DisinfectionExecute ();    // Dezinfekce
      ControlExecute ();         // Automaticke rizeni topeni/chlazeni
      FAirExecute ();            // Vypocet polohy klapek - az po ControlExecute(), abych mohl vyuzivat promennou Control.AverageTemperature
      ServoCheck ();             // Kontrola poruch serv
      LogExecute ();             // Zapis do FIFO

      if (Control.EmergencyMode) {
         AlarmClear ();           // V nouzovem rezimu nemuze byt alarm (ale muze se volanim fci vyhlasit, takze ho tady snuluju)
      }
      AlarmExecute ();           // Obslouzim piskani alarmu na konci kazdeho mericiho cyklu
      CheckTouch ();

      Redraw |= REDRAW_PERIODIC;  // Prekreslim nove nactene hodnoty
      return(K_REDRAW);
      }
   // casovac blikani :
   if (timer_blink) {
      // uplynul cas blikani
      timer_blink = 0;         // zrus priznak
      if (blink_on) {
         return(K_BLINK_ON);
      }
      else {
         return(K_BLINK_OFF);
      }
   }
   // testuj necinnost :
   if (_Timeout > USER_EVENT_TIMEOUT) {
      _Timeout = 0;             // zahajime dalsi cekani
      return(K_TIMEOUT);
   }
   CheckTouch ();
   return(K_IDLE);             // zadna udalost
   } // SysYield

   //-----------------------------------------------------------------------------
   // Cekani na udalost viz System.h
   //-----------------------------------------------------------------------------

byte SysWaitEvent (void)
// Cekani na udalost
{
   byte key;

   while (1) {
      key = SysYield ();
      if (key != K_IDLE) {
         return(key);      // neprazdna udalost
      }
   }
} // SysWaitEvent

//-----------------------------------------------------------------------------
// Nulovani timeoutu viz System.h
//-----------------------------------------------------------------------------

void SysFlushTimeout (void)
// Nuluje timeout klavesnice
{
   _Timeout = 0;
   // nastavit kontext v modulu Kbd, aby nezustal vysilat treba K_REPEAT
} // SysFlush_Timeout

//-----------------------------------------------------------------------------
// Nastaveni timeoutu viz System.h
//-----------------------------------------------------------------------------

void SysSetTimeout (void)
// Nastavi timeout klavesnice
{
   _Timeout = USER_EVENT_TIMEOUT + 1;
} // SysSetTimeout

//-----------------------------------------------------------------------------
// Prerusovaci rutina casovace 0
//-----------------------------------------------------------------------------

static void Timer0Handler (void) interrupt INT_TIMER0
// Prerusovaci rutina casovace 0
{
   SetTimer0 (TIMER0_PERIOD);                // s touto periodou
   // vykonne funkce :
   CheckTrigger (counter1s, timer1s = 1);    // casovac 1 s
   PcaTrigger ();                             // handler asynchronniho pipani
   TouchTrigger ();                           // handler touch panelu
   // casovac blikani :
   counter_blink--;
   if (counter_blink == 0) {
      // docitali jsme, nastav na novou hodnotu
      timer_blink = 1;                       // nastav priznak
      if (blink_on) {
         counter_blink = -256 + BLINK_OFF;   // bylo roznuto,  perioda pro zhasni
      }
      else {
         counter_blink = BLINK_ON;           // bylo zhasnuto, perioda pro rozsvit
      }
      blink_on = !blink_on;                  // novy smer citani
   }
} // Timer0Handler

//-----------------------------------------------------------------------------
// Prerusovaci rutina casovace 1
//-----------------------------------------------------------------------------

static void Timer1Handler (void) interrupt INT_TIMER1
// Prerusovaci rutina casovace 0
{
   SetTimer1 (TIMER1_PERIOD);                // s touto periodou
   // vykonne funkce :
   // Podsvit
   BrightnessCounter++;
   if (BrightnessCounter <= Brightness) {
      DisplayPodsvit (YES);
   }
   else {
      DisplayPodsvit (NO);
   }
   if (BrightnessCounter > BRIGHTNESS_MAX) {
      BrightnessCounter = 0;
   }
} // Timer1Handler

//-----------------------------------------------------------------------------
// Zpozdeni
//-----------------------------------------------------------------------------

void SysDelay (unsigned int n)
{
   // Uprava pro krystal 18.432MHz
   unsigned char m;                                                                     // m = R5, n = R6+R7
                                                                             // T = 1.085 mikrosec.
   while (n > 0) {              // DELAY: SETB C/MOV A,R7/SUBB A,#0/MOV A,R6/SUBB A,#0/JC KONEC *7T*
      WDTRST = 0x1E;
      WDTRST = 0xE1;
      m = 255; do --m; while (m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
      m = 255; do --m; while (m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
      m = 253; do --m; while (m != 0);                                // MOV R5,#193/DJNZ R5,$ *387T*
      // Pro X2 mod musim 2x tolik!!
      m = 255; do --m; while (m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
      m = 255; do --m; while (m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
      m = 253; do --m; while (m != 0);                                // MOV R5,#193/DJNZ R5,$ *387T*
      --n;                          // MOV A,R7/DEC R7/JNZ ODSKOK/DEC R6/ODSKOK: SJMP DELAY *5T(4T)*
   }                                                                             // SJMP DELAY *2T*
} // SysDelay
