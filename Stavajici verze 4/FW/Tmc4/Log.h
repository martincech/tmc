//*****************************************************************************
//
//    Log.c - Ukladani do FIFO
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Log_H__
   #define __Log_H__

#include "Hardware.h"     // pro podminenou kompilaci

extern bit _UkladatZaznamy;
extern byte __xdata__ Driver;                 // Cislo nalogovaneho ridice

//-----------------------------------------------------------------------------
// Zapisy do FIFO
//-----------------------------------------------------------------------------

void LogInit( void);
// Inicializuje modul

// Pozastaveni zapisu
#define LogPause()      _UkladatZaznamy = NO

// Uvolneni zapisu
#define LogResume()     _UkladatZaznamy = YES

void LogExecute( void);
// Prekopiruje aktualni stav do FifoData a v urceny cas zapise
// Volat po mereni

void LogWriteProtocol(byte type, dword parameter);
// Ulozi do logu udalost

TYesNo LogReset( void);
// Smaze logger

void LogClear( void);
// Nulovani kumulovanych velicin loggeru

void LogCreateStamp();
// Vyplneni razitka

TYesNo LogCopy( void);
// Prekopiruje interni EEPROM na externi modul

#endif

