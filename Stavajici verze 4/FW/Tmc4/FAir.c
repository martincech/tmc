//*****************************************************************************
//
//    FAir.c - Ovladani cerstveho vzduchu
//    Version 1.0
//
//*****************************************************************************

#include "FAir.h"
#include "Tmc.h"               // Rizeni
#include "Temp.h"              // Teploty
#include "Aout.h"              // Analogove vystupy
#include "Control.h"           // Automaticke rizeni
#include "Co2.h"               // Regulator CO2
#include "Ds18x20\Ds18x20.h"    // Prevod teploty na interni format

TFreshAir __xdata__ FreshAir;

#ifndef __TMCREMOTE__

#define FAIR_TEMPERATURE_RANGE                  (TempMk1C(4))           // +-2 stupen = 4 stupne
#define FAIR_TEMPERATURE_RANGE_COOLING          (TempMk1C(2))           // Pro chlazeni zaviram klapky rychleji, v pasmu 2C

#ifdef USE_ON_OFF_HEATING
#define FAIR_HEATING_OFFSET                (TempMk01C(0, 4))       // U bufiku: Pri vzrustu prumerne teploty o 0.4C nad cilovou teprve zacinam otevirat napor
#else
#define FAIR_HEATING_OFFSET                (TempMk01C(0, 2))       // U Webasta: Pri vzrustu prumerne teploty o 0.2C nad cilovou teprve zacinam otevirat napor
#endif // USE_ON_OFF_HEATING
#define FAIR_HEATING_OUTSIDE_TEMP_LIMIT    (TempMk1C(5))           // Od okolni teploty >= 5C provadim rizeni klapek pri auto topeni vyssi rychlosti

#define FAIR_MIN                0               // Minimalni hodnota cerstveho vzduchu odpovidajici zcela zavrenemu servu
#define FAIR_MAX                100             // Maximalni hodnota cerstveho vzduchu odpovidajici zcela otevrenemu servu
#define FAIR_RANGE              (FAIR_MAX - FAIR_MIN)        // Rozsah cerstveho vzduchu v procentech

#ifdef USE_CO2

// Pokud pouziva rizeni podle CO2, v rucnim rezimu a automatu podle teploty nezaviram napor uplne
#define FAIR_MANUAL_MIN         10              // Minimalni hodnota cerstveho vzduchu v rucnim rezimu klapek
#define FAIR_MANUAL_MAX         100             // Maximalni hodnota cerstveho vzduchu v rucnim rezimu klapek
#define FAIR_MANUAL_STEP        9               // Prirustek v % v manualnim rezimu (celkem 10 kroku)

#define FAIR_AUTO_TEMP_MIN      10              // Minimalni hodnota cerstveho vzduchu v automatu podle teploty. Odpovida napeti 2.8V.
#define FAIR_AUTO_TEMP_MAX      100             // Maximalni hodnota cerstveho vzduchu v automatu podle teploty

#define FAIR_AUTO_CO2_MIN       0               // Minimalni hodnota cerstveho vzduchu v automatu podle CO2
#define FAIR_AUTO_CO2_MAX       100             // Maximalni hodnota cerstveho vzduchu v automatu podle CO2

#else

// Pokud se nepouziva cidlo CO2, v rucnim rezimu a automatu podle teploty zaviram napor uplne, na servu je zarazka
#define FAIR_MANUAL_MIN         0               // Minimalni hodnota cerstveho vzduchu v rucnim rezimu klapek
#define FAIR_MANUAL_MAX         100             // Maximalni hodnota cerstveho vzduchu v rucnim rezimu klapek
#define FAIR_MANUAL_STEP        10              // Prirustek v % v manualnim rezimu (celkem 10 kroku)

#define FAIR_AUTO_TEMP_MIN      0               // Minimalni hodnota cerstveho vzduchu v automatu podle teploty
#define FAIR_AUTO_TEMP_MAX      100             // Maximalni hodnota cerstveho vzduchu v automatu podle teploty

#define FAIR_AUTO_CO2_MIN       0               // Minimalni hodnota cerstveho vzduchu v automatu podle CO2
#define FAIR_AUTO_CO2_MAX       100             // Maximalni hodnota cerstveho vzduchu v automatu podle CO2

#endif // USE_CO2

#define FAIR_MANUAL_RANGE       (FAIR_MANUAL_MAX - FAIR_MANUAL_MIN)        // Rozsah cerstveho vzduchu v procentech
#define FAIR_AUTO_TEMP_RANGE    (FAIR_AUTO_TEMP_MAX - FAIR_AUTO_TEMP_MIN)        // Rozsah cerstveho vzduchu v procentech
#define FAIR_AUTO_CO2_RANGE     (FAIR_AUTO_CO2_MAX - FAIR_AUTO_CO2_MIN)        // Rozsah cerstveho vzduchu v procentech

#define FAIR_MIN_DAC            0x0B            // Vystup DA prevodniku, aby dal 2V (zcela zavrene servo)
#define FAIR_MAX_DAC            0x3E            // Vystup DA prevodniku, aby dal 10V (zcela otevrene servo)
#define FAIR_MAX_DAC_UL         0x24            // Vystup DA prevodniku, aby dal 6V (zcela otevrene servo u Ultralightu, otvira se jen cca do pulky)

// Konstanty PID regulatoru :
#define     REG_TS        10             // vzorkovaci perioda v sekundach

// Prepocet teploty na interni format regulatoru :
extern int __xdata__ RequestedTemperature;

//-----------------------------------------------------------------------------
// Lokalni funkce
//-----------------------------------------------------------------------------

static void SetFAirHeating(int Rozdil);
  // Nastavi automaticky klapky pri topeni, <Rozdil> je okolni - recirkulacni teplota + FAIR_TEMPERATURE_RANGE / 2

static void SetFAirCooling(int Rozdil);
  // Nastavi automaticky klapky pri chlazeni, <Rozdil> je okolni - recirkulacni teplota + FAIR_TEMPERATURE_RANGE / 2

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void FAirInit() {
  // Inicializace
  FreshAir.Mode    = FAIR_AUTO;
  FreshAir.Percent = FAIR_AUTO_TEMP_MIN;        // Radsi trosku pootevrene servo
  FreshAir.FloorFlap.Mode = FLAP_AUTO;
  FreshAir.FloorFlap.Percent = 0;

  // Regulator klapek
  FreshAir.Pid.SamplingPeriod = REG_TS;
  FreshAir.Pid.KpDenominator  = 1;       // Kp je cele cislo
  FreshAir.Pid.TiMax          = 1000;
  FreshAir.Pid.LsbDenominator = 16;      // LSB teploty je 1/16
  FAirRegulatorUpdateParameters();
  // Target do PID ukladam az v kroku regulatoru, tady neni treba
  PidInit(&FreshAir.Pid);
}

//-----------------------------------------------------------------------------
// Predani parametru regulatoru
//-----------------------------------------------------------------------------

void FAirRegulatorUpdateParameters() {
  // Preberu parametry z configu do PID regulatoru
  FreshAir.Pid.Kp = -(int)Config.FreshAirRegKp;        // Otocene, pri vzrustajicim rozdilu teplot chci otevirat napor
  FreshAir.Pid.Ti =  Config.FreshAirRegTi;
  FreshAir.Pid.Td =  Config.FreshAirRegTd;
}

//-----------------------------------------------------------------------------
// Novy rezim cerstveho vzduchu
//-----------------------------------------------------------------------------

void FAirNewMode(TFreshAirMode Mode) {
  // Nastavi novy rezim cerstveho vzduchu

  // Pokud prave prepnul na automat klapek, inicializuju PID regulator podle CO2 i regulator klapek, aby tam netrcely stare hodnoty
  if (FreshAir.Mode != FAIR_AUTO && Mode == FAIR_AUTO) {
    PidInit(&FreshAir.Pid);
#ifdef USE_CO2
    Co2RegulatorInit();
#endif // USE_CO2
  }

#ifdef USE_CO2
  // Pokud se pouziva rizeni podle CO2, musim osetrit specialni situace
  // Pokud prave prepnul na rucni rezim, nesmim povolit uplne zavreni naporu. Pokud byl v automatu napor
  // zcela zavreny podle regulace CO2, tak po prepnuti do rucniho rezimu by se drzelo otevreni z automatu.
  // V tomto pripade napor trosku pootevru
  if (FreshAir.Mode != FAIR_MANUAL && Mode == FAIR_MANUAL && FreshAir.Percent < FAIR_MANUAL_MIN) {
    FreshAir.Percent = FAIR_MANUAL_MIN;  // Omezim
  }
#endif // USE_CO2

  // Nastavim novy rezim
  FreshAir.Mode = Mode;
}

//-----------------------------------------------------------------------------
// Novy automaticky rezim (prepnuti z auto topeni na chlazeni a naopak)
//-----------------------------------------------------------------------------

void FAirNewAutoMode() {
  // Novy automaticky rezim (prepnuti z auto topeni na chlazeni a naopak)
  PidInit(&FreshAir.Pid);       // Inicializuju PID regulaci pootevirani klapek
}

//-----------------------------------------------------------------------------
// Pridani cerstveho vzduchu v rucnim rezimu
//-----------------------------------------------------------------------------

void FAirManualIncrease() {
  // Pridani cerstveho vzduchu v rucnim rezimu
  FreshAir.Percent += FAIR_MANUAL_STEP;
  if (FreshAir.Percent > FAIR_MANUAL_MAX) {
    FreshAir.Percent = FAIR_MANUAL_MAX;   // Omezim
  }
  FreshAir.Percent -= FreshAir.Percent % FAIR_MANUAL_STEP;  // Zaokrouhlim na krok
}

//-----------------------------------------------------------------------------
// Ubrani cerstveho vzduchu v rucnim rezimu
//-----------------------------------------------------------------------------

void FAirManualDecrease() {
  // Ubrani cerstveho vzduchu v rucnim rezimu
  if (FreshAir.Percent < FAIR_MANUAL_STEP) {
    FreshAir.Percent = FAIR_MANUAL_MIN;
  } else {
    FreshAir.Percent -= FAIR_MANUAL_STEP;
  }
  if (FreshAir.Percent < FAIR_MANUAL_MIN) {
    FreshAir.Percent = FAIR_MANUAL_MIN;  // Omezim
  }
  FreshAir.Percent -= FreshAir.Percent % FAIR_MANUAL_STEP;  // Zaokrouhlim na krok
}

//-----------------------------------------------------------------------------
// Novy rezim podlahovych klapek
//-----------------------------------------------------------------------------

void FAirNewFloorFlapMode(TFlapMode Mode) {
  // Nastavi novy rezim klapky v podlaze

  // Pokud se pouziva rizeni podle CO2, musim osetrit specialni situace
#ifdef USE_CO2
  // Pokud prave prepnul na rucni rezim, nesmim povolit uplne zavreni podlahy. Pokud byl v automatu napor
  // zcela zavreny podle regulace CO2, tak po prepnuti do rucniho rezimu by se drzelo otevreni z automatu.
  // V tomto pripade podlahu trosku pootevru
  if (FreshAir.FloorFlap.Mode != FLAP_MANUAL && Mode == FLAP_MANUAL && FreshAir.FloorFlap.Percent < FAIR_MANUAL_MIN) {
    FreshAir.FloorFlap.Percent = FAIR_MANUAL_MIN;  // Omezim
  }
#endif // USE_CO2

  // Nastavim novy rezim
  FreshAir.FloorFlap.Mode = Mode;
}

//-----------------------------------------------------------------------------
// Pridani (otevreni) podlahove klapky v rucnim rezimu
//-----------------------------------------------------------------------------

void FAirFloorFlapManualIncrease() {
  // Pridani (otevreni) podlahove klapky v rucnim rezimu
  byte Percent;         // Usetri 29 bajtu

  Percent = FreshAir.FloorFlap.Percent;
  Percent += FAIR_MANUAL_STEP;
  if (Percent > FAIR_MANUAL_MAX) {
    Percent = FAIR_MANUAL_MAX;   // Omezim
  }
  Percent -= Percent % FAIR_MANUAL_STEP;  // Zaokrouhlim na krok
  FreshAir.FloorFlap.Percent = Percent;
}

//-----------------------------------------------------------------------------
// Ubrani (zavreni) podlahove klapky v rucnim rezimu
//-----------------------------------------------------------------------------

void FAirFloorFlapManualDecrease() {
  // Ubrani (zavreni) podlahove klapky v rucnim rezimu
  byte Percent;         // Usetri 13 bajtu

  Percent = FreshAir.FloorFlap.Percent;
  if (Percent < FAIR_MANUAL_STEP) {
    Percent = 0;
  } else {
    Percent -= FAIR_MANUAL_STEP;
  }
  if (Percent < FAIR_MANUAL_MIN) {
    Percent = FAIR_MANUAL_MIN;  // Omezim
  }
  Percent -= Percent % FAIR_MANUAL_STEP;  // Zaokrouhlim na krok
  FreshAir.FloorFlap.Percent = Percent;
}

//-----------------------------------------------------------------------------
// Prepocet procent na polohu serva
//-----------------------------------------------------------------------------

byte FAirCalculateServoPosition(byte Percent) {
  // Prepoctu hodnotu v procentech na polohu serva, procenta jsou v mezich FAIR_MIN az FAIR_MAX, pocet poloh serva je FAIR_FLAP_STEPS
  if (Percent < FAIR_MIN) {
    Percent = FAIR_MIN;
  }
  if (Percent > FAIR_MAX) {
    Percent = FAIR_MAX;
  }
  Percent -= FAIR_MIN;
  Percent += (FAIR_MAX - FAIR_MIN) / ( 2 * (FAIR_FLAP_STEPS - 1));  // Pripoctu polovinu kroku pro zaokrouhleni
  return (unsigned int)((unsigned int)Percent * (unsigned int)(FAIR_FLAP_STEPS - 1)/(unsigned int)(FAIR_MAX - FAIR_MIN));
}

//-----------------------------------------------------------------------------
// Krok regulace
//-----------------------------------------------------------------------------

static byte PrepoctiCerstvyVzduchNaDAC(byte CerstvyVzduch, byte MaxDAC) {
  // Prepocte cerstvy vzduch z procent na analogovy vystup (DAC) - pouziva kalibraci FRESH_AIR_MIN_DAC az <MaxDAC>
  if (CerstvyVzduch < FAIR_MIN) CerstvyVzduch = FAIR_MIN;
  if (CerstvyVzduch > FAIR_MAX) CerstvyVzduch = FAIR_MAX;
  CerstvyVzduch -= FAIR_MIN;
  return (word)((word)FAIR_MIN_DAC + (word)CerstvyVzduch * (word)(MaxDAC - FAIR_MIN_DAC) / (word)(FAIR_MAX-FAIR_MIN));
}

#ifdef USE_CO2
static void SetFreshAirByCo2() {
  // Pokud je cidlo CO2 v poruse, nema cenu nic delat
  if (Co2.Failure) {
    Co2RegulatorInit();
    // Cerstvy vzduch necham nastaveny podle teploty
    return;
  }

  // Pokud nemam zmerenou platnou koncentraci CO2, regulaci zatim neprovadim podle CO2 neprovadim, ponecham regulaci podle teploty
  if (!Co2.IsValueValid) {
    return;
  }

  Co2RegulatorStep();

  if (FreshAir.Percent == FAIR_AUTO_TEMP_MIN) {
    // Podle regulace klapek na zaklade teploty jsou klapky plne zavrene (se skvirou). Muzu regulovat podle CO2, klapky muzu privrit i vice otevrit.
    // V prvnim kroku regulace po spusteni ventilace je CO2 nulove, nez zacne merit cidlo CO2. Hned po startu nastavim napor podle automatu teploty (se skvirou),
    // jinak by se napor zcela zavrel (CO2 je nulove). V prvnim kroku sem nelezu.
    FreshAir.Percent = Co2.FreshAir;
    return;
  }

  // Klapky jsou podle teploty lehce pootevrene, podle CO2 je muzu pouze otevrit vice, nemuzu je privrit
  if (Co2.FreshAir > FreshAir.Percent) {
    // Podle CO2 se ma napor otevrit vice, pootevru tedy podle CO2 (kurata nemaji kyslik)
    FreshAir.Percent = Co2.FreshAir;
  }
}
#endif // USE_CO2

static void SetFreshAirByForbiddenRange() {
  // Omezim cerstvy vzduch podle zakazaneho pasma
  if (Config.MinFreshAirForbidden >= Config.MaxFreshAirForbidden) {
    // Zakazane pasmo neni definovane, neomezuju. Nastaveni stejnych hodnot vyradi zakazane pasmo z provozu
    return;
  }
  if (FreshAir.Percent < Config.MinFreshAirForbidden || FreshAir.Percent > Config.MaxFreshAirForbidden) {
    // Cerstvy vzduch je mimo zakazane pasmo
    return;
  }

  // Cerstvy vzduch je v zakazanem pasmu a tam nema co delat. Rozhodnu, na kterou stranu mimo pasmo je bliz a tam ho nastavim

  // Test, zda je zakazane pasmo az na okraji otevreni/zavreni
  if (Config.MinFreshAirForbidden == FAIR_MIN) {
    // Zakazane pasmo zacina uplne vlevo (napr. 0-5%), nastavim maximalni hodnotu
    if (Config.MaxFreshAirForbidden < FAIR_MAX) {
      FreshAir.Percent = Config.MaxFreshAirForbidden + 1;
    } else {
      FreshAir.Percent = FAIR_MAX;              // Je zakazane cele pasmo 0-100%, ale neco nastavit musim
    }
    return;
  }
  if (Config.MaxFreshAirForbidden == FAIR_MAX) {
    // Zakazane pasmo konci uplne vpravo (napr. 90-100%), nastavim minimalni hodnotu
    FreshAir.Percent = Config.MinFreshAirForbidden - 1;         // Min mez urcite neni 0 (to testuju vys), muzu si dovolit o jedno mensi
    return;
  }

  // Zakazane pasmo neni na kraji, je nekde uprostred
  if (FreshAir.Percent - Config.MinFreshAirForbidden < Config.MaxFreshAirForbidden - FreshAir.Percent) {
    // Je to bliz k minimu
    FreshAir.Percent = Config.MinFreshAirForbidden - 1;         // Min mez urcite neni 0 (to testuju vys), muzu si dovolit o jedno mensi
  } else {
    // Je to bliz k maximu
    FreshAir.Percent = Config.MaxFreshAirForbidden + 1;         // Max mez urcite neni 100 (to testuju vys), muzu si dovolit o jedno vetsi
  }
}

void FAirExecute() {
  // Nastavi serva naporu, recirkulace a podlahy do pozadovane polohy
  int Rozdil;    // Rozdil teploty okoli a vyfukovane (v internim formatu)

  // Pokud je prepinac Fresh air v poloze AUTO, nastavim vystup fresh air podle okolni a vyfukovane teploty. Jinak nastavim natvrdo podle polohy.
  // Reguulaci provadim s periodou AREG_TS sekund, stejnou jako u regulatoru teploty. Pricitani a nulovani citace ma na starosti fce HeatingRegulatorStep().
  if (FreshAir.Mode == FAIR_AUTO) {
    // Recirkulace je v automatickem rezimu, vypocitam polohu klapek
    switch (Control.Mode) {
      case MODE_OFF: {
        // V poloze OFF zavru napor a podlahu a otevru recirkulaci
        FreshAir.Percent = FAIR_MIN;    // Zcela zavru, nenechavam zadnou skviru
        break;
      }
      case MODE_VENTILATION: {
        // V poloze ventilace napor na plno otevru - nevim, zda chce topit nebo chladit. Regulace podle cilove teploty by to mohlo kmitat.
        FreshAir.Percent = FAIR_MAX;
        break;
      }
      default: { // MODE_AUTO
        // Automaticky rezim

        // Otestuju pripadnou poruchu vsech cidel ve skrini
        if (Control.AverageTemperature == AUTO_TEMP_ERROR) {
          FreshAir.Percent = 100;       // Nastavim natvrdo na napor
          break;                        // Nepokracuju na automatickou regulaci
        }
        // Pokud nejake cidlo meri, pokracuju
        // V ostatnich polohach provadim regulaci podle okolni a vyfukovane teploty - vim, zda chce chladit nebo topit
        // Reguluju proporcionalne, s pasmem proporcionality FRESH_AIR_PROPORTIONAL_BAND
        if (Temp[TEMP_OUTSIDE] == AUTO_TEMP_ERROR || Temp[TEMP_RECIRCULATION] == AUTO_TEMP_ERROR) {
          // Jedna z teplot nemeri => nastavim natvrdo
          FreshAir.Percent = 100;
        } else {
          // Teploty jsou v poradku, muzu je brat v uvahu
          Rozdil = Temp[TEMP_OUTSIDE] - Temp[TEMP_RECIRCULATION];
          Rozdil += FAIR_TEMPERATURE_RANGE / 2;  // Posunu si to do pocatku, z +-1C udelam 0-2C
          if (Control.AutoMode == AUTO_MODE_HEATING) {
            // Prave se topi => chci do skrine privadet teplejsi vzduch
            SetFAirHeating(Rozdil);
          } else {
            // Prave se chladi => chci do skrine privadet chladnejsi vzduch
            SetFAirCooling(Rozdil);
          }
        }//else

#ifdef USE_CO2
        // Nyni mam vypoctenou polohu naporu na zaklade teplot. Vypoctu take polohu na zaklade regulace CO2 a zvolim spravne otevreni
        SetFreshAirByCo2();
#endif // USE_CO2

        // Omezim podle zakazaneho pasma, to az na zaver, v zakazanem pasmu klapky nesmi byt, protoze tam napr. nefunguje dobre ventilace kurat.
        // Vse kvuli podezreni v navesu Wimex s HatchCare, kdy s cerstvym vzduchem 50% hlasili problemy uprostred navesu
        // Pokud by se napr. klapky drzely privrene dele a stoupala by teplota nebo CO2, velmi rychle se naintegruje rozdil a klapky se otevrou vic (preskoci se zakazane pasmo).
        // Asi je mensi zlo nechat trochu nastoupat CO2 nez nastavit klapky do zakazaneho pasma, kde nemusi byt provetrana vsechna kurata
        SetFreshAirByForbiddenRange();

        // Po vypoctu radsi omezim
        if (FreshAir.Percent < FAIR_MIN) {
          FreshAir.Percent = FAIR_MIN;
        }
        if (FreshAir.Percent > FAIR_MAX) {
          FreshAir.Percent = FAIR_MAX;
        }
      }//default
    }//switch
  }
  // Podlahova klapka - pokud je v automatickem rezimu, nastavim ji stejne jako napor. V manualnim rezimu ponecham rucne nastavenou hodnotu.
  if (FreshAir.FloorFlap.Mode == FLAP_AUTO) {
    // Podlahova klapka je v automatickem rezimu
    FreshAir.FloorFlap.Percent = FreshAir.Percent;
  }
  // Vypoctu polohy klapek pro prekresleni a logovani
  FreshAir.Position = FAirCalculateServoPosition(FreshAir.Percent);
  FreshAir.FloorFlap.Position = FAirCalculateServoPosition(FreshAir.FloorFlap.Percent);
  // Nastavim analogove vystupy
  AoutSetInductionServo(PrepoctiCerstvyVzduchNaDAC(FreshAir.Percent, FAIR_MAX_DAC));
#ifdef FLOOR_FLAP_ULTRALIGHT
  // Podlahova klapka Ultralightu, otevira se jen cca do pulky
  AoutSetFloorServo(PrepoctiCerstvyVzduchNaDAC(FreshAir.FloorFlap.Percent, FAIR_MAX_DAC_UL));
#else
  // Klasicke auto, kde se podlahova klapka otvira naplno
  AoutSetFloorServo(PrepoctiCerstvyVzduchNaDAC(FreshAir.FloorFlap.Percent, FAIR_MAX_DAC));
#endif
}

//-----------------------------------------------------------------------------
// Nastaveni klapek pri topeni
//-----------------------------------------------------------------------------

static void SetFAirHeating(int Rozdil) {
  // Nastavi automaticky klapky pri topeni, <Rozdil> je okolni - recirkulacni teplota + FAIR_TEMPERATURE_RANGE / 2

  if (Rozdil <= 0) {
    // Okoli je chladnejsi nez recirkulace. Bud klapky zavru, nebo je podle prumerne teploty ovladam specialne
    if (Control.Mode == MODE_AUTO && Control.AverageTemperature > RequestedTemperature + FAIR_HEATING_OFFSET) {
      // Jsem v automatu a prumerna teplota ve skrini je o FAIR_HEATING_OFFSET vyssi jak cilova teplota.
      // Pro rucni topeni tato regulace klapek nema vyznam (nemuzu brat v uvahu cilovou teplotu, ta muze byt nyni libovolna)
      // Reguluju klapky tak, aby byly zcela zavrene pri prumerne teplote = cilova a zcela otevrene pri prumerne = cilova + hystereze (kdy by melo
      // dojit k prepnuti do chlazeni). Melo by se tak zabranit oscilacim mezi topenim a chlazenim.

      // Krok regulatoru
      FreshAir.Pid.Target = (int)(RequestedTemperature + FAIR_HEATING_OFFSET) >> 4;  // Update cilove teploty, mohla se zmenit. Spodni 4 bity se nemeri
      PidStep(&FreshAir.Pid, Control.AverageTemperature >> 4);                  // Spodni 4 bity se nemeri

      // Vystup regulatoru je 0..100%, ale otevreni naporu je FAIR_AUTO_TEMP_MIN..100% (nejde od nuly). Prepoctu.
      FreshAir.Percent = FAIR_AUTO_TEMP_MIN + (int)(FAIR_AUTO_TEMP_RANGE * (int)FreshAir.Pid.Output / 100);
    } else {
      // Teplota venku je uz moc studena, zcela zavru
      FreshAir.Percent = FAIR_AUTO_TEMP_MIN;
      PidInit(&FreshAir.Pid);           // Klapky se nereguluji podle PID
    }
  } else {
    PidInit(&FreshAir.Pid);           // Klapky se nereguluji podle PID
    if (Rozdil >= FAIR_TEMPERATURE_RANGE) {
      // Teplota venku je dost tepla (o FAIR_TEMPERATURE_RANGE teplejsi nez recirkulace), zcela otevru a budu tak brat teply vzduch z vnejsku
      FreshAir.Percent = FAIR_AUTO_TEMP_MAX;
    } else {
      // Rozdil je v pasmu proporcionality, delam proporcionalni regulaci
      FreshAir.Percent = FAIR_AUTO_TEMP_MIN;    // Kvuli zrychleni na dvakrat
      FreshAir.Percent += (long)((long)Rozdil * (long)FAIR_AUTO_TEMP_RANGE / (long)FAIR_TEMPERATURE_RANGE);
    }
  }
} // SetFAirHeating

//-----------------------------------------------------------------------------
// Nastaveni klapek pri chlazeni
//-----------------------------------------------------------------------------

static void SetFAirCooling(int Rozdil) {
  // Nastavi automaticky klapky pri chlazeni, <Rozdil> je okolni - recirkulacni teplota + FAIR_TEMPERATURE_RANGE / 2
  Rozdil -= FAIR_TEMPERATURE_RANGE / 2;  // Uvedu zase do puvodniho stavu, abych nemusel menit kod mimo tuto fci. Rozdil je ted okolni - recirkulacni teplota.

  if (Rozdil <= 0) {
    // Teplota venku je dost studena, zcela otevru
    FreshAir.Percent = FAIR_AUTO_TEMP_MAX;
  } else if (Rozdil >= FAIR_TEMPERATURE_RANGE_COOLING) {
    // Teplota venku je uz moc tepla, zcela zavru, daleko lepsi vzduch je ve skrini
    FreshAir.Percent = FAIR_AUTO_TEMP_MIN;
  } else {
    // Rozdil je v pasmu proporcionality, delam proporcionalni regulaci
    FreshAir.Percent = FAIR_AUTO_TEMP_MAX;    // Kvuli zrychleni na dvakrat
    FreshAir.Percent -= (long)((long)Rozdil * (long)FAIR_AUTO_TEMP_RANGE / (long)FAIR_TEMPERATURE_RANGE_COOLING);
  }
  PidInit(&FreshAir.Pid);             // Klapky se nereguluji podle PID
} // SetFAirCooling


#endif // __TMCREMOTE__
