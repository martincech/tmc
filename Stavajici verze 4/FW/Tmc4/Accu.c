//*****************************************************************************
//
//    Accu.c - Mereni napeti a proudu akumulatoru
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#include "Accu.h"
#include "Ain.h"                 // Analogove vstupy
#include "Alarm.h"               // Indikace poruch
#include "Adc0838\Adc0838.h"      // AD converter - ADC_RANGE

// Globalni promenne
TAccu __xdata__ Accu;

#ifndef __TMCREMOTE__

// Promenne pro prumerovani
#define POCET_PRUMERU_AKU 3                     // Kolik hodnot se bere do prumeru u mereni napeti a proudu akumulatoru
typedef struct {
  word Sum;     // Suma
  byte Count;   // Pocet prvku v sume
  byte Value;   // Aktualni prumerna hodnota
} TAverage;
static TAverage __xdata__ VoltageAverage;       // Prumerovani napeti
static byte __xdata__     FirstMeasurement;     // Flag, ze se meri prvni hodnota a mam ji hned zobrazit, bez prumerovani - aby to po zapnuti nezustalo prazdne

// Rozsahy
// Kalibracni konstanty mereni :
#define ACCU_VOLTAGE_MIN        0                 // merime od 0
#define ACCU_VOLTAGE_MAX     3353                 // maximalni rozsah * 0.01V = 255 LSB
#define ACCU_VOLTAGE_RANGE (ACCU_VOLTAGE_MAX - ACCU_VOLTAGE_MIN)    // rozsah napeti V

// Promenne pro poruchu
static byte __xdata__ FaultCounter = 0; // Pocitadlo trvani poruchy
#define MAX_FAULT_TIME 60               // Maximalni doba trvani poruchy v sekundach 

// Lokalni fce
static byte SaveValue(TAverage xdata *Average, byte value);


//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void AccuInit() {
  // Inicializace mereni akumulatoru
  Accu.LowVoltage = NO;
  FaultCounter    = 0;          // Pocitam od nuly

  VoltageAverage.Sum   = 0;
  VoltageAverage.Count = 0;
  FirstMeasurement     = YES;

}

//-----------------------------------------------------------------------------
// Kontrola akumulatoru
//-----------------------------------------------------------------------------

void AccuCheck(TYesNo IsDieselRunning) {
  // Zkontroluje napeti a proud akumulatoru. Volat podle potreby
  dword value;
  byte MinVoltage;

  // Napeti prepocteme na cele volty :
  value = AinVoltage;
  value *= ACCU_VOLTAGE_RANGE;
  value /= (ADC_RANGE * 100);
  if (SaveValue(&VoltageAverage, value + ACCU_VOLTAGE_MIN)) {
    // Je zprumerovano, ulozim
    Accu.Voltage = VoltageAverage.Value;
  }//if

  // Hlidani nizkeho napeti - musim provadet kazdy cyklus
  MinVoltage = IsDieselRunning ? ACCU_MIN_VOLTAGE_DIESEL : ACCU_MIN_VOLTAGE;    // Min. napeti stanovim podle toho, zda bezi diesel
  AlarmCheckFaultCounterMaxTime(Accu.Voltage < MinVoltage, &FaultCounter, &Accu.LowVoltage, MAX_FAULT_TIME);
  FirstMeasurement = NO;        // Zmerili jsme po startu
} // AccuCheck

//-----------------------------------------------------------------------------
// Prumerovani hodnot
//-----------------------------------------------------------------------------

static byte SaveValue(TAverage xdata *Average, byte value) {
  // Zprumeruje a ulozi napeti, pri nove hodnote vrati YES
  Average->Sum += value;
  Average->Count++;
  if (Average->Count >= POCET_PRUMERU_AKU) {
    Average->Value = Average->Sum / Average->Count;
    Average->Sum   = 0;   // Sumuju od zacatku
    Average->Count = 0;
    return YES;
  }//if
  if (FirstMeasurement) {
    // Meri se poprve
    Average->Value = value;          // Prvni zmerenou hodnotu beru bez prumerovani
    return YES;
  }//if
  return NO;
} // SaveValue

#endif // __TMCREMOTE__
