//*****************************************************************************
//
//    AirPress.c - Mereni tlakove ztraty vzduchu
//    Version 1.0
//
//*****************************************************************************

#include "Tmc.h"
#include "AirPress.h"
#include "Ain.h"               // Analogove vstupy
#include "Alarm.h"             // Indikace poruch

byte __xdata__ FiltersAirPressure;
byte __xdata__ FiltersHighAirPressure;

#ifndef __TMCREMOTE__

#include "Stat\Average.h"    // Klouzavy prumer

// Vypocet prumeru - tlak dost lita, tak radsi prumeruju
static TAverage __xdata__ FiltersAirPressureAverage;

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void AirPressureInit() {
  // Inicializace
  FiltersAirPressure     = 0;
  FiltersHighAirPressure = NO;

  // Inicializace klouzaveho prumeru podle konfigurace
  AverageSetLength(&FiltersAirPressureAverage, AVERAGE_CAPACITY);
  AverageClear(&FiltersAirPressureAverage);
}

//-----------------------------------------------------------------------------
// Vypocet
//-----------------------------------------------------------------------------

#if defined(USE_FILTER_PRESSURE)

#define AIR_PRESSURE_TABLE_COUNT    2
static byte code AinTable[AIR_PRESSURE_TABLE_COUNT ] = {0, 0xFF};         // Napeti Ain 0 a 10V
static byte code AirPressureTable[AIR_PRESSURE_TABLE_COUNT ] = {0, 500 / AIR_PRESSURE_LSB};          // Tlakova ztrata 0 a 500 Pa (dilek AIR_PRESSURE_LSB)

#endif // USE_FILTER_PRESSURE

void AirPressureCalc() {
  // Tlak filtru

#ifdef EMC_TEST_MODE
  FiltersAirPressure = 40 / AIR_PRESSURE_LSB;
  FiltersHighAirPressure = NO;
  return;
#endif

#ifdef USE_FILTER_PRESSURE
  AverageAdd(&FiltersAirPressureAverage, AinCalcValue(AinFiltersPressure, AinTable, AirPressureTable, AIR_PRESSURE_TABLE_COUNT));
  FiltersAirPressure = AverageGetAverage(&FiltersAirPressureAverage);
  if ((word)FiltersAirPressure * AIR_PRESSURE_LSB > Config.MaxFiltersAirPressure) {
    FiltersHighAirPressure = YES;
    AlarmSet();
  } else {
    FiltersHighAirPressure = NO;
  }
#else
  FiltersAirPressure     = 0;
  FiltersHighAirPressure = NO;
#endif // USE_FILTER_PRESSURE

}

#endif // __TMCREMOTE__
