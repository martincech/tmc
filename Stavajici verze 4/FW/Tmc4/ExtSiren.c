//*****************************************************************************
//
//    ExtSiren.c - Ovladani externi sireny
//    Version 1.0
//
//*****************************************************************************

#include "ExtSiren.h"
#include "Dout.h"              // Digitalni vystupy

#ifndef __TMCREMOTE__


// Struktura pro ovladani sireny (sirena nepiska pri chybe porad, ale prerusovane)
#define SIREN_ON_TIME  4                // Doba zapnuti v sekundach - behem teto periody navic sirena houka prerusovane
#define SIREN_OFF_TIME 10               // Doba vypnuti v sekundach

typedef enum {
  SIREN_OFF,                            // Sirena je prave vypnuta, neni zadna porucha
  SIREN_FAULT_ON,                       // Je porucha, sirena je prave zapnuta
  SIREN_FAULT_OFF                       // Je porucha, sirena je prave vypnuta
} TSirenState;

typedef struct {
  TSirenState Status;                   // Aktualni stav sireny
  byte Counter;                         // Pocitadlo pro dobu on/off
} TSiren;
static TSiren __xdata__ Siren;

#define SirenOn() DoutSetExternalSiren(YES)       // Zapnu externi sirenu
#define SirenOff() DoutSetExternalSiren(NO)       // Vypnu externi sirenu


//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void ExtSirenInit() {
  // Inicializuje externi sireny
  Siren.Status = SIREN_OFF;    // Citac se vynuluje po zmene stavu
  SirenOff();
}

//-----------------------------------------------------------------------------
// Obsluha externi sireny
//-----------------------------------------------------------------------------

void ExtSirenExecute() {
  // Obsluha externi sireny, volat kazdou sekundu
  Siren.Counter++;
  switch (Siren.Status) {
    case SIREN_OFF: {
      // Sirena je prave vypnuta => ihned po vyskytu poruchy ji zapnu
      Siren.Status  = SIREN_FAULT_ON;
      Siren.Counter = 0;        // Pocitam od nuly
      break;
    }
    case SIREN_FAULT_ON: {
      // Sirena je prave zapnuta, hlidam, aby nehoukala moc dlouho
      if (Siren.Counter > SIREN_ON_TIME) {
        // Majak uz sviti danou dobu => vypnu ho
        Siren.Status  = SIREN_FAULT_OFF;
        Siren.Counter = 0;        // Pocitam od nuly
      }
      break;
    }
    case SIREN_FAULT_OFF: {
      // Sirena je prave vypnuta, ale porucha trva. Hlidam, aby sirena nebyla vypnuta moc dlouho.
      if (Siren.Counter > SIREN_OFF_TIME) {
        // Sirean je uz vypnuta danou dobu => zase ji zapnu
        Siren.Status  = SIREN_FAULT_ON;
        Siren.Counter = 0;        // Pocitam od nuly
      }
      break;
    }
  }//switch

  if (Siren.Status == SIREN_FAULT_ON && Siren.Counter % 2 == 0) {
    // Zapnu sirenu - behem periody SIREN_FAULT_ON ovladam i prerusovane piskani
    SirenOn();
  } else {
    // Vypnu sirenu
    SirenOff();
  }
}

#endif // __TMCREMOTE__
