//*****************************************************************************
//
//    ElMotor.c - Beh elektromotoru
//    Version 1.0
//
//*****************************************************************************


#ifndef __ElMotor_H__
   #define __ElMotor_H__

#include "Hardware.h"     // pro podminenou kompilaci

extern byte __xdata__ ElMotorOn;    // YES/NO zapnuty elektromotor


void ElMotorExecute();
  // Nacteni behu elektromotoru, volat az po nacteni behu dieselu a dobijeni

#endif
