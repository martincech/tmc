//*****************************************************************************
//
//    Aout.c - Analogove vystupy
//    Version 1.0
//
//*****************************************************************************

#include "Aout.h"

byte __xdata__ AoutInductionServo;       // Ridici signal serva naporu a recirkulace
byte __xdata__ AoutFloorServo;           // Ridici signal serva v podlaze
byte __xdata__ AoutHeatingServo;         // Ridici signal serva topeni

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void AoutInit() {
  // Inicializuje analogovych vystupu
  DacInit();     // Inicializace
}

//-----------------------------------------------------------------------------
// Nastaveni jednotlivych vystupu
//-----------------------------------------------------------------------------

void AoutSetInductionServo(byte Value) {
  // Nastavi obe naporova serva a recirkulacni servo
  AoutInductionServo = Value;
  DacWrite(TmcAVY12, Value);
  DacWrite(TmcAVY12 + 1, Value);
}

void AoutSetHeatingServo(byte Value) {
  // Nastavi servo topeni (je na 2 vystupech)
  AoutHeatingServo = Value;
  DacWrite(TmcAVY34, Value);
  DacWrite(TmcAVY34 + 1, Value);
}

void AoutSetFloorServo(byte Value) {
  // Nastavi servo v podlaze
  AoutFloorServo = Value;
  DacWrite(TmcAVY5, Value);
}

