//*****************************************************************************
//
//    Menu.c - TMC display module
//    Version 1.0
//
//*****************************************************************************

#include "Menu.h"
#include "MenuPos.h"           // Souradnice pro zobrazeni
#include "Temp.h"              // Teploty
#include "Diag.h"              // Diagnosticke zobrazeni
#include "Beep.h"              // Repro
#include "FAir.h"              // Regulace cerstrveho vzduchu
#include "Heating.h"           // Regulace topeni
#include "Cooling.h"           // Regulace chlazeni
#include "Tmc.h"               // struktura rizeni
#include "Cfg.h"               // Konfigurace
#include "Servo.h"             // Stav serv
#include "Accu.h"              // Napeti a proud akumulatoru
#include "Log.h"               // Logovani do pameti (mazani logu)
#include "Diesel.h"            // Diesel motor
#include "Charging.h"          // Dobijeni
#include "ElMotor.h"           // Elektromotor
#include "Fan.h"               // Ventilatory
#include "Control.h"           // Automaticke rizeni
#include "Alarm.h"             // Indikace poruch
#include "Print.h"             // Tisk protokolu
#include "Co2.h"               // Koncentrace CO2
#include "Humidity.h"          // Relativni vlhkost
#include "AirPress.h"          // Tlak radiatoru a filtru
#include "Disinf.h"            // Dezinfekce
#include "System.h"     // "operacni system"
#include "Sed1335\Sed1335.h"    // display
#include "Pg320240\Pg320240.h"   // display
#include "conio.h"      // jednoduchy display
#include "bcd.h"
#include "Ads7846\Ads7846.h"    // touch screen
#include "TouchCtl\TouchCtl.h"   // Pracovni plocha touch screenu
#include "Rx8025\Rx8025.h"     // RTC pro prime nastaveni data a casu v menu
#include "X9313\X9313.h"      // e2pot
#include "Thermo\Thermo.h"     // TempMk1C()

#include "..\Tmcr4\Tmcr.h"      // Stav komunikace

TDisplayMode __xdata__ DisplayMode;
TDisplayMode __xdata__ OldDisplayMode;
byte __xdata__ Redraw;
byte __xdata__ DiagnosticCounter;// Pocitadlo aktivity volby diagnostickeho rezimu
byte __xdata__ DiagnosticMode;  // YES/NO zobrazeni diagnostiky
byte Brightness = BRIGHTNESS_MAX;      // Zpracovava se v preruseni, radeji v DATA - chci, aby rovnou po zapnuti jednotky svitil

TUn UN;                             // datovy typ je definovan v uni.h

// Lokalni promenne
byte _Klavesa;                    // interni buffer klavesy

#define MENU_FADE     300   // Doba cekani po vyberu v milisekundach

// Ovladani podlahovych klapek
#define MENU_POLOZKA_X   8
#define MENU_POLOZKA_DX  (30*8)
#define MENU_POLOZKA_DY  36
#define MENU_YPrvniPolozky 3
#define CANCEL_Y (MENU_YPrvniPolozky + 200)


// Lokalni funkce:
static void DrawTemperature1C (byte X, byte Y, char Temp);
// Zobrazi teplotu s presnosti 1C na pozici X, Y (X je osmice pixelu)

static void PrepareMenu ();
// Provede nezbytne ukony pred vstupem do menu

static void DrawSmallButton (byte x, byte y, byte code *symbol);
// Vykresli male tlacitko 32x32px se symbolem uprostred

static void DrawBigButton (byte x, byte y, byte code *symbol, TYesNo thick);
// Vykresli velke tlacitko 48x48px se symbolem uprostred

static void MenuWait ();
// Zobrazi na displeji Cekejte..


#define ContrastUp() E2PotDown()
// Zvysi kontrast displeje o 1 krok

#define ContrastDown() E2PotUp()
// Snizi kontrast displeje o 1 krok

#define BrightnessUp()   if (Brightness < BRIGHTNESS_MAX) Brightness++
// Zvysi podsvit displeje o 1 krok

#define BrightnessDown() if (Brightness > BRIGHTNESS_MIN) Brightness--
// Snizi podsvit displeje o 1 krok

void MenuSetup ();
// Menu nastaveni

void MenuRecords ();
// Menu zaznamy

void MenuPrintProtocol ();
// Vytiskne zaznamy za zvolene obdobi

//-----------------------------------------------------------------------------
// Poloha mrizky (pouziva se dale)
//-----------------------------------------------------------------------------

#define GRID_X1                 11
#define GRID_X2                 26

//-----------------------------------------------------------------------------
// Stringy - uvodni obrazovka
//-----------------------------------------------------------------------------

byte code STR_STARTUP_1[] = "VEIT Electronics";
byte code STR_STARTUP_2[] = "tel: +420 545 235 252";
byte code STR_STARTUP_3[] = "e-mail: info@veit.cz";
byte code STR_STARTUP_4[] = "www.veit.cz";

//-----------------------------------------------------------------------------
// Stringy - cestina
//-----------------------------------------------------------------------------

#ifdef LANG_CZ

byte code STR_ZADEJTE_CILOVOU_TEPLOTU[] = "Zadejte c\x08Elovou teplotu:";
byte code STR_STUPNE_CELSIA[] = "\x07FC";
byte code STR_STUPNE_FAHRENHEITA[] = "\x07FF";
byte code STR_STUPNE_KRAT_SEKUNDY[] = "\x07FC*s";
byte code STR_SEKUND[] = "s";
byte code STR_HODIN[] = "h";
byte code STR_CHYBA_NEPLATNA_HODNOTA[] = "Nen\x08E zadan\x089 platn\x089 hodnota";

byte code STR_LOZENI_PREPRAVEK[] = "N\x089klad";

byte code STR_ZADEJTE_DATUM[] = "Zadejte aktu\x089ln\x08E datum:";
byte code STR_ZADEJTE_CAS[] = "Zadejte aktu\x089ln\x08E \x08Aas:";

byte code STR_SMAZAT_PAMET_1[] = "Chcete smazat v\x092echny";
byte code STR_SMAZAT_PAMET_2[] = "z\x089znamy z pam\x08Dti?";

byte code STR_OK[] = "OK";
byte code STR_OPAKOVAT[] = "Znovu";
byte code STR_ZRUSIT[] = "Zru\x092it";
byte code STR_ANO[] = "Ano";
byte code STR_NE[] = "Ne";

byte code STR_MENU[] = "Menu";
byte code STR_TISK_PROTOKOLU[] = "Tisk protokolu";
byte code STR_ZAZNAMY[] = "Z\x089znamy";
byte code STR_NASTAVENI[] = "Nastaven\x08E";
byte code STR_MEZE[] = "Meze";
byte code STR_REGULATOR[] = "Regul\x089tor";
byte code STR_DIESEL[] = "Diesel";
byte code STR_LOGIN[] = "P\x091ihl\x089\x092en\x08E";

byte code STR_TEPLOTA[] = "Teplota";
byte code STR_VZDUCHOVE_FILTRY[] = "Vzduchov\x08C filtry";
byte code STR_CERSTVY_VZDUCH[] = "\x099erstv\x096 vzduch";
byte code STR_CO2[] = "CO2";

byte code STR_HESLO[] = "Zadejte heslo:";

byte code STR_ZADEJTE_MAXIMALNI_TLAK[] = "Maxim\x089ln\x08E tlak:";
byte code STR_PASCAL[] = "Pa";

byte code STR_ZADEJTE_MAXIMALNI_CO2[] = "Maxim\x089ln\x08E koncentrace CO2:";
byte code STR_PPM[] = "ppm";

byte code STR_JEDNOTKY[] = "Jednotky";
byte code STR_ZADEJTE_HYSTEREZI[] = "Hystereze:";
byte code STR_ZADEJTE_MAXIMALNI_PREKMIT[] = "Maxim\x089ln\x08E p\x091ekmit:";
byte code STR_ZADEJTE_ZESILENI[] = "Zes\x08Elen\x08E:";
byte code STR_ZADEJTE_INTEGRACNI_CAS[] = "Integra\x08An\x08E \x08Aas:";
byte code STR_ZADEJTE_DERIVACNI_CAS[] = "Deriva\x08An\x08E \x08Aas:";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MIN1[] = "Zak\x089zan\x096 rozsah \x08Aerstv\x08Cho";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MIN2[] = "vzduchu - Minimum";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MAX1[] = "Zak\x089zan\x096 rozsah \x08Aerstv\x08Cho";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MAX2[] = "vzduchu - Maximum";

byte code STR_INFORMACE_DIESEL[] = "Informace";
byte code STR_VYMENA_OLEJE[] = "V\x096m\x08Dna oleje";
//byte code STR_PERIODA_VYMENY_OLEJE[] = "Perioda v\x096m\x08Dny oleje";
byte code STR_NULOVANI_MOTOHODIN[] = "Nulov\x089n\x08E motohodin";

// Diesel
byte code STR_MOTOHODINY[] = "Motohodiny:";
byte code STR_VYMENA_OLEJE_PRI[] = "V\x096m\x08Dna oleje:";
byte code STR_VYMENA_OLEJE_1[] = "Provedli jste v\x096m\x08Dnu";
byte code STR_VYMENA_OLEJE_2[] = "oleje?";
byte code STR_ZADEJTE_TYP[] = "Typ:";
byte code STR_KUBOTA[] = "Kubota";
byte code STR_DEUTZ[] = "Deutz";
byte code STR_ZADEJTE_PERIODU_VYMENY_OLEJE[] = "Perioda v\x096m\x08Dny oleje:";
//byte code STR_NULOVAT_MOTOHODINY[] = "Vynulovat motohodiny?";

// Mez ve skrini
byte code STR_ODCHYLKA_VE_SKRINI_NAD_1[] = "Maxim\x089ln\x08E odchylka nad";
byte code STR_ODCHYLKA_VE_SKRINI_NAD_2[] = "c\x08Elovou teplotou:";
byte code STR_ODCHYLKA_VE_SKRINI_POD_1[] = "Maxim\x089ln\x08E odchylka pod";
byte code STR_ODCHYLKA_VE_SKRINI_POD_2[] = "c\x08Elovou teplotou:";

// Menu zaznamy
byte code STR_ZKOPIROVAT_DO_MODULU[] = "Zkop\x08Erovat z\x089znamy";
byte code STR_PERIODA_UKLADANI[] = "Perioda ukl\x089d\x089n\x08E";
byte code STR_IDENTIFIKACNI_CISLO[] = "Identifika\x08An\x08E \x08A\x08Eslo";

// Perioda ukladani
byte code STR_PERIODA_1[] = "1 minuta";
byte code STR_PERIODA_5[] = "5 minut";
byte code STR_PERIODA_10[] = "10 minut";
byte code STR_PERIODA_20[] = "20 minut";

// Kopie modulu
byte code STR_PRIPOJTE_MODUL1[] = "Zasu\x08Fte do konektoru";
byte code STR_PRIPOJTE_MODUL2[] = "pam\x08D\x093ov\x096 modul";
byte code STR_CEKEJTE[] = "\x099ekejte...";
byte code STR_KOPIROVANI_OK[] = "Kop\x08Erov\x089n\x08E v po\x091\x089dku";
byte code STR_KOPIROVANI_CHYBA[] = "Chyba";

// Login
byte code STR_POUZIVAT_LOGIN1[] = "Pou\x097\x08Evat p\x091ihl\x089\x092en\x08E";
byte code STR_POUZIVAT_LOGIN2[] = "\x091idi\x08Ae?";
byte code STR_LOGIN_RIDICE[] = "P\x091ihl\x089\x092en\x08E \x091idi\x08Ae:";

// Tisk protokolu
byte code STR_ZADEJTE_ZAZNAMY_NA_RADEK1[] = "Zadejte pr\x095m\x08Drov\x089n\x08E";
byte code STR_ZADEJTE_ZAZNAMY_NA_RADEK2[] = "z\x089znam\x095:";
byte code STR_ZADEJTE_POCATECNI_DATUM1[] = "Zadejte po\x08A\x089te\x08An\x08E";
byte code STR_ZADEJTE_POCATECNI_DATUM2[] = "datum:";
byte code STR_ZADEJTE_POCATECNI_CAS1[] = "Zadejte po\x08A\x089te\x08An\x08E";
byte code STR_ZADEJTE_POCATECNI_CAS2[] = "\x08Aas:";
byte code STR_ZADEJTE_KONCOVE_DATUM1[] = "Zadejte koncov\x08C";
byte code STR_ZADEJTE_KONCOVE_DATUM2[] = "datum:";
byte code STR_ZADEJTE_KONCOVY_CAS1[] = "Zadejte koncov\x096";
byte code STR_ZADEJTE_KONCOVY_CAS2[] = "\x08Aas:";
byte code STR_TISKNOUT_KOPII1[] = "Tisknout kopii";
byte code STR_TISKNOUT_KOPII2[] = "protokolu?";

// Dezinfekce
byte code STR_DEZINFEKCE_START1[] = "Opravdu zah\x089jit";
byte code STR_DEZINFEKCE_START2[] = "dezinfekci?";
byte code STR_DEZINFEKCE_ZAHAJENA[] = "Dezinfekce zah\x089jena";
byte code STR_DEZINFEKCE_CHYBA[] = "Porucha dezinfekce";

// Porucha spojeni pro terminal
byte code STR_VYPADEK_SPOJENI[] = "V\x096padek spojen\x08E";

// Symboly s textem
byte code SYMBOL_TLACITKO_STOP[38] = { 4,9,120,132,132,128,120,4,132,132,120,254,16,16,16,16,16,16,16,16,28,34,65,65,65,65,65,34,28,62,33,33,33,62,32,32,32,32 };
byte code SYMBOL_TLACITKO_AUTO[38] = { 4,9,16,40,40,40,68,124,68,130,130,130,130,130,130,130,130,130,68,56,254,16,16,16,16,16,16,16,16,60,66,129,129,129,129,129,66,60 };
byte code SYMBOL_DEL[29] = { 3,9,124,66,65,65,65,65,65,66,124,63,32,32,32,63,32,32,32,63,32,32,32,32,32,32,32,32,63 };
byte code SYMBOL_MODE_AUTO[34] = { 2,16,0,0,0,0,0,37,85,85,117,85,82,0,0,0,0,0,0,0,0,0,0,114,37,37,37,37,34,0,0,0,0,0 };
byte code SYMBOL_MODE_STOP[26] = { 3,8,115,136,128,96,16,8,136,112,230,137,144,144,144,144,137,134,60,34,162,162,188,160,32,32 };

#endif  // LANG_CZ

//-----------------------------------------------------------------------------
// Stringy - polstina
//-----------------------------------------------------------------------------

#ifdef LANG_PL

byte code STR_ZADEJTE_CILOVOU_TEPLOTU_1[] = "Prosz\x08B wpisa\x08A temperatur\x08B";
byte code STR_ZADEJTE_CILOVOU_TEPLOTU_2[] = "docelow\x089:";
byte code STR_STUPNE_CELSIA[] = "\x07FC";
byte code STR_STUPNE_FAHRENHEITA[] = "\x07FF";
byte code STR_STUPNE_KRAT_SEKUNDY[] = "\x07FC*s";
byte code STR_SEKUND[] = "s";
byte code STR_HODIN[] = "h";
byte code STR_CHYBA_NEPLATNA_HODNOTA_1[] = "Nie jest podana wa\x091na";
byte code STR_CHYBA_NEPLATNA_HODNOTA_2[] = "warto\x08F\x08A";

byte code STR_LOZENI_PREPRAVEK[] = "\x095adunek";

byte code STR_ZADEJTE_DATUM[] = "Prosz\x08B wpisa\x08A aktualn\x089 dat\x08B:";
byte code STR_ZADEJTE_CAS[] = "Prosz\x08B wpisa\x08A aktualny czas";

byte code STR_SMAZAT_PAMET_1[] = "Chc\x089 Pa\x08Dstwo skasowa\x08A";
byte code STR_SMAZAT_PAMET_2[] = "wszystkie zapisy z pami\x08Bci?";

byte code STR_OK[] = "OK";
byte code STR_OPAKOVAT[] = "Znowu";
byte code STR_ZRUSIT[] = "Anulowa\x08A";
byte code STR_ANO[] = "Tak";
byte code STR_NE[] = "Nie";

byte code STR_MENU[] = "Menu";
byte code STR_TISK_PROTOKOLU[] = "Druk protoko\x08Cu";
byte code STR_ZAZNAMY[] = "Zapisy";
byte code STR_NASTAVENI[] = "Ustawienia";
byte code STR_MEZE[] = "Granice";
byte code STR_REGULATOR[] = "Regulator";
byte code STR_DIESEL[] = "Diesel";
byte code STR_LOGIN[] = "Logowanie do systemu";

byte code STR_TEPLOTA[] = "Temperatura";
byte code STR_VZDUCHOVE_FILTRY[] = "Filtry powietrza";
byte code STR_CERSTVY_VZDUCH[] = "\x098wie\x091e powietrze";
byte code STR_CO2[] = "CO2";

byte code STR_HESLO[] = "Prosz\x08B poda\x08A has\x08Co:";

byte code STR_ZADEJTE_MAXIMALNI_TLAK[] = "Maksymalne ci\x08Fnienie:";
byte code STR_PASCAL[] = "Pa";

byte code STR_ZADEJTE_MAXIMALNI_CO2[] = "Maksymalne st\x08B\x091enie CO2:";
byte code STR_PPM[] = "ppm";

byte code STR_JEDNOTKY[] = "Jednostki";
byte code STR_ZADEJTE_HYSTEREZI[] = "Histereza:";
byte code STR_ZADEJTE_MAXIMALNI_PREKMIT[] = "Maksymalne przekroczenie:";
byte code STR_ZADEJTE_ZESILENI[] = "Wzmocnienie:";
byte code STR_ZADEJTE_INTEGRACNI_CAS[] = "Czas integracyjny:";
byte code STR_ZADEJTE_DERIVACNI_CAS[] = "Czas r\x08E\x091niczkowania:";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MIN1[] = "Niedopuszczalny zakres";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MIN2[] = "\x08Fwie\x091ego powietrza - Minimum";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MAX1[] = "Niedopuszczalny zakres";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MAX2[] = "\x08Fwie\x091ego powietrza - Maximum";

byte code STR_INFORMACE_DIESEL[] = "Menu informacyjne";
byte code STR_VYMENA_OLEJE[] = "Wymiana oleju";
//byte code STR_PERIODA_VYMENY_OLEJE[] = "Czasokres wymiany oleju";
byte code STR_NULOVANI_MOTOHODIN[] = "Zerowanie motogodzin";

// Diesel
byte code STR_MOTOHODINY[] = "Motogodziny:";
byte code STR_VYMENA_OLEJE_PRI[] = "Wymiana oleju:";
byte code STR_VYMENA_OLEJE_1[] = "Przeprowadzili Pa\x08Dstwo";
byte code STR_VYMENA_OLEJE_2[] = "wymian\x08B oleju?";
byte code STR_ZADEJTE_TYP[] = "Typ:";
byte code STR_KUBOTA[] = "Kubota";
byte code STR_DEUTZ[] = "Deutz";
byte code STR_ZADEJTE_PERIODU_VYMENY_OLEJE[] = "Czasokres wymiany oleju:";
//byte code STR_NULOVAT_MOTOHODINY[] = "Zerowa\x08A motogodziny?";

// Mez ve skrini
byte code STR_ODCHYLKA_VE_SKRINI_NAD_1[] = "Maks. odchylenie powy\x091ej";
byte code STR_ODCHYLKA_VE_SKRINI_NAD_2[] = "temperatury docelowej:";
byte code STR_ODCHYLKA_VE_SKRINI_POD_1[] = "Maks. odchylenie poni\x091ej";
byte code STR_ODCHYLKA_VE_SKRINI_POD_2[] = "temperatury docelowej:";

// Menu zaznamy
byte code STR_ZKOPIROVAT_DO_MODULU[] = "Skopiowa\x08A zapisy";
byte code STR_PERIODA_UKLADANI[] = "Czasokres zapisu";
byte code STR_IDENTIFIKACNI_CISLO[] = "Numer identyfikacyjny";

// Perioda ukladani
byte code STR_PERIODA_1[] = "1 minuta";
byte code STR_PERIODA_5[] = "5 minut";
byte code STR_PERIODA_10[] = "10 minut";
byte code STR_PERIODA_20[] = "20 minut";

// Kopie modulu
byte code STR_PRIPOJTE_MODUL1[] = "Prosz\x08B wsun\x089\x08A do konektora";
byte code STR_PRIPOJTE_MODUL2[] = "modu\x08C pami\x08Bciowy";
byte code STR_CEKEJTE[] = "Prosz\x08B czeka\x08A...";
byte code STR_KOPIROVANI_OK[] = "Kopiowanie w porz\x089dku";
byte code STR_KOPIROVANI_CHYBA[] = "B\x08C\x089d";

// Login
byte code STR_POUZIVAT_LOGIN1[] = "Stosowa\x08A logowanie";
byte code STR_POUZIVAT_LOGIN2[] = "kierowcy?";
byte code STR_LOGIN_RIDICE[] = "Logowanie kierowcy:";

// Tisk protokolu
byte code STR_ZADEJTE_ZAZNAMY_NA_RADEK1[] = "Prosz\x08B zada\x08A wyliczenie";
byte code STR_ZADEJTE_ZAZNAMY_NA_RADEK2[] = "\x08Frednich zapis\x08Ew:";
byte code STR_ZADEJTE_POCATECNI_DATUM1[] = "Prosz\x08B poda\x08A dat\x08B";
byte code STR_ZADEJTE_POCATECNI_DATUM2[] = "pocz\x089tkow\x089:";
byte code STR_ZADEJTE_POCATECNI_CAS1[] = "Prosz\x08B poda\x08A czas";
byte code STR_ZADEJTE_POCATECNI_CAS2[] = "pocz\x089tkowy:";
byte code STR_ZADEJTE_KONCOVE_DATUM1[] = "Prosz\x08B poda\x08A dat\x08B";
byte code STR_ZADEJTE_KONCOVE_DATUM2[] = "ko\x08Dcow\x089:";
byte code STR_ZADEJTE_KONCOVY_CAS1[] = "Prosz\x08B poda\x08A czas";
byte code STR_ZADEJTE_KONCOVY_CAS2[] = "ko\x08Dcowy:";
byte code STR_TISKNOUT_KOPII1[] = "Drukowa\x08A kopi\x08B";
byte code STR_TISKNOUT_KOPII2[] = "protoko\x08Cu?";

// Dezinfekce
byte code STR_DEZINFEKCE_START1[] = "Naprawd\x08B rozpocz\x089\x08A";
byte code STR_DEZINFEKCE_START2[] = "dezynfekcj\x08B?";
byte code STR_DEZINFEKCE_ZAHAJENA[] = "Dezynfekcja zosta\x08C rozpocz\x08Bty";
byte code STR_DEZINFEKCE_CHYBA[] = "B\x08C\x089d dezynfekcji";

// Porucha spojeni pro terminal
byte code STR_VYPADEK_SPOJENI[] = "Przerwa po\x08C\x089czenia";

// Symboly s textem
byte code SYMBOL_TLACITKO_STOP[38] = { 4,9,120,132,132,128,120,4,132,132,120,254,16,16,16,16,16,16,16,16,28,34,65,65,65,65,65,34,28,62,33,33,33,62,32,32,32,32 };
byte code SYMBOL_TLACITKO_AUTO[38] = { 4,9,16,40,40,40,68,124,68,130,130,130,130,130,130,130,130,130,68,56,254,16,16,16,16,16,16,16,16,60,66,129,129,129,129,129,66,60 };
byte code SYMBOL_DEL[29] = { 3,9,124,66,65,65,65,65,65,66,124,63,32,32,32,63,32,32,32,63,32,32,32,32,32,32,32,32,63 };
byte code SYMBOL_MODE_AUTO[34] = { 2,16,0,0,0,0,0,37,85,85,117,85,82,0,0,0,0,0,0,0,0,0,0,114,37,37,37,37,34,0,0,0,0,0 };
byte code SYMBOL_MODE_STOP[26] = { 3,8,115,136,128,96,16,8,136,112,230,137,144,144,144,144,137,134,60,34,162,162,188,160,32,32 };

#endif // LANG_PL

//-----------------------------------------------------------------------------
// Stringy - anglictina
//-----------------------------------------------------------------------------

#ifdef LANG_EN

byte code STR_ZADEJTE_CILOVOU_TEPLOTU[] = "Target temperature:";
byte code STR_STUPNE_CELSIA[] = "\x07FC";
byte code STR_STUPNE_FAHRENHEITA[] = "\x07FF";
byte code STR_STUPNE_KRAT_SEKUNDY[] = "\x07FC*s";
byte code STR_SEKUND[] = "s";
byte code STR_HODIN[] = "h";
byte code STR_CHYBA_NEPLATNA_HODNOTA[] = "The value is not valid";

byte code STR_LOZENI_PREPRAVEK[] = "Load";

byte code STR_ZADEJTE_DATUM[] = "Current date:";
byte code STR_ZADEJTE_CAS[] = "Current time:";

byte code STR_SMAZAT_PAMET_1[] = "Do you want to delete";
byte code STR_SMAZAT_PAMET_2[] = "all records from memory?";

byte code STR_OK[] = "OK";
byte code STR_OPAKOVAT[] = "Retry";
byte code STR_ZRUSIT[] = "Cancel";
byte code STR_ANO[] = "Yes";
byte code STR_NE[] = "No";

byte code STR_MENU[] = "Menu";
byte code STR_TISK_PROTOKOLU[] = "Print protocol";
byte code STR_ZAZNAMY[] = "Records";
byte code STR_NASTAVENI[] = "Setup";
byte code STR_MEZE[] = "Limits";
byte code STR_REGULATOR[] = "Regulator";
byte code STR_DIESEL[] = "Diesel";
byte code STR_LOGIN[] = "Login";

byte code STR_TEPLOTA[] = "Temperature";
byte code STR_VZDUCHOVE_FILTRY[] = "Air filters";
byte code STR_CERSTVY_VZDUCH[] = "Fresh air";
byte code STR_CO2[] = "CO2";

byte code STR_HESLO[] = "Enter password:";

byte code STR_ZADEJTE_MAXIMALNI_TLAK[] = "Maximum pressure:";
byte code STR_PASCAL[] = "Pa";

byte code STR_ZADEJTE_MAXIMALNI_CO2[] = "Maximum CO2 level:";
byte code STR_PPM[] = "ppm";

byte code STR_JEDNOTKY[] = "Units";
byte code STR_ZADEJTE_HYSTEREZI[] = "Hysteresis:";
byte code STR_ZADEJTE_MAXIMALNI_PREKMIT[] = "Maximum overshoot:";
byte code STR_ZADEJTE_ZESILENI[] = "Gain:";
byte code STR_ZADEJTE_INTEGRACNI_CAS[] = "Integration time:";
byte code STR_ZADEJTE_DERIVACNI_CAS[] = "Derivation time:";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MIN1[] = "Forbidden range of fresh air";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MIN2[] = "Minimum";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MAX1[] = "Forbidden range of fresh air";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MAX2[] = "Maximum";

byte code STR_INFORMACE_DIESEL[] = "Information";
byte code STR_VYMENA_OLEJE[] = "Oil changed";
//byte code STR_PERIODA_VYMENY_OLEJE[] = "Change oil period";
byte code STR_NULOVANI_MOTOHODIN[] = "Clear motohours";

// Diesel
byte code STR_MOTOHODINY[] = "Motohours:";
byte code STR_VYMENA_OLEJE_PRI[] = "Next oil change:";
byte code STR_VYMENA_OLEJE_1[] = "Have you changed";
byte code STR_VYMENA_OLEJE_2[] = "the oil?";
byte code STR_ZADEJTE_TYP[] = "Type:";
byte code STR_KUBOTA[] = "Kubota";
byte code STR_DEUTZ[] = "Deutz";
byte code STR_ZADEJTE_PERIODU_VYMENY_OLEJE[] = "Change oil period:";
//byte code STR_NULOVAT_MOTOHODINY[] = "Clear the motohours?";

// Mez ve skrini
byte code STR_ODCHYLKA_VE_SKRINI_NAD_1[] = "Maximum offset above";
byte code STR_ODCHYLKA_VE_SKRINI_NAD_2[] = "the target temperature:";
byte code STR_ODCHYLKA_VE_SKRINI_POD_1[] = "Maximum offset below";
byte code STR_ODCHYLKA_VE_SKRINI_POD_2[] = "the target temperature:";

// Menu zaznamy
byte code STR_ZKOPIROVAT_DO_MODULU[] = "Copy records";
byte code STR_PERIODA_UKLADANI[] = "Saving period";
byte code STR_IDENTIFIKACNI_CISLO[] = "Identification number";

// Perioda ukladani
byte code STR_PERIODA_1[] = "1 minute";
byte code STR_PERIODA_5[] = "5 minutes";
byte code STR_PERIODA_10[] = "10 minutes";
byte code STR_PERIODA_20[] = "20 minutes";

// Kopie modulu
byte code STR_PRIPOJTE_MODUL1[] = "Insert the memory";
byte code STR_PRIPOJTE_MODUL2[] = "module";
byte code STR_CEKEJTE[] = "Wait...";
byte code STR_KOPIROVANI_OK[] = "Copy successful";
byte code STR_KOPIROVANI_CHYBA[] = "Error";

// Login
byte code STR_POUZIVAT_LOGIN1[] = "Use login";
byte code STR_POUZIVAT_LOGIN2[] = "of the driver?";
byte code STR_LOGIN_RIDICE[] = "Driver login:";

// Tisk protokolu
byte code STR_ZADEJTE_ZAZNAMY_NA_RADEK1[] = "Averaging";
byte code STR_ZADEJTE_ZAZNAMY_NA_RADEK2[] = "of the records:";
byte code STR_ZADEJTE_POCATECNI_DATUM1[] = "Date";
byte code STR_ZADEJTE_POCATECNI_DATUM2[] = "of the beginning:";
byte code STR_ZADEJTE_POCATECNI_CAS1[] = "Time";
byte code STR_ZADEJTE_POCATECNI_CAS2[] = "of the beginning:";
byte code STR_ZADEJTE_KONCOVE_DATUM1[] = "Date";
byte code STR_ZADEJTE_KONCOVE_DATUM2[] = "of the end:";
byte code STR_ZADEJTE_KONCOVY_CAS1[] = "Time";
byte code STR_ZADEJTE_KONCOVY_CAS2[] = "of the end:";
byte code STR_TISKNOUT_KOPII1[] = "Print another";
byte code STR_TISKNOUT_KOPII2[] = "copy?";

// Dezinfekce
byte code STR_DEZINFEKCE_START1[] = "Really start";
byte code STR_DEZINFEKCE_START2[] = "disinfection?";
byte code STR_DEZINFEKCE_ZAHAJENA[] = "Disinfection has been started";
byte code STR_DEZINFEKCE_CHYBA[] = "Disinfection error";

// Porucha spojeni pro terminal
byte code STR_VYPADEK_SPOJENI[] = "Connection error";

// Symboly s textem
byte code SYMBOL_TLACITKO_STOP[38] = { 4,9,120,132,132,128,120,4,132,132,120,254,16,16,16,16,16,16,16,16,28,34,65,65,65,65,65,34,28,62,33,33,33,62,32,32,32,32 };
byte code SYMBOL_TLACITKO_AUTO[38] = { 4,9,16,40,40,40,68,124,68,130,130,130,130,130,130,130,130,130,68,56,254,16,16,16,16,16,16,16,16,60,66,129,129,129,129,129,66,60 };
byte code SYMBOL_DEL[29] = { 3,9,124,66,65,65,65,65,65,66,124,63,32,32,32,63,32,32,32,63,32,32,32,32,32,32,32,32,63 };
byte code SYMBOL_MODE_AUTO[34] = { 2,16,0,0,0,0,0,37,85,85,117,85,82,0,0,0,0,0,0,0,0,0,0,114,37,37,37,37,34,0,0,0,0,0 };
byte code SYMBOL_MODE_STOP[26] = { 3,8,115,136,128,96,16,8,136,112,230,137,144,144,144,144,137,134,60,34,162,162,188,160,32,32 };

#endif  // LANG_EN

//-----------------------------------------------------------------------------
// Stringy - rustina
//-----------------------------------------------------------------------------

#ifdef LANG_RU

byte code STR_ZADEJTE_CILOVOU_TEPLOTU[] = "Tpe\x089ye\x093a\x0A2 \x096e\x093\x095epa\x096ypa:";
byte code STR_STUPNE_CELSIA[] = "\x07FC";
byte code STR_STUPNE_FAHRENHEITA[] = "\x07FF";
byte code STR_STUPNE_KRAT_SEKUNDY[] = "\x07FC*s";
byte code STR_SEKUND[] = "ce\x091";
byte code STR_HODIN[] = "\x09A";
byte code STR_CHYBA_NEPLATNA_HODNOTA[] = "\x0A7\x094a\x09Ae\x094\x08Fe \x094e \x08Ce\x090c\x096\x08A\x08F\x096e\x092\x09F\x094o";

byte code STR_LOZENI_PREPRAVEK[] = "\x0ACo\x08Bpy\x08E\x091a";

byte code STR_ZADEJTE_DATUM[] = "Te\x091y\x09Ca\x0A2 \x08Ca\x096a:";
byte code STR_ZADEJTE_CAS[] = "Te\x091y\x09Cee \x08Ape\x093\x0A2:";

byte code STR_SMAZAT_PAMET_1[] = "B\x09E xo\x096\x08F\x096e y\x08Ca\x092\x08F\x096\x09F \x08Ace";
byte code STR_SMAZAT_PAMET_2[] = "\x08Ea\x095\x08Fc\x08F \x08F\x08E \x095a\x093\x0A2\x096\x08F?";

byte code STR_OK[] = "O\x0AA";
byte code STR_OPAKOVAT[] = "\x0ACo\x08A\x096op";
byte code STR_ZRUSIT[] = "O\x096\x093e\x094a";
byte code STR_ANO[] = "\x0A5a";
byte code STR_NE[] = "He\x096";

byte code STR_MENU[] = "Me\x094\x0A1";
byte code STR_TISK_PROTOKOLU[] = "Pac\x095e\x09Aa\x096a\x096\x09F \x095po\x096o\x091o\x092";
byte code STR_ZAZNAMY[] = "\x0A7a\x095\x08Fc\x08F";
byte code STR_NASTAVENI[] = "Hac\x096po\x090\x091a";
byte code STR_MEZE[] = "\x0A4pa\x094\x08F\x099\x09E";
byte code STR_REGULATOR[] = "Pe\x08By\x092\x0A2\x096op";
byte code STR_DIESEL[] = "\x0A5\x08F\x08Ee\x092\x09F";
byte code STR_LOGIN[] = "\x0A8\x08Ce\x094\x096\x08F\x98\x08F\x091a\x099\x08F\x0A2";

byte code STR_TEPLOTA[] = "Te\x093\x095epa\x096ypa";
byte code STR_VZDUCHOVE_FILTRY[] = "Bo\x08E\x08Cy\x09B\x094\x09Ee \x098\x08F\x092\x09F\x096p\x09E";
byte code STR_CERSTVY_VZDUCH[] = "\x0B0\x08Fc\x09E\x090 \x08Ao\x08E\x08C\x097x";
byte code STR_CO2[] = "CO2";

byte code STR_HESLO[] = "Be\x08C\x08F\x096e \x095apo\x092\x09F:";

byte code STR_ZADEJTE_MAXIMALNI_TLAK[] = "Ma\x091c\x08F\x093a\x092\x09F\x094oe \x08Ca\x08A\x092e\x094\x08Fe:";
byte code STR_PASCAL[] = "\x0ACa";

byte code STR_ZADEJTE_MAXIMALNI_CO2_1[] = "Ma\x091c\x08F\x093a\x092\x09F\x094a\x0A2";
byte code STR_ZADEJTE_MAXIMALNI_CO2_2[] = "\x091o\x094\x099e\x094\x096pa\x099\x08Fa CO2:";
byte code STR_PPM[] = "ppm";

byte code STR_JEDNOTKY[] = "Mepa";
byte code STR_ZADEJTE_HYSTEREZI[] = "\x0A4\x08Fc\x096epe\x08E\x08Fc:";
byte code STR_ZADEJTE_MAXIMALNI_PREKMIT[] = "Ma\x091c\x08F\x093a\x092\x09F\x094oe \x095pe\x08A\x09E\x09Be\x094\x08Fe:";
byte code STR_ZADEJTE_ZESILENI[] = "\x0ACp\x08Fpoc\x096:";
byte code STR_ZADEJTE_INTEGRACNI_CAS[] = "Bpe\x093\x0A2 \x08F\x094\x096e\x08Bpa\x099\x08F\x08F:";
byte code STR_ZADEJTE_DERIVACNI_CAS[] = "Bpe\x093\x0A2 \x08Cep\x08F\x08Aa\x099\x08F\x08F:";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MIN1[] = "Forbidden range of fresh air";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MIN2[] = "Minimum";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MAX1[] = "Forbidden range of fresh air";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MAX2[] = "Maximum";

byte code STR_INFORMACE_DIESEL[] = "\x0A8\x094\x098op\x093a\x099\x08F\x0A2";
byte code STR_VYMENA_OLEJE[] = "Mac\x092o \x08Ea\x093e\x094e\x094o";
//byte code STR_PERIODA_VYMENY_OLEJE[] = "\x0ACep\x08Fo\x08C \x08Ea\x093e\x094\x09E \x093ac\x092a";
byte code STR_NULOVANI_MOTOHODIN[] = "C\x089poc \x093o\x096o\x09Aaco\x08A";

// Diesel
byte code STR_MOTOHODINY[] = "Mo\x096o\x09Aac\x09E:";
byte code STR_VYMENA_OLEJE_PRI[] = "Mac\x092o \x08Ea\x093e\x094e\x094o:";
byte code STR_VYMENA_OLEJE_1[] = "B\x09E \x08Ea\x093e\x094\x08F\x092\x08F";
byte code STR_VYMENA_OLEJE_2[] = "\x093ac\x092o?";
byte code STR_ZADEJTE_TYP[] = "T\x08F\x095:";
byte code STR_KUBOTA[] = "Kubota";
byte code STR_DEUTZ[] = "Deutz";
byte code STR_ZADEJTE_PERIODU_VYMENY_OLEJE[] = "\x0ACep\x08Fo\x08C \x08Ea\x093e\x094\x09E \x093ac\x092a:";
//byte code STR_NULOVAT_MOTOHODINY[] = "C\x089poc\x08F\x096\x09F \x093o\x096o\x09Aac\x09E?";

// Mez ve skrini
byte code STR_ODCHYLKA_VE_SKRINI_NAD_1[] = "Ma\x091c. o\x096\x091\x092o\x094e\x094\x08Fe c\x08Aepx";
byte code STR_ODCHYLKA_VE_SKRINI_NAD_2[] = "yc\x096a\x094o\x08A\x092e\x094\x094o\x090 \x096e\x093\x095epa\x096yp\x09E:";
byte code STR_ODCHYLKA_VE_SKRINI_POD_1[] = "Ma\x091c. o\x096\x091\x092o\x094e\x094\x08Fe \x094\x08F\x08De";
byte code STR_ODCHYLKA_VE_SKRINI_POD_2[] = "yc\x096a\x094o\x08A\x092e\x094\x094o\x090 \x096e\x093\x095epa\x096yp\x09E:";

// Menu zaznamy
byte code STR_ZKOPIROVAT_DO_MODULU[] = "C\x091o\x095\x08Fpo\x08Aa\x096\x09F \x08Ea\x095\x08Fc\x08F";
byte code STR_PERIODA_UKLADANI[] = "\x0A8\x094\x096ep\x08Aa\x092 coxpa\x094e\x094\x08F\x0A2";
byte code STR_IDENTIFIKACNI_CISLO[] = "\x0A8\x08Ce\x094\x096\x08F\x098\x08F\x091a\x099\x08Fo\x094\x094\x09E\x090 \x094o\x093ep";

// Perioda ukladani
byte code STR_PERIODA_1[] = "1 \x093\x08F\x094y\x096a";
byte code STR_PERIODA_5[] = "5 \x093\x08F\x094y\x096";
byte code STR_PERIODA_10[] = "10 \x093\x08F\x094y\x096";
byte code STR_PERIODA_20[] = "20 \x093\x08F\x094y\x096";

// Kopie modulu
byte code STR_PRIPOJTE_MODUL1[] = "Bc\x096a\x08A\x09F\x096e \x093o\x08Cy\x092\x09F";
byte code STR_PRIPOJTE_MODUL2[] = "\x095a\x093\x09F\x096\x08F";
byte code STR_CEKEJTE[] = "\x0A6\x08C\x08F\x096e...";
byte code STR_KOPIROVANI_OK[] = "\x0AAo\x095\x08Fpo\x08Aa\x094\x08Fe \x08Ea\x08Aep\x09Be\x094o";
byte code STR_KOPIROVANI_CHYBA[] = "O\x09B\x08F\x089\x091a";

// Login
byte code STR_POUZIVAT_LOGIN1[] = "\x0A8c\x095o\x092\x09F\x08Eo\x08Aa\x096\x09F";
byte code STR_POUZIVAT_LOGIN2[] = "\x08F\x08Ce\x094\x096\x08F\x098\x08F\x091a\x099\x08F\x0A1 \x08Ao\x08C\x08F\x096e\x092\x0A2?";
byte code STR_LOGIN_RIDICE[] = "\x0ACapo\x092\x09F \x08Ao\x08C\x08F\x096e\x092\x0A2:";

// Tisk protokolu
byte code STR_ZADEJTE_ZAZNAMY_NA_RADEK1[] = "B\x09E\x08Aec\x096\x08F cpe\x08C\x094ee";
byte code STR_ZADEJTE_ZAZNAMY_NA_RADEK2[] = "\x08E\x094a\x09Ae\x094\x08Fe \x08F\x08E \x08Ea\x095\x08Fce\x090:";
byte code STR_ZADEJTE_POCATECNI_DATUM1[] = "\x0A5a\x096a";
byte code STR_ZADEJTE_POCATECNI_DATUM2[] = "\x094a\x09Aa\x092a:";
byte code STR_ZADEJTE_POCATECNI_CAS1[] = "Bpe\x093\x0A2";
byte code STR_ZADEJTE_POCATECNI_CAS2[] = "\x094a\x09Aa\x092a:";
byte code STR_ZADEJTE_KONCOVE_DATUM1[] = "\x0A5a\x096a";
byte code STR_ZADEJTE_KONCOVE_DATUM2[] = "o\x091o\x094\x09Aa\x094\x08F\x0A2:";
byte code STR_ZADEJTE_KONCOVY_CAS1[] = "Bpe\x093\x0A2";
byte code STR_ZADEJTE_KONCOVY_CAS2[] = "o\x091o\x094\x09Aa\x094\x08F\x0A2:";
byte code STR_TISKNOUT_KOPII1[] = "Pac\x095e\x09Aa\x096a\x096\x09F e\x09Ce";
byte code STR_TISKNOUT_KOPII2[] = "\x091o\x095\x08F\x0A1?";

// Dezinfekce
byte code STR_DEZINFEKCE_START1[] = "Really start";
byte code STR_DEZINFEKCE_START2[] = "disinfection?";
byte code STR_DEZINFEKCE_ZAHAJENA[] = "Disinfection has been started";
byte code STR_DEZINFEKCE_CHYBA[] = "Disinfection error";

// Porucha spojeni pro terminal
byte code STR_VYPADEK_SPOJENI[] = "O\x09B\x08F\x089\x091a coe\x08C\x08F\x094e\x094\x08F\x0A2";

// Symboly s textem
byte code SYMBOL_TLACITKO_STOP[38] = { 4,9,56,68,130,128,128,128,130,68,56,254,16,16,16,16,16,16,16,16,28,34,65,65,65,65,65,34,28,63,33,33,33,33,33,33,33,33 };
byte code SYMBOL_TLACITKO_AUTO[38] = { 4,9,24,36,36,36,66,126,66,129,129,124,66,66,66,124,66,66,66,124,254,16,16,16,16,16,16,16,16,60,66,129,129,129,129,129,66,60 };
byte code SYMBOL_DEL[29] = { 3,9,0,69,69,41,41,17,18,103,4,0,225,34,34,36,39,36,248,16,0,15,137,137,73,201,73,49,0 };
byte code SYMBOL_MODE_AUTO[34] = { 2,16,0,0,0,0,0,38,85,86,117,85,86,0,0,0,0,0,0,0,0,0,0,114,37,37,37,37,34,0,0,0,0,0 };
byte code SYMBOL_MODE_STOP[26] = { 3,8,115,136,128,128,128,128,136,112,230,137,144,144,144,144,137,134,62,34,162,162,162,162,34,34 };

#endif  // LANG_RU


//-----------------------------------------------------------------------------
// Stringy - rumunstina
//-----------------------------------------------------------------------------

#ifdef LANG_RO

byte code STR_ZADEJTE_CILOVOU_TEPLOTU[] = "Temperatura dorit\x089:";
byte code STR_STUPNE_CELSIA[] = "\x07FC";
byte code STR_STUPNE_FAHRENHEITA[] = "\x07FF";
byte code STR_STUPNE_KRAT_SEKUNDY[] = "\x07FC*s";
byte code STR_SEKUND[] = "s";
byte code STR_HODIN[] = "h";
byte code STR_CHYBA_NEPLATNA_HODNOTA[] = "Valoarea nu este valida";

byte code STR_LOZENI_PREPRAVEK[] = "\x090nc\x089rc\x089tura";

byte code STR_ZADEJTE_DATUM[] = "Data curent\x089:";
byte code STR_ZADEJTE_CAS[] = "Ora curent\x089:";

byte code STR_SMAZAT_PAMET_1[] = "Dori\x08Di s\x089 \x08Cterge\x08Di toate";
byte code STR_SMAZAT_PAMET_2[] = "\x08Bnregistr\x089rile din memorie?";

byte code STR_OK[] = "OK";
byte code STR_OPAKOVAT[] = "Retragere";
byte code STR_ZRUSIT[] = "Anulare";
byte code STR_ANO[] = "Da";
byte code STR_NE[] = "Nu";

byte code STR_MENU[] = "Meniu";
byte code STR_TISK_PROTOKOLU[] = "Protocol listare";
byte code STR_ZAZNAMY[] = "\x090nregistr\x089ri";
byte code STR_NASTAVENI[] = "Instalare";
byte code STR_MEZE[] = "Limite";
byte code STR_REGULATOR[] = "Regulator";
byte code STR_DIESEL[] = "Diesel";
byte code STR_LOGIN[] = "Conectare";

byte code STR_TEPLOTA[] = "Temperatur\x089";
byte code STR_VZDUCHOVE_FILTRY[] = "Filtre de aer";
byte code STR_CERSTVY_VZDUCH[] = "Aer proasp\x089t";
byte code STR_CO2[] = "CO2";

byte code STR_HESLO[] = "Introduce\x08Di parola:";

byte code STR_ZADEJTE_MAXIMALNI_TLAK[] = "Presiunea maxim\x089:";
byte code STR_PASCAL[] = "Pa";

byte code STR_ZADEJTE_MAXIMALNI_CO2[] = "Concentra\x08Dia maxim\x089 de CO2:";
byte code STR_PPM[] = "ppm";

byte code STR_JEDNOTKY[] = "Unit\x089\x08Di";
byte code STR_ZADEJTE_HYSTEREZI[] = "Histerezis:";
byte code STR_ZADEJTE_MAXIMALNI_PREKMIT[] = "Oscila\x08Dia maxim\x089:";
byte code STR_ZADEJTE_ZESILENI[] = "Progresie:";
byte code STR_ZADEJTE_INTEGRACNI_CAS[] = "Timp de integrare:";
byte code STR_ZADEJTE_DERIVACNI_CAS[] = "Timp derivat:";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MIN1[] = "Gama interzis\x089 de aer proasp\x089t";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MIN2[] = "Minimum";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MAX1[] = "Gama interzis\x089 de aer proasp\x089t";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MAX2[] = "Maximum";

byte code STR_INFORMACE_DIESEL[] = "Informa\x08Dii";
byte code STR_VYMENA_OLEJE[] = "Schimb ulei";
//byte code STR_PERIODA_VYMENY_OLEJE[] = "Perioada de schimb ulei";
byte code STR_NULOVANI_MOTOHODIN[] = "\x091tergere ore de lucru motor";

// Diesel
byte code STR_MOTOHODINY[] = "Ore de functionare motor:";
byte code STR_VYMENA_OLEJE_PRI[] = "Schimb ulei:";
byte code STR_VYMENA_OLEJE_1[] = "A\x08Di schimbat";
byte code STR_VYMENA_OLEJE_2[] = "uleiul?";
byte code STR_ZADEJTE_TYP[] = "Tipul:";
byte code STR_KUBOTA[] = "Kubota";
byte code STR_DEUTZ[] = "Deutz";
byte code STR_ZADEJTE_PERIODU_VYMENY_OLEJE[] = "Perioada de schimb ulei:";
//byte code STR_NULOVAT_MOTOHODINY_1[] = "\x091terge\x08Di orele de lucru";
//byte code STR_NULOVAT_MOTOHODINY_2[] = "ale motorului?";

// Mez ve skrini
byte code STR_ODCHYLKA_VE_SKRINI_NAD_1[] = "Devia\x08Dia maxim\x089 peste";
byte code STR_ODCHYLKA_VE_SKRINI_NAD_2[] = "temperatura dorit\x089:";
byte code STR_ODCHYLKA_VE_SKRINI_POD_1[] = "Devia\x08Dia maxim\x089 sub";
byte code STR_ODCHYLKA_VE_SKRINI_POD_2[] = "temperatura dorit\x089:";

// Menu zaznamy
byte code STR_ZKOPIROVAT_DO_MODULU[] = "Copiere \x08Bnregistr\x089ri";
byte code STR_PERIODA_UKLADANI[] = "Perioada de salvare";
byte code STR_IDENTIFIKACNI_CISLO[] = "Num\x089r de identificare";

// Perioda ukladani
byte code STR_PERIODA_1[] = "1 minut";
byte code STR_PERIODA_5[] = "5 minute";
byte code STR_PERIODA_10[] = "10 minute";
byte code STR_PERIODA_20[] = "20 minute";

// Kopie modulu
byte code STR_PRIPOJTE_MODUL1[] = "Intruduce\x08Di modulul";
byte code STR_PRIPOJTE_MODUL2[] = "de memorie";
byte code STR_CEKEJTE[] = "A\x08Ctepta\x08Di...";
byte code STR_KOPIROVANI_OK[] = "Copiere reu\x08Cit\x089";
byte code STR_KOPIROVANI_CHYBA[] = "Eroare";

// Login
byte code STR_POUZIVAT_LOGIN1[] = "Folosi\x08Di conectarea";
byte code STR_POUZIVAT_LOGIN2[] = "\x08Coferului?";
byte code STR_LOGIN_RIDICE[] = "Conectare \x08Cofer:";

// Tisk protokolu
byte code STR_ZADEJTE_ZAZNAMY_NA_RADEK1[] = "Nr. mediu";
byte code STR_ZADEJTE_ZAZNAMY_NA_RADEK2[] = "de \x08Bnregistr\x089ri:";
byte code STR_ZADEJTE_POCATECNI_DATUM1[] = "Data";
byte code STR_ZADEJTE_POCATECNI_DATUM2[] = "\x08Bnceperii:";
byte code STR_ZADEJTE_POCATECNI_CAS1[] = "Ora";
byte code STR_ZADEJTE_POCATECNI_CAS2[] = "\x08Bnceperii:";
byte code STR_ZADEJTE_KONCOVE_DATUM1[] = "Data";
byte code STR_ZADEJTE_KONCOVE_DATUM2[] = "termin\x089rii:";
byte code STR_ZADEJTE_KONCOVY_CAS1[] = "Ora";
byte code STR_ZADEJTE_KONCOVY_CAS2[] = "termin\x089rii:";
byte code STR_TISKNOUT_KOPII1[] = "Lista\x08Di \x08Bnc\x089";
byte code STR_TISKNOUT_KOPII2[] = "un exemplar?";

// Dezinfekce
byte code STR_DEZINFEKCE_START1[] = "Really start";
byte code STR_DEZINFEKCE_START2[] = "disinfection?";
byte code STR_DEZINFEKCE_ZAHAJENA[] = "Disinfection has been started";
byte code STR_DEZINFEKCE_CHYBA[] = "Disinfection error";

// Porucha spojeni pro terminal
byte code STR_VYPADEK_SPOJENI[] = "Eroare de conectare";

// Symboly s textem
byte code SYMBOL_TLACITKO_STOP[38] = { 4,9,120,132,132,128,120,4,132,132,120,254,16,16,16,16,16,16,16,16,28,34,65,65,65,65,65,34,28,62,33,33,33,62,32,32,32,32 };
byte code SYMBOL_TLACITKO_AUTO[38] = { 4,9,16,40,40,40,68,124,68,130,130,130,130,130,130,130,130,130,68,56,254,16,16,16,16,16,16,16,16,60,66,129,129,129,129,129,66,60 };
byte code SYMBOL_DEL[29] = { 3,9,124,66,65,65,65,65,65,66,124,63,32,32,32,63,32,32,32,63,32,32,32,32,32,32,32,32,63 };
byte code SYMBOL_MODE_AUTO[34] = { 2,16,0,0,0,0,0,37,85,85,117,85,82,0,0,0,0,0,0,0,0,0,0,114,37,37,37,37,34,0,0,0,0,0 };
byte code SYMBOL_MODE_STOP[26] = { 3,8,115,136,128,96,16,8,136,112,230,137,144,144,144,144,137,134,60,34,162,162,188,160,32,32 };

#endif  // LANG_RO

//-----------------------------------------------------------------------------
// Stringy - nemcina
//-----------------------------------------------------------------------------

#ifdef LANG_GE

byte code STR_ZADEJTE_CILOVOU_TEPLOTU[] = "Temperatur Sollwert:";
byte code STR_STUPNE_CELSIA[] = "\x07FC";
byte code STR_STUPNE_FAHRENHEITA[] = "\x07FF";
byte code STR_STUPNE_KRAT_SEKUNDY[] = "\x07FC*s";
byte code STR_SEKUND[] = "sec.";
byte code STR_HODIN[] = "Std.";
byte code STR_CHYBA_NEPLATNA_HODNOTA[] = "Dieser Wert ist nicht zul\x089ssig";

byte code STR_LOZENI_PREPRAVEK[] = "Beladung";

byte code STR_ZADEJTE_DATUM[] = "Aktuelles Datum:";
byte code STR_ZADEJTE_CAS[] = "Aktuelle Zeit:";

byte code STR_SMAZAT_PAMET_1[] = "Wollen Sie alle Eintr\x089ge";
byte code STR_SMAZAT_PAMET_2[] = "im Speicher l\x08Aschen?";

byte code STR_OK[] = "OK";
byte code STR_OPAKOVAT[] = "Wiederh.";
byte code STR_ZRUSIT[] = "Abbrechen";
byte code STR_ANO[] = "Ja";
byte code STR_NE[] = "Nein";

byte code STR_MENU[] = "Men\x08B";
byte code STR_TISK_PROTOKOLU[] = "Protokoll drucken";
byte code STR_ZAZNAMY[] = "Aufzeichnungen";
byte code STR_NASTAVENI[] = "Einstellungen";
byte code STR_MEZE[] = "Limits";
byte code STR_REGULATOR[] = "Regler";
byte code STR_DIESEL[] = "Diesel";
byte code STR_LOGIN[] = "Anmelden";

byte code STR_TEPLOTA[] = "Temperatur";
byte code STR_VZDUCHOVE_FILTRY[] = "Luftfilter";
byte code STR_CERSTVY_VZDUCH[] = "Frische Luft";
byte code STR_CO2[] = "CO2";

byte code STR_HESLO[] = "Passwort eingeben:";

byte code STR_ZADEJTE_MAXIMALNI_TLAK[] = "Maximaler Druck:";
byte code STR_PASCAL[] = "Pa";

byte code STR_ZADEJTE_MAXIMALNI_CO2_1[] = "Maximale Konzentration";
byte code STR_ZADEJTE_MAXIMALNI_CO2_2[] = "von CO2:";
byte code STR_PPM[] = "ppm";

byte code STR_JEDNOTKY[] = "Einheiten";
byte code STR_ZADEJTE_HYSTEREZI[] = "Hysterese:";
byte code STR_ZADEJTE_MAXIMALNI_PREKMIT[] = "Maximale \x08Fberschwingung:";
byte code STR_ZADEJTE_ZESILENI[] = "Anstieg:";
byte code STR_ZADEJTE_INTEGRACNI_CAS[] = "Integrationsdauer:";
byte code STR_ZADEJTE_DERIVACNI_CAS[] = "Derivationsdauer:";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MIN1[] = "Verbotene Frischluftmenge";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MIN2[] = "Minimum";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MAX1[] = "Verbotene Frischluftmenge";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MAX2[] = "Maximum";

byte code STR_INFORMACE_DIESEL[] = "Information";
byte code STR_VYMENA_OLEJE[] = "\x08El gewechselt";
//byte code STR_PERIODA_VYMENY_OLEJE[] = "\x08Elwechselperiode einstellen";
byte code STR_NULOVANI_MOTOHODIN[] = "Betriebsstunden zur\x08Bcksetzen";

// Diesel
byte code STR_MOTOHODINY[] = "Betriebsstunden:";
byte code STR_VYMENA_OLEJE_PRI[] = "N\x089chster \x08Elwechsel:";
byte code STR_VYMENA_OLEJE_1[] = "Haben Sie das \x08El";
byte code STR_VYMENA_OLEJE_2[] = "gewechselt?";
byte code STR_ZADEJTE_TYP[] = "Art:";
byte code STR_KUBOTA[] = "Kubota";
byte code STR_DEUTZ[] = "Deutz";
byte code STR_ZADEJTE_PERIODU_VYMENY_OLEJE[] = "\x08Elwechselperiode:";

// Mez ve skrini
byte code STR_ODCHYLKA_VE_SKRINI_NAD_1[] = "Maximale obere Temperatur-";
byte code STR_ODCHYLKA_VE_SKRINI_NAD_2[] = "abweichung Sollwert:";
byte code STR_ODCHYLKA_VE_SKRINI_POD_1[] = "Maximale untere Temperatur-";
byte code STR_ODCHYLKA_VE_SKRINI_POD_2[] = "abweichung vom Sollwert:";

// Menu zaznamy
byte code STR_ZKOPIROVAT_DO_MODULU[] = "Alle Eintr\x089ge kopieren";
byte code STR_PERIODA_UKLADANI[] = "Aufbewahrungsdauer";
byte code STR_IDENTIFIKACNI_CISLO[] = "Identifikationsnummer";

// Perioda ukladani
byte code STR_PERIODA_1[] = "1 Minute";
byte code STR_PERIODA_5[] = "5 Minuten";
byte code STR_PERIODA_10[] = "10 Minuten";
byte code STR_PERIODA_20[] = "20 Minuten";

// Kopie modulu
byte code STR_PRIPOJTE_MODUL1[] = "F\x08Bgen Sie";
byte code STR_PRIPOJTE_MODUL2[] = "das Speichermodul ein";
byte code STR_CEKEJTE[] = "Warten Sie...";
byte code STR_KOPIROVANI_OK[] = "Erfolgreich \x08Bbertragen";
byte code STR_KOPIROVANI_CHYBA[] = "Fehler";

// Login
byte code STR_POUZIVAT_LOGIN1[] = "Fahreranmeldung";
byte code STR_POUZIVAT_LOGIN2[] = "nutzen?";
byte code STR_LOGIN_RIDICE[] = "Nummer des Fahrers:";

// Tisk protokolu
byte code STR_ZADEJTE_ZAZNAMY_NA_RADEK1[] = "Durchschnittsberechnung";
byte code STR_ZADEJTE_ZAZNAMY_NA_RADEK2[] = "der Eintr\x089ge:";
byte code STR_ZADEJTE_POCATECNI_DATUM1[] = "Datum";
byte code STR_ZADEJTE_POCATECNI_DATUM2[] = "des Protokollbeginns:";
byte code STR_ZADEJTE_POCATECNI_CAS1[] = "Zeit";
byte code STR_ZADEJTE_POCATECNI_CAS2[] = "des Protokollbeginns:";
byte code STR_ZADEJTE_KONCOVE_DATUM1[] = "Datum";
byte code STR_ZADEJTE_KONCOVE_DATUM2[] = "des Protokollendes:";
byte code STR_ZADEJTE_KONCOVY_CAS1[] = "Zeit";
byte code STR_ZADEJTE_KONCOVY_CAS2[] = "des Protokollendes:";
byte code STR_TISKNOUT_KOPII1[] = "Weitere Kopie";
byte code STR_TISKNOUT_KOPII2[] = "drucken?";

// Dezinfekce
byte code STR_DEZINFEKCE_START1[] = "Soll Desinfizieren";
byte code STR_DEZINFEKCE_START2[] = "eingesetzt werden?";
byte code STR_DEZINFEKCE_ZAHAJENA1[] = "Desinfizieren";
byte code STR_DEZINFEKCE_ZAHAJENA2[] = "wurde eingesetzt";
byte code STR_DEZINFEKCE_CHYBA1[] = "St\x08Arung";
byte code STR_DEZINFEKCE_CHYBA2[] = "des Desinfiziersystems";

// Porucha spojeni pro terminal
byte code STR_VYPADEK_SPOJENI[] = "Verbindung fehlgeschlagen";

// Symboly s textem
byte code SYMBOL_TLACITKO_STOP[38] = { 4,9,120,132,132,128,120,4,132,132,120,254,16,16,16,16,16,16,16,16,28,34,65,65,65,65,65,34,28,62,33,33,33,62,32,32,32,32 };
byte code SYMBOL_TLACITKO_AUTO[38] = { 4,9,16,40,40,40,68,124,68,130,130,130,130,130,130,130,130,130,68,56,254,16,16,16,16,16,16,16,16,60,66,129,129,129,129,129,66,60 };
byte code SYMBOL_DEL[29] = { 3,9,124,66,65,65,65,65,65,66,124,63,32,32,32,63,32,32,32,63,32,32,32,32,32,32,32,32,63 };
byte code SYMBOL_MODE_AUTO[34] = { 2,16,0,0,0,0,0,37,85,85,117,85,82,0,0,0,0,0,0,0,0,0,0,114,37,37,37,37,34,0,0,0,0,0 };
byte code SYMBOL_MODE_STOP[26] = { 3,8,115,136,128,96,16,8,136,112,230,137,144,144,144,144,137,134,60,34,162,162,188,160,32,32 };

#endif  // LANG_GE

//-----------------------------------------------------------------------------
// Stringy - swedstina
//-----------------------------------------------------------------------------

#ifdef LANG_SW

byte code STR_ZADEJTE_CILOVOU_TEPLOTU[] = "Riktv\x08Arde:";
byte code STR_STUPNE_CELSIA[] = "\x07FC";
byte code STR_STUPNE_FAHRENHEITA[] = "\x07FF";
byte code STR_STUPNE_KRAT_SEKUNDY[] = "\x07FC*s";
byte code STR_SEKUND[] = "s";
byte code STR_HODIN[] = "h";
byte code STR_CHYBA_NEPLATNA_HODNOTA[] = "V\x08Ardet \x08Ar ogiltigt";

byte code STR_LOZENI_PREPRAVEK[] = "Last";

byte code STR_ZADEJTE_DATUM[] = "Aktuellt datum:";
byte code STR_ZADEJTE_CAS[] = "Aktuell tid:";

byte code STR_SMAZAT_PAMET_1[] = "Vill du radera alla";
byte code STR_SMAZAT_PAMET_2[] = "protokoll fr\x089n minnet?";

byte code STR_OK[] = "OK";
byte code STR_OPAKOVAT[] = "N. F\x08Brs\x08Bk";
byte code STR_ZRUSIT[] = "Avbryt";
byte code STR_ANO[] = "Ja";
byte code STR_NE[] = "Nej";

byte code STR_MENU[] = "Meny";
byte code STR_TISK_PROTOKOLU[] = "Skriv ut protokoll";
byte code STR_ZAZNAMY[] = "Protokoll";
byte code STR_NASTAVENI[] = "Inst\x08Allningar";
byte code STR_MEZE[] = "Gr\x08Anser";
byte code STR_REGULATOR[] = "Regulator";
byte code STR_DIESEL[] = "Diesel";
byte code STR_LOGIN[] = "Inloggning";

byte code STR_TEPLOTA[] = "Temperatur";
byte code STR_VZDUCHOVE_FILTRY[] = "Luftfilter";
byte code STR_CERSTVY_VZDUCH[] = "Frisk luft";
byte code STR_CO2[] = "CO2";

byte code STR_HESLO[] = "Ange l\x08Bsenord:";

byte code STR_ZADEJTE_MAXIMALNI_TLAK[] = "Maximalt tryck:";
byte code STR_PASCAL[] = "Pa";

byte code STR_ZADEJTE_MAXIMALNI_CO2_1[] = "Maximal koncentration";
byte code STR_ZADEJTE_MAXIMALNI_CO2_2[] = "av CO2:";
byte code STR_PPM[] = "ppm";

byte code STR_JEDNOTKY[] = "Enheter";
byte code STR_ZADEJTE_HYSTEREZI[] = "Hysteres:";
byte code STR_ZADEJTE_MAXIMALNI_PREKMIT[] = "Max. till\x089ten \x08Bversv\x08Angning:";
byte code STR_ZADEJTE_ZESILENI[] = "Faktor:";
byte code STR_ZADEJTE_INTEGRACNI_CAS[] = "Integrationstid:";
byte code STR_ZADEJTE_DERIVACNI_CAS[] = "Deriveringstid:";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MIN1[] = "F\x08Brbjudet utbud av frisk";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MIN2[] = "luft - Minimum";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MAX1[] = "F\x08Brbjudet utbud av frisk";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MAX2[] = "luft - Maximum";

byte code STR_INFORMACE_DIESEL[] = "Information";
byte code STR_VYMENA_OLEJE[] = "Olja bytt";
//byte code STR_PERIODA_VYMENY_OLEJE[] = "Oljebyte intervall";
byte code STR_NULOVANI_MOTOHODIN[] = "Rensa motortimmar";

// Diesel
byte code STR_MOTOHODINY[] = "Motortimmar:";
byte code STR_VYMENA_OLEJE_PRI[] = "N\x08Asta oljebyte:";
byte code STR_VYMENA_OLEJE_1[] = "Har du bytt";
byte code STR_VYMENA_OLEJE_2[] = "olja?";
byte code STR_ZADEJTE_TYP[] = "Typ:";
byte code STR_KUBOTA[] = "Kubota";
byte code STR_DEUTZ[] = "Deutz";
byte code STR_ZADEJTE_PERIODU_VYMENY_OLEJE[] = "Oljebyte intervall:";

// Mez ve skrini
byte code STR_ODCHYLKA_VE_SKRINI_NAD_1[] = "Maximal offset";
byte code STR_ODCHYLKA_VE_SKRINI_NAD_2[] = "\x08Bver riktv\x08Ardet:";
byte code STR_ODCHYLKA_VE_SKRINI_POD_1[] = "Maximal offset";
byte code STR_ODCHYLKA_VE_SKRINI_POD_2[] = "under riktv\x08Ardet:";

// Menu zaznamy
byte code STR_ZKOPIROVAT_DO_MODULU[] = "Kopiera protokoll";
byte code STR_PERIODA_UKLADANI[] = "Spara interval";
byte code STR_IDENTIFIKACNI_CISLO[] = "Identifikations nummer";

// Perioda ukladani
byte code STR_PERIODA_1[] = "1 minut";
byte code STR_PERIODA_5[] = "5 minuter";
byte code STR_PERIODA_10[] = "10 minuter";
byte code STR_PERIODA_20[] = "20 minuter";

// Kopie modulu
byte code STR_PRIPOJTE_MODUL1[] = "S\x08Att i minnesmodul";
byte code STR_PRIPOJTE_MODUL2[] = " ";
byte code STR_CEKEJTE[] = "V\x08Anta...";
byte code STR_KOPIROVANI_OK[] = "Kopiering lyckad";
byte code STR_KOPIROVANI_CHYBA[] = "Fel";

// Login
byte code STR_POUZIVAT_LOGIN1[] = "Anv\x08And f\x08Brarinloggning?";
byte code STR_POUZIVAT_LOGIN2[] = " ";
byte code STR_LOGIN_RIDICE[] = "F\x08Brarinloggning:";

// Tisk protokolu
byte code STR_ZADEJTE_ZAZNAMY_NA_RADEK1[] = "Medelv\x08Arden";
byte code STR_ZADEJTE_ZAZNAMY_NA_RADEK2[] = "av protokoll:";
byte code STR_ZADEJTE_POCATECNI_DATUM1[] = "Startdatum:";
byte code STR_ZADEJTE_POCATECNI_DATUM2[] = " ";
byte code STR_ZADEJTE_POCATECNI_CAS1[] = "Starttid:";
byte code STR_ZADEJTE_POCATECNI_CAS2[] = " ";
byte code STR_ZADEJTE_KONCOVE_DATUM1[] = "Slutdatum:";
byte code STR_ZADEJTE_KONCOVE_DATUM2[] = " ";
byte code STR_ZADEJTE_KONCOVY_CAS1[] = "Sluttid:";
byte code STR_ZADEJTE_KONCOVY_CAS2[] = " ";
byte code STR_TISKNOUT_KOPII1[] = "Skriva ut en kopia till?";
byte code STR_TISKNOUT_KOPII2[] = " ";

// Dezinfekce
byte code STR_DEZINFEKCE_START1[] = "Really start";
byte code STR_DEZINFEKCE_START2[] = "disinfection?";
byte code STR_DEZINFEKCE_ZAHAJENA[] = "Disinfection has been started";
byte code STR_DEZINFEKCE_CHYBA[] = "Disinfection error";

// Porucha spojeni pro terminal
byte code STR_VYPADEK_SPOJENI[] = "Anslutnings fel";

// Symboly s textem
byte code SYMBOL_TLACITKO_STOP[38] = { 4,9,57,68,68,64,56,4,68,68,56,241,66,68,68,68,68,68,66,65,143,72,40,40,47,40,40,72,136,60,162,162,162,60,32,32,32,32 };
byte code SYMBOL_TLACITKO_AUTO[38] = { 4,9,16,40,40,40,68,124,68,130,130,130,130,130,130,130,130,130,68,56,254,16,16,16,16,16,16,16,16,60,66,129,129,129,129,129,66,60 };
byte code SYMBOL_DEL[29] = { 3,9,124,66,65,65,65,65,65,66,124,63,32,32,32,63,32,32,32,63,32,32,32,32,32,32,32,32,63 };
byte code SYMBOL_MODE_AUTO[34] = { 2,16,0,0,0,0,0,37,85,85,117,85,82,0,0,0,0,0,0,0,0,0,0,114,37,37,37,37,34,0,0,0,0,0 };
byte code SYMBOL_MODE_STOP[26] = { 3,8,103,146,130,66,34,18,146,98,57,69,69,69,69,69,69,57,206,41,41,41,206,8,8,8 };

#endif  // LANG_SW

//-----------------------------------------------------------------------------
// Stringy - madarstina
//-----------------------------------------------------------------------------

#ifdef LANG_HU

byte code STR_ZADEJTE_CILOVOU_TEPLOTU[] = "K\x08Bv\x089nt h\x08Em\x08Ars\x08Aklet:";
byte code STR_STUPNE_CELSIA[] = "\x07FC";
byte code STR_STUPNE_FAHRENHEITA[] = "\x07FF";
byte code STR_STUPNE_KRAT_SEKUNDY[] = "\x07FC*s";
byte code STR_SEKUND[] = "m";
byte code STR_HODIN[] = "\x08C";
byte code STR_CHYBA_NEPLATNA_HODNOTA[] = "Az \x08Art\x08Ak nem \x08Arv\x08Anyes";

byte code STR_LOZENI_PREPRAVEK[] = "Berakod\x089s";

byte code STR_ZADEJTE_DATUM[] = "Foly\x08C d\x089tum:";
byte code STR_ZADEJTE_CAS[] = "Foly\x08C id\x08E:";

byte code STR_SMAZAT_PAMET_1[] = "T\x08Dr\x08Dlni szeretn\x08A a mem\x08C-";
byte code STR_SMAZAT_PAMET_2[] = "ri\x089ban lev\x08E \x08Dsszes adatot?";

byte code STR_OK[] = "OK";
byte code STR_OPAKOVAT[] = "\x098jra";
byte code STR_ZRUSIT[] = "M\x08Agse";
byte code STR_ANO[] = "Igen";
byte code STR_NE[] = "Nem";

byte code STR_MENU[] = "Men\x090";
byte code STR_TISK_PROTOKOLU[] = "Nyomtassa a r\x08Dgz\x08Bt\x08Ast";
byte code STR_ZAZNAMY[] = "M\x08Ar\x08Asek";
byte code STR_NASTAVENI[] = "Be\x089ll\x08Bt\x089sok";
byte code STR_MEZE[] = "Korl\x089tok";
byte code STR_REGULATOR[] = "Szab\x089lyoz\x08C";
byte code STR_DIESEL[] = "D\x08Bzel";
byte code STR_LOGIN[] = "Bel\x08Ap\x08As";

byte code STR_TEPLOTA[] = "H\x08Em\x08Ars\x08Aklet";
byte code STR_VZDUCHOVE_FILTRY[] = "L\x08Agsz\x091r\x08Ek";
byte code STR_CERSTVY_VZDUCH[] = "Friss leveg\x08E";
byte code STR_CO2[] = "CO2";

byte code STR_HESLO[] = "\x094rja be jelszav\x089t:";

byte code STR_ZADEJTE_MAXIMALNI_TLAK[] = "Maxim\x089lis nyom\x089s:";
byte code STR_PASCAL[] = "Pa";

byte code STR_ZADEJTE_MAXIMALNI_CO2[] = "Maxim\x089lis koncentr\x089ci\x08C CO2:";
byte code STR_PPM[] = "ppm";

byte code STR_JEDNOTKY[] = "Egys\x08Agek";
byte code STR_ZADEJTE_HYSTEREZI[] = "Hiszter\x08Azis:";
byte code STR_ZADEJTE_MAXIMALNI_PREKMIT[] = "Maxim\x089lis h\x08Et\x08Fll\x08Dv\x08As:";
byte code STR_ZADEJTE_ZESILENI[] = "Nyeres\x08Ag:";
byte code STR_ZADEJTE_INTEGRACNI_CAS[] = "Teljes\x08Bt\x08Asi id\x08E:";
byte code STR_ZADEJTE_DERIVACNI_CAS[] = "Deriv\x089l\x089si id\x08E:";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MIN1[] = "Tilos a friss leveg\x08E";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MIN2[] = "hat\x08Ct\x089vols\x089ga - Minimum";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MAX1[] = "Tilos a friss leveg\x08E";
byte code STR_ZADEJTE_ZAKAZANE_PASMO_MAX2[] = "hat\x08Ct\x089vols\x089ga - Maximum";

byte code STR_INFORMACE_DIESEL[] = "Inform\x089ci\x08C";
byte code STR_VYMENA_OLEJE[] = "Olaj lecser\x08Alve";
//byte code STR_PERIODA_VYMENY_OLEJE[] = "Olajcsere peri\x08Cdus";
byte code STR_NULOVANI_MOTOHODIN[] = "\x099zem\x08Cra t\x08Drl\x08Ase";

// Diesel
byte code STR_MOTOHODINY[] = "\x099zem\x08Cra:";
byte code STR_VYMENA_OLEJE_PRI[] = "K\x08Dvetkez\x08E olajcsere:";
byte code STR_VYMENA_OLEJE_1[] = "Kicser\x08Alte";
byte code STR_VYMENA_OLEJE_2[] = "az olajat?";
byte code STR_ZADEJTE_TYP[] = "T\x08Bpusa:";
byte code STR_KUBOTA[] = "Kubota";
byte code STR_DEUTZ[] = "Deutz";
byte code STR_ZADEJTE_PERIODU_VYMENY_OLEJE[] = "Olajcsere peri\x08Cdus:";

// Mez ve skrini
byte code STR_ODCHYLKA_VE_SKRINI_NAD_1[] = "Maxim\x089lis fels\x08E kapcsol\x089si";
byte code STR_ODCHYLKA_VE_SKRINI_NAD_2[] = "h\x08Em\x08Ars\x08Aklet k\x090l\x08Dnbs\x08Ag:";
byte code STR_ODCHYLKA_VE_SKRINI_POD_1[] = "Maxim\x089lis als\x08C kapcsol\x089si";
byte code STR_ODCHYLKA_VE_SKRINI_POD_2[] = "h\x08Em\x08Ars\x08Aklet k\x090l\x08Dnbs\x08Ag:";

// Menu zaznamy
byte code STR_ZKOPIROVAT_DO_MODULU[] = "M\x08Ar\x08Asek m\x089sol\x089sa";
byte code STR_PERIODA_UKLADANI[] = "Ment\x08Asi peri\x08Cdus";
byte code STR_IDENTIFIKACNI_CISLO[] = "Azonos\x08Bt\x089si sz\x089m";

// Perioda ukladani
byte code STR_PERIODA_1[] = "1 perc";
byte code STR_PERIODA_5[] = "5 perc";
byte code STR_PERIODA_10[] = "10 perc";
byte code STR_PERIODA_20[] = "20 perc";

// Kopie modulu
byte code STR_PRIPOJTE_MODUL1[] = "Tegye be a mem\x08Cria";
byte code STR_PRIPOJTE_MODUL2[] = "modult";
byte code STR_CEKEJTE[] = "V\x089rjon...";
byte code STR_KOPIROVANI_OK[] = "M\x089sol\x089s sikeres";
byte code STR_KOPIROVANI_CHYBA[] = "Hiba";

// Login
byte code STR_POUZIVAT_LOGIN1[] = "Haszn\x089lja a sof\x08Er";
byte code STR_POUZIVAT_LOGIN2[] = "bel\x08Ap\x08As\x08At?";
byte code STR_LOGIN_RIDICE[] = "Sof\x08Er bel\x08Ap\x08As:";

// Tisk protokolu
byte code STR_ZADEJTE_ZAZNAMY_NA_RADEK1[] = "M\x08Ar\x08Asek";
byte code STR_ZADEJTE_ZAZNAMY_NA_RADEK2[] = "\x089tlaga:";
byte code STR_ZADEJTE_POCATECNI_DATUM1[] = "Kezd\x08As";
byte code STR_ZADEJTE_POCATECNI_DATUM2[] = "d\x089tuma:";
byte code STR_ZADEJTE_POCATECNI_CAS1[] = "Kezd\x08As";
byte code STR_ZADEJTE_POCATECNI_CAS2[] = "ideje(\x08C/p):";
byte code STR_ZADEJTE_KONCOVE_DATUM1[] = "Befejez\x08As";
byte code STR_ZADEJTE_KONCOVE_DATUM2[] = "d\x089tuma:";
byte code STR_ZADEJTE_KONCOVY_CAS1[] = "Befejez\x08As";
byte code STR_ZADEJTE_KONCOVY_CAS2[] = "ideje(\x08C/p):";
byte code STR_TISKNOUT_KOPII1[] = "M\x089solatot";
byte code STR_TISKNOUT_KOPII2[] = "nyomtat?";

// Dezinfekce
byte code STR_DEZINFEKCE_START1[] = "T\x08Anyleg fert\x08Etlen\x08Bt\x08As";
byte code STR_DEZINFEKCE_START2[] = "elind\x08Bt\x089s\x089hoz?";
byte code STR_DEZINFEKCE_ZAHAJENA[] = "Fert\x08Etlen\x08Bt\x08E indult";
byte code STR_DEZINFEKCE_CHYBA[] = "Hiba fert\x08Etlen\x08Bt\x08E";

// Porucha spojeni pro terminal
byte code STR_VYPADEK_SPOJENI[] = "Csatlakoz\x089si hiba";

// Symboly s textem
byte code SYMBOL_TLACITKO_STOP[38] = { 4,9,120,132,132,128,120,4,132,132,120,254,16,16,16,16,16,16,16,16,28,34,65,65,65,65,65,34,28,62,33,33,33,62,32,32,32,32 };
byte code SYMBOL_TLACITKO_AUTO[38] = { 4,9,16,40,40,40,68,124,68,130,130,130,130,130,130,130,130,130,68,56,254,16,16,16,16,16,16,16,16,60,66,129,129,129,129,129,66,60 };
byte code SYMBOL_DEL[29] = { 3,9,124,66,65,65,65,65,65,66,124,63,32,32,32,63,32,32,32,63,32,32,32,32,32,32,32,32,63 };
byte code SYMBOL_MODE_AUTO[34] = { 2,16,0,0,0,0,0,37,85,85,117,85,82,0,0,0,0,0,0,0,0,0,0,114,37,37,37,37,34,0,0,0,0,0 };
byte code SYMBOL_MODE_STOP[26] = { 3,8,115,136,128,96,16,8,136,112,230,137,144,144,144,144,137,134,60,34,162,162,188,160,32,32 };

#endif  // LANG_HU

//-----------------------------------------------------------------------------
// Stringy - polozky menu pro zjednoduseni
//-----------------------------------------------------------------------------

byte code * code STR_MAIN_MENU_ITEMS[] = { STR_LOGIN, STR_TISK_PROTOKOLU, STR_ZAZNAMY, STR_NASTAVENI };
byte code * code STR_MENU_PERIOD_ITEMS[] = { STR_PERIODA_1, STR_PERIODA_5, STR_PERIODA_10, STR_PERIODA_20 };
byte code * code STR_MENU_SAVING_ITEMS[] = { STR_ZKOPIROVAT_DO_MODULU, STR_PERIODA_UKLADANI, STR_IDENTIFIKACNI_CISLO };
byte code * code STR_MENU_DIESEL_ITEMS[] = { STR_INFORMACE_DIESEL, STR_VYMENA_OLEJE, STR_NASTAVENI, STR_NULOVANI_MOTOHODIN };
byte code * code STR_MENU_SETUP_ITEMS[] = { 
   STR_MEZE, 
#ifndef NO_DIESEL
   STR_DIESEL, 
#endif
   STR_LOGIN, 
   STR_REGULATOR 
};

byte code * code STR_MENU_LIMITS_ITEMS[] = { 
   STR_TEPLOTA
   , STR_VZDUCHOVE_FILTRY
#ifdef USE_CO2
   , STR_CO2 
#endif
};
byte code * code STR_MENU_REGULATOR_ITEMS[] = { 
STR_TEPLOTA
, STR_CERSTVY_VZDUCH
#ifdef USE_CO2
, STR_CO2 
#endif
};
byte code * code STR_MENU_UNITS_ITEMS[] = { STR_STUPNE_CELSIA, STR_STUPNE_FAHRENHEITA };
byte code * code STR_MENU_DIESEL_TYPE_ITEMS[] = { STR_KUBOTA, STR_DEUTZ };

//-----------------------------------------------------------------------------
// Symboly
//-----------------------------------------------------------------------------

// Klapky
#define KLAPKA_VYP   FAIR_FLAP_STEPS   // Index vypnute (odpojene) klapky
unsigned char code SYMBOL_KLAPKA[17][34] = {
2,16,0,0,0,0,0,0,1,251,251,1,0,0,0,0,0,0,0,0,0,0,0,0,128,223,223,128,0,0,0,0,0,0,
2,16,0,0,0,0,0,0,1,27,251,225,0,0,0,0,0,0,0,0,0,0,0,0,135,223,216,128,0,0,0,0,0,0,
2,16,0,0,0,0,0,0,1,3,123,249,0,0,0,0,0,0,0,0,0,0,0,0,159,222,192,128,0,0,0,0,0,0,
2,16,0,0,0,0,0,0,1,3,27,249,224,0,0,0,0,0,0,0,0,0,0,15,159,216,192,128,0,0,0,0,0,0,
2,16,0,0,0,0,0,0,1,3,11,57,120,64,0,0,0,0,0,0,0,0,6,31,156,192,192,128,0,0,0,0,0,0,
2,16,0,0,0,0,0,0,1,3,3,25,56,112,0,0,0,0,0,0,0,2,14,60,144,192,192,128,0,0,0,0,0,0,
2,16,0,0,0,0,0,0,1,3,3,9,28,120,32,0,0,0,0,0,0,14,28,56,144,192,192,128,0,0,0,0,0,0,
2,16,0,0,0,0,0,0,1,3,3,1,12,56,48,0,0,0,0,0,12,28,56,48,128,192,192,128,0,0,0,0,0,0,
2,16,0,0,0,0,0,0,1,3,3,1,12,12,24,24,16,0,0,0,12,24,56,48,128,192,192,128,0,0,0,0,0,0,
2,16,0,0,0,0,0,0,1,3,3,1,4,14,12,24,16,0,0,8,24,56,112,32,128,192,192,128,0,0,0,0,0,0,
2,16,0,0,0,0,0,0,1,3,3,1,0,6,14,12,0,0,0,24,48,48,96,32,128,192,192,128,0,0,0,0,0,0,
2,16,0,0,0,0,0,0,1,3,3,1,0,7,6,6,4,0,32,48,112,96,224,0,128,192,192,128,0,0,0,0,0,0,
2,16,0,0,0,0,0,0,1,3,3,1,0,3,3,6,6,0,96,96,96,224,192,0,128,192,192,128,0,0,0,0,0,0,
2,16,0,0,0,0,0,0,1,3,3,1,0,3,3,3,3,2,64,192,192,192,192,0,128,192,192,128,0,0,0,0,0,0,
2,16,0,0,0,1,1,0,1,3,3,1,0,1,1,3,3,3,192,192,192,192,128,0,128,192,192,128,0,128,128,0,0,0,
2,16,1,1,1,1,1,0,1,3,3,1,0,1,1,1,1,1,128,128,128,128,128,0,128,192,192,128,0,128,128,128,128,128,
2,16,0,0,0,8,28,14,7,3,3,7,14,28,8,0,0,0,0,0,0,16,56,112,224,192,192,224,112,56,16,0,0,0 };

// Desetinna tecka u teploty ve skrini
byte code SYMBOL_TECKA_TEPLOTA[4] = { 1,2,6,6 };
byte code SYMBOL_TECKA_TEPLOTA_2[4] = { 1,2,246,246 };     // Spec. pripad pro velke cislice koncici 2
byte code SYMBOL_TECKA_TEPLOTA_4[4] = { 1,2,134,134 };     // Spec. pripad pro velke cislice koncici 4

// Tlacitko OK
byte code SYMBOL_TLACITKO_OK[70] = { 4,17,0,0,0,0,0,0,0,0,1,3,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,128,192,224,113,59,31,14,4,0,0,0,0,1,3,7,14,28,56,112,224,192,128,0,0,0,24,56,112,224,192,128,0,0,0,0,0,0,0,0,0,0,0 };

// Tlacitka pro volbu rezimu
byte code SYMBOL_TLACITKO_VENTILATOR[98] = { 4,24,0,0,0,0,0,0,0,0,0,7,8,8,8,8,7,0,0,0,0,0,0,0,0,0,3,4,4,4,4,4,2,2,1,1,243,15,15,51,193,1,2,2,2,2,4,4,4,3,192,32,32,32,64,64,64,64,128,131,204,240,240,207,128,128,64,64,32,32,32,32,32,192,0,0,0,0,0,0,0,0,0,224,16,16,16,16,224,0,0,0,0,0,0,0,0,0 };
byte code SYMBOL_TLACITKO_DEZINFEKCE_SKRINE[82] = { 4,20,0,0,0,63,64,128,128,128,128,128,128,128,128,128,128,135,136,120,8,7,63,64,64,192,64,97,116,113,116,97,64,64,64,64,63,0,128,255,128,0,255,0,0,0,64,0,0,64,0,0,64,0,0,0,255,1,2,254,2,1,254,1,1,1,1,1,1,1,1,1,1,1,1,1,254,200,40,48,32,192 };
byte code SYMBOL_TLACITKO_DEZINFEKCE_KOL[114] = { 4,28,128,128,64,40,28,62,30,15,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,168,0,160,3,135,15,31,30,62,61,61,61,62,30,31,15,7,3,0,0,0,0,0,0,0,0,0,0,0,0,248,254,255,255,143,115,219,253,117,253,219,115,143,255,255,254,248,0,0,0,0,0,0,0,0,0,0,0,0,0,0,128,192,192,224,224,224,224,224,192,192,128,0,0,0 };

byte code * code SYMBOLY_TLACITKA_REZIM[5] = { SYMBOL_TLACITKO_STOP, SYMBOL_TLACITKO_AUTO, SYMBOL_TLACITKO_VENTILATOR, SYMBOL_TLACITKO_DEZINFEKCE_KOL, SYMBOL_TLACITKO_DEZINFEKCE_SKRINE };  // Pro zjednoduseni

// Tlacitka pro volbu klapek
byte code SYMBOL_TLACITKO_RUCNE[66] = { 4,16,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,26,38,38,18,18,104,152,136,64,32,32,16,8,4,4,128,112,72,74,77,73,9,1,2,2,2,4,4,8,8,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
byte code SYMBOL_TLACITKO_RECIRKULACE[78] = { 4,19,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,127,127,62,126,252,204,136,128,128,128,128,128,128,128,192,224,120,63,15,0,0,4,14,7,3,1,1,1,1,1,1,1,1,3,7,30,252,240,0,0,0,0,0,0,128,128,128,128,128,128,128,128,0,0,0,0,0 };
byte code SYMBOL_TLACITKO_NAPOR[70] = { 4,17,0,0,0,3,3,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,255,255,0,0,8,14,15,255,255,15,14,8,0,0,8,14,15,255,255,15,14,0,0,128,192,192,128,0,0,0,0,0,0,128,192,192,128,0 };
byte code SYMBOL_TLACITKO_KLAPKA_ZAVRIT[26] = { 4,6,0,0,87,167,0,0,1,3,255,255,3,1,128,192,255,255,192,128,0,0,229,234,0,0 };
byte code SYMBOL_TLACITKO_KLAPKA_OTEVRIT[90] = { 4,22,0,0,0,0,0,0,0,0,0,0,80,160,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,3,7,7,3,1,1,1,1,1,1,1,1,1,128,128,128,128,128,128,128,128,128,192,224,224,192,128,128,128,128,128,128,128,128,128,0,0,0,0,0,0,0,0,0,0,5,10,0,0,0,0,0,0,0,0,0,0 };
byte code SYMBOL_RUKA_MALA[24] = { 2,11,4,27,42,42,106,160,128,64,32,32,16,0,0,128,192,160,160,32,64,64,128,128 };
byte code SYMBOL_STUPNE_CELSIA[16] = { 2,7,35,84,36,4,4,4,3,128,64,0,0,0,64,128 };
byte code SYMBOL_STUPNE_FAHRENHEITA[16] = { 2,7,39,84,36,7,4,4,4,128,0,0,0,0,0,0 };

// Lozeni prepravek
byte code SYMBOL_LOZENI_PLNE[74] = { 4,18,213,170,213,170,213,170,213,170,213,170,213,170,213,170,213,170,213,170,85,170,85,170,85,170,85,170,85,170,85,170,85,170,85,170,85,170,85,170,85,170,85,170,85,170,85,170,85,170,85,170,85,170,85,170,85,171,85,171,85,171,85,171,85,171,85,171,85,171,85,171,85,171 };
byte code SYMBOL_LOZENI_LINKA[6] = { 4,1,127,255,255,254 };
byte code SYMBOL_LOZENI_SIPKA[24] = { 2,11,1,3,7,15,31,63,7,7,7,7,7,128,192,224,240,248,252,224,224,224,224,224 };
byte code SYMBOL_LOZENI_MINI[6][34] = {
2,16,255,213,170,213,170,213,170,213,170,213,170,213,170,213,170,255,192,64,192,64,192,64,192,64,192,64,192,64,192,64,192,192,
2,16,255,213,170,213,170,213,170,213,170,213,255,128,128,128,128,255,192,64,192,64,192,64,192,64,192,64,192,64,64,64,64,192,
2,16,255,213,170,213,170,255,128,128,128,128,255,128,128,128,128,255,192,64,192,64,192,192,64,64,64,64,192,64,64,64,64,192,
2,16,255,128,128,128,128,255,170,213,170,213,170,213,170,213,170,255,192,64,64,64,64,192,192,64,192,64,192,64,192,64,192,192,
2,16,255,128,128,128,128,255,128,128,128,128,255,213,170,213,170,255,192,64,64,64,64,192,64,64,64,64,192,64,192,64,192,192,
2,16,255,128,128,128,128,255,170,213,170,213,255,128,128,128,128,255,192,64,64,64,64,192,192,64,192,64,192,64,64,64,64,192 };

// Zobrazeni rezimu v hlavnim zobrazeni
byte code SYMBOL_MODE_MANUAL[34] = { 2,16,1,26,38,38,18,18,104,152,136,64,32,32,16,8,4,4,128,112,72,74,77,73,9,1,2,2,2,4,4,8,8,8 };
byte code SYMBOL_MODE_CHLAZENI[34] = { 2,16,5,3,17,81,113,61,79,7,3,111,61,121,17,33,3,5,160,192,136,138,158,188,242,224,194,246,188,158,138,136,192,160 };
byte code SYMBOL_MODE_TOPENI[34] = { 2,16,0,0,0,28,62,99,65,0,0,28,62,99,65,0,0,0,0,0,0,62,6,14,250,242,0,62,6,14,250,242,0,0 };
byte code SYMBOL_MODE_VENTILACE[34] = { 2,16,0,3,4,4,2,1,1,59,71,73,48,1,1,1,0,0,0,0,128,128,128,12,146,226,220,128,128,64,32,32,192,0 };
//byte code SYMBOL_MODE_CHLAZENI_ON[16] = {2,7,2,1,9,7,9,1,2,128,0,32,192,32,0,128};
byte code SYMBOL_MODE_TOPENI_ON[16] = { 2,7,2,2,3,2,0,1,0,0,128,128,128,128,192,128 };
byte code SYMBOL_MODE_TOPENI_ON_2X[16] = { 2,7,2,2,3,2,0,1,0,48,144,160,176,128,192,128 };

byte code SYMBOL_STUPNICE[11][46] = {
4,11,0,0,0,0,0,0,0,0,0,0,219,0,0,0,0,0,0,0,0,0,0,109,0,0,0,0,0,0,0,0,0,0,182,0,0,0,0,0,0,0,0,0,0,219,
4,11,0,0,0,0,0,0,0,0,0,24,219,0,0,0,0,0,0,0,0,0,0,109,0,0,0,0,0,0,0,0,0,0,182,0,0,0,0,0,0,0,0,0,0,219,
4,11,0,0,0,0,0,0,0,0,3,27,219,0,0,0,0,0,0,0,0,0,0,109,0,0,0,0,0,0,0,0,0,0,182,0,0,0,0,0,0,0,0,0,0,219,
4,11,0,0,0,0,0,0,0,0,3,27,219,0,0,0,0,0,0,0,96,96,96,109,0,0,0,0,0,0,0,0,0,0,182,0,0,0,0,0,0,0,0,0,0,219,
4,11,0,0,0,0,0,0,0,0,3,27,219,0,0,0,0,0,0,12,108,108,108,109,0,0,0,0,0,0,0,0,0,0,182,0,0,0,0,0,0,0,0,0,0,219,
4,11,0,0,0,0,0,0,0,0,3,27,219,0,0,0,0,0,1,13,109,109,109,109,0,0,0,0,0,128,128,128,128,128,182,0,0,0,0,0,0,0,0,0,0,219,
4,11,0,0,0,0,0,0,0,0,3,27,219,0,0,0,0,0,1,13,109,109,109,109,0,0,0,0,48,176,176,176,176,176,182,0,0,0,0,0,0,0,0,0,0,219,
4,11,0,0,0,0,0,0,0,0,3,27,219,0,0,0,0,0,1,13,109,109,109,109,0,0,0,6,54,182,182,182,182,182,182,0,0,0,0,0,0,0,0,0,0,219,
4,11,0,0,0,0,0,0,0,0,3,27,219,0,0,0,0,0,1,13,109,109,109,109,0,0,0,6,54,182,182,182,182,182,182,0,0,192,192,192,192,192,192,192,192,219,
4,11,0,0,0,0,0,0,0,0,3,27,219,0,0,0,0,0,1,13,109,109,109,109,0,0,0,6,54,182,182,182,182,182,182,0,24,216,216,216,216,216,216,216,216,219,
4,11,0,0,0,0,0,0,0,0,3,27,219,0,0,0,0,0,1,13,109,109,109,109,0,0,0,6,54,182,182,182,182,182,182,3,27,219,219,219,219,219,219,219,219,219 };

// Diesel
//byte code SYMBOL_TEPLOTA_DIESELU[26] = {2,12,1,2,2,2,2,3,3,3,7,7,7,3,0,128,128,128,128,128,128,128,192,192,192,128};
#define MAX_STAVNAFTY (ADSL_STEPS-1)  // Stav nafty muze byt 0 az 10
byte code SYMBOL_TEPLOTA_DIESELU_STUPNICE_KOSTRA[50] = { 6,8,255,128,128,128,128,160,162,255,255,0,0,0,0,0,34,255,255,0,0,0,0,2,34,255,255,0,0,0,0,0,34,255,255,0,0,0,0,0,34,255,248,8,8,8,8,40,40,248 };
byte code SYMBOL_TEPLOTA_DIESELU_STUPNICE[11][14] = {
6,2,128,128,0,0,0,0,0,0,0,0,8,8,
6,2,190,190,0,0,0,0,0,0,0,0,8,8,
6,2,191,191,224,224,0,0,0,0,0,0,8,8,
6,2,191,191,254,254,0,0,0,0,0,0,8,8,
6,2,191,191,255,255,224,224,0,0,0,0,8,8,
6,2,191,191,255,255,254,254,0,0,0,0,8,8,
6,2,191,191,255,255,255,255,224,224,0,0,8,8,
6,2,191,191,255,255,255,255,254,254,0,0,8,8,
6,2,191,191,255,255,255,255,255,255,224,224,8,8,
6,2,191,191,255,255,255,255,255,255,254,254,8,8,
6,2,191,191,255,255,255,255,255,255,255,255,232,232 };
byte code SYMBOL_OLEJ[34] = { 2,16,0,0,0,0,7,2,39,88,72,48,16,31,0,0,0,0,0,0,0,0,0,0,56,208,36,68,142,14,4,0,0,0 };

// Agregaty
byte code SYMBOL_DIESEL[34] = { 2,16,0,0,0,0,0,63,64,64,64,64,64,63,0,0,0,0,0,0,0,0,0,242,10,14,10,18,32,192,0,0,0,0 };
byte code SYMBOL_ELEKTROMOTOR[34] = { 2,16,0,0,0,1,1,3,3,7,7,15,13,1,1,1,3,2,64,192,128,128,128,128,128,176,240,224,224,192,128,0,0,0 };
byte code SYMBOL_DOBIJENI[34] = { 2,16,0,0,68,42,17,3,0,14,63,32,36,46,36,32,63,0,0,0,0,128,128,128,0,56,254,2,2,58,2,2,254,0 };
//byte code SYMBOL_VYKON_VENTILATORU[34] = {2,16,0,24,20,12,3,3,3,12,20,24,0,115,68,52,20,99,0,48,80,96,128,128,128,96,80,48,0,48,180,136,150,6};

// Poruchy
byte code SYMBOL_PORUCHA_SERVA_TOPENI[34] = { 2,16,0,0,1,115,1,4,8,0,0,12,18,1,0,12,18,1,32,64,0,156,0,0,0,0,0,112,48,208,0,112,48,208 };

// Symboly v hlavnim menu
byte code SYMBOL_MENU_JAS[36] = { 2,17,0,0,32,17,6,8,8,16,208,16,8,8,6,17,32,0,0,128,128,2,196,48,8,8,4,5,4,8,8,48,196,2,128,128 };
byte code SYMBOL_MENU_KONTRAST[32] = { 2,15,7,25,33,65,65,129,129,129,129,129,65,65,33,25,7,192,176,88,172,84,170,86,170,86,170,84,172,88,176,192 };
byte code SYMBOL_MENU_PLUS[30] = { 2,14,1,1,1,1,1,1,127,127,1,1,1,1,1,1,128,128,128,128,128,128,254,254,128,128,128,128,128,128 };
byte code SYMBOL_MENU_MINUS[6] = { 2,2,127,127,254,254 };
byte code SYMBOL_MENU_REPRO[40] = { 2,19,0,0,0,0,1,2,124,68,68,68,68,68,124,2,1,0,0,0,0,16,48,80,144,16,16,16,16,16,16,16,16,16,16,16,144,80,48,16 };
byte code SYMBOL_MENU_REPRO_KRIZEK[40] = { 2,19,0,0,64,32,17,10,124,70,69,68,69,70,124,10,17,32,64,0,0,16,48,81,146,20,24,16,48,80,144,80,48,16,24,20,146,81,48,16 };
byte code SYMBOL_SIPKA_VPRAVO[8] = { 1,6,4,6,127,127,6,4 };

// Vykon ventilatoru
byte code SYMBOL_FAN_POWER[10] = { 1,8,32,82,37,26,88,164,74,4 };
byte code SYMBOL_100_PROCENT[14] = { 2,6,36,106,42,42,42,36,74,162,164,164,168,74 };
byte code SYMBOL_50_PROCENT[14] = { 2,6,28,17,25,5,5,24,148,68,72,72,80,148 };

// CO2
byte code SYMBOL_CO2[34] = { 2,16,12,17,17,17,17,12,0,0,0,0,0,25,21,21,25,17,192,32,44,34,36,200,14,0,0,0,0,138,85,85,149,0 };

// Vlhkost
byte code SYMBOL_VLHKOST[18] = { 1,16,16,16,40,40,68,68,56,0,0,0,36,84,40,10,21,18 };

// Tlak filtru
byte code SYMBOL_TLAK_FILTRY[34] = { 2,16,5,5,5,10,10,31,0,0,0,0,28,18,18,28,17,16,64,64,64,160,160,240,0,0,0,0,0,192,32,224,32,224 };

// Zakazane repro v TMC
byte code SYMBOL_REPRO_KRIZEK[34] = { 2,16,0,0,0,1,3,5,57,41,41,41,57,5,3,1,0,0,0,0,0,0,0,0,68,40,16,40,68,0,0,0,0,0 };

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

#if (VERSION_HW < 10)
#define X_VERZE 24               // Umisteni cisla verze na displeji pri startu
#else
#define X_VERZE 22               // Umisteni cisla verze na displeji pri startu
#endif
#define Y_VERZE 217
#define X_ID    0                  // Umisteni identifikacniho cisla verze na displeji pri startu
#define Y_ID    217

void MenuInit () {
   // Inicializace zobrazeni, pred volanim uz musi byt nactena konfigurace
 /*  byte x;
   byte y;
   byte code *p;*/


   DisplayInit ();
   DisplaySetPlane (DISPLAY_PLANE1);
   DisplayPlane (NO);
   DisplayMode = DISPLAY_TEMP;
   OldDisplayMode = DISPLAY_TEMP;
   Redraw = REDRAW_ALL;
   DiagnosticCounter = 0;
   DiagnosticMode = NO;
   // Podsvit - chci, aby zacal hned svitit
   Brightness = BRIGHTNESS_MAX;


   /*  DisplayClear();
   //  p = SYMBOL_KLAPKA[KLAPKA_VYP];
     p = SYMBOL_TEPLOTA_DIESELU_STUPNICE_KOSTRA;
   //  p = SYMBOL_RUKA_MALA;
     for (y = 0; y < DISPLAY_HEIGHT - p[1] - 1; y += p[1] + 1) {
       for (x = 0; x < DISPLAY_WIDTH / 8 - p[0] - 1; x += p[0]) {
         DisplaySymbol(x, y, p);
         WatchDog();
       }
     }
     DisplayPlane(YES);
     while(EA) {
       WatchDog();
     }*/



     // Zobrazim uvodni obrazovku
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, 30, STR_STARTUP_1);
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, 70, STR_STARTUP_2);
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, 100, STR_STARTUP_3);
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, 140, STR_STARTUP_4);
   // Zobrazim verzi firmware
   DisplaySetFont (FONT_NUM_TAHOMA18);
   DisplayMoveTo (X_VERZE, Y_VERZE);
   putchar (0x30 + VERSION_MAJOR);
   putchar ('.');
   putchar (0x30 + VERSION_MINOR / 10);
   putchar (0x30 + VERSION_MINOR % 10);
   putchar ('.');
   putchar (0x30 + VERSION_BUILD);
   putchar ('-');
#if (VERSION_HW < 10)
   putchar (0x30 + VERSION_HW);
#else
   putchar (0x30 + VERSION_HW / 10);
   putchar (0x30 + VERSION_HW % 10);
#endif
#ifndef __TMCREMOTE__
   // Zobrazim identifikacni cislo
   UN.DT = bindec (Config.IdentificationNumber);
   DisplayCharTahoma18 (X_ID, Y_ID - 5, '#');
   DisplayMoveTo (X_ID + 2, Y_ID);
   putchar (0x30 + hnib (UN.ST.X4));
   putchar (0x30 + lnib (UN.ST.X4));
#endif // __TMCREMOTE__
   DisplayPlane (YES);
   // Blikani
   Blink.Show = YES;
   Blink.Change = NO;
}

//-----------------------------------------------------------------------------
// Dotaz
//-----------------------------------------------------------------------------

static TYesNo AskQuestion (byte code *Row1, byte code *Row2) {
   DisplayClear ();
   DisplayPlane (NO);
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, 65, Row1);
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, 95, Row2);
   DisplayYesNo ();
   DisplayPlane (YES);
   return DisplayExecuteButtons (2);
}

//-----------------------------------------------------------------------------
// Vykrelovani menu pro zjednoduseni
//-----------------------------------------------------------------------------

void DrawTwoLinesNumPad (byte code *Text1, byte code *Text2, TYesNo Sign) {
   // Vykresli numpad se 2 radky.
   DisplayClear ();
   DisplayPlane (NO);
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, NUMPAD_2ROWS_Y1, Text1);
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, NUMPAD_2ROWS_Y2, Text2);
   DisplayDrawNumPad (Sign);
   DisplayPlane (YES);
}

void DrawOneLineNumPad (byte code *Text, TYesNo Sign) {
   // Vykresli numpad s 1 radkem.
   DisplayClear ();
   DisplayPlane (NO);
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, NUMPAD_1ROW_Y, Text);
   DisplayDrawNumPad (Sign);
   DisplayPlane (YES);
}

//-----------------------------------------------------------------------------
// Standardni pracovni plochy
//-----------------------------------------------------------------------------

// Plus a minus pres celou obrazovku

#define MODE_BUTTON_PLUS_X       32
#define MODE_BUTTON_PLUS_Y       100
#define MODE_BUTTON_PLUS_WIDTH   6
#define MODE_BUTTON_PLUS_HEIGHT  48
#define MODE_BUTTON_MINUS_X      2
#define MODE_BUTTON_OK_X         32
#define MODE_BUTTON_OK_Y         186
#define MODE_BUTTON_OK_WIDTH     6
#define MODE_BUTTON_OK_HEIGHT    48

enum {
   ID_SCALE_MINUS,
   ID_SCALE_PLUS,
   ID_SCALE_OK
};

TTouchButton code PlusMinusButtons[] = {
  { // Minus vcetne autorepeat
    ID_SCALE_PLUS,
    MODE_BUTTON_PLUS_X * 8,
    MODE_BUTTON_PLUS_Y,
    MODE_BUTTON_PLUS_WIDTH * 8,
    MODE_BUTTON_PLUS_HEIGHT,
    YES
  },
  { // Plus vcetne autorepeat
    ID_SCALE_MINUS,
    MODE_BUTTON_MINUS_X * 8,
    MODE_BUTTON_PLUS_Y,
    MODE_BUTTON_PLUS_WIDTH * 8,
    MODE_BUTTON_PLUS_HEIGHT,
    YES
  },
  { // OK, bez autorepeat
    ID_SCALE_OK,
    MODE_BUTTON_OK_X * 8,
    MODE_BUTTON_OK_Y,
    MODE_BUTTON_OK_WIDTH * 8,
    MODE_BUTTON_OK_HEIGHT,
    NO
  }
};

TTouchWorkplace code PlusMinusWorkplace = {
  PlusMinusButtons,
  ArrayLength(PlusMinusButtons)
};

//-----------------------------------------------------------------------------
// Blikani roviny
//-----------------------------------------------------------------------------

void MenuBlink (TYesNo On) {
   // Zobrazi / schova blikajici rovinu
   DisplaySetPlane (DISPLAY_PLANE2);  // Prepnu na blikajici rovinu
   DisplayPlane (On);                 // Zapnu/vypnu podle pozadavku
   DisplaySetPlane (DISPLAY_PLANE1);  // Prepnu zpet na standardni rovinu
}

//-----------------------------------------------------------------------------
// Prace s klavesami
//-----------------------------------------------------------------------------

byte WaitForKey () {
   // Ceka na stisk klavesy
   while (1) {
      if (_Klavesa == K_TIMEOUT) {
         SysYield ();             // vypadeni muze trvat dlouho, dej cas pro operacni system
         return(K_TIMEOUT);     // opakuj, dokud vsichni nevypadnou
      }
      _Klavesa = SysWaitEvent ();
      switch (_Klavesa) {
      case K_REDRAW:
         continue;            // v menu nas nezajima, cekej dal
      case K_BLINK_OFF:
         Blink.Show = NO;
         Blink.Change = YES;   // Nastavim flag, ze mam zmenit zobrazeni. Musim vynulovat az to zobrazeni zmenim.
         return(K_REDRAW);   // neco neskodneho pro prekresleni
      case K_BLINK_ON:
         Blink.Show = YES;
         Blink.Change = YES;   // Nastavim flag, ze mam zmenit zobrazeni. Musim vynulovat az to zobrazeni zmenim.
         return(K_REDRAW);   // neco neskodneho pro prekresleni
      default:
         return(_Klavesa);   // repeat si rozeberou jednotlive funkce
      }
   }
} // WaitForKey

#define PosliKlavesu( k)  _Klavesa = k
// Zaslani klavesy

#define ZerKlavesu()      _Klavesa = K_NULL;
// Spotrebovani klavesy

//-----------------------------------------------------------------------------
// Graf stupnice
//-----------------------------------------------------------------------------

#define SCALE_X          9
#define SCALE_Y          147   // Spodni Y souradnice
#define SCALE_BAR_DX     2     // Roztec sloupcu
#define SCALE_BAR_DY     4     // Zmena vysky na 1 krok (SCALE_STEP %)
#define SCALE_BAR_MOVE_Y 1     // Posun sloupce nahoru
#define SCALE_BAR_VALUE  0xFF  // Hodnota osmice pixelu ve sloupci
#define SCALE_STEP       10    // Krok procent
#define SCALE_STEPS      (100 / SCALE_STEP)    // Celkovy pocet sloupcu

void DrawScale (byte Percent) {
   // Vykresli graf stupnice 0-100% po kroku SCALE_STEP %
   byte j;

   if (Percent > SCALE_STEP / 2) {
      Percent += SCALE_STEP / 2;   // Zaokrouhlim, 4% tak ukaze 0, 5% ukaze 1
   }
   Percent /= SCALE_STEP;   // Z procent prevedu na index
   if (Percent > SCALE_STEPS) {
      Percent = SCALE_STEPS;  // Radeji omezim
   }
   // Sloupce
   for (j = 0; j <= SCALE_STEPS; j++) {
      if (j > 0 && j <= Percent) {  // Sloupec kreslim az od 10%
        // Vykreslim sloupec
         DisplayVertLine (SCALE_X + j * SCALE_BAR_DX, SCALE_Y - SCALE_BAR_MOVE_Y - j * SCALE_BAR_DY, j * SCALE_BAR_DY, SCALE_BAR_VALUE);
      }
      else {
         // Smazu sloupec
         DisplayVertLine (SCALE_X + j * SCALE_BAR_DX, SCALE_Y - SCALE_BAR_MOVE_Y - j * SCALE_BAR_DY, j * SCALE_BAR_DY, 0x00);
      }
      DisplayHorLine (SCALE_X + j * SCALE_BAR_DX, SCALE_Y, 1);   // Spodni linka
   }
}

//-----------------------------------------------------------------------------
// Prekresleni teplot
//-----------------------------------------------------------------------------

static void DrawTemperature1C (byte X, byte Y, char Temp) {
   // Zobrazi teplotu s presnosti 1C na pozici X, Y (X je osmice pixelu)
   DisplayMoveTo (X, Y);
   if (Temp == TEMP_INVALID_1C) {
      // Chyba mereni
      cputs (" --");
   }
   else if (Temp > 99) {
      // Teplota na 3 cislice ve Fahrenheitech, nemuze byt zaporna
      cprintdec (Temp, FMT_UNSIGNED | 3);
   }
   else {
      // Platne mereni, teplota na 2 cislice
      if (Temp < 0) {
         putchar ('-');
         Temp = -Temp;
      }
      else {
         putchar (' ');
      }
      cprintdec (Temp, FMT_UNSIGNED | 2);
   }
}

#define TEMP_ALARM_DX 10     // Veliost obdelnika pri chybe
#define TEMP_ALARM_DY 44

static void DrawTemperature01C (byte X, byte Y, int Temp, TYesNo Fault) {
   // Zobrazi teplotu na pozici X, Y na 0.1C velikosti 40, desetiny velikosti 18. Pokud je <Fault> = YES, vykresli i blikajici obdelnik
   // Cele stupne
   DisplaySetFont (FONT_NUM_MYRIAD40);
   DrawTemperature1C (X, Y, Temp / 10);   // Cele stupne
   // Desetinna tecka, specialni pripady pro cele stupne koncici na 2 a 4, kde mne to standardni tecka prekreslovala
   switch (abs ((Temp / 10) % 10)) {   // Zajima me absolutni hodnota jednotek stupnu
   case 2: DisplaySymbol (X + 11, Y + 34, SYMBOL_TECKA_TEPLOTA_2); break;
   case 4: DisplaySymbol (X + 11, Y + 34, SYMBOL_TECKA_TEPLOTA_4); break;
   default: DisplaySymbol (X + 11, Y + 34, SYMBOL_TECKA_TEPLOTA);
   }
   // Desetiny
   DisplaySetFont (FONT_NUM_TAHOMA18);
   DisplayMoveTo (X + 12, Y + 18);
   if (Temp == TEMP_INVALID_01C) {
      putchar ('-');
   }
   else {
      if (Temp < 0) {
         Temp = -Temp;
      }
      putchar (0x30 + Temp % 10);
   }

   // Porucha
   DisplaySetPlane (DISPLAY_PLANE2);             // blikajici rovina
   DisplayMoveTo (X + 4, Y - 4);
   if (Fault) {
      // Teplota se ma zobrazit a je chyba
      DisplayBox (TEMP_ALARM_DX, TEMP_ALARM_DY);           // Vykreslim alarm
   }
   else {
      DisplayClearBox (TEMP_ALARM_DX, TEMP_ALARM_DY);      // Smazu alarm
   }
   DisplaySetPlane (DISPLAY_PLANE1);             // Zpet
}

static void RedrawTemperatures () {
   // Prekresli vsechny teploty
   DisplaySetFont (FONT_NUM_MYRIAD32);
   DrawTemperature1C (0, TEMP_Y1 + 4, TempToDisplay1Degree (Temp1C[TEMP_OUTSIDE]));
   DrawTemperature1C (0, TEMP_Y2 + 4, TempToDisplay1Degree (Temp1C[TEMP_CHANNEL]));
   DrawTemperature1C (0, TEMP_Y3 + 4, TempToDisplay1Degree (Temp1C[TEMP_RECIRCULATION]));
   // Skrin
#ifdef USE_TWO_SENSORS
   DrawTemperature01C (11, TEMP_BODY_Y1, TempToDisplay01Degree (Temp01C[TEMP_FRONT]), Control.TempFaults[TEMP_FRONT].Fault);
   DrawTemperature01C (11, TEMP_BODY_Y2, TempToDisplay01Degree (Temp01C[TEMP_REAR]), Control.TempFaults[TEMP_REAR].Fault);
#else
   DrawTemperature01C (11, TEMP_BODY_Y1, TempToDisplay01Degree (Temp01C[TEMP_FRONT]), Control.TempFaults[TEMP_FRONT].Fault);
   DrawTemperature01C (11, TEMP_BODY_Y2, TempToDisplay01Degree (Temp01C[TEMP_MIDDLE]), Control.TempFaults[TEMP_MIDDLE].Fault);
   DrawTemperature01C (11, TEMP_BODY_Y3, TempToDisplay01Degree (Temp01C[TEMP_REAR]), Control.TempFaults[TEMP_REAR].Fault);
#endif // USE_TWO_SENSORS
   // Korekce prekreslene mrizky
   DisplayVertDotLine (GRID_X1, 0, TEMP_Y3 + 35, 0x08);
}

//-----------------------------------------------------------------------------
// Prekresleni CO2
//-----------------------------------------------------------------------------

#ifdef USE_CO2

#define CO2_X   (GRID_X1 + 4)
#define CO2_DX  8
#define CO2_DY  22

static void RedrawCo2 () {
   // Prekresli CO2
   word value;
   DisplaySetFont (FONT_NUM_TAHOMA18);
   DisplayMoveTo (CO2_X, CO2_Y);
   value = Co2.Value == 100 ? 9999 : (word)Co2.Value * CO2_LSB;  // Omezim na 9999ppm kvuli zobrazeni
   if (Co2.Failure) {
      cputs ("----");
   }
   else {
      cprintdec (value, FMT_UNSIGNED | 4);
   }

   // Prekroceni maximalni koncentrace CO2
   DisplaySetPlane (DISPLAY_PLANE2);             // blikajici rovina
   DisplayMoveTo (CO2_X, CO2_Y - 2);
   if (Co2.OverLimit || Co2.Failure) {
      DisplayBox (CO2_DX, CO2_DY);           // Vykreslim alarm
   }
   else {
      DisplayClearBox (CO2_DX, CO2_DY);      // Smazu alarm
   }
   DisplaySetPlane (DISPLAY_PLANE1);             // Zpet
}

#else
#define RedrawCo2();
#endif // USE_CO2

//-----------------------------------------------------------------------------
// Prekresleni vlhkosti
//-----------------------------------------------------------------------------

#ifdef USE_HUMIDITY

#define HUMIDITY_X   (GRID_X1 + 6)

static void RedrawHumidity () {
   // Prekresli vlhkost
   byte value;
   DisplaySetFont (FONT_NUM_TAHOMA18);
   DisplayMoveTo (HUMIDITY_X, HUMIDITY_Y);
   value = Humidity.Value == 100 ? 99 : Humidity.Value;  // Omezim na 99% kvuli zobrazeni
   cprintdec (value, FMT_UNSIGNED | 2);
}

#else
#define RedrawHumidity();
#endif // USE_HUMIDITY

//-----------------------------------------------------------------------------
// Prekresleni tlaku filtru a radiatoru
//-----------------------------------------------------------------------------

#define AIR_PRESSURE_X  2
#define AIR_PRESSURE_DX 6
#define AIR_PRESSURE_DY 22

static void RedrawAirPressure () {
   // Prekresli tlak filtru a ventilatoru
   DisplaySetFont (FONT_NUM_TAHOMA18);
#ifdef USE_FILTER_PRESSURE
   DisplayMoveTo (AIR_PRESSURE_X, AIR_PRESSURE_Y2);
   cprintdec ((word)FiltersAirPressure * AIR_PRESSURE_LSB, FMT_UNSIGNED | 3);

   // Prekroceni maximalniho tlaku filtru
   DisplaySetPlane (DISPLAY_PLANE2);             // blikajici rovina
   DisplayMoveTo (AIR_PRESSURE_X, AIR_PRESSURE_Y2 - 2);
   if (FiltersHighAirPressure) {
      DisplayBox (AIR_PRESSURE_DX, AIR_PRESSURE_DY);           // Vykreslim alarm
   }
   else {
      DisplayClearBox (AIR_PRESSURE_DX, AIR_PRESSURE_DY);      // Smazu alarm
   }
   DisplaySetPlane (DISPLAY_PLANE1);             // Zpet

#endif
}

//-----------------------------------------------------------------------------
// Prekresleni klapek
//-----------------------------------------------------------------------------

#define LOCK_FAIR_X  3
#define LOCK_FAIR_Y  (FLAPS_Y2 + 3)

#define LOCK_FLOOR_X 15

static void DrawFlap (byte X, byte Y, byte Value, TServoStatus Status) {
   // Vykresli klapku s hodnotou vcetne podlahy nalevo i napravo od klapky, X a Y jsou souradnice symbolu klapky
   DisplayHorLineValue (X - 1, Y + 7, 1, 0xA8); DisplayHorLineValue (X - 1, Y + 8, 1, 0x54);
   if (Status == SERVO_STATUS_OFF) {
      // Servo je v rucnim rezimu, zobrazim krizek (Value muzu zmenit, pouziva se sice nize, ale ne kdyz je Status = SERVO_STATUS_OFF
      Value = KLAPKA_VYP;
   }
   DisplaySymbol (X, Y, SYMBOL_KLAPKA[Value]);
   DisplayHorLineValue (X + 2, Y + 7, 1, 0x2A); DisplayHorLineValue (X + 2, Y + 8, 1, 0x15);

   /*  DisplaySetFont(FONT_SYSTEM);
     DisplayGotoRC(Y / 8 - 1, X);
     putchar(0x30 + Status);*/

     // Chyba serva - zobrazim do blikajici roviny
   DisplaySetPlane (DISPLAY_PLANE2);             // blikajici rovina
   if (Status == SERVO_STATUS_TIMEOUT || Status == SERVO_STATUS_CONTROL) {
      // Chyba serva, vykreslim do blikajici roviny
      DisplaySymbol (X, Y, SYMBOL_KLAPKA[Value]);
   }
   else {
      // Stav serva je bez chyby, smazu z blikajici roviny
      DisplayMoveTo (X, Y);
      DisplayClearBox (SYMBOL_KLAPKA[0][0], SYMBOL_KLAPKA[0][1]);
   }
   DisplaySetPlane (DISPLAY_PLANE1);
}

#ifdef USE_FLOOR_FLAPS

static void DrawFloorFlap (byte y) {
   DrawFlap (18, y, FreshAir.FloorFlap.Position, ServoStatus[SERVO_FLOOR]);
   if (FreshAir.FloorFlap.Mode == FLAP_MANUAL) {
      // Klapka se ovlada rucne, zobrazim zamek
      DisplaySymbol (LOCK_FLOOR_X, y + 2, SYMBOL_RUKA_MALA);
   }
}

#endif // USE_FLOOR_FLAPS

static void RedrawFlaps () {
   // Prekresli vsechny vzduchove klapky. Rucni ovladni se nemusi mazat, vzdy pri zmenese stejne maze cely displej.
   // Napor
   DrawFlap (3, FLAPS_Y1, FreshAir.Position, ServoStatus[SERVO_INDUCTION1]);   // Horni
   DrawFlap (7, FLAPS_Y1, FreshAir.Position, ServoStatus[SERVO_INDUCTION2]);   // Spodni
   // Recirkulace
   DrawFlap (5, FLAPS_Y2, KLAPKA_VYP - 1 - FreshAir.Position, ServoStatus[SERVO_RECIRCULATION]);   // Recirkulace je invertovane
   if (FreshAir.Mode == FAIR_MANUAL) {
      // Cerstvy vzduch je rucne, vykreslim zamek
      DisplaySymbol (LOCK_FAIR_X - 1, LOCK_FAIR_Y - 1, SYMBOL_RUKA_MALA);
   }

#ifdef USE_FLOOR_FLAPS
   // Predni a zadni podlaha
   DrawFloorFlap (FLOOR_FLAPS_Y1);
#ifndef USE_TWO_SENSORS
   DrawFloorFlap (FLOOR_FLAPS_Y2);
#endif // USE_TWO_SENSORS
#endif // USE_FLOOR_FLAPS
}

//-----------------------------------------------------------------------------
// Prekresleni data a casu
//-----------------------------------------------------------------------------

#define TIME_X          (GRID_X2 + 2)
#define TIME_Y          0

static void RedrawDateTime () {
   // Prekresli datum a cas
   // Datum
   DisplaySetFont (FONT_SYSTEM);
   DisplayMoveTo (TIME_X, TIME_Y + 4);
   cbyte (bbin2bcd (Time.Day)); putchar ('.'); cbyte (bbin2bcd (Time.Month)); putchar ('.'); cword (wbin2bcd (Time.Year)); putchar (' ');
   // Cas
   DisplaySetFont (FONT_NUM_TAHOMA18);
   DisplayMoveTo (TIME_X, TIME_Y + 18);
   cbyte (bbin2bcd (Time.Hour)); putchar (':'); cbyte (bbin2bcd (Time.Min));
}

//-----------------------------------------------------------------------------
// Prekresleni rezimu
//-----------------------------------------------------------------------------

#define MODE_X1         (GRID_X2 + 2)
#define MODE_X2         (MODE_X1 + 3)
#define MODE_X3         (MODE_X1 + 7)
#define MODE_Y          43

#define MODE_SCALE_STEPS 10

static void DrawSmallScale (byte x, byte y, byte Percent) {
   // Vykresli stupnici u rezimu s aktualni hodnotou Percent
   if (Percent > MODE_SCALE_STEPS / 2) {
      Percent += MODE_SCALE_STEPS / 2;   // Zaokrouhlim, 4% tak ukaze 0, 5% ukaze 1
   }
   Percent /= MODE_SCALE_STEPS;   // Z procent prevedu na index
   if (Percent > MODE_SCALE_STEPS) {
      Percent = MODE_SCALE_STEPS;  // Radeji omezim
   }
   DisplaySymbol (x, y, SYMBOL_STUPNICE[Percent]);
}

#ifndef USE_ON_OFF_HEATING

static void DrawModeScale (byte Percent) {
   DrawSmallScale (MODE_X3, MODE_Y + 17, Percent);
}

static void DrawModeHeatingFlame () {
   // Pokud je zapaleny plamen topeni 1 nebo 2, vykresli plamen
   if (Heating.Flame1 && Heating.Flame2) {
      DisplaySymbol (MODE_X2 + 2, MODE_Y + 20, SYMBOL_MODE_TOPENI_ON_2X); // Oba plameny topeni
   }
   else if (Heating.Flame1 || Heating.Flame2) {
      DisplaySymbol (MODE_X2 + 2, MODE_Y + 20, SYMBOL_MODE_TOPENI_ON);    // Jeden plamen topeni
   }
}

#endif // USE_ON_OFF_HEATING

static void RedrawMode () {
   // Prekresli rezim
   // Smazu predchozi obsah, jednotlive rezimy se bohuzel nekryji
   DisplayMoveTo (MODE_X1, MODE_Y + 17);
   DisplayClearBox (DISPLAY_WIDTH / 8 - MODE_X1, 16);
   // Vykreslim novy rezim
   switch (Control.Mode) {
   case MODE_OFF: {
      DisplaySymbol (MODE_X1, MODE_Y + 19, SYMBOL_MODE_STOP);
      break;
   }
   case MODE_AUTO: {
      DisplaySymbol (MODE_X1, MODE_Y + 15, SYMBOL_MODE_AUTO);
      if (Control.AutoMode == AUTO_MODE_HEATING) {
#ifdef USE_ON_OFF_HEATING
         if (Heating.On) {
            DisplaySymbol (MODE_X2, MODE_Y + 15, SYMBOL_MODE_TOPENI);
         }
         else {
            DisplaySymbol (MODE_X2, MODE_Y + 15, SYMBOL_MODE_VENTILACE);
         }
#else
         if (Heating.HeatingCircuitState == HEATING_CIRCUIT_OFF) {
            DisplaySymbol (MODE_X2, MODE_Y + 15, SYMBOL_MODE_VENTILACE);
         }
         else {
            DisplaySymbol (MODE_X2, MODE_Y + 15, SYMBOL_MODE_TOPENI);
            DrawModeHeatingFlame ();
            DrawModeScale (Heating.Percent);
         }
#endif // USE_ON_OFF_HEATING
      }
      else {
         if (Cooling.On) {
            DisplaySymbol (MODE_X2, MODE_Y + 15, SYMBOL_MODE_CHLAZENI);
         }
         else {
            DisplaySymbol (MODE_X2, MODE_Y + 15, SYMBOL_MODE_VENTILACE);
         }
      }
      break;
   }
   case MODE_VENTILATION: {
      DisplaySymbol (MODE_X1, MODE_Y + 15, SYMBOL_MODE_MANUAL);
      DisplaySymbol (MODE_X2, MODE_Y + 15, SYMBOL_MODE_VENTILACE);
      break;
   }
   }//switch
}

//-----------------------------------------------------------------------------
// Prekresleni akumulatoru
//-----------------------------------------------------------------------------

#define ACCU_Y          133
#define ACCU_ALARM_DX   4
#define ACCU_ALARM_DY   20
#define FAN_POWER_X     35
#define FAN_POWER_Y     143
#define FAN_POWER_FAN_DY        5

static void RedrawAccu () {
   // Prekresli napeti akumulatoru a vykon ventilatoru
   // Napeti
   DisplaySetFont (FONT_NUM_TAHOMA18);
   DisplayMoveTo (28, ACCU_Y + 3);
   cbyte (bbin2bcd (Accu.Voltage));
   // Nizke napeti akumulatoru
   DisplaySetPlane (DISPLAY_PLANE2);             // blikajici rovina
   DisplayMoveTo (28, ACCU_Y + 2);
   if (Accu.LowVoltage) {
      DisplayBox (ACCU_ALARM_DX, ACCU_ALARM_DY);           // Vykreslim alarm
   }
   else {
      DisplayClearBox (ACCU_ALARM_DX, ACCU_ALARM_DY);      // Smazu alarm
   }
   DisplaySetPlane (DISPLAY_PLANE1);             // Zpet

   // Vykon ventilatoru (jen pokud bezi)
   // Smazu celou oblast, dost se tam toho vykresluje na ruznych pozicich
   DisplayMoveTo (FAN_POWER_X, FAN_POWER_Y - FAN_POWER_FAN_DY);
   DisplayClearBox (SYMBOL_STUPNICE[0][0], SYMBOL_STUPNICE[0][1] + FAN_POWER_FAN_DY);
   if (Fan.On) {
      if (Fan.AutoMode) {
         // Zobrazuju vykon ventilace
         DrawSmallScale (FAN_POWER_X, FAN_POWER_Y, FanCalcPowerForDisplay (Fan.Power));
         DisplaySymbol (FAN_POWER_X, FAN_POWER_Y - FAN_POWER_FAN_DY, SYMBOL_FAN_POWER);
      }
      else {
         // Rucne prepnuto v rozvadeci
         DisplaySymbol (FAN_POWER_X, FAN_POWER_Y - 4, SYMBOL_RUKA_MALA);
         if (Fan.Power == 100) {
            // Rucne prepnuto na 100% vykonu
            DisplaySymbol (FAN_POWER_X + 2, FAN_POWER_Y - 1, SYMBOL_100_PROCENT);
         }
         else {
            // Rucne prepnuto na 50% vykonu
            DisplaySymbol (FAN_POWER_X + 2, FAN_POWER_Y - 1, SYMBOL_50_PROCENT);
         }
      }
   }
}

//-----------------------------------------------------------------------------
// Prekresleni dieselu
//-----------------------------------------------------------------------------

static void DrawSymbol16x16 (byte X, byte Y, byte code *Symbol, TYesNo On) {
   if (On) {
      DisplaySymbol (X, Y, Symbol);
   }
   else {
      DisplayMoveTo (X, Y);
      DisplayClearBox (Symbol[0], Symbol[1]);
   }
}

#define ENGINES_Y        156
#define ELMOT_Y          ENGINES_Y
#define ELMOT_RUNNING_X  28

static void RedrawElmot(){
// Beh elektromotoru
   DrawSymbol16x16 (ELMOT_RUNNING_X, ELMOT_Y + 3, SYMBOL_ELEKTROMOTOR, ElMotorOn);
}

#ifndef NO_DIESEL

#define DIESEL_Y          ENGINES_Y
#define DIESEL_RUNNING_X  28
#define DIESEL_TEPLOTA_X  31
#define DIESEL_OLEJ_X     (DIESEL_TEPLOTA_X + 7)

void DrawDieselTemp () {
   DisplaySymbol (DIESEL_TEPLOTA_X, DIESEL_Y + 7, SYMBOL_TEPLOTA_DIESELU_STUPNICE_KOSTRA);
   DisplaySymbol (DIESEL_TEPLOTA_X, DIESEL_Y + 9, SYMBOL_TEPLOTA_DIESELU_STUPNICE[Diesel.Temperature]);
}

static void RedrawDiesel () {
   // Prekresli stav dieselu
   // Chod dieselu nebo elektromotoru
   if (Diesel.On) {
      DisplaySymbol (DIESEL_RUNNING_X, DIESEL_Y + 3, SYMBOL_DIESEL);

      // Teplota
      if (Diesel.Temperature >= DSL_TEMP_STEPS) {
         // Radeji omezim i zde, kvuli prijimaci, kde se nevola fce pro logovani
         Diesel.Temperature = DSL_TEMP_STEPS - 1;
      }
      DrawDieselTemp ();
   }
   else {
      // Diesel bud nejede, nebo se nepouziva
      // Beh elektromotoru
      RedrawElmot();
      // Umazu zbyvajici cast pouzivanou jen pro diesel
      DisplayMoveTo (DIESEL_TEPLOTA_X, DIESEL_Y + 3);
      DisplayClearBox (DISPLAY_WIDTH / 8 - DIESEL_TEPLOTA_X - 1, SYMBOL_DIESEL[1]);
      // Musim smazat i blikajici rovinu (prehrati a vymena oleje)
      DisplaySetPlane (DISPLAY_PLANE2);             // blikajici rovina
      DisplayMoveTo (DIESEL_TEPLOTA_X, DIESEL_Y + 3);
      DisplayClearBox (DISPLAY_WIDTH / 8 - DIESEL_TEPLOTA_X, SYMBOL_DIESEL[1]);
      DisplaySetPlane (DISPLAY_PLANE1);             // Zpet
   }

   // Poruchy do blikajici roviny
   // Prehraty diesel
   DisplaySetPlane (DISPLAY_PLANE2);             // blikajici rovina
   if (Diesel.Overheated) {
      DrawDieselTemp ();
   }
   else {
      DisplayMoveTo (DIESEL_TEPLOTA_X, DIESEL_Y + 7);
      DisplayClearBox (SYMBOL_TEPLOTA_DIESELU_STUPNICE_KOSTRA[0], SYMBOL_TEPLOTA_DIESELU_STUPNICE_KOSTRA[1]);      // Smazu alarm
   }
   // Vymena oleje - u TMC mazat nemusim, po vymene oleje se smaze cely displej. U TMCR mazat musim!
   if (Diesel.ChangeOil) {
      DisplaySymbol (DIESEL_OLEJ_X, DIESEL_Y + 3, SYMBOL_OLEJ);
#ifdef __TMCREMOTE__
   }
   else {
      // U TMCR smazu
      DisplayMoveTo (DIESEL_OLEJ_X, DIESEL_Y + 3);
      DisplayClearBox (SYMBOL_OLEJ[0], SYMBOL_OLEJ[1]);
#endif // __TMCREMOTE__
   }

   DisplaySetPlane (DISPLAY_PLANE1);             // Zpet
}
#else 
#define RedrawDiesel() RedrawElmot()
#endif
//-----------------------------------------------------------------------------
// Prekresleni agregatu
//-----------------------------------------------------------------------------

#define AGG_REPRO_X        38

static void RedrawAggregates () {
   // Ztisene repro
   DrawSymbol16x16 (AGG_REPRO_X, FAULTS_Y + 3, SYMBOL_REPRO_KRIZEK, Alarm.Quiet);
}

//-----------------------------------------------------------------------------
// Prekresleni poruch
//-----------------------------------------------------------------------------

#define FAULTS_FAN_X            28
#define FAULTS_CHARGING_X       30
#define FAULTS_COOLING_X        32
#define FAULTS_HEATING_X        32
#define FAULTS_HEATING_SERVO_X  34
#define FAULTS_DIESEL_X         36

static void RedrawFaults () {
   // Prekresli poruchy do blikajici roviny
   DisplaySetPlane (DISPLAY_PLANE2);             // blikajici rovina
   DrawSymbol16x16 (FAULTS_CHARGING_X, FAULTS_Y + 2, SYMBOL_DOBIJENI, Charging.Failure);                          // Porucha dobijeni
   DrawSymbol16x16 (FAULTS_FAN_X, FAULTS_Y + 2, SYMBOL_MODE_VENTILACE, Fan.Failure);                              // Porucha ventilatoru
   if (Cooling.Failure) {
      DrawSymbol16x16 (FAULTS_COOLING_X, FAULTS_Y + 4, SYMBOL_MODE_CHLAZENI, YES);                                 // Porucha chlazeni
   }
   else if (Heating.Failure) {
      DrawSymbol16x16 (FAULTS_HEATING_X, FAULTS_Y + 2, SYMBOL_MODE_TOPENI, YES);                                   // Porucha topeni Webasto
   }
   else {
      // Ani porucha chlazeni, ani topeni
      DisplayMoveTo (FAULTS_COOLING_X, FAULTS_Y + 2);
      DisplayClearBox (SYMBOL_MODE_CHLAZENI[0], 18);       // 18, aby to smazalo obe poruchy chlazeni i topeni
   }
   DrawSymbol16x16 (FAULTS_HEATING_SERVO_X, FAULTS_Y + 2, SYMBOL_PORUCHA_SERVA_TOPENI, Heating.ServoFailure);     // Porucha serva topeni
#ifndef NO_DIESEL
   DrawSymbol16x16 (FAULTS_DIESEL_X, FAULTS_Y + 2, SYMBOL_DIESEL, Diesel.EcuError);                  // Porucha motoru  
#endif
   DisplaySetPlane (DISPLAY_PLANE1);             // Zpet
}

//-----------------------------------------------------------------------------
// Prekresleni mrizky
//-----------------------------------------------------------------------------

#define TARGET_TEMPERATURE_Y  88        // Cilova teplota a lozeni
#define MENU_X                (GRID_X2 + 2)

static void RightHorLine1 (byte Y) {
   DisplayHorLineValue (GRID_X2, Y, 1, 0x05);
   DisplayHorDotLine (GRID_X2 + 1, Y, DISPLAY_WIDTH / 8 - GRID_X2 - 1);
}

static void RightHorLine2 (byte Y) {
   DisplayHorLineValue (GRID_X2, Y, 1, 0x0A);
   DisplayHorLineValue (GRID_X2 + 1, Y, DISPLAY_WIDTH / 8 - GRID_X2 - 1, 0xAA);
}

static void RedrawMenuSymbol () {
   // Prekresli symbol menu
   DisplayButtonFrame (MENU_X, MENU_Y + 6, DISPLAY_WIDTH / 8 - MENU_X - 1, 34, NO);
   DisplayStringCenterTahoma18 (MENU_X * 8 + (DISPLAY_WIDTH / 8 - MENU_X - 1) * 8 / 2, MENU_Y + 8, STR_MENU);
}

static void RedrawGrid () {
   // Prekresli mrizku
   DisplayClear ();
   // Rozdeleni vertikalnimi carami
   DisplayVertDotLine (GRID_X1, 0, DISPLAY_HEIGHT - 1, 0x08);
   DisplayVertDotLine (GRID_X2, 0, DISPLAY_HEIGHT - 1, 0x08);
   if (!Control.EmergencyMode) {
      // Horizontalni cary vpravo
      RightHorLine1 (MODE_Y);
      RightHorLine2 (TARGET_TEMPERATURE_Y);
      RightHorLine1 (ACCU_Y);
      RightHorLine2 (ENGINES_Y);
#if (SENSORS_NUMBER_OF_ROWS == 0)
      RightHorLine2 (FAULTS_Y);
      RightHorLine2 (MENU_Y);
#elif (SENSORS_NUMBER_OF_ROWS == 1)
      RightHorLine2 (FAULTS_Y);
      DisplayHorLineValue (0, MENU_Y, DISPLAY_WIDTH / 8, 0xAA);
#else
      DisplayHorLineValue (0, FAULTS_Y, DISPLAY_WIDTH / 8, 0xAA);
      RightHorLine2 (MENU_Y);
#endif
      // Cilova teplota a lozeni
      DisplaySetFont (FONT_NUM_TAHOMA18);
      DrawTemperature1C (GRID_X2 + 1, TARGET_TEMPERATURE_Y + 14, Config.TargetTemperature);
      DisplaySymbol (GRID_X2 + 7, TARGET_TEMPERATURE_Y + 25, Config.TempUnits == UNITS_CELSIUS ? SYMBOL_STUPNE_CELSIA : SYMBOL_STUPNE_FAHRENHEITA);
      if (Config.Localization >= LOC_CELE && Config.Localization <= _LOC_LAST) {
         DisplaySymbol (DISPLAY_WIDTH / 8 - 2, TARGET_TEMPERATURE_Y + 15, SYMBOL_LOZENI_MINI[Config.Localization]);
      }
      // Symbol V u akumulatoru
      DisplaySetFont (FONT_SYSTEM);
      DisplayMoveTo (32, ACCU_Y + 14);
      putchar ('V');
      // Symboly CO2
#ifdef USE_CO2
      DisplaySymbol (CO2_X + 8, CO2_Y + 1, SYMBOL_CO2);
#endif
      // Symboly vlhkosti
#ifdef USE_HUMIDITY
      DisplaySymbol (HUMIDITY_X + 4, HUMIDITY_Y + 1, SYMBOL_VLHKOST);
#endif
      // Symboly tlaku filtru
#ifdef USE_FILTER_PRESSURE
      DisplaySymbol (AIR_PRESSURE_X + 6, AIR_PRESSURE_Y2 + 1, SYMBOL_TLAK_FILTRY);
#endif
   }//if
   // Symbol menu
   RedrawMenuSymbol ();
}

//-----------------------------------------------------------------------------
// Prekresleni dezinfekce skrine
//-----------------------------------------------------------------------------

static void DrawDisinfectionStarted () {
   if (Redraw != REDRAW_ALL) {
      return;             // Prekreslil jsem v minulem kroce
   }
   DisplayClear ();
   DisplayPlane (NO);
#ifdef LANG_GE
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, 65, STR_DEZINFEKCE_ZAHAJENA1);
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, 95, STR_DEZINFEKCE_ZAHAJENA2);
#else
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, 85, STR_DEZINFEKCE_ZAHAJENA);
#endif
   DisplayPlane (YES);
}

static void DrawDisinfectionError () {
   if (Redraw != REDRAW_ALL) {
      return;             // Prekreslil jsem v minulem kroce
   }
   BeepAlarm ();
   DisplayClear ();
   DisplayPlane (NO);
#ifdef LANG_GE
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, 65, STR_DEZINFEKCE_CHYBA1);
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, 95, STR_DEZINFEKCE_CHYBA2);
#else
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, 85, STR_DEZINFEKCE_CHYBA);
#endif
   DisplayPlane (YES);
}

//-----------------------------------------------------------------------------
// Prekresleni displeje
//-----------------------------------------------------------------------------

void MenuRedraw () {
   // Prekresleni displeje
#ifdef __TMCREMOTE__
static byte PrevCommunicationError = NO;
   if(PrevCommunicationError != CommunicationError){
      Redraw = REDRAW_ALL;
      PrevCommunicationError = CommunicationError;
   }
   if (CommunicationError) {
      if(Redraw == REDRAW_ALL){        
         // neni spojeni, vypis chybovou zpravu
         DisplayClear (); // Mazu cely displej
         // Nejdriv smazu blikajici rovinu, kdyby doted blikala nejaka teplota
         DisplaySetPlane (DISPLAY_PLANE2);             // blikajici rovina
         DisplayMoveTo (0, 0);
         DisplayFillBox (DISPLAY_WIDTH / 8, DISPLAY_HEIGHT, 0);
         DisplaySetPlane (DISPLAY_PLANE1);             // Zpet
         // Smazu vse krome tlacitka menu - to tam zustane a navic nechci, aby blikalo
         DisplayMoveTo (0, 0);
         DisplayClearBox (MENU_X, DISPLAY_HEIGHT);
         DisplayMoveTo (MENU_X, 0);
         DisplayClearBox (DISPLAY_WIDTH / 8, MENU_Y + 1);
         DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2 - 20, STR_VYPADEK_SPOJENI);
         // Menu
         RedrawMenuSymbol ();
         AlarmSet();         
         Redraw = 0;
      }
      return;
   }
#endif

   if (DisplayMode != OldDisplayMode) {
      Redraw = REDRAW_ALL;
      OldDisplayMode = DisplayMode;
   }

   switch (DisplayMode) {
   case DISPLAY_DISINFECTION_STARTED:
      DrawDisinfectionStarted ();
      Redraw = 0;  // Vse jsem vykreslil
      return;

   case DISPLAY_DISINFECTION_ERROR:
      DrawDisinfectionError ();
      Redraw = 0;  // Vse jsem vykreslil
      return;
   }

   // Standardni zobrazeni teplot DISPLAY_TEMP, pokracuju dal

   if (Redraw & REDRAW_GRID || (Control.OldEmergencyMode != Control.EmergencyMode)) {
      // Pri zmene nouzoveho rezimu taky prekreslim mrizku
      // Pri ukonceni dezinfekce taky
      RedrawGrid ();
   }
   if (Control.EmergencyMode) {
      // V nouzovem rezimu prekresluju jen teploty a cas
      RedrawTemperatures ();
      RedrawDateTime ();
      if (DiagnosticMode) {
         DiagRedraw ();
      }
      Redraw = 0;  // Pokud je ihned pri zapnuti jednotky nouzovy rezim, porad by to prekreslovalo mrizku
      return;
   }
   // Standardni ovladani pres jednotku
   if (Redraw & REDRAW_PERIODIC) {
      RedrawTemperatures ();
      RedrawCo2 ();
      RedrawHumidity ();
      RedrawAirPressure ();
      RedrawDateTime ();
      RedrawFlaps ();
      RedrawMode ();
      RedrawAccu ();
      RedrawDiesel();
      RedrawAggregates ();
      RedrawFaults ();
      if (DiagnosticMode) {
         DiagRedraw ();
      }
   }
   Redraw = 0;  // Vse jsem vykreslil
}

//-----------------------------------------------------------------------------
// Zmena rezimu
//-----------------------------------------------------------------------------

// Poloha hornich tlacitek
#define MODE_TOP_BUTTON_X        1     // Pozice hornich tlacitek
#define MODE_TOP_BUTTON_Y        10    // Pozice hornich tlacitek
#define MODE_TOP_BUTTON_DX       8     // Roztec hornich tlacitek
#define MODE_TOP_BUTTON_WIDTH    6     // Sirka hornich tlacitek
#define MODE_TOP_BUTTON_HEIGHT   48    // Vyska hornich tlacitek

// Pracovni plocha
enum {
   MODE_ID_OFF,
   MODE_ID_AUTO,
   MODE_ID_VENTILATION,
   MODE_ID_DISINFECTION_WHEELS,
   MODE_ID_DISINFECTION_BODY
};
TTouchButton code ModeButtons[] = {
  { // Vypnuti
    MODE_ID_OFF,
    (MODE_TOP_BUTTON_X) * 8,
    MODE_TOP_BUTTON_Y,
    MODE_TOP_BUTTON_WIDTH * 8,
    MODE_TOP_BUTTON_HEIGHT,
    NO
  },
  { // Automaticky mod
    MODE_ID_AUTO,
    (MODE_TOP_BUTTON_X + 1 * MODE_TOP_BUTTON_DX) * 8,
    MODE_TOP_BUTTON_Y,
    MODE_TOP_BUTTON_WIDTH * 8,
    MODE_TOP_BUTTON_HEIGHT,
    NO
  },
  { // Ventilace
    MODE_ID_VENTILATION,
    (MODE_TOP_BUTTON_X + 2 * MODE_TOP_BUTTON_DX) * 8,
    MODE_TOP_BUTTON_Y,
    MODE_TOP_BUTTON_WIDTH * 8,
    MODE_TOP_BUTTON_HEIGHT,
    NO
  },
  { // Dezinfence kol
    MODE_ID_DISINFECTION_WHEELS,
    (MODE_TOP_BUTTON_X + 3 * MODE_TOP_BUTTON_DX) * 8,
    MODE_TOP_BUTTON_Y,
    MODE_TOP_BUTTON_WIDTH * 8,
    MODE_TOP_BUTTON_HEIGHT,
    NO
  },
  { // Dezinfence skrine - variantne je nebo neni zobrazene, proto je az na konci
    MODE_ID_DISINFECTION_BODY,
    (MODE_TOP_BUTTON_X + 4 * MODE_TOP_BUTTON_DX) * 8,
    MODE_TOP_BUTTON_Y,
    MODE_TOP_BUTTON_WIDTH * 8,
    MODE_TOP_BUTTON_HEIGHT,
    NO
  }
};
TTouchWorkplace code ModeWorkplace = {
  ModeButtons,
  ArrayLength(ModeButtons)
};

static void DrawBigButton (byte x, byte y, byte code *symbol, TYesNo thick) {
   // Vykresli velke tlacitko 48x48px se symbolem uprostred
   DisplayButtonFrame (x, y, MODE_TOP_BUTTON_WIDTH, MODE_TOP_BUTTON_HEIGHT, thick);
   DisplaySymbol (x + 1, y + MODE_TOP_BUTTON_HEIGHT / 2 - symbol[1] / 2, symbol);
}

static void DrawModeTopButton (TControlMode Mode, TYesNo thick) {
   // Vykresli horni tlacitko s indexem index a tloustkou thick
   DrawBigButton (MODE_TOP_BUTTON_X + Mode * MODE_TOP_BUTTON_DX, MODE_TOP_BUTTON_Y, SYMBOLY_TLACITKA_REZIM[Mode], thick);
}

static void ModeTopButtons (TControlMode SelectedMode) {
   // Vykresli vsechna horni tlacitka tenkou linkou a tlacitko OK vpravo dole
   byte j;
   for (j = 0; j < ArrayLength(ModeButtons); j++) {
      if ((!Disinfection.IsInstalled || Disinfection.Mode != DISINFECTION_OFF) && j >= MODE_ID_DISINFECTION_WHEELS) {
         continue;         // Pokud neni dezinfekce nainstalovana nebo pokud bezi nektera dezinfekce, tlacitka pro dezinfekci nezobrazuju.
      }
      if (j == MODE_ID_DISINFECTION_BODY && !DisinfectionBodyCanBeStarted ()) {
         continue;         // Dezinfekci skrine vykreslim jen pokud je vypnuta ventilace, tj. nejsou tam kurata
      }
      DrawModeTopButton (j, SelectedMode == j);
   }
}

// Vykresli tlacitko podle rezimu <Mode>, s vyberem tloustky
void ModeTopButton (TControlMode Mode, TYesNo Thick) {
   DrawModeTopButton (Mode, Thick);
}

static void ModeSetNewMode (TControlMode Mode) {
   // Pipne, nastavi novy rezim a prekresli
   BeepKey ();
   ModeTopButton (Control.Mode, NO);      // Stary rezim tence
   ControlSetNewMode (Mode);              // Nastavim rezim
   ModeTopButton (Mode, YES);             // Novy rezim tluste
}

static void ModeStartDisinfection (byte Id) {
   // Pipne, spusti zvolenou dezinfekci a prekresli. Stavajici rezim cinnosti ponecha.
   BeepKey ();

   // Zapnu dezinfekci, kterou chce
   if (Id == MODE_ID_DISINFECTION_WHEELS) {
      // Dezinfekce kol
      ModeTopButton (Control.Mode, NO);      // Stavajici rezim tence
      ModeTopButton (Id, YES);               // Novy rezim tluste
      SysDelay (MENU_FADE);                  // Chvili pockam
      DisinfectionWheelsStart ();
   }
   else {
      // Dezinfekce skrine - tady rezim neprekresluju, protoze se pred tim zobrazil dotaz
      DisinfectionBodyStart ();
   }
}

void MenuMode () {
   // Menu pro zmenu rezimu
   DisplayClear ();
   // Horni tlacitka, zaroven zvyrazni aktualne vybrany rezim
   ModeTopButtons (Control.Mode);
   // Na displeji se dela jakysi kaz, nakreslim ho aspon do rohu displeje
   DisplayMoveTo (0, DISPLAY_HEIGHT - 1);
   DisplayClearBox (1, 1);
   while (1) {
      switch (WaitForKey ()) {
      case K_TIMEOUT: {
         BeepKey ();
         return;
      }
      case K_TOUCH: {
         // Obsluha hornich tlacitek pro vyber rezimu
         switch (TouchCtlCheck (&ModeWorkplace)) {
         case MODE_ID_OFF: {
            ModeSetNewMode (MODE_OFF);
            SysDelay (MENU_FADE);
            return;
         }
         case MODE_ID_AUTO: {
            ModeSetNewMode (MODE_AUTO);
            SysDelay (MENU_FADE);
            return;
         }
         case MODE_ID_VENTILATION: {
            ModeSetNewMode (MODE_VENTILATION);
            SysDelay (MENU_FADE);
            return;
         }
         case MODE_ID_DISINFECTION_WHEELS: {
            if (!DisinfectionWheelsCanBeStarted ()) {
               break;         // Dezinfekci kol povolim jen pokud je dezinfekce vypnuta
            }
            ModeStartDisinfection (MODE_ID_DISINFECTION_WHEELS);
            return;
         }
         case MODE_ID_DISINFECTION_BODY: {
            if (!DisinfectionBodyCanBeStarted ()) {
               break;            // Dezinfekci skrine povolim jen pokud je dezinfekce vypnuta a je vypnuta ventilace, tj. nejsou tam kurata
            }
            BeepKey ();
            if (!AskQuestion (STR_DEZINFEKCE_START1, STR_DEZINFEKCE_START2)) {
               return;           // Nechce zahajit dezinfekci
            }
            ModeStartDisinfection (MODE_ID_DISINFECTION_BODY);
            return;
         }
         }//switch
         break;
      }
      }//switch
   }//while
}

//-----------------------------------------------------------------------------
// Zmena recirkulace
//-----------------------------------------------------------------------------

// Poloha hornich tlacitek
#define FRESHAIR_TOP_BUTTON_X        1     // Pozice hornich tlacitek
#define FRESHAIR_TOP_BUTTON_Y        10    // Pozice hornich tlacitek
#define FRESHAIR_TOP_BUTTON_DX       8     // Roztec hornich tlacitek
#define FRESHAIR_TOP_BUTTON_WIDTH    6     // Sirka hornich tlacitek
#define FRESHAIR_TOP_BUTTON_HEIGHT   48    // Vyska hornich tlacitek

// Pracovni plocha
enum {
   FRESHAIR_ID_AUTO,
   FRESHAIR_ID_MANUAL
};
TTouchButton code FreshAirButtons[] = {
  { // Automaticky mod
    FRESHAIR_ID_AUTO,
    FRESHAIR_TOP_BUTTON_X * 8,
    FRESHAIR_TOP_BUTTON_Y,
    FRESHAIR_TOP_BUTTON_WIDTH * 8,
    FRESHAIR_TOP_BUTTON_HEIGHT,
    NO
  },
  { // Rucne
    FRESHAIR_ID_MANUAL,
    (FRESHAIR_TOP_BUTTON_X + 1 * FRESHAIR_TOP_BUTTON_DX) * 8,
    FRESHAIR_TOP_BUTTON_Y,
    FRESHAIR_TOP_BUTTON_WIDTH * 8,
    FRESHAIR_TOP_BUTTON_HEIGHT,
    NO
  }
};
TTouchWorkplace code FreshAirWorkplace = {
  FreshAirButtons,
  ArrayLength(FreshAirButtons)
};

static void DrawFlapsTopButton (byte Mode, TYesNo thick) {
   // Vykresli horni tlacitko s indexem index a tloustkou thick
   DrawBigButton (FRESHAIR_TOP_BUTTON_X + Mode * FRESHAIR_TOP_BUTTON_DX, FRESHAIR_TOP_BUTTON_Y, Mode == FAIR_AUTO ? SYMBOL_TLACITKO_AUTO : SYMBOL_TLACITKO_RUCNE, thick);
}

static void FlapsTopButtons (byte SelectedMode) {
   DrawFlapsTopButton (FAIR_AUTO, SelectedMode == FAIR_AUTO);
   DrawFlapsTopButton (FAIR_MANUAL, SelectedMode == FAIR_MANUAL);
   // Tlacitko OK
   DrawBigButton (MODE_BUTTON_OK_X, MODE_BUTTON_OK_Y, SYMBOL_TLACITKO_OK, NO);
}

static void FlapsDrawScaleButtons (byte code *LeftSymbol, byte code *RightSymbol, byte Mode, byte Percent) {
   // Tlacitka +/-
   DrawBigButton (MODE_BUTTON_PLUS_X, MODE_BUTTON_PLUS_Y, LeftSymbol, NO);
   DrawBigButton (MODE_BUTTON_MINUS_X, MODE_BUTTON_PLUS_Y, RightSymbol, NO);
   // Stupnice
   if (Mode == FAIR_MANUAL) {
      DrawScale (Percent);
   }
}

static void FreshAirDrawScaleButtons () {
   FlapsDrawScaleButtons (SYMBOL_TLACITKO_NAPOR, SYMBOL_TLACITKO_RECIRKULACE, FreshAir.Mode, FreshAir.Percent);
}

static void FreshAirSetNewMode (TFreshAirMode Mode) {
   // Pipne, nastavi novy rezim a prekresli
   BeepKey ();
   DrawFlapsTopButton (FreshAir.Mode, NO);  // Stary rezim tence
   FAirNewMode (Mode);
   DrawFlapsTopButton (Mode, YES);        // Novy rezim tluste
   // Prekreslim obsah podle zvoleneho rezimu
   if (Mode == FAIR_MANUAL) {
      // Rucni rizeni - zobrazim stupnici
      FreshAirDrawScaleButtons ();
   }
   else {
      // Ostatni - vse smazu
      DisplayMoveTo (0, MODE_TOP_BUTTON_Y + MODE_TOP_BUTTON_HEIGHT);
      DisplayClearBox (DISPLAY_WIDTH / 8, DISPLAY_HEIGHT - (MODE_TOP_BUTTON_Y + MODE_TOP_BUTTON_HEIGHT));
   }
}

void MenuFreshAir () {
   // Menu pro zmenu recirkulace
   DisplayClear ();
   // Horni tlacitka, zaroven zvyrazni aktualne vybrany rezim
   FlapsTopButtons (FreshAir.Mode);
   // Na displeji se dela jakysi kaz, nakreslim ho aspon do rohu displeje
   DisplayMoveTo (0, DISPLAY_HEIGHT - 1);
   DisplayClearBox (1, 1);

   if (FreshAir.Mode == FAIR_MANUAL) {
      FreshAirDrawScaleButtons ();
   }
   while (1) {
      switch (WaitForKey ()) {
      case K_TIMEOUT: {
         BeepKey ();
         return;
      }
      case K_TOUCH: {
         // Obsluha hornich tlacitek pro vyber rezimu
         switch (TouchCtlCheck (&FreshAirWorkplace)) {
         case FRESHAIR_ID_AUTO: {
            FreshAirSetNewMode (FAIR_AUTO);
            SysDelay (MENU_FADE);
            return;
         }
         case FRESHAIR_ID_MANUAL: {
            FreshAirSetNewMode (FAIR_MANUAL);
            break;
         }
         }//switch
         // Obsluha rucniho otevreni klapek
         switch (TouchCtlCheck (&PlusMinusWorkplace)) {
         case ID_SCALE_MINUS: {
            if (FreshAir.Mode != FAIR_MANUAL) {
               break;        // Jen v rucnim rezimu
            }
            BeepKey ();
            FAirManualDecrease ();
            DrawScale (FreshAir.Percent);
            break;
         }
         case ID_SCALE_PLUS: {
            if (FreshAir.Mode != FAIR_MANUAL) {
               break;        // Jen v rucnim rezimu
            }
            BeepKey ();
            FAirManualIncrease ();
            DrawScale (FreshAir.Percent);
            break;
         }
         case ID_SCALE_OK: {
            BeepKey ();
            return;
         }
         }//switch
         break;
      }
      }//switch
   }//while
}

//-----------------------------------------------------------------------------
// Zmena klapek v podlaze
//-----------------------------------------------------------------------------

// Poloha hornich tlacitek
#define FLAPS_TOP_BUTTON_X        1     // Pozice hornich tlacitek
#define FLAPS_TOP_BUTTON_Y        10    // Pozice hornich tlacitek
#define FLAPS_TOP_BUTTON_DX       8     // Roztec hornich tlacitek
#define FLAPS_TOP_BUTTON_WIDTH    6     // Sirka hornich tlacitek
#define FLAPS_TOP_BUTTON_HEIGHT   48    // Vyska hornich tlacitek

// Pracovni plocha
enum {
   FLAPS_ID_AUTO,
   FLAPS_ID_MANUAL
};
TTouchButton code FlapsButtons[] = {
  { // Automaticky mod
    FLAPS_ID_AUTO,
    FLAPS_TOP_BUTTON_X * 8,
    FLAPS_TOP_BUTTON_Y,
    FLAPS_TOP_BUTTON_WIDTH * 8,
    FLAPS_TOP_BUTTON_HEIGHT,
    NO
  },
  { // Rucne
    FLAPS_ID_MANUAL,
    (FLAPS_TOP_BUTTON_X + 1 * FLAPS_TOP_BUTTON_DX) * 8,
    FLAPS_TOP_BUTTON_Y,
    FLAPS_TOP_BUTTON_WIDTH * 8,
    FLAPS_TOP_BUTTON_HEIGHT,
    NO
  }
};

TTouchWorkplace code FlapsWorkplace = {
  FlapsButtons,
  ArrayLength(FlapsButtons)
};

#if defined(USE_FLOOR_FLAPS) && defined(USE_FLOOR_FLAPS_CONTROL)

static void FloorFlapsDrawScaleButtons () {
   FlapsDrawScaleButtons (SYMBOL_TLACITKO_KLAPKA_OTEVRIT, SYMBOL_TLACITKO_KLAPKA_ZAVRIT, FreshAir.FloorFlap.Mode, FreshAir.FloorFlap.Percent);
}

static void FloorFlapsSetNewMode (TFlapMode Mode) {
   // Pipne, nastavi novy rezim a prekresli
   BeepKey ();
   DrawFlapsTopButton (FreshAir.FloorFlap.Mode, NO);  // Stary rezim tence
   FAirNewFloorFlapMode (Mode);
   DrawFlapsTopButton (Mode, YES);        // Novy rezim tluste
   // Prekreslim obsah podle zvoleneho rezimu
   if (Mode == FLAP_MANUAL) {
      // Rucni rizeni - zobrazim stupnici
      FloorFlapsDrawScaleButtons ();
   }
   else {
      // Ostatni - vse smazu
      DisplayMoveTo (0, MODE_TOP_BUTTON_Y + MODE_TOP_BUTTON_HEIGHT);
      DisplayClearBox (DISPLAY_WIDTH / 8, DISPLAY_HEIGHT - (MODE_TOP_BUTTON_Y + MODE_TOP_BUTTON_HEIGHT));
   }
}

void MenuFloorFlaps () {
   // Menu pro zmenu podlahovych klapek, aktualne editovana klapka (predni/zadni) je v promenne Flap
   DisplayClear ();
   // Horni tlacitka, zaroven zvyrazni aktualne vybrany rezim
   FlapsTopButtons (FreshAir.FloorFlap.Mode);
   // Na displeji se dela jakysi kaz, nakreslim ho aspon do rohu displeje
   DisplayMoveTo (0, DISPLAY_HEIGHT - 1);
   DisplayClearBox (1, 1);

   if (FreshAir.FloorFlap.Mode == FLAP_MANUAL) {
      FloorFlapsDrawScaleButtons ();
   }
   while (1) {
      switch (WaitForKey ()) {
      case K_TIMEOUT: {
         BeepKey ();
         return;
      }
      case K_TOUCH: {
         // Obsluha hornich tlacitek pro vyber rezimu
         switch (TouchCtlCheck (&FlapsWorkplace)) {
         case FLAPS_ID_AUTO: {
            FloorFlapsSetNewMode (FLAP_AUTO);
            SysDelay (MENU_FADE);
            return;
         }
         case FLAPS_ID_MANUAL: {
            FloorFlapsSetNewMode (FLAP_MANUAL);
            break;
         }
         }//switch
         // Obsluha vykonu topeni a chlazeni
         switch (TouchCtlCheck (&PlusMinusWorkplace)) {
         case ID_SCALE_MINUS: {
            if (FreshAir.FloorFlap.Mode != FAIR_MANUAL) {
               break;        // Jen v rucnim rezimu
            }
            BeepKey ();
            FAirFloorFlapManualDecrease ();
            DrawScale (FreshAir.FloorFlap.Percent);
            break;
         }
         case ID_SCALE_PLUS: {
            if (FreshAir.FloorFlap.Mode != FAIR_MANUAL) {
               break;        // Jen v rucnim rezimu
            }
            BeepKey ();
            FAirFloorFlapManualIncrease ();
            DrawScale (FreshAir.FloorFlap.Percent);
            break;
         }
         case ID_SCALE_OK: {
            BeepKey ();
            return;
         }
         }//switch
         break;
      }
      }//switch
   }//while
}

#endif // defined(USE_FLOOR_FLAPS) && defined(USE_FLOOR_FLAPS_CONTROL)

//-----------------------------------------------------------------------------
// Neplatna hodnota
//-----------------------------------------------------------------------------

static TYesNo MenuInvalidValue () {
   // Zobrazi menu neplatna hodnota a vrati 1, pokud se chce opravit
   DisplayClear ();
   DisplayPlane (NO);
#if defined LANG_PL
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, 65, STR_CHYBA_NEPLATNA_HODNOTA_1);
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, 95, STR_CHYBA_NEPLATNA_HODNOTA_2);
#else
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, 85, STR_CHYBA_NEPLATNA_HODNOTA);
#endif
   DisplayRetryCancel ();
   DisplayPlane (YES);
   return (DisplayExecuteButtons (2));
}

//-----------------------------------------------------------------------------
// Heslo
//-----------------------------------------------------------------------------

#ifndef __TMCREMOTE__

TYesNo MenuPassword (TYesNo Special) {
   // Special = YES => zadava se jine heslo nez normalne
   while (1) {
      DrawOneLineNumPad (STR_HESLO, NO);
      UN.DT = 0;
      if (!DisplayExecuteNumPad (6, 90, 6, 0)) {
         return NO;
      }
      // Zmacknul Enter
      if ((Special && UN.DT == MANUFACTURE_PASSWORD_1) || (!Special && UN.DT == MANUFACTURE_PASSWORD_2)) {
         return YES;
      }
      else {
         // Zadana perioda je mimo rozsah
         if (!MenuInvalidValue ()) {
            return NO;  // Nechce zadat znovu
         }
         // Pokud stisknul Enter, pokracuju v cyklu, aby zadal hodnotu znovu
      }//else
   }//while
}

#endif // __TMCREMOTE__

//-----------------------------------------------------------------------------
// Volba cilove teploty
//-----------------------------------------------------------------------------

void MenuNewTargetTemperature (char Temp) {
   // Nastavi novou cilovou teplotu <Temp> v celych stupnich se znamenkem
   Config.TargetTemperature = Temp;
#ifdef __TMCREMOTE__
   TmcrCfgSave (CFG_TARGET_TEMPERATURE);
#else
   CfgSave ();
   LogWriteProtocol (PRT_NASTAVENI_TEPLOTY, Temp); // zaprotokolujeme
   ClearFaultsInBody ();  // Po zmene cilove teploty merim najeti teploty ve skrini do pasma cilove teploty od pocatku
#endif // __TMCREMOTE__
}

void MenuTargetTemperature () {
   while (1) {
#if defined LANG_PL
      DrawTwoLinesNumPad (STR_ZADEJTE_CILOVOU_TEPLOTU_1, STR_ZADEJTE_CILOVOU_TEPLOTU_2, YES);
#else
      DrawOneLineNumPad (STR_ZADEJTE_CILOVOU_TEPLOTU, YES);
#endif
      DisplayStringTahoma18 (116, 90, Config.TempUnits == UNITS_CELSIUS ? STR_STUPNE_CELSIA : STR_STUPNE_FAHRENHEITA);
      UN.DT = Config.TargetTemperature;
      if (!DisplayExecuteNumPad (9, 90, 2, 1)) {
         return;
      }
      // Zmacknul Enter
      if (TempIsTargetOk (UN.DS)) {
         // Zadana teplota je v poradku, ulozim ji
         MenuNewTargetTemperature (UN.DS);
         return;
      }
      else {
         // Zadana teplota je mimo rozsah
         if (!MenuInvalidValue ()) {
            return;  // Nechce zadat znovu
         }
         // Pokud stisknul Enter, pokracuju v cyklu, aby zadal teplotu znovu
      }//else
   }//while
}

//-----------------------------------------------------------------------------
// Volba lozeni
//-----------------------------------------------------------------------------

#define LOZENI_X (3*8)
#define LOZENI_Y 90
#define LOZENI_DX (6*8)
#define LOZENI_DY 58

static void KostraLozeni (byte X) {
   // Vykresli kostru lozeni
   DisplaySymbol (X + 1, LOZENI_Y - 15, SYMBOL_LOZENI_SIPKA);
   DisplayVertLine (X, LOZENI_Y + 1, 56, 0x80);
   DisplayVertLine (X + 3, LOZENI_Y + 1, 56, 0x01);
   DisplaySymbol (X, LOZENI_Y, SYMBOL_LOZENI_LINKA);
   DisplaySymbol (X, LOZENI_Y + 57, SYMBOL_LOZENI_LINKA);
   DisplayHorLine (X, LOZENI_Y + 19, 4);
   DisplayHorLine (X, LOZENI_Y + 38, 4);
}

void MenuNewLocalization (byte Localization) {
   // Nastavi nove lozeni <Localization>
   Config.Localization = Localization;
#ifdef __TMCREMOTE__
   TmcrCfgSave (CFG_LOCALIZATION);
#else
   CfgSave ();
   LogWriteProtocol (PRT_NASTAVENI_LOZENI, Localization);
#endif // __TMCREMOTE__
}

#ifdef USE_TWO_SENSORS
// Pouzivaji se jen 2 teplotni cidla (dodavka)
#define LOCALIZATION_COUNT 3          // Pocet kombinaci cidel
#else
// Pouzivaji se 3 teplotni cidla
#define LOCALIZATION_COUNT 6          // Pocet kombinaci cidel
#endif // USE_TWO_SENSORS


void MenuLocalization () {
   byte j;

   DisplayClear ();
   // Vykeslim polozky menu
   DisplayPlane (NO);
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, NUMPAD_1ROW_Y, STR_LOZENI_PREPRAVEK);
   for (j = 0; j < LOCALIZATION_COUNT; j++) {
      KostraLozeni (LOZENI_X / 8 + j * (LOZENI_DX / 8));
   }
   // Cele
   DisplaySymbol (LOZENI_X / 8, LOZENI_Y + 1, SYMBOL_LOZENI_PLNE);
   DisplaySymbol (LOZENI_X / 8, LOZENI_Y + 20, SYMBOL_LOZENI_PLNE);
   DisplaySymbol (LOZENI_X / 8, LOZENI_Y + 39, SYMBOL_LOZENI_PLNE);
#ifdef USE_TWO_SENSORS
   // Predni tretina
   DisplaySymbol (LOZENI_X / 8 + 1 * LOZENI_DX / 8, LOZENI_Y + 1, SYMBOL_LOZENI_PLNE);
   // Zadni tretina
   DisplaySymbol (LOZENI_X / 8 + 2 * LOZENI_DX / 8, LOZENI_Y + 39, SYMBOL_LOZENI_PLNE);
#else
   // Predni polovina
   DisplaySymbol (LOZENI_X / 8 + 1 * LOZENI_DX / 8, LOZENI_Y + 1, SYMBOL_LOZENI_PLNE);
   DisplaySymbol (LOZENI_X / 8 + 1 * LOZENI_DX / 8, LOZENI_Y + 20, SYMBOL_LOZENI_PLNE);
   // Predni tretina
   DisplaySymbol (LOZENI_X / 8 + 2 * LOZENI_DX / 8, LOZENI_Y + 1, SYMBOL_LOZENI_PLNE);
   // Zadni polovina
   DisplaySymbol (LOZENI_X / 8 + 3 * LOZENI_DX / 8, LOZENI_Y + 20, SYMBOL_LOZENI_PLNE);
   DisplaySymbol (LOZENI_X / 8 + 3 * LOZENI_DX / 8, LOZENI_Y + 39, SYMBOL_LOZENI_PLNE);
   // Zadni tretina
   DisplaySymbol (LOZENI_X / 8 + 4 * LOZENI_DX / 8, LOZENI_Y + 39, SYMBOL_LOZENI_PLNE);
   // Stredni tretina
   DisplaySymbol (LOZENI_X / 8 + 5 * LOZENI_DX / 8, LOZENI_Y + 20, SYMBOL_LOZENI_PLNE);
#endif // USE_TWO_SENSORS

   DisplayStringTahoma18 (MENU_POLOZKA_X, CANCEL_Y, STR_ZRUSIT);
   DisplayPlane (YES);

   while (1) {
      switch (WaitForKey ()) {
      case K_TIMEOUT: {
         BeepKey ();
         return;
      }
      case K_TOUCH:
         if (Touch.Repeat) break;   // Nechci autorepeat
         if (Touch.Y >= LOZENI_Y - 15 && Touch.Y <= LOZENI_Y + LOZENI_DY) {
            for (j = 0; j < LOCALIZATION_COUNT; j++) {
               if (Touch.X >= LOZENI_X + (word)j * LOZENI_DX && Touch.X <= LOZENI_X + (word)(j + 1) * LOZENI_DX) {
                  BeepKey ();
#ifdef USE_TWO_SENSORS
                  switch (j) {
                  case 1:
                     MenuNewLocalization (LOC_PREDNI_TRETINA);
                     break;
                  case 2:
                     MenuNewLocalization (LOC_ZADNI_TRETINA);
                     break;
                  default:
                     MenuNewLocalization (LOC_CELE);
                     break;
                  }
#else
                  MenuNewLocalization (j);
#endif // USE_TWO_SENSORS
                  return;
               }
            }//for
         }
         else if (Touch.X >= MENU_POLOZKA_X && Touch.X <= MENU_POLOZKA_X + MENU_POLOZKA_DX) {
            if (Touch.Y >= CANCEL_Y && Touch.Y <= DISPLAY_HEIGHT) {
               // Cancel
               BeepKey ();
               return;
            }//else
         }//if
         break;
      }//switch
   }//while
}

//-----------------------------------------------------------------------------
// Smazani pameti
//-----------------------------------------------------------------------------

#ifndef __TMCREMOTE__

void MenuDoEraseLog () {
   // Provede vymaz vsech zaznamu a zaprotokoluje
   LogReset ();
   LogWriteProtocol (PRT_SMAZANI_LOGU, 0);
}

#endif // __TMCREMOTE__

TYesNo MenuAskEraseLog () {
   // Zepta se, zda chce uzivatel smazat zaznamy
   return AskQuestion (STR_SMAZAT_PAMET_1, STR_SMAZAT_PAMET_2);
}

//-----------------------------------------------------------------------------
// Menu datum a cas
//-----------------------------------------------------------------------------

void MenuNewTime (TTransferTime data *NewTime) {
   // Nastavi novy cas <NewTime>
#ifndef __TMCREMOTE__
   MenuDoEraseLog ();
#endif // __TMCREMOTE__
   TimeSetNewTime (NewTime);
#ifndef __TMCREMOTE__
   Time.Changed = YES;      // Doslo ke zmene casu
   LogWriteProtocol (PRT_NASTAVENI_HODIN, 0); // zaprotokolujeme
#endif // __TMCREMOTE__
}

static TYesNo EditDate (byte code *Row1, byte code *Row2) {
   // Zepta se na datum a ulozi ho do UN
   if (Row2 == NULL) {
      DrawOneLineNumPad (Row1, NO);
   }
   else {
      DrawTwoLinesNumPad (Row1, Row2, NO);
   }
   UN.Word.Lsb = wbin2bcd (Time.Year);
   UN.ST.X2 = bbin2bcd (Time.Month);
   UN.ST.X1 = bbin2bcd (Time.Day);
   UN.DT = dbcd2bin (UN.DT);
   return DisplayExecuteNumPad (2, 90, DISPLAY_EDIT_DATE, 0);
}

static TYesNo EditTime (byte code *Row1, byte code *Row2) {
   // Zepta se na cas a ulozi ho do UN
   if (Row2 == NULL) {
      DrawOneLineNumPad (Row1, NO);
   }
   else {
      DrawTwoLinesNumPad (Row1, Row2, NO);
   }
   UN.ST.X1 = UN.ST.X2 = 0;
   UN.Word.Lsb = 100 * (word)Time.Hour + Time.Min;
   return DisplayExecuteNumPad (7, 90, DISPLAY_EDIT_TIME, 0);
}

void MenuDateTime () {
   TTransferTime NewTime;

   // Datum
   if (!EditDate (STR_ZADEJTE_DATUM, NULL)) {
      return;
   }
   UN.DT = dbin2bcd (UN.DT);  // Rovnou si to dam do BCD
   NewTime.Time.Day = bbcd2bin (UN.ST.X1);
   NewTime.Time.Month = bbcd2bin (UN.ST.X2);
   NewTime.Time.Year = wbcd2bin (UN.Word.Lsb);
   // Cas
   if (!EditTime (STR_ZADEJTE_CAS, NULL)) {
      return;
   }
   UN.DT = dbin2bcd (UN.DT);
   NewTime.Time.Hour = bbcd2bin (UN.ST.X3);
   NewTime.Time.Min = bbcd2bin (UN.ST.X4);

   // Po zmene data a casu musi smazat celou pamet - zeptam se ho, zda chce. Pokud nechce, nelze zmenit datum a acas.
   if (!MenuAskEraseLog ()) {
      return;
   }
   MenuNewTime (&NewTime);
}

//-----------------------------------------------------------------------------
// Hlavni menu
//-----------------------------------------------------------------------------

#define JAS_X            35
#define JAS_Y            2
#define KONTRAST_X       35
#define KONTRAST_Y       104
#define ZVUK_Y           206
#define MENU_TLACITKO_SIRKA 32
#define MENU_TLACITKO_VYSKA 32

#define MENU_POLOZKA_X   8
#define MENU_POLOZKA_DX  (30*8)
#define MENU_POLOZKA_DY  36

#define MENU_YPrvniPolozky 3

#define MENU_YPrvniPolozky 3

void RedrawSound () {
   DrawSmallButton (JAS_X, ZVUK_Y, Alarm.Quiet ? SYMBOL_MENU_REPRO_KRIZEK : SYMBOL_MENU_REPRO);
}

enum {
   ID_MAIN_MENU_LOGIN,
   ID_MAIN_MENU_PRINT,
   ID_MAIN_MENU_RECORDS,
   ID_MAIN_MENU_SETUP,
   ID_MAIN_MENU_BRIGHTNESS_UP,
   ID_MAIN_MENU_BRIGHTNESS_DOWN,
   ID_MAIN_MENU_CONTRAST_UP,
   ID_MAIN_MENU_CONTRAST_DOWN,
   ID_MAIN_MENU_SOUND,
   ID_MAIN_MENU_CANCEL
};

#ifndef __TMCREMOTE__

TTouchButton code MainMenuButtons[] = {
  { // Login
    ID_MAIN_MENU_LOGIN,
    0,
    ID_MAIN_MENU_LOGIN * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Tisk protokolu
    ID_MAIN_MENU_PRINT,
    0,
    ID_MAIN_MENU_PRINT * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Zaznam
    ID_MAIN_MENU_RECORDS,
    0,
    ID_MAIN_MENU_RECORDS * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Nastaveni
    ID_MAIN_MENU_SETUP,
    0,
    ID_MAIN_MENU_SETUP * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Zvyseni jasu
    ID_MAIN_MENU_BRIGHTNESS_UP,
    (JAS_X * 8),
    JAS_Y,
    MENU_TLACITKO_SIRKA,
    MENU_TLACITKO_VYSKA,
    YES  // Vcetne autorepeat
  },
  { // Snizeni jasu
    ID_MAIN_MENU_BRIGHTNESS_DOWN,
    (JAS_X * 8),
    JAS_Y + 65,
    MENU_TLACITKO_SIRKA,
    MENU_TLACITKO_VYSKA,
    YES  // Vcetne autorepeat
  },
  { // Zvyseni kontrastu
    ID_MAIN_MENU_CONTRAST_UP,
    (KONTRAST_X * 8),
    KONTRAST_Y,
    MENU_TLACITKO_SIRKA,
    MENU_TLACITKO_VYSKA,
    YES  // Vcetne autorepeat
  },
  { // Snizeni kontrastu
    ID_MAIN_MENU_CONTRAST_DOWN,
    (KONTRAST_X * 8),
    KONTRAST_Y + 65,
    MENU_TLACITKO_SIRKA,
    MENU_TLACITKO_VYSKA,
    YES  // Vcetne autorepeat
  },
  { // Zvuk
    ID_MAIN_MENU_SOUND,
    (JAS_X * 8),
    ZVUK_Y,
    MENU_TLACITKO_SIRKA,
    MENU_TLACITKO_VYSKA,
    NO
  },
  { // Zrusit
    ID_MAIN_MENU_CANCEL,
    0,
    CANCEL_Y,
    MENU_POLOZKA_DX,
    DISPLAY_HEIGHT - (CANCEL_Y),
    NO
  }
};

#else

TTouchButton code MainMenuButtons[] = {
  { // Zvyseni jasu
    ID_MAIN_MENU_BRIGHTNESS_UP,
    (JAS_X * 8),
    JAS_Y,
    MENU_TLACITKO_SIRKA,
    MENU_TLACITKO_VYSKA,
    YES  // Vcetne autorepeat
  },
  { // Snizeni jasu
    ID_MAIN_MENU_BRIGHTNESS_DOWN,
    (JAS_X * 8),
    JAS_Y + 65,
    MENU_TLACITKO_SIRKA,
    MENU_TLACITKO_VYSKA,
    YES  // Vcetne autorepeat
  },
  { // Zvyseni kontrastu
    ID_MAIN_MENU_CONTRAST_UP,
    (KONTRAST_X * 8),
    KONTRAST_Y,
    MENU_TLACITKO_SIRKA,
    MENU_TLACITKO_VYSKA,
    YES  // Vcetne autorepeat
  },
  { // Snizeni kontrastu
    ID_MAIN_MENU_CONTRAST_DOWN,
    (KONTRAST_X * 8),
    KONTRAST_Y + 65,
    MENU_TLACITKO_SIRKA,
    MENU_TLACITKO_VYSKA,
    YES  // Vcetne autorepeat
  },
  { // Zvuk
    ID_MAIN_MENU_SOUND,
    (JAS_X * 8),
    ZVUK_Y,
    MENU_TLACITKO_SIRKA,
    MENU_TLACITKO_VYSKA,
    NO
  },
  { // Zrusit
    ID_MAIN_MENU_CANCEL,
    0,
    CANCEL_Y,
    MENU_POLOZKA_DX,
    DISPLAY_HEIGHT - (CANCEL_Y),
    NO
  }
};

#endif // __TMCREMOTE__

TTouchWorkplace code MainMenuWorkplace = {
  MainMenuButtons,
  ArrayLength(MainMenuButtons)
};


#define MENU_SIDE_BUTTON_WIDTH  4
#define MENU_SIDE_BUTTON_HEIGHT 32

static void DrawSmallButton (byte x, byte y, byte code *symbol) {
   // Vykresli male tlacitko 32x32px se symbolem uprostred
   DisplayButtonFrame (x, y, MENU_SIDE_BUTTON_WIDTH, MENU_SIDE_BUTTON_HEIGHT, NO);
   DisplaySymbol (x + 1, y + MENU_SIDE_BUTTON_HEIGHT / 2 - symbol[1] / 2, symbol);
}

#ifndef __TMCREMOTE__

static void DrawMenuItems (byte code *title, byte code **items, byte count, byte xOffset) {
   // Vykresli polozky menu a dole Zrusit, ponecha zobrazeni na displeji skryte
   byte j, yOffset;

   DisplayClear ();
   DisplayPlane (NO);

   if (title == NULL) {
      yOffset = 0;
   }
   else {
      yOffset = 1;
      DisplayStringTahoma18 (2, MENU_YPrvniPolozky, title);
   }

   for (j = 0; j < count; j++) {
      DisplayStringTahoma18 (MENU_POLOZKA_X + xOffset, MENU_YPrvniPolozky + yOffset * MENU_POLOZKA_DY, items[j]);
      yOffset++;
   }

   // Tlacitko zrusit
   DisplayStringTahoma18 (MENU_POLOZKA_X, CANCEL_Y, STR_ZRUSIT);
}

#endif //__TMCREMOTE__


void MenuMain () {
   // Vykreslim polozky menu
#ifndef __TMCREMOTE__
   DrawMenuItems (NULL, STR_MAIN_MENU_ITEMS, ArrayLength(STR_MAIN_MENU_ITEMS), 0);
#else
   DisplayClear ();
   DisplayPlane (NO);
   // Tlacitko zrusit
   DisplayStringTahoma18 (MENU_POLOZKA_X, CANCEL_Y, STR_ZRUSIT);
#endif // __TMCREMOTE__

   // Jas a kontrast
   DisplayVertDotLine (34, 0, 240, 0x80);
   // Jas
   DrawSmallButton (JAS_X, JAS_Y, SYMBOL_MENU_PLUS);
   DisplaySymbol (JAS_X + 1, JAS_Y + 34, SYMBOL_MENU_JAS);
   DrawSmallButton (JAS_X, JAS_Y + 53, SYMBOL_MENU_MINUS);
   // Kontrast
   DisplayHorDotLine (34, KONTRAST_Y - 9, 6);
   DrawSmallButton (KONTRAST_X, KONTRAST_Y, SYMBOL_MENU_PLUS);
   DisplaySymbol (KONTRAST_X + 1, KONTRAST_Y + 35, SYMBOL_MENU_KONTRAST);
   DrawSmallButton (KONTRAST_X, KONTRAST_Y + 53, SYMBOL_MENU_MINUS);
   // Zvuk
   DisplayHorDotLine (34, ZVUK_Y - 9, 6);
   RedrawSound ();
   DisplayPlane (YES);
   while (1) {
      switch (WaitForKey ()) {
      case K_TIMEOUT: {
         BeepKey ();
         return;
      }
      case K_TOUCH: {
         switch (TouchCtlCheck (&MainMenuWorkplace)) {
#ifndef __TMCREMOTE__
         case ID_MAIN_MENU_LOGIN: {
            PrepareMenu ();
            MenuLogin ();
            return;
         }
         case ID_MAIN_MENU_PRINT: {
            PrepareMenu ();
            MenuPrintProtocol ();
            return;
         }
         case ID_MAIN_MENU_RECORDS: {
            PrepareMenu ();
            MenuRecords ();
            return;
         }
         case ID_MAIN_MENU_SETUP: {
            PrepareMenu ();
            MenuSetup ();
            return;
         }
#endif //__TMCREMOTE__
         case ID_MAIN_MENU_BRIGHTNESS_UP: {
            BeepKey ();
            BrightnessUp ();
            break;
         }
         case ID_MAIN_MENU_BRIGHTNESS_DOWN: {
            BeepKey ();
            BrightnessDown ();
            break;
         }
         case ID_MAIN_MENU_CONTRAST_UP: {
            BeepKey ();
            ContrastUp ();
            break;
         }
         case ID_MAIN_MENU_CONTRAST_DOWN: {
            BeepKey ();
            ContrastDown ();
            break;
         }
         case ID_MAIN_MENU_SOUND: {
            if (!Alarm.ShowAlarm && !Alarm.Quiet) {
               break;            // Pokud neni vyhlasen alarm, sirenu nejde dopredu ztlumit. Muze ji ztlumit, az nastane alarm.
            }
            BeepKey ();
            AlarmChange ();
            RedrawSound ();
            break;
         }
         case ID_MAIN_MENU_CANCEL: {
            BeepKey ();
            return;
         }
         }//switch
         break;
      }
      }//switch
   }//while
}

//-----------------------------------------------------------------------------
// Login ridice
//-----------------------------------------------------------------------------


#ifndef __TMCREMOTE__   // Predefinovano v Tmcr.c51

void MenuNewLogin (byte Number) {
   // Provede samotny login ridice <Number>
   Driver = Number;   // Ulozim jen do RAM, po dalsim zapnuti se zaloguje znovu
   LogWriteProtocol (PRT_PRIHLASENI_RIDICE, Driver); // zaprotokolujeme
}

void MenuLogin () {
   // Provede login ridice
   while (1) {
      DrawOneLineNumPad (STR_LOGIN_RIDICE, NO);
      UN.DT = Driver;   // Stavajici ridic
      if (!DisplayExecuteNumPad (10, 90, 2, NO)) {
         return;   // Necham nalogovaneho puvodniho ridice nebo 0 po startu
      }
      // Zmacknul Enter
      if (UN.ST.X4 >= MIN_LOGIN && UN.ST.X4 <= MAX_LOGIN) {
         // Zadane cislo ridice je v poradku
         MenuNewLogin (UN.ST.X4);
         break;
      }
      else {
         // Zadana teplota je mimo rozsah
         if (!MenuInvalidValue ()) {
            return;  // Nechce zadat znovu
         }
         // Pokud stisknul Enter, pokracuju v cyklu, aby zadal teplotu znovu
      }//else
   }
   DisplayClear (); // Aby se to hned smazalo, nez se za 1 sekundu displej prekresli
}

#endif // __TMCREMOTE__


//-----------------------------------------------------------------------------
// Podpurne funkce pro menu regulatoru
//-----------------------------------------------------------------------------

#ifndef __TMCREMOTE__

TYesNo EnterRegulatorTime (byte code *Text, word OldValue) {
   // Zadani integracniho nebo derivacniho casu, vysledek preda v UN.Word.Lsb
   while (1) {
      DrawOneLineNumPad (Text, NO);
      DisplayStringTahoma18 (146, 90, STR_SEKUND);
      UN.DT = OldValue;
      if (!DisplayExecuteNumPad (8, 90, 4, NO)) {
         return NO;
      }
      // Zmacknul Enter
      if (UN.Word.Lsb <= 9000) {
         // Zadana teplota je v poradku, predavam ji v UN.Word.Lsb
         return YES;
      }
      else {
         // Zadana teplota je mimo rozsah
         if (!MenuInvalidValue ()) {
            return NO;  // Nechce zadat znovu
         }
         // Pokud stisknul Enter, pokracuju v cyklu, aby zadal teplotu znovu
      }//else
   }//while
}

#endif // __TMCREMOTE__

//-----------------------------------------------------------------------------
// Menu nastaveni regulatoru teploty
//-----------------------------------------------------------------------------

#ifndef __TMCREMOTE__

enum {
   ID_MENU_UNITS_CELSIUS = 0,    // Polozky odpovidaji TTempUnits
   ID_MENU_UNITS_FAHRENHEIT = 1,
   ID_MENU_UNITS_CANCEL
};

TTouchButton code MenuUnitsButtons[] = {
  { // Celsius
    ID_MENU_UNITS_CELSIUS,
    0,
    MENU_YPrvniPolozky + (ID_MENU_UNITS_CELSIUS + 1) * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Fahrenheit
    ID_MENU_UNITS_FAHRENHEIT,
    0,
    MENU_YPrvniPolozky + (ID_MENU_UNITS_FAHRENHEIT + 1) * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Zrusit
    ID_MENU_UNITS_CANCEL,
    0,
    CANCEL_Y,
    MENU_POLOZKA_DX,
    DISPLAY_HEIGHT - (CANCEL_Y),
    NO
  }
};

TTouchWorkplace code MenuUnitsWorkplace = {
  MenuUnitsButtons,
  ArrayLength(MenuUnitsButtons)
};

#define SIPKA_OFFSET_X 1
#define SIPKA_OFFSET_Y 11
#define DX_MINUTA      13

byte MenuUnits () {
   byte  NewUnits;

   // Jednotky
   DrawMenuItems (STR_JEDNOTKY, STR_MENU_UNITS_ITEMS, ArrayLength(STR_MENU_UNITS_ITEMS), DX_MINUTA);
   // Sipka u aktualnich jednotek
   DisplaySymbol (SIPKA_OFFSET_X, MENU_YPrvniPolozky + (Config.TempUnits + 1) * MENU_POLOZKA_DY + SIPKA_OFFSET_Y, SYMBOL_SIPKA_VPRAVO);
   DisplayPlane (YES);
   while (1) {
      switch (WaitForKey ()) {
      case K_TIMEOUT: {
         BeepKey ();
         return K_TIMEOUT;
      }
      case K_TOUCH: {
         NewUnits = TouchCtlCheck (&MenuUnitsWorkplace);
         switch (NewUnits) {
         case ID_MENU_UNITS_CELSIUS:
         case ID_MENU_UNITS_FAHRENHEIT:
            BeepKey ();
            return NewUnits;

         case ID_MENU_UNITS_CANCEL:
            BeepKey ();
            return K_ESC;
         }
         break;
      }
      }//switch
   }//while
}

void MenuRegulatorTemperature () {
   byte  NewUnits;
   int16 NewAutoHysteresis;
   byte  NewMaxOvershootSum;
   byte  NewRegKp;
   word  NewRegTi;

   // Jednotky
   NewUnits = MenuUnits ();
   if (NewUnits != UNITS_CELSIUS && NewUnits != UNITS_FAHRENHEIT) {
      return;
   }

   // Hystereze prechodu
   while (1) {
      DrawOneLineNumPad (STR_ZADEJTE_HYSTEREZI, NO);
      DisplayStringTahoma18 (135, 90, STR_STUPNE_CELSIA);
      UN.DT = 10 * TempGet1C (Config.AutoHysteresis + TempMk001C (0, 5)) + TempGet01C (Config.AutoHysteresis + TempMk001C (0, 5));
      if (!DisplayExecuteNumPad (9, 90, 0x21, NO)) {
         return;
      }
      // Zmacknul Enter
      // Hodnota 5-20 je ulozena jen v LSB bajtu => nemusim pracovat s celym UN.DT
      if (UN.ST.X4 >= 5 && UN.ST.X4 <= 50) {
         // Zadana teplota je v poradku, ulozim ji
         NewAutoHysteresis = TempMk01C (UN.ST.X4 / 10, UN.ST.X4 - ((UN.ST.X4 / 10) * 10));  // Desitky a jednotky
         break;
      }
      else {
         // Zadana teplota je mimo rozsah
         if (!MenuInvalidValue ()) {
            return;  // Nechce zadat znovu
         }
         // Pokud stisknul Enter, pokracuju v cyklu, aby zadal teplotu znovu
      }
   }

   // Maximalni suma prekmitu
   while (1) {
      DrawOneLineNumPad (STR_ZADEJTE_MAXIMALNI_PREKMIT, NO);
      DisplayStringTahoma18 (119, 90, STR_STUPNE_KRAT_SEKUNDY);
      UN.DT = Config.MaxOvershootSum;
      if (!DisplayExecuteNumPad (6, 90, 3, NO)) {
         return;
      }
      // Zmacknul Enter
      if (UN.Word.Lsb <= 127) {         // Musi byt max. 127, vic nejde
        // Zadana teplota je v poradku, ulozim ji
         NewMaxOvershootSum = UN.ST.X4;    // Jen do 127, tj. 1 bajt
         break;
      }
      else {
         // Zadana teplota je mimo rozsah
         if (!MenuInvalidValue ()) {
            return;  // Nechce zadat znovu
         }
         // Pokud stisknul Enter, pokracuju v cyklu, aby zadal teplotu znovu
      }//else
   }//while

   // Kp
   while (1) {
      DrawOneLineNumPad (STR_ZADEJTE_ZESILENI, NO);
      UN.DT = Config.RegKp;
      if (!DisplayExecuteNumPad (9, 90, 3, NO)) {
         return;
      }
      // Zmacknul Enter
      if (UN.Word.Lsb <= 255) {
         // Zadana teplota je v poradku, ulozim ji
         NewRegKp = UN.ST.X4;      // Max 1 bajt
         break;
      }
      else {
         // Zadana teplota je mimo rozsah
         if (!MenuInvalidValue ()) {
            return;  // Nechce zadat znovu
         }
         // Pokud stisknul Enter, pokracuju v cyklu, aby zadal teplotu znovu
      }//else
   }//while

   // Ti
   if (!EnterRegulatorTime (STR_ZADEJTE_INTEGRACNI_CAS, Config.RegTi)) {
      return;
   }
   NewRegTi = UN.Word.Lsb;

   // Td
   if (!EnterRegulatorTime (STR_ZADEJTE_DERIVACNI_CAS, Config.RegTd)) {
      return;
   }
   // Pokud zmenil jednotky, radeji nastavim default hodnoty u zadavanych teplot - cilova teplota a max. odchylky
   if (Config.TempUnits != NewUnits) {
      // Zmenil jednotky. Nepouzivam funkce MenuNewTargetTemperature a MenuNewOffset, mrsilo to derivacni cas, asi jak
      // se volalo vice CfgSave() po sobe.
      if (NewUnits == UNITS_CELSIUS) {
         Config.TargetTemperature = DEFAULT_TARGET_TEMPERATURE_C;
         Config.MaxOffsetBelow = DEFAULT_MAX_OFFSET_BELOW_C;
         Config.MaxOffsetAbove = DEFAULT_MAX_OFFSET_ABOVE_C;
      }
      else {
         Config.TargetTemperature = DEFAULT_TARGET_TEMPERATURE_F;
         Config.MaxOffsetBelow = DEFAULT_MAX_OFFSET_BELOW_F;
         Config.MaxOffsetAbove = DEFAULT_MAX_OFFSET_ABOVE_F;
      }
      ClearFaultsInBody ();  // Po zmene cilove teploty merim najeti teploty ve skrini do pasma cilove teploty od pocatku
   }

   // Ulozim nove parametry regulace
   Config.TempUnits = NewUnits;
   Config.AutoHysteresis = NewAutoHysteresis;
   Config.MaxOvershootSum = NewMaxOvershootSum;
   Config.RegKp = NewRegKp;
   Config.RegTi = NewRegTi;
   Config.RegTd = UN.Word.Lsb;
   CfgSave ();
   LogWriteProtocol (PRT_NASTAVENI_JEDNOTKY, Config.TempUnits);
   LogWriteProtocol (PRT_NASTAVENI_HYSTEREZE, Config.AutoHysteresis);                // Ve vnitrnim formatu, v SW na PC prepocitat na stupne C
   LogWriteProtocol (PRT_NASTAVENI_REG_KP, Config.RegKp);
   LogWriteProtocol (PRT_NASTAVENI_REG_TI, Config.RegTi);
   LogWriteProtocol (PRT_NASTAVENI_REG_TD, Config.RegTd);
   LogWriteProtocol (PRT_NASTAVENI_MAX_PREKMIT, Config.MaxOvershootSum);

   // Predam nove parametry do PID regulatoru topeni
   Heating.Pid.Kp = Config.RegKp;
   Heating.Pid.Ti = Config.RegTi;
   Heating.Pid.Td = Config.RegTd;
}

#endif // __TMCREMOTE__


//-----------------------------------------------------------------------------
// Menu nastaveni regulatoru CO2
//-----------------------------------------------------------------------------

#ifndef __TMCREMOTE__

void MenuRegulatorFreshAir () {
   byte NewRegKp;
   word NewRegTi, NewRegTd;
   byte NewMinFreshAirForbidden;

   // Kp
   while (1) {
      DrawOneLineNumPad (STR_ZADEJTE_ZESILENI, NO);
      UN.DT = Config.FreshAirRegKp;
      if (!DisplayExecuteNumPad (9, 90, 3, NO)) {
         return;
      }
      // Zmacknul Enter
      if (UN.Word.Lsb <= 255) {
         // Zadana teplota je v poradku, ulozim ji
         NewRegKp = UN.ST.X4;      // Max 1 bajt
         break;
      }
      else {
         // Zadana teplota je mimo rozsah
         if (!MenuInvalidValue ()) {
            return;  // Nechce zadat znovu
         }
         // Pokud stisknul Enter, pokracuju v cyklu, aby zadal teplotu znovu
      }//else
   }//while

   // Ti
   if (!EnterRegulatorTime (STR_ZADEJTE_INTEGRACNI_CAS, Config.FreshAirRegTi)) {
      return;
   }
   NewRegTi = UN.Word.Lsb;

   // Td
   if (!EnterRegulatorTime (STR_ZADEJTE_DERIVACNI_CAS, Config.FreshAirRegTd)) {
      return;
   }
   NewRegTd = UN.Word.Lsb;

   // Minimum zakazaneho pasma
   while (1) {
      DrawTwoLinesNumPad (STR_ZADEJTE_ZAKAZANE_PASMO_MIN1, STR_ZADEJTE_ZAKAZANE_PASMO_MIN2, NO);
      UN.DT = Config.MinFreshAirForbidden;
      if (!DisplayExecuteNumPad (6, 90, 3, NO)) {
         return;
      }
      // Zmacknul Enter
      if (UN.Word.Lsb <= 100) {
         // Zadana hodnota je v poradku, ulozim ji
         NewMinFreshAirForbidden = UN.ST.X4;
         break;
      }
      else {
         // Zadana teplota je mimo rozsah
         if (!MenuInvalidValue ()) {
            return;  // Nechce zadat znovu
         }
         // Pokud stisknul Enter, pokracuju v cyklu, aby zadal teplotu znovu
      }//else
   }//while

   // Maximum zakazaneho pasma
   while (1) {
      DrawTwoLinesNumPad (STR_ZADEJTE_ZAKAZANE_PASMO_MAX1, STR_ZADEJTE_ZAKAZANE_PASMO_MAX2, NO);
      UN.DT = Config.MaxFreshAirForbidden;
      if (!DisplayExecuteNumPad (6, 90, 3, NO)) {  // Musim delat na 3 znaky, max je 100
         return;
      }
      // Zmacknul Enter
      if (UN.Word.Lsb >= NewMinFreshAirForbidden && UN.Word.Lsb <= 100) {
         // Zadana hodnota je v poradku, ulozim ji
         break;
      }
      else {
         // Zadana teplota je mimo rozsah
         if (!MenuInvalidValue ()) {
            return;  // Nechce zadat znovu
         }
         // Pokud stisknul Enter, pokracuju v cyklu, aby zadal teplotu znovu
      }//else
   }//while


   Config.FreshAirRegKp = NewRegKp;
   Config.FreshAirRegTi = NewRegTi;
   Config.FreshAirRegTd = NewRegTd;
   Config.MinFreshAirForbidden = NewMinFreshAirForbidden;
   Config.MaxFreshAirForbidden = UN.ST.X4;

   CfgSave ();
   LogWriteProtocol (PRT_NASTAVENI_FRESH_AIR_REG_KP, Config.FreshAirRegKp);
   LogWriteProtocol (PRT_NASTAVENI_FRESH_AIR_REG_TI, Config.FreshAirRegTi);
   LogWriteProtocol (PRT_NASTAVENI_FRESH_AIR_REG_TD, Config.FreshAirRegTd);

   // Predam nove parametry do PID regulatoru klapek
   FAirRegulatorUpdateParameters ();
}

#endif // __TMCREMOTE__


//-----------------------------------------------------------------------------
// Menu nastaveni regulatoru CO2
//-----------------------------------------------------------------------------

#ifndef __TMCREMOTE__

#ifdef USE_CO2

void MenuRegulatorCo2 () {
   word NewTarget;
   word NewRegKp;
   word NewRegTi;

   // Target
   while (1) {
#if defined(LANG_RU) || defined(LANG_GE) || defined(LANG_SW)
      DrawTwoLinesNumPad (STR_ZADEJTE_MAXIMALNI_CO2_1, STR_ZADEJTE_MAXIMALNI_CO2_2, NO);
#else
      DrawOneLineNumPad (STR_ZADEJTE_MAXIMALNI_CO2, NO);
#endif
      DisplayStringTahoma18 (129, 90, STR_PPM);
      UN.DT = Config.Co2Target;
      if (!DisplayExecuteNumPad (6, 90, 4, NO)) {
         return;
      }
      // Zmacknul Enter
      if (UN.Word.Lsb >= 100) {             // Pod 100ppm nema cenu, maximum neomezuju, necham 9999
        // Zadana teplota je v poradku, ulozim ji
         NewTarget = UN.Word.Lsb;
         break;
      }
      else {
         // Zadana teplota je mimo rozsah
         if (!MenuInvalidValue ()) {
            return;  // Nechce zadat znovu
         }
         // Pokud stisknul Enter, pokracuju v cyklu, aby zadal teplotu znovu
      }//else
   }//while

   // Kp
   DrawOneLineNumPad (STR_ZADEJTE_ZESILENI, NO);
   UN.DT = Config.Co2RegKp;
   if (!DisplayExecuteNumPad (9, 90, 0x43, NO)) {
      return;
   }
   // Zmacknul Enter
   NewRegKp = UN.Word.Lsb;       // 0-9999

   // Ti
   if (!EnterRegulatorTime (STR_ZADEJTE_INTEGRACNI_CAS, Config.Co2RegTi)) {
      return;
   }
   NewRegTi = UN.Word.Lsb;

   // Td
   if (!EnterRegulatorTime (STR_ZADEJTE_DERIVACNI_CAS, Config.Co2RegTd)) {
      return;
   }
   Config.Co2Target = NewTarget;
   Config.Co2RegKp = NewRegKp;
   Config.Co2RegTi = NewRegTi;
   Config.Co2RegTd = UN.Word.Lsb;
   CfgSave ();
   LogWriteProtocol (PRT_NASTAVENI_CO2_REG_TARGET, Config.Co2Target);
   LogWriteProtocol (PRT_NASTAVENI_CO2_REG_KP, Config.Co2RegKp);
   LogWriteProtocol (PRT_NASTAVENI_CO2_REG_TI, Config.Co2RegTi);
   LogWriteProtocol (PRT_NASTAVENI_CO2_REG_TD, Config.Co2RegTd);

   // Predam nove parametry do PID regulatoru CO2
   Co2RegulatorUpdateParameters ();
}

#endif // USE_CO2

#endif // __TMCREMOTE__


//-----------------------------------------------------------------------------
// Menu regulator
//-----------------------------------------------------------------------------

#ifndef __TMCREMOTE__

enum {
   ID_MENU_REGULATOR_TEMPERATURE,
   ID_MENU_REGULATOR_FRESH_AIR,
#ifdef USE_CO2
   ID_MENU_REGULATOR_CO2,
#endif // USE_CO2
   ID_MENU_REGULATOR_CANCEL
};

TTouchButton code MenuRegulatorButtons[] = {
  { // Regulator teploty
    ID_MENU_REGULATOR_TEMPERATURE,
    0,
    MENU_YPrvniPolozky + (ID_MENU_REGULATOR_TEMPERATURE + 1) * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Regulator cerstveho vzduchu
    ID_MENU_REGULATOR_FRESH_AIR,
    0,
    MENU_YPrvniPolozky + (ID_MENU_REGULATOR_FRESH_AIR + 1) * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
#ifdef USE_CO2
  { // Regulator CO2
    ID_MENU_REGULATOR_CO2,
    0,
    MENU_YPrvniPolozky + (ID_MENU_REGULATOR_CO2 + 1) * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
#endif // USE_CO2
  { // Zrusit
    ID_MENU_REGULATOR_CANCEL,
    0,
    CANCEL_Y,
    MENU_POLOZKA_DX,
    DISPLAY_HEIGHT - (CANCEL_Y),
    NO
  }
};

TTouchWorkplace code MenuRegulatorWorkplace = {
  MenuRegulatorButtons,
  ArrayLength(MenuRegulatorButtons)
};

void MenuRegulator () {
   // Menu nastaveni regulatoru
   // Vykreslim polozky menu
   DrawMenuItems (STR_REGULATOR, STR_MENU_REGULATOR_ITEMS, ArrayLength(STR_MENU_REGULATOR_ITEMS), 0);
   DisplayPlane (YES);
   while (1) {
      switch (WaitForKey ()) {
      case K_TIMEOUT:
         BeepKey ();
         return;

      case K_TOUCH:
         switch (TouchCtlCheck (&MenuRegulatorWorkplace)) {
         case ID_MENU_REGULATOR_TEMPERATURE:
            PrepareMenu ();
            MenuRegulatorTemperature ();
            return;

         case ID_MENU_REGULATOR_FRESH_AIR:
            PrepareMenu ();
            MenuRegulatorFreshAir ();
            return;

#ifdef USE_CO2
         case ID_MENU_REGULATOR_CO2:
            PrepareMenu ();
            MenuRegulatorCo2 ();
            return;
#endif // USE_CO2

         case ID_MENU_REGULATOR_CANCEL:
            BeepKey ();
            return;

         }//switch
         break;

      }//switch
   }//while
}

#endif // __TMCREMOTE__


//-----------------------------------------------------------------------------
// Zobrazeni "cekejte"
//-----------------------------------------------------------------------------

#ifndef __TMCREMOTE__

static void MenuWait () {
   // Zobrazi na displeji Cekejte..
   DisplayClear ();
   DisplayPlane (NO);
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, 85, STR_CEKEJTE);
   DisplayPlane (YES);
}

#endif // __TMCREMOTE__

//-----------------------------------------------------------------------------
// Menu zaznam
//-----------------------------------------------------------------------------

#ifndef __TMCREMOTE__

enum {
   ID_MENU_PERIOD_1 = 1,    // Polozky odpovidaji minutam
   ID_MENU_PERIOD_5 = 5,
   ID_MENU_PERIOD_10 = 10,
   ID_MENU_PERIOD_20 = 20,
   ID_MENU_PERIOD_CANCEL
};

TTouchButton code MenuPeriodButtons[] = {
  { // 1 minuta
    ID_MENU_PERIOD_1,
    0,
    MENU_YPrvniPolozky + 1 * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // 5 minut
    ID_MENU_PERIOD_5,
    0,
    MENU_YPrvniPolozky + 2 * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // 10 minut
    ID_MENU_PERIOD_10,
    0,
    MENU_YPrvniPolozky + 3 * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // 20 minut
    ID_MENU_PERIOD_20,
    0,
    MENU_YPrvniPolozky + 4 * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Zrusit
    ID_MENU_PERIOD_CANCEL,
    0,
    CANCEL_Y,
    MENU_POLOZKA_DX,
    DISPLAY_HEIGHT - (CANCEL_Y),
    NO
  }
};

TTouchWorkplace code MenuPeriodWorkplace = {
  MenuPeriodButtons,
  ArrayLength(MenuPeriodButtons)
};

#define SIPKA_OFFSET_X 1
#define SIPKA_OFFSET_Y 11
#define DX_MINUTA      13

void MenuSavingPeriod () {
   // Menu perioda ukaldani
   byte j;

   // Vykreslim polozky menu
   DrawMenuItems (STR_PERIODA_UKLADANI, STR_MENU_PERIOD_ITEMS, ArrayLength(STR_MENU_PERIOD_ITEMS), DX_MINUTA);
   // Sipka u aktualni zvolene periody
   switch (Config.SavingPeriod) {
   case 1:  DisplaySymbol (SIPKA_OFFSET_X, MENU_YPrvniPolozky + 1 * MENU_POLOZKA_DY + SIPKA_OFFSET_Y, SYMBOL_SIPKA_VPRAVO); break;
   case 5:  DisplaySymbol (SIPKA_OFFSET_X, MENU_YPrvniPolozky + 2 * MENU_POLOZKA_DY + SIPKA_OFFSET_Y, SYMBOL_SIPKA_VPRAVO); break;
   case 10: DisplaySymbol (SIPKA_OFFSET_X, MENU_YPrvniPolozky + 3 * MENU_POLOZKA_DY + SIPKA_OFFSET_Y, SYMBOL_SIPKA_VPRAVO); break;
   case 20: DisplaySymbol (SIPKA_OFFSET_X, MENU_YPrvniPolozky + 4 * MENU_POLOZKA_DY + SIPKA_OFFSET_Y, SYMBOL_SIPKA_VPRAVO); break;
   }
   DisplayPlane (YES);
   while (1) {
      switch (WaitForKey ()) {
      case K_TIMEOUT: {
         BeepKey ();
         return;
      }
      case K_TOUCH: {
         j = TouchCtlCheck (&MenuPeriodWorkplace);
         switch (j) {
         case ID_MENU_PERIOD_1:
         case ID_MENU_PERIOD_5:
         case ID_MENU_PERIOD_10:
         case ID_MENU_PERIOD_20: {
            BeepKey ();
            Config.SavingPeriod = j;    // Primo odpovida minutam
            CfgSave ();
            LogWriteProtocol (PRT_NASTAVENI_PERIODY_UKLADANI, Config.SavingPeriod);
            return;
         }
         case ID_MENU_PERIOD_CANCEL: {
            BeepKey ();
            return;
         }
         }//switch
         break;
      }
      }//switch
   }//while
}

void MenuCopyLog () {
   // Zkopiruje data do pomatoveho modulu
   DisplayClear ();
   DisplayPlane (NO);
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, 65, STR_PRIPOJTE_MODUL1);
   DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, 95, STR_PRIPOJTE_MODUL2);
   DisplayOkCancel ();
   DisplayPlane (YES);
   if (DisplayExecuteButtons (2)) {
      MenuWait ();
      // Prekopiruju celou vnitrni EEPROM do pametoveho modulu
      if (LogCopy ()) {
         // Kopirovani probehlo v poradku
         DisplayClear ();
         DisplayPlane (NO);
         DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, 85, STR_KOPIROVANI_OK);
         LogWriteProtocol (PRT_EXPORT_DAT, YES);
      }
      else {
         // Nastala nejaka chyba, napr. neni pripojen modul, verifikace neprobehla ok atd.
         DisplayClear ();
         DisplayPlane (NO);
         DisplayStringCenterTahoma18 (DISPLAY_WIDTH / 2, 85, STR_KOPIROVANI_CHYBA);
         LogWriteProtocol (PRT_EXPORT_DAT, NO);
      }//else
      DisplayOk ();
      DisplayPlane (YES);
      DisplayExecuteButtons (1);
   }//if
}

enum {
   ID_MENU_RECORDS_COPY,
   ID_MENU_RECORDS_PERIOD,
   ID_MENU_RECORDS_ID,
   ID_MENU_RECORDS_CANCEL
};

TTouchButton code MenuRecordsButtons[] = {
  { // Kopie do modulu
    ID_MENU_RECORDS_COPY,
    0,
    MENU_YPrvniPolozky + (ID_MENU_RECORDS_COPY + 1) * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Perioda ukladani
    ID_MENU_RECORDS_PERIOD,
    0,
    MENU_YPrvniPolozky + (ID_MENU_RECORDS_PERIOD + 1) * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Identifikacni cislo
    ID_MENU_RECORDS_ID,
    0,
    MENU_YPrvniPolozky + (ID_MENU_RECORDS_ID + 1) * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Zrusit
    ID_MENU_RECORDS_CANCEL,
    0,
    CANCEL_Y,
    MENU_POLOZKA_DX,
    DISPLAY_HEIGHT - (CANCEL_Y),
    NO
  }
};

TTouchWorkplace code MenuRecordsWorkplace = {
  MenuRecordsButtons,
  ArrayLength(MenuRecordsButtons)
};

void MenuIdentificationNumber () {
   DrawOneLineNumPad (STR_IDENTIFIKACNI_CISLO, NO);
   UN.DT = Config.IdentificationNumber;
   if (!DisplayExecuteNumPad (10, 90, 2, NO)) {
      return;
   }
   Config.IdentificationNumber = UN.ST.X4;
   CfgSave ();
   LogWriteProtocol (PRT_NASTAVENI_IDENTIFIKACNIHO_CISLA, Config.IdentificationNumber);
}

void MenuRecords () {
   // Menu zaznamy
   // Vykreslim polozky menu
   DrawMenuItems (STR_ZAZNAMY, STR_MENU_SAVING_ITEMS,  ArrayLength(STR_MENU_SAVING_ITEMS), 0);
   DisplayPlane (YES);
   while (1) {
      switch (WaitForKey ()) {
      case K_TIMEOUT: {
         BeepKey ();
         return;
      }
      case K_TOUCH: {
         switch (TouchCtlCheck (&MenuRecordsWorkplace)) {
         case ID_MENU_RECORDS_COPY: {
            PrepareMenu ();
            MenuCopyLog ();
            return;
         }
         case ID_MENU_RECORDS_PERIOD: {
            PrepareMenu ();
            MenuSavingPeriod ();
            return;
         }
         case ID_MENU_RECORDS_ID: {
            PrepareMenu ();
            MenuIdentificationNumber ();
            return;
         }
         case ID_MENU_RECORDS_CANCEL: {
            BeepKey ();
            return;
         }
         }//switch
         break;
      }
      }//switch
   }//while
}

#endif // __TMCREMOTE__

//-----------------------------------------------------------------------------
// Menu mez teploty ve skrini
//-----------------------------------------------------------------------------

#ifndef __TMCREMOTE__

void MenuNewOffset (byte Above, byte Below) {
   // Nastavi nove meze nad a pod cilovou teplotou
   // Znici UN.DT
   Config.MaxOffsetBelow = Below;
   Config.MaxOffsetAbove = Above;
   // Zaprotokoluju
   CfgSave ();
   UN.DT = Below;
   UN.ST.X3 = Above;     // V UN.DT uz ted mam MaxOffsetBelow, staci doplnit Above
   LogWriteProtocol (PRT_NASTAVENI_MEZI_SKRINE, UN.DT);
   ClearFaultsInBody ();  // Po zmene max. odchylky teploty merim najeti teploty ve skrini do pasma cilove teploty od pocatku
}

void MenuOffset () {
   byte NewMaxOffsetAbove;

   DrawTwoLinesNumPad (STR_ODCHYLKA_VE_SKRINI_NAD_1, STR_ODCHYLKA_VE_SKRINI_NAD_2, NO);
   DisplayStringTahoma18 (116, 90, Config.TempUnits == UNITS_CELSIUS ? STR_STUPNE_CELSIA : STR_STUPNE_FAHRENHEITA);
   UN.DT = Config.MaxOffsetAbove;
   if (!DisplayExecuteNumPad (9, 90, 2, NO)) {
      return;
   }
   // Zmacknul Enter
   NewMaxOffsetAbove = UN.ST.X4;

   DrawTwoLinesNumPad (STR_ODCHYLKA_VE_SKRINI_POD_1, STR_ODCHYLKA_VE_SKRINI_POD_2, NO);
   DisplayStringTahoma18 (116, 90, Config.TempUnits == UNITS_CELSIUS ? STR_STUPNE_CELSIA : STR_STUPNE_FAHRENHEITA);
   UN.DT = Config.MaxOffsetBelow;
   if (!DisplayExecuteNumPad (9, 90, 2, NO)) {
      return;
   }
   // Zmacknul Enter, ulozim obe meze
   MenuNewOffset (NewMaxOffsetAbove, UN.ST.X4);
}

#endif // __TMCREMOTE__

//-----------------------------------------------------------------------------
// Menu nastaveni meze tlakove ztraty na vzduchovych filtrech
//-----------------------------------------------------------------------------

#ifndef __TMCREMOTE__

void MenuAirFiltersPressure () {
   while (1) {
      DrawOneLineNumPad (STR_ZADEJTE_MAXIMALNI_TLAK, NO);
      DisplayStringTahoma18 (119, 90, STR_PASCAL);
      UN.DT = Config.MaxFiltersAirPressure;
      if (!DisplayExecuteNumPad (6, 90, 3, NO)) {
         return;
      }
      // Zmacknul Enter
      if (UN.Word.Lsb >= 10) {             // Pod 10Pa nema cenu, maximum neomezuju, necham 999
        // Zadana teplota je v poradku, ulozim ji
         Config.MaxFiltersAirPressure = UN.Word.Lsb;
         CfgSave ();
         LogWriteProtocol (PRT_NASTAVENI_MAX_TLAKU_FILTRU, Config.MaxFiltersAirPressure);
         break;
      }
      else {
         // Zadana teplota je mimo rozsah
         if (!MenuInvalidValue ()) {
            return;  // Nechce zadat znovu
         }
         // Pokud stisknul Enter, pokracuju v cyklu, aby zadal teplotu znovu
      }//else
   }//while
}

#endif // __TMCREMOTE__

//-----------------------------------------------------------------------------
// Menu nastaveni meze tlakove ztraty na vzduchovych filtrech
//-----------------------------------------------------------------------------

#ifndef __TMCREMOTE__

void MenuCo2Limit () {
   while (1) {
#if defined(LANG_RU) || defined(LANG_GE) || defined(LANG_SW)
      DrawTwoLinesNumPad (STR_ZADEJTE_MAXIMALNI_CO2_1, STR_ZADEJTE_MAXIMALNI_CO2_2, NO);
#else
      DrawOneLineNumPad (STR_ZADEJTE_MAXIMALNI_CO2, NO);
#endif
      DisplayStringTahoma18 (129, 90, STR_PPM);
      UN.DT = Config.MaxCo2;
      if (!DisplayExecuteNumPad (6, 90, 4, NO)) {
         return;
      }
      // Zmacknul Enter
      if (UN.Word.Lsb >= 100) {             // Pod 100ppm nema cenu, maximum neomezuju, necham 9999
        // Zadana teplota je v poradku, ulozim ji
         Config.MaxCo2 = UN.Word.Lsb;
         CfgSave ();
         LogWriteProtocol (PRT_NASTAVENI_MAX_CO2, Config.MaxCo2);
         break;
      }
      else {
         // Zadana teplota je mimo rozsah
         if (!MenuInvalidValue ()) {
            return;  // Nechce zadat znovu
         }
         // Pokud stisknul Enter, pokracuju v cyklu, aby zadal teplotu znovu
      }//else
   }//while
}

#endif // __TMCREMOTE__

//-----------------------------------------------------------------------------
// Menu meze
//-----------------------------------------------------------------------------

#ifndef __TMCREMOTE__

enum {
   ID_MENU_LIMITS_TEMPERATURE,
   ID_MENU_LIMITS_AIR_FILTERS,
   ID_MENU_LIMITS_CO2,
   ID_MENU_LIMITS_CANCEL
};

TTouchButton code MenuLimitsButtons[] = {
  { // Mez teploty
    ID_MENU_LIMITS_TEMPERATURE,
    0,
    MENU_YPrvniPolozky + (ID_MENU_LIMITS_TEMPERATURE + 1) * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Mez tlaku filtru
    ID_MENU_LIMITS_AIR_FILTERS,
    0,
    MENU_YPrvniPolozky + (ID_MENU_LIMITS_AIR_FILTERS + 1) * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Mez CO2
    ID_MENU_LIMITS_CO2,
    0,
    MENU_YPrvniPolozky + (ID_MENU_LIMITS_CO2 + 1) * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Zrusit
    ID_MENU_LIMITS_CANCEL,
    0,
    CANCEL_Y,
    MENU_POLOZKA_DX,
    DISPLAY_HEIGHT - (CANCEL_Y),
    NO
  }
};

TTouchWorkplace code MenuLimitsWorkplace = {
  MenuLimitsButtons,
  ArrayLength(MenuLimitsButtons)
};

void MenuLimits () {
   // Menu nastaveni mezi
   // Vykreslim polozky menu
   DrawMenuItems (STR_MEZE, STR_MENU_LIMITS_ITEMS, ArrayLength(STR_MENU_LIMITS_ITEMS), 0);
   DisplayPlane (YES);
   while (1) {
      switch (WaitForKey ()) {
      case K_TIMEOUT: {
         BeepKey ();
         return;
      }
      case K_TOUCH: {
         switch (TouchCtlCheck (&MenuLimitsWorkplace)) {
         case ID_MENU_LIMITS_TEMPERATURE: {
            PrepareMenu ();
            MenuOffset ();
            return;
         }
         case ID_MENU_LIMITS_AIR_FILTERS: {
            PrepareMenu ();
            if (MenuPassword (NO)) {
               PrepareMenu ();
               MenuAirFiltersPressure ();
            }
            return;
         }
         case ID_MENU_LIMITS_CO2: {
            PrepareMenu ();
            if (MenuPassword (NO)) {
               PrepareMenu ();
               MenuCo2Limit ();
            }
            return;
         }
         case ID_MENU_LIMITS_CANCEL: {
            BeepKey ();
            return;
         }
         }//switch
         break;
      }
      }//switch
   }//while
}

#endif // __TMCREMOTE__

//-----------------------------------------------------------------------------
// Menu diesel
//-----------------------------------------------------------------------------

#if !defined(__TMCREMOTE__) && !defined(NO_DIESEL)

void MenuDieselInfo () {
   // Zobrazi informace o dieselu
   DisplayClear ();
   DisplayPlane (NO);
   DisplaySetFont (FONT_NUM_TAHOMA18);

   // Motohodiny
   DisplayStringTahoma18 (10, 10, STR_MOTOHODINY);
   DisplayMoveTo (23, 50);
   cprintdec (Diesel.MotoHours, FMT_UNSIGNED | 5);
   DisplayStringTahoma18 (275, 45, STR_HODIN);

   // Vymena oleje
   DisplayStringTahoma18 (10, 100, STR_VYMENA_OLEJE_PRI);
   DisplayMoveTo (23, 140);
   cprintdec (Config.ChangeOilAtHours, FMT_UNSIGNED | 5);
   DisplayStringTahoma18 (275, 135, STR_HODIN);

   DisplayPlane (YES);
   DisplayOk ();
   DisplayExecuteButtons (1);
}

void MenuDieselChangeOil () {
   // Provede vymenu oleje
   if (!AskQuestion (STR_VYMENA_OLEJE_1, STR_VYMENA_OLEJE_2)) {
      return;
   }
   DieselQuitOil ();
   LogWriteProtocol (PRT_VYMENA_OLEJE, 0);
}

enum {
   ID_MENU_DIESEL_TYPE_KUBOTA = 0,    // Polozky odpovidaji TDieselType
   ID_MENU_DIESEL_TYPE_DEUTZ = 1,
   ID_MENU_DIESEL_TYPE_CANCEL   
};

TTouchButton code MenuDieselTypeButtons[] = {
  { // Kubota
    ID_MENU_DIESEL_TYPE_KUBOTA,
    0,
    MENU_YPrvniPolozky + (ID_MENU_DIESEL_TYPE_KUBOTA + 1) * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Deutz
    ID_MENU_DIESEL_TYPE_DEUTZ,
    0,
    MENU_YPrvniPolozky + (ID_MENU_DIESEL_TYPE_DEUTZ + 1) * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Zrusit
    ID_MENU_DIESEL_TYPE_CANCEL,
    0,
    CANCEL_Y,
    MENU_POLOZKA_DX,
    DISPLAY_HEIGHT - (CANCEL_Y),
    NO
  }
};

TTouchWorkplace code MenuDieselTypeWorkplace = {
  MenuDieselTypeButtons,
  ArrayLength(MenuDieselTypeButtons)
};

byte MenuDieselType () {
   byte  NewType;

   DrawMenuItems (STR_ZADEJTE_TYP, STR_MENU_DIESEL_TYPE_ITEMS, ArrayLength(STR_MENU_DIESEL_TYPE_ITEMS), DX_MINUTA);
   // Sipka u aktualniho typu
   DisplaySymbol (SIPKA_OFFSET_X, MENU_YPrvniPolozky + (Config.DieselType + 1) * MENU_POLOZKA_DY + SIPKA_OFFSET_Y, SYMBOL_SIPKA_VPRAVO);
   DisplayPlane (YES);
   while (1) {
      switch (WaitForKey ()) {
      case K_TIMEOUT: {
         BeepKey ();
         return K_TIMEOUT;
      }
      case K_TOUCH: {
         NewType = TouchCtlCheck (&MenuDieselTypeWorkplace);
         switch (NewType) {
         case ID_MENU_DIESEL_TYPE_KUBOTA:
         case ID_MENU_DIESEL_TYPE_DEUTZ:
            BeepKey ();
            return NewType;

         case ID_MENU_DIESEL_TYPE_CANCEL:
            BeepKey ();
            return K_ESC;
         }
         break;
      }
      }//switch
   }//while
}

void MenuDieselChangeOilPeriod () {
   while (1) {
      DrawOneLineNumPad (STR_ZADEJTE_PERIODU_VYMENY_OLEJE, NO);
      DisplayStringTahoma18 (146, 90, STR_HODIN);
      UN.DT = Config.ChangeOilPeriod;
      if (!DisplayExecuteNumPad (10, 90, 3, NO)) {
         return;
      }
      // Zmacknul Enter
      if (UN.Word.Lsb > 0) {      // Zadava se na 3 mista, tj. maximum neni treba kontrolovat
        // Zadana hodnota je v poradku, ulozim ji
         Config.ChangeOilPeriod = UN.Word.Lsb;
         CfgSave ();
         LogWriteProtocol (PRT_NASTAVENI_PERIODY_OLEJE, Config.ChangeOilPeriod);
         break;
      }
      else {
         // Zadana teplota je mimo rozsah
         if (!MenuInvalidValue ()) {
            return;  // Nechce zadat znovu
         }
         // Pokud stisknul Enter, pokracuju v cyklu, aby zadal teplotu znovu
      }//else
   }//while
}

void MenuDieselSettings () {
   UN.ST.X4 = MenuDieselType ();
   if (UN.ST.X4 == K_ESC) {
      return;
   }
   Config.DieselType = UN.ST.X4;

   // Podle vybraneho typu automaticky prednastavim periodu vymeny oleje a ulozim
   Config.ChangeOilPeriod = Config.DieselType == DIESEL_TYPE_KUBOTA ? DEFAULT_OIL_PERIOD_KUBOTA : DEFAULT_OIL_PERIOD_DEUTZ;
   CfgSave ();

   // Umoznim mu prednastavenou periodu zmenit
   MenuDieselChangeOilPeriod ();
}

static TYesNo EditMotohours (byte code *title) {
   while (1) {
      DrawOneLineNumPad (title, NO);
      if (!DisplayExecuteNumPad (7, 90, 5, 0)) {
         return NO;
      }
      // Zmacknul Enter
      if (UN.DT <= 0xFFFF) {
         return YES;
      }
      else {
         // Zadana hodnota je mimo rozsah
         if (!MenuInvalidValue ()) {
            return NO;  // Nechce zadat znovu
         }
         // Pokud stisknul Enter, pokracuju v cyklu, aby zadal hodnotu znovu
      }
   }
}

void MenuDieselSetMotohours () {
   // Nastavi motohodiny a vymenu oleje
   word NewMotohours;

   // Motohodiny
   UN.DT = Diesel.MotoHours;
   if (!EditMotohours (STR_MOTOHODINY)) {
      return;
   }
   NewMotohours = UN.Word.Lsb;

   // Vymena oleje pri urcitem stavu motohodin
   UN.DT = NewMotohours;
   if (!EditMotohours (STR_VYMENA_OLEJE_PRI)) {
      return;
   }
   DieselSet (NewMotohours, UN.Word.Lsb);
   LogWriteProtocol (PRT_SMAZANI_MOTOHODIN, 0);      // Prozatim ponecham nulovani motohodin
}

enum {
   ID_MENU_DIESEL_INFO,
   ID_MENU_DIESEL_CHANGE_OIL,
   ID_MENU_DIESEL_SETTINGS,
   ID_MENU_DIESEL_ZERO_MOTOHOURS,
   ID_MENU_DIESEL_CANCEL
};

TTouchButton code MenuDieselButtons[] = {
  { // Informace
    ID_MENU_DIESEL_INFO,
    0,
    MENU_YPrvniPolozky + (ID_MENU_DIESEL_INFO + 1) * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Vymena oleje
    ID_MENU_DIESEL_CHANGE_OIL,
    0,
    MENU_YPrvniPolozky + (ID_MENU_DIESEL_CHANGE_OIL + 1) * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Nastaveni
    ID_MENU_DIESEL_SETTINGS,
    0,
    MENU_YPrvniPolozky + (ID_MENU_DIESEL_SETTINGS+1)  * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Nulovani motohodin
    ID_MENU_DIESEL_ZERO_MOTOHOURS,
    0,
    MENU_YPrvniPolozky + (ID_MENU_DIESEL_ZERO_MOTOHOURS + 1) * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Zrusit
    ID_MENU_DIESEL_CANCEL,
    0,
    CANCEL_Y,
    MENU_POLOZKA_DX,
    DISPLAY_HEIGHT - (CANCEL_Y),
    NO
  }
};

TTouchWorkplace code MenuDieselWorkplace = {
  MenuDieselButtons,
  ArrayLength(MenuDieselButtons)
};

void MenuDiesel () {
   // Menu nastaveni dieselu
   // Vykreslim polozky menu
   DrawMenuItems (STR_DIESEL, STR_MENU_DIESEL_ITEMS, ArrayLength(STR_MENU_DIESEL_ITEMS), 0);
   DisplayPlane (YES);
   while (1) {
      switch (WaitForKey ()) {
      case K_TIMEOUT: {
         BeepKey ();
         return;
      }
      case K_TOUCH: {
         switch (TouchCtlCheck (&MenuDieselWorkplace)) {
         case ID_MENU_DIESEL_INFO: {
            PrepareMenu ();
            MenuDieselInfo ();
            return;
         }
         case ID_MENU_DIESEL_CHANGE_OIL: {
            PrepareMenu ();
            MenuDieselChangeOil ();
            return;
         }
         case ID_MENU_DIESEL_SETTINGS: {
            PrepareMenu ();
            if (MenuPassword (YES)) {
               PrepareMenu ();
               MenuDieselSettings ();
            }
            return;
         }
         case ID_MENU_DIESEL_ZERO_MOTOHOURS: {
            PrepareMenu ();
            if (MenuPassword (YES)) {            // Spec. heslo
               PrepareMenu ();
               MenuDieselSetMotohours ();
            }
            return;
         }
         case ID_MENU_DIESEL_CANCEL: {
            BeepKey ();
            return;
         }
         }//switch
         break;
      }
      }//switch
   }//while
}

#endif // __TMCREMOTE__

//-----------------------------------------------------------------------------
// Menu nastaveni
//-----------------------------------------------------------------------------

#ifndef __TMCREMOTE__

void MenuUseLogin () {
   Config.Login = AskQuestion (STR_POUZIVAT_LOGIN1, STR_POUZIVAT_LOGIN2);
   CfgSave ();
   LogWriteProtocol (PRT_NASTAVENI_LOGIN, Config.Login);
}

#endif // __TMCREMOTE__

enum {
   ID_MENU_SETUP_LIMITS,
#if !defined(NO_DIESEL)
   ID_MENU_SETUP_DIESEL,
#endif
   ID_MENU_SETUP_LOGIN,
   ID_MENU_SETUP_REGULATOR,
   ID_MENU_SETUP_CANCEL
};

#ifndef __TMCREMOTE__

TTouchButton code MenuSetupButtons[] = {
  { // Meze
    ID_MENU_SETUP_LIMITS,
    0,
    MENU_YPrvniPolozky + (ID_MENU_SETUP_LIMITS + 1) * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
#if !defined(NO_DIESEL)
  { // Diesel
    ID_MENU_SETUP_DIESEL,
    0,
    MENU_YPrvniPolozky + (ID_MENU_SETUP_DIESEL + 1) * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
#endif
  { // Login
    ID_MENU_SETUP_LOGIN,
    0,
    MENU_YPrvniPolozky + (ID_MENU_SETUP_LOGIN + 1) * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Regulator
    ID_MENU_SETUP_REGULATOR,
    0,
    MENU_YPrvniPolozky + (ID_MENU_SETUP_REGULATOR + 1) * MENU_POLOZKA_DY,
    MENU_POLOZKA_DX,
    MENU_POLOZKA_DY,
    NO
  },
  { // Zrusit
    ID_MENU_SETUP_CANCEL,
    0,
    CANCEL_Y,
    MENU_POLOZKA_DX,
    DISPLAY_HEIGHT - (CANCEL_Y),
    NO
  }
};

#else

TTouchButton code MenuSetupButtons[] = {
  { // Zrusit
    ID_MENU_SETUP_CANCEL,
    0,
    CANCEL_Y,
    MENU_POLOZKA_DX,
    DISPLAY_HEIGHT - (CANCEL_Y),
    NO
  }
};

#endif // __TMCREMOTE__

TTouchWorkplace code MenuSetupWorkplace = {
  MenuSetupButtons,
  ArrayLength(MenuSetupButtons)
};

#ifndef __TMCREMOTE__

void MenuSetup () {
   // Menu nastaveni
   // Vykreslim polozky menu
   DrawMenuItems (STR_NASTAVENI, STR_MENU_SETUP_ITEMS, ArrayLength(STR_MENU_SETUP_ITEMS), 0);
   DisplayPlane (YES);
   while (1) {
      switch (WaitForKey ()) {
      case K_TIMEOUT: {
         BeepKey ();
         return;
      }
      case K_TOUCH: {
         switch (TouchCtlCheck (&MenuSetupWorkplace)) {
         case ID_MENU_SETUP_LIMITS: {
            PrepareMenu ();
            MenuLimits ();
            return;
         }
#if !defined(NO_DIESEL)
         case ID_MENU_SETUP_DIESEL: {
            PrepareMenu ();
            MenuDiesel ();
            return;
         }
#endif
         case ID_MENU_SETUP_LOGIN: {
            PrepareMenu ();
            if (MenuPassword (NO)) {
               PrepareMenu ();
               MenuUseLogin ();
            }
            return;
         }
         case ID_MENU_SETUP_REGULATOR: {
            PrepareMenu ();
            if (MenuPassword (YES)) {
               PrepareMenu ();
               MenuRegulator ();
            }
            return;
         }
         case ID_MENU_SETUP_CANCEL: {
            BeepKey ();
            return;
         }
         }//switch
         break;
      }
      }//switch
   }//while
}

#endif // __TMCREMOTE__

//-----------------------------------------------------------------------------
// Tisk protokolu
//-----------------------------------------------------------------------------

#ifndef __TMCREMOTE__

static dword UlozDatum () {
   // Vrati datum v UN.DT tak, abych ho mel ve formatu RRRRMMDD, tj. napr. 20021231 pro 31.12.2002
   byte day, month;
   word year;

   UN.DT = dbin2bcd (UN.DT);
   day = bbcd2bin (UN.ST.X1);
   month = bbcd2bin (UN.ST.X2);
   year = wbcd2bin (UN.Word.Lsb);

   UN.Word.Msb = wbin2bcd (year);
   UN.ST.X3 = bbin2bcd (month);
   UN.ST.X4 = bbin2bcd (day);
   return dbcd2bin (UN.DT);
}

/*static dword UlozDatum() {
  // Ulozi datum tak, abych ho mel ve formatu RRRRMMDD, tj. napr. 20021231 pro 31.12.2002
  dword Date;

  UN.DT = bindec(UN.DT);  // Dam si to do BCD - potrebuju totiz zprehazet poradi roku, mesice a dne
  // Ulozim si datum tak, abych ho mel ve formaty RRRRMMDD, tj. napr. 20021231 pro 31.12.2002
  Date = (word)(1000 * (word)hnib(UN.ST.X3) + 100 * (word)lnib(UN.ST.X3) + 10 * (word)hnib(UN.ST.X4) + (word)lnib(UN.ST.X4));  // Rok
  Date *= 10000L;  // Posunu rok na prvni pozici
  Date += (word)(1000 * (word)hnib(UN.ST.X2) + 100 * (word)lnib(UN.ST.X2));  // Mesic dam za rok
  Date += (word)(10 * (word)hnib(UN.ST.X1) + (word)lnib(UN.ST.X1));  // A nakonec den
  return Date;
}*/

void MenuPrintProtocol () {
   // Vytiskne zaznamy za zvolene obdobi

   // Typ protokolu


   // Pocatecni datum
   if (!EditDate (STR_ZADEJTE_POCATECNI_DATUM1, STR_ZADEJTE_POCATECNI_DATUM2)) {
      return;
   }
   PocatecniDatum = UlozDatum ();

   // Pocatecni cas
   if (!EditTime (STR_ZADEJTE_POCATECNI_CAS1, STR_ZADEJTE_POCATECNI_CAS2)) {
      return;
   }
   PocatecniCas = UN.Word.Lsb;  // Je to urcite jen v lsb

   // Koncove datum
   if (!EditDate (STR_ZADEJTE_KONCOVE_DATUM1, STR_ZADEJTE_KONCOVE_DATUM2)) {
      return;
   }
   KoncoveDatum = UlozDatum ();

   // Koncovy cas
   if (!EditTime (STR_ZADEJTE_KONCOVY_CAS1, STR_ZADEJTE_KONCOVY_CAS2)) {
      return;
   }
   KoncovyCas = UN.Word.Lsb;  // Je to urcite jen v lsb

   // Zeptam se na pocet zaznamu na radek
   while (1) {
      DrawTwoLinesNumPad (STR_ZADEJTE_ZAZNAMY_NA_RADEK1, STR_ZADEJTE_ZAZNAMY_NA_RADEK2, NO);
      UN.DT = 3;    // Default 3 zaznamy na radek
      if (!DisplayExecuteNumPad (11, 90, 1, NO)) {
         return;
      }
      // Zmacknul Enter
      DisplayClear ();
      if (UN.ST.X4 > 0) {
         // Zadany pocet radku je v poradku
         PocetZaznamuNaRadek = UN.ST.X4;
         break;
      }
      else {
         // Zadana perioda je mimo rozsah
         if (!MenuInvalidValue ()) {
            return;  // Nechce zadat znovu
         }
         // Pokud stisknul Enter, pokracuju v cyklu, aby zadal hodnotu znovu
      }//else
   }//while

   // Nacitam jednotlive zaznamy
   MenuWait ();

   if (!PrintPrepare ()) {
      return;      // prazdne FIFO
   }
   // Zastavim ukladani zaznamu do pameti - v prubehu tisku to meri a reguluje (volam SysYield()), ale nechci, aby se mne menily zaznamy
   // Pri odchodu z tisku to zase rozjet => za timto radkem nesmi byt nikde return.
   LogPause ();                 // pozastaveni zaznamu
   while (1) {    // Muze tisknout libovolny pocet kopii
      PrintStart ();               // obsazeni linky
      PrintProtocol ();
      PrintStop ();                // uvolneni linky
      // Zeptam se ho, zda chce tisknout dalsi kopii
      // Bohuzel se pri cekani na odpoved zase meri (vola se SysYield()) a muze se i ukladat, takze se to muze drobet zmenit (cekam ale max. 30sec, takze minimalne)
      if (!AskQuestion (STR_TISKNOUT_KOPII1, STR_TISKNOUT_KOPII2)) {
         break;  // Nechce tisknout kopii
      }
      // Pokud dosel sem, chce tisknout kopii
      MenuWait ();
   }//while
   // Rozjedu znovu ukladani
   LogResume ();                // uvolneni zaznamu
   // Zaprotokoluji, ze jsem tisknul
   // Neprotokoluji jednotlive kopie, protoze kdyby tisknul celou pamet apamet by byla v prepisu, tak zapisem protokolu z 1. vytisku by se cela pamet
   // posunula (smazal by se nejstarsi zaznam) a kopie by pak byla odlisna od 1. vytisku.
   LogWriteProtocol (PRT_TISK_ZAZNAMU, 0);
}

#endif //__TMCREMOTE__

//-----------------------------------------------------------------------------
// Reakce na stisk touch panelu v hlavnim zobrazeni
//-----------------------------------------------------------------------------

enum {
   ID_MAIN_FRESH_AIR,
   ID_MAIN_FRONT_FLAPS,
#ifndef USE_TWO_SENSORS
   ID_MAIN_REAR_FLAPS,
#endif // USE_TWO_SENSORS
   ID_MAIN_TIME,
   ID_MAIN_MODE,
   ID_MAIN_TARGET_TEMPERATURE,
   ID_MAIN_MENU,
   ID_MAIN_DIAGNOSTIC
};

TTouchButton code MainButtons[] = {
  { // Recirkulace
    ID_MAIN_FRESH_AIR,
    1 * 8,
    FLAPS_Y2 - 10,
    (GRID_X1 - 2) * 8,
    40,
    NO
  },
  { // Predni podlahove klapky
    ID_MAIN_FRONT_FLAPS,
    (GRID_X1 + 1) * 8,
    FLOOR_FLAPS_Y1 - 10,
    (GRID_X2 - GRID_X1 - 2) * 8,
    40,
    NO
  },
#ifndef USE_TWO_SENSORS
  { // Zadni podlahove klapky
    ID_MAIN_REAR_FLAPS,
    (GRID_X1 + 1) * 8,
    FLOOR_FLAPS_Y2 - 10,
    (GRID_X2 - GRID_X1 - 2) * 8,
    40,
    NO
  },
#endif // USE_TWO_SENSORS
  { // Datum a cas
    ID_MAIN_TIME,
    (TIME_X) * 8,
    TIME_Y,
    (DISPLAY_WIDTH / 8 - TIME_X - 2) * 8,
    43,
    NO
  },
  { // Rezim
    ID_MAIN_MODE,
    (MODE_X1) * 8,
    MODE_Y + 2,
    (DISPLAY_WIDTH / 8 - MODE_X1 - 2) * 8,
    44,
    NO
  },
  { // Cilova teplota
    ID_MAIN_TARGET_TEMPERATURE,
    (GRID_X2 + 1) * 8,
    TARGET_TEMPERATURE_Y + 2,
    (DISPLAY_WIDTH / 8 - GRID_X2 - 2) * 8,
    44,
    NO
  },
  { // Menu
    ID_MAIN_MENU,
    (GRID_X2 + 1) * 8,
    MENU_Y + 2,
    (DISPLAY_WIDTH / 8 - GRID_X2 - 2) * 8,
    DISPLAY_HEIGHT - MENU_Y - 2,
    NO
  },
  { // Diagnostika
    ID_MAIN_DIAGNOSTIC,
    0,
    0,
    50,
    50,
    NO
  }
};

TTouchWorkplace code MainWorkplace = {
  MainButtons,
  ArrayLength(MainButtons)
};

static void PrepareMenu () {
   // Provede nezbytne ukony pred vstupem do menu
   BeepKey ();
   MenuBlink (YES);     // Pro menu trvale rozsvitit
   ZerKlavesu ();
}

void MenuTouch () {
   // Zpracuje stisk touch panelu v hlavnim zobrazeni

   // Lze ovladat pouze v pripade, ze se prave zobrazuji teploty
   if (DisplayMode != DISPLAY_TEMP) {
      return;
   }

   switch (TouchCtlCheck (&MainWorkplace)) {

   case ID_MAIN_FRESH_AIR: {
      if (Control.EmergencyMode) {
         break;          // V nouzovem rezimu je neaktivni
      }
#ifdef __TMCREMOTE__
      if (CommunicationError) {
         break;          // Pri chybe komunikace je neaktivni
      }
#endif // __TMCREMOTE__
      PrepareMenu ();
      MenuFreshAir ();
      Redraw = REDRAW_ALL;
      MenuRedraw ();
      break;
   }

#if defined(USE_FLOOR_FLAPS) && defined(USE_FLOOR_FLAPS_CONTROL)
#ifdef USE_TWO_SENSORS
   case ID_MAIN_FRONT_FLAPS: {
#else
   case ID_MAIN_FRONT_FLAPS:
   case ID_MAIN_REAR_FLAPS: {
#endif // USE_TWO_SENSORS
      if (Control.EmergencyMode) {
         break;          // V nouzovem rezimu je neaktivni
      }
#ifdef __TMCREMOTE__
      if (CommunicationError) {
         break;          // Pri chybe komunikace je neaktivni
      }
#endif // __TMCREMOTE__
      PrepareMenu ();
      MenuFloorFlaps ();
      Redraw = REDRAW_ALL;
      MenuRedraw ();
      break;
   }
#endif // defined(USE_FLOOR_FLAPS) && defined(USE_FLOOR_FLAPS_CONTROL)

   case ID_MAIN_TIME: {
#ifdef __TMCREMOTE__
      if (CommunicationError) {
         break;          // Pri chybe komunikace je neaktivni
      }
#endif // __TMCREMOTE__
      PrepareMenu ();
      MenuDateTime ();
      Redraw = REDRAW_ALL;
      MenuRedraw ();
      break;
   }

   case ID_MAIN_MODE: {
      if (Control.EmergencyMode) {
         break;          // V nouzovem rezimu je neaktivni
      }
#ifdef __TMCREMOTE__
      if (CommunicationError) {
         break;          // Pri chybe komunikace je neaktivni
      }
#endif // __TMCREMOTE__
      PrepareMenu ();
      MenuMode ();
      Redraw = REDRAW_ALL;
      MenuRedraw ();
      break;
   }

   case ID_MAIN_TARGET_TEMPERATURE: {
      if (Control.EmergencyMode) {
         break;          // V nouzovem rezimu je neaktivni
      }
#ifdef __TMCREMOTE__
      if (CommunicationError) {
         break;          // Pri chybe komunikace je neaktivni
      }
#endif // __TMCREMOTE__
      PrepareMenu ();
      MenuTargetTemperature ();
      MenuLocalization ();
      Redraw = REDRAW_ALL;
      MenuRedraw ();
      break;
   }

   case ID_MAIN_MENU: {
      PrepareMenu ();
      MenuMain ();
      Redraw = REDRAW_ALL;
      MenuRedraw ();
      break;
   }

   case ID_MAIN_DIAGNOSTIC: {
      if (DiagnosticCounter < MAX_DIAGNOSTIC_COUNTER) {
         BeepKey ();
         DiagnosticMode = YES;
         DiagnosticCounter = MAX_DIAGNOSTIC_COUNTER;     // Zneaktivni volbu
      }
      break;
   }
   }//switch
}

