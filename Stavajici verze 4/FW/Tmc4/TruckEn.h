//*****************************************************************************
//
//    TruckEn.c - Beh motoru vozidla
//    Version 1.0
//
//*****************************************************************************


#ifndef __TruckEn_H__
   #define __TruckEn_H__

#include "Hardware.h"     // pro podminenou kompilaci

extern byte __xdata__ TruckEngineOn;    // YES/NO nastartovany motor vozidla


void TruckEngineExecute();
  // Nacteni behu motoru vozidla

#endif
