//*****************************************************************************
//
//    Charging.c - Dobijeni akumulatoru
//    Version 1.0
//
//*****************************************************************************

#include "Charging.h"
#include "Din.h"               // Digitalni vstupy
#include "Diesel.h"            // Diesel motor
#include "Alarm.h"             // Indikace poruch

TCharging __xdata__ Charging;

#ifndef __TMCREMOTE__

// Promenne pro poruchu
static byte __xdata__ FaultCounter = 0; // Pocitadlo trvani poruchy

//-----------------------------------------------------------------------------
// Nacteni dobijeni
//-----------------------------------------------------------------------------

void ChargingExecute() {
  // Nacteni dobijeni

  // Dobijeni
  Charging.On = (TYesNo)(DinCharging == DIN_CHARGING_ON);

  // Porucha dobijeni - z chodu dieselu a nabijeni aku odvodim poruchu dobijeni aku. Nefunguje pri behu elektromotoru.
  Charging.Failure = NO;        // Default bez chyby
  AlarmCheckFaultCounter(Diesel.On && !Charging.On, &FaultCounter, &Charging.Failure);
}

#endif // __TMCREMOTE__
