//*****************************************************************************
//
//    Hardware.h  - TMC hardware descriptions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

//-----------------------------------------------------------------------------
// Jazyk
//-----------------------------------------------------------------------------
#if !defined(LANG_CZ) && !defined(LANG_PL) && !defined(LANG_EN) && !defined(LANG_RU) && !defined(LANG_RO) && !defined(LANG_GE) && !defined(LANG_SW) && !defined(LANG_HU)
#define LANG_CZ 1
//#define LANG_PL 1
//#define LANG_EN 1
//#define LANG_RU 1
//#define LANG_RO 1
//#define LANG_GE 1
//#define LANG_SW 1
//#define LANG_HU 1
#endif

#define MANUFACTURE_PASSWORD_1 280677
#define MANUFACTURE_PASSWORD_2 281018

#include "89C51ED2.h"
#include "Rd2\Rd2.h"
#include "cpu.h"
#include "uni.h"
#include <intrins.h>
#include <datconv.h>
#include <absacc.h>             // makro XBYTE
#include <stddef.h>             // makro offsetof
#include "LogData.h"           // Struktura pro ukladani
#include "HwDef.h"             // Definice jednotlivych HW konfiguraci aut

#define __ATTY__       1        // prekladat vysilac terminalu


//-----------------------------------------------------------------------------
// Globalni definice
//-----------------------------------------------------------------------------

#define TmcAccuOk()   TmcLOWBAT

// spolecne vodice (podle schematu) :

// Port 0 :

#define TmcDATA      P0                // datova sbernice
// datove bity :
sbit TmcD0      = TmcDATA^0;           // D0
sbit TmcD1      = TmcDATA^1;           // D1
sbit TmcD2      = TmcDATA^2;           // D2
sbit TmcD3      = TmcDATA^3;           // D3
sbit TmcD4      = TmcDATA^4;           // D4
sbit TmcD5      = TmcDATA^5;           // D5
sbit TmcD6      = TmcDATA^6;           // D6
sbit TmcD7      = TmcDATA^7;           // D7

// Port 1 :
sbit TmcTP1     = P1^0;                // TP1 - teplomer Rx
sbit TmcTP3     = P1^1;                // TP3 - teplomer Rx
sbit TmcTP4     = P1^2;                // TP4 - teplomer Rx
sbit TmcTP5     = P1^3;                // TP5 - teplomer Rx
sbit TmcRX2     = P1^4;                // RX2 - VOLNO
sbit TmcTX2     = P1^5;                // TX2 - VOLNO
sbit TmcTX3     = P1^6;                // TX3 - vysilac tiskarna
sbit TmcSI      = P1^7;                // SI  - EEPROM, MC33298, ADS7846

// Port 2 :
sbit TmcMAJAK   = P2^0;                // MAJAK
sbit TmcCLKAD   = P2^1;                // CLKAD - hodiny TLC0838 + WR displeje
sbit TmcDAD     = P2^2;                // DAD   - data TLC0838
sbit TmcOUTDI   = P2^3;                // OUTDI - vystup 74HC165
sbit TmcPISK    = P2^4;                // PISK  - sirena
sbit TmcSCL     = P2^5;                // SCK   - I2C clock (RX8025, TDA8444)
sbit TmcSCKM    = P2^6;                // SCKM  - hodiny modulu
sbit TmcCSD     = P2^7;                // CSD   - chipselect display

// Port 3 :
sbit TmcRXD     = P3^0;                // RxD   - prijimac ADM202/Flip
sbit TmcTXD     = P3^1;                // TxD   - vysilac  ADM202/Flip
sbit TmcPENIN   = P3^2;                // PENIN - INT0, preruseni ADS7846
sbit TmcSCK     = P3^3;                // SCK - EEPROM, MC33298, ADS7846
sbit TmcCSEE    = P3^4;                // CSEE  - chipselect EEPROM
sbit TmcCSDO    = P3^5;                // CSDO  - chipselect MC33298
sbit TmcCS1AD   = P3^6;                // CS1AD - chipselect TLC0838
sbit TmcCSM     = P3^7;                // CSM   - chipselect modulu

// Port 4 :
sbit TmcTP2     = P4^0;                // TP2   - teplomer Rx
sbit TmcTP6     = P4^1;                // TP6   - teplomer Rx
sbit TmcTP7     = P4^2;                // TP7   - teplomer Rx
sbit TmcCSPAN   = P4^3;                // CSPAN - chipselect ADS7846
sbit TmcSO      = P4^4;                // SO  - EEPROM, MC33298, ADS7846
sbit TmcSOM     = P4^5;                // SOM   - data modulu
sbit TmcRESD    = P4^6;                // RESD  - reset display
sbit TmcCS2AD   = P4^7;                // CS2AD - chipselect TLC0838

// Port 5 :
sbit TmcCLKDI   = P5^0;                // CLKDI  - hodiny 74HC165
sbit TmcSDA     = P5^1;                // SDA    - I2C clock (RX8025, TDA8444)
sbit TmcLOWBAT  = P5^2;                // LOWBAT - kontrola napajeni
sbit TmcSVIT    = P5^3;                // SVIT   - podsvit display
sbit TmcCSEPOT  = P5^4;                // CSEPOT - chipselect potenciometru
sbit TmcPULLTP  = P5^5;                // PULLTP - tvrdy pullup pro cidla
sbit TmcA0D     = P5^6;                // A0D    - A0 display
sbit TmcBUZTP   = P5^7;                // BUZTP  - teplomery Tx


//-----------------------------------------------------------------------------
// Prirazeni vstupu periferii
//-----------------------------------------------------------------------------

// Vstupy citace PCA :
#define TmcCEX0  TmcTP5                // Modul 0
#define TmcCEX1  TmcRX2                // Modul 1
#define TmcCEX2  TmcSO                 // Modul 2
#define TmcCEX3  TmcSCK                // Modul 3

// Adresy A/D prevodniku 1 :
#define TmcAV1   ADC_SINGLE_CH0        // A/D prevodnik 1/0 LP
#define TmcAV2   ADC_SINGLE_CH1        // A/D prevodnik 1/1 CHKFLOOR
#define TmcAV3   ADC_SINGLE_CH2        // A/D prevodnik 1/2 HP
#define TmcAV4   ADC_SINGLE_CH3        // A/D prevodnik 1/3 HEATTEMP
#define TmcAV5   ADC_SINGLE_CH4        // A/D prevodnik 1/4 CO2
#define TmcAV6   ADC_SINGLE_CH5        // A/D prevodnik 1/5 HUM
#define TmcAV7   ADC_SINGLE_CH6        // A/D prevodnik 1/6 MTEMP
#define TmcAV8   ADC_SINGLE_CH7        // A/D prevodnik 1/7 100/50% FAN

// Adresy A/D prevodniku 2 :
#define TmcAV910 ADC_DIFF_CH0          // A/D prevodnik 2/0,1 
#define TmcAV9   ADC_SINGLE_CH0        // A/D prevodnik 2/0 PRESSFILTER
#define TmcAV10  ADC_SINGLE_CH1        // A/D prevodnik 2/1 DIS_READY
#define TmcAV11  ADC_SINGLE_CH2        // A/D prevodnik 2/2 24V
#define TmcAV12  ADC_SINGLE_CH3        // A/D prevodnik 2/3 CHKHEAT
#define TmcAV13  ADC_SINGLE_CH4        // A/D prevodnik 2/4 CHKI1
#define TmcAV14  ADC_SINGLE_CH5        // A/D prevodnik 2/5 CHKI2
#define TmcAV15  ADC_SINGLE_CH6        // A/D prevodnik 2/6 CHKREC
#define TmcAV16  ADC_SINGLE_CH7        // A/D prevodnik 2/7 GND Offset

//#define TmcAVDIF ADC_DIFF_CH0          // A/D prevodnik 2/0,1 diferencialni vstup
//#define TmcAVAKU TmcAV11               // A/D prevodnik mereni aku 24V

// Adresy D/A prevodniku :
#define TmcAVY5   4                    // D/A prevodnik 4
#define TmcAVY6   5                    // D/A prevodnik 5
#define TmcAVY7   6                    // D/A prevodnik 6
#define TmcAVY8   7                    // D/A prevodnik 7

#define TmcAVY12  0                    // D/A prevodnik 1+2
#define TmcAVY34  2                    // D/A prevodnik 3+4

// Digitalni vstupy :
#define TmcDI1    0x01                 // vstup DI1 (D0) ALT
#define TmcDI2    0x02                 // vstup DI2 (D1) MOTOR
#define TmcDI3    0x04                 // vstup DI3 (D2) EMERGENCY
#define TmcDI4    0x08                 // vstup DI4 (D3) FAILH/C
#define TmcDI5    0x10                 // vstup DI5 (D4) FLAME1
#define TmcDI6    0x20                 // vstup DI6 (D5) FAILVENT
#define TmcDI7    0x40                 // vstup DI7 (D6) D+
#define TmcDI8    0x80                 // vstup DI8 (D7) FLAME2

// Digitalni vystupy :
#define TmcDVY1   0x01                 // vystup DVY1 (OP0) DISINFECTION POWER
#define TmcDVY2   0x02                 // vystup DVY2 (OP1) DISINFECTION BODY
#define TmcDVY3   0x04                 // vystup DVY3 (OP2) DISINFECTION WHEELS
#define TmcDVY4   0x08                 // vystup DVY4 (OP3) SIREN
#define TmcDVY5   0x10                 // vystup DVY5 (OP4) COOL
#define TmcDVY6   0x20                 // vystup DVY6 (OP5) HEAT
#define TmcDVY7   0x40                 // vystup DVY7 (OP6) VENTILATION
#define TmcDVY8   0x80                 // vystup DVY8 (OP7) PUMP

//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 18.432 MHz

#define FXTAL 18432000L

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :
// Pozor, pokud se pouziva dynamicke rizeni CPU_X2, vsechny ostatni zarizeni musi byt v X1 modu (T0_X2, T1_X2 atd. musi byt nastaveny na 1). Pokud
// jsou ostatni zarizeni nastavene v X2 modu, po prepnuti procesoru do X1 modu se prepnou soucasne i zarizeni. Napr. SPI je stale v X2 a prepina se do X1.
#define CPU_X2     2                   // procesor v rezimu X2
#define T0_X2      1                   // timer 0 v rezimu X2
#define T1_X2      1                   // timer 1 v rezimu X2
#define T2_X2      1                   // timer 2 v rezimu X2
#define SI_X2      1                   // COM v rezimu X2
#define PCA_X2     1                   // PCA v rezimu X2
#define WD_X2      1                   // WatchDog v rezimu X2
#define SPI_X2     1                   // SPI v rezimu X2 (zatim neni implementovano, bezi stale v X2 na max. rychlost)

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :

#define DOUBLE_BAUDRATE  1             // zdvojena baudova rychlost (1=ne, 2=ano)

// Casovac 0 :

#define TIMER0_PERIOD 20               // perioda casovace 0 v ms
#define TIMER1_PERIOD 1                // perioda casovace 1 v ms

#define __xdata__ xdata                // pouziva se XDATA - interni XRAM

//-----------------------------------------------------------------------------
// Tonovy generator PCA
//-----------------------------------------------------------------------------

#define PcaSoundOut      TmcPISK       // vystup generatoru

// PCA Modul 3 :
#define SOUND_CCAPM      CCAPM3
#define SOUND_COUNTER_L  CCAP3L
#define SOUND_COUNTER_H  CCAP3H
#define SOUND_FLAG       CCF3


// Podminena kompilace :

#define PCA_SOUND        1             // povoleni zvukoveho vystupu
#define PCA_ASYNC_BEEP   1             // asynchronni pipani

//-----------------------------------------------------------------------------
// COM parametry interniho UART - terminal
//-----------------------------------------------------------------------------

#define COM_BAUD         19200         // Rychlost linky v baudech

#define COM_PARITY_EVEN  1             // Suda parita
//#define COM_PARITY_ODD 1             // Licha parita

//#define COM_TIMER_1    1             // K casovani linky se vyuzije casovac 1
#define COM_TIMER_2      1             // K casovani linky se vyuzije casovac 2

#define __comdata__ xdata              // Umisteni bufferu

#define COM_RX_TIMEOUT      10000      // Meziznakovy timeout [us]

#define COM_RX_BUFFER_SIZE     64      // prijimaci buffer
#define COM_TX_BUFFER_SIZE    128      // vysilaci buffer

#define COM_RX_WAIT      1             // Preklada se funkce ComRxWait
#define COM_FLUSH_CHARS  1             // preklada se funkce ComFlushChars

//-----------------------------------------------------------------------------
// COM3 pres PCA
//-----------------------------------------------------------------------------

#define PcaTX3           TmcTX3        // vysilaci port

// PCA Modul 2 :
#define TX3_CCAPM        CCAPM2
#define TX3_COUNTER_L    CCAP2L
#define TX3_COUNTER_H    CCAP2H
#define TX3_FLAG         CCF2

#define COM3_BAUD        9600

#define PCA_COM3         1             // povoleni prekladu COM3

//-----------------------------------------------------------------------------
// Interni EEPROM
//-----------------------------------------------------------------------------

// ochrana proti vybiti aku :

#define IepAccuOk()  TmcAccuOk()       // nastavi se na funkci hlidani AKU

// prace s prerusenim :

#define IepDisableInts()  EA = 0
#define IepEnableInts()   EA = 1

// Podminena kompilace :

//#define IEP_READ_BYTE     1          // cteni jednoho bytu
//#define IEP_WRITE_BYTE    1          // zapis jednoho bytu

//-----------------------------------------------------------------------------
// Ovladani majaku
//-----------------------------------------------------------------------------

#define TmcMajak( On) TmcMAJAK = !(On)

//-----------------------------------------------------------------------------
// Pripojeni X9313
//-----------------------------------------------------------------------------

#define E2PotCS    TmcCSEPOT           // Chip select
#define E2PotUD    TmcD0               // Smer Up/Down
#define E2PotINC   TmcD1               // Provedeni 1 kroku

//-----------------------------------------------------------------------------
// Pripojeni displeje
//-----------------------------------------------------------------------------

#define DisplayData TmcDATA            // datova sbernice

#define DisplayCS   TmcCSD             // /CS vyber cipu
#define DisplayA0   TmcA0D             // prikaz/data nekdy oznaceny jako CD
#define DisplayWR   TmcCLKAD           // zapis /WR
#define DisplayRES  TmcRESD            // reset /RES

// makra nezavisla na zapojeni pinu (volne k pouziti) :

#define DisplayAddressCommand() DisplayA0 = 1
#define DisplayAddressData()    DisplayA0 = 0

#define DisplayReset() DisplayRES = 1
#define DisplayRun()   DisplayRES = 0

// Pracovni mod displeje :
#define DISPLAY_MODE_GRAPHICS2 1       // graficky mod displeje - 2 roviny
//#define DISPLAY_MODE_MIXED 1             // textovy mod

#define DisplayPodsvit( On)   TmcSVIT = (On)  // rizeni podsvitu

//-----------------------------------------------------------------------------
// Jednoduche zobrazeni
//-----------------------------------------------------------------------------

#define DISPLAY_OFFSET     1
#define DISPLAY_PLANE      1
#define DISPLAY_PUTCHAR    1
#define DISPLAY_PATTERN    1
//#define DISPLAY_CURSOR     1        // nastavovani kurzoru
#define DISPLAY_CLEAR      1        // mazani
//#define DISPLAY_CLR_EOL    1        // mazani do konce radku
#define DISPLAY_GOTO_RC    1
#define DISPLAY_MOVE_TO    1
#define DISPLAY_FILL_BOX   1

//-----------------------------------------------------------------------------
// Displej PG320240
//-----------------------------------------------------------------------------

// Podmineny preklad :
#define DISPLAY_VERT_LINE               1   // Vodorovna cara
#define DISPLAY_VERT_DOT_LINE           1   // Vodorovna teckovana cara
#define DISPLAY_HOR_LINE                1   // Horizontalni plna i teckovana cara

#define DISPLAY_FONT_SYSTEM             1   // Systemovy neproporcialni font
#define DISPLAY_FONT_NUM_MYRIAD40       1   // Numericky neproporcionalni font Myriad 40
#define DISPLAY_FONT_NUM_MYRIAD32       1   // Numericky neproporcionalni font Myriad 23
#define DISPLAY_FONT_NUM_TAHOMA18       1   // Numericky neproporcionalni font Tahoma 18

#ifdef LANG_EN
   #define DISPLAY_FONT_TAHOMA18           1   // Proporcionalni font Tahoma18 EN 
#endif
#ifdef LANG_CZ
   #define DISPLAY_FONT_TAHOMA18_CZ        1   // Proporcionalni font Tahoma18 CZ
#endif
#ifdef LANG_PL
   #define DISPLAY_FONT_TAHOMA18_PL        1   // Proporcionalni font Tahoma18 PL
#endif
#ifdef LANG_RU
   #define DISPLAY_FONT_TAHOMA18_RU        1   // Proporcionalni font Tahoma18 RU
#endif
#ifdef LANG_RO
   #define DISPLAY_FONT_TAHOMA18_RO        1   // Proporcionalni font Tahoma18 RO
#endif
#ifdef LANG_GE
   #define DISPLAY_FONT_TAHOMA18_DE        1   // Proporcionalni font Tahoma18 DE
#endif
#ifdef LANG_SW
   #define DISPLAY_FONT_TAHOMA18_SW        1   // Proporcionalni font Tahoma18 SW
#endif
#ifdef LANG_HU
   #define DISPLAY_FONT_TAHOMA18_HU        1  // Proporcionalni font Tahoma18 HU
#endif

#define DISPLAY_SYMBOL                  1   // Zobrazeni symbolu

#define DISPLAY_CHAR_TAHOMA18           1
#define DISPLAY_STRING_TAHOMA18         1
#define DISPLAY_STRING_CENTER_TAHOMA18  1

#define DISPLAY_BUTTON_FRAME            1
#define DISPLAY_BUTTON_TEXT             1
#define DISPLAY_RETRY_CANCEL            1
#define DISPLAY_OK_CANCEL               1
#define DISPLAY_YES_NO                  1
#define DISPLAY_EXECUTE_BUTTONS         1

#define DISPLAY_NUMPAD                  1

//-----------------------------------------------------------------------------
// Pripojeni klavesnice
//-----------------------------------------------------------------------------


// definice klaves :
typedef enum {
   // systemove klavesy :
   K_NULL  = 0,                        // vyhrazeno k pouziti pro menu & okna
   _K_FIRSTUSER,

   // klavesy, v zavislosti na konstrukci klavesnice :
   K_RIGHT = _K_FIRSTUSER,             // sipka doprava
   K_LEFT,                             // sipka doleva
   K_UP,                               // sipka nahoru
   K_DOWN,                             // sipka dolu
   K_ENTER,                            // Enter
   K_ESC,                              // Esc
   K_TOUCH,                            // aktivni touchpad
   K_TIMEOUT,                          // timeout
   K_REDRAW,                           // prekresleni
   K_BLINK_OFF,                        // periodicke blikani - zhasni
   K_BLINK_ON,                         // periodicke blikani - rozsvit

   // systemove klavesy :
   K_REPEAT       = 0x80,              // opakovani klavesy OR ke klavese
   K_RELEASED     = 0xFE,              // pusteni klavesy (jednotlive i autorepeat)
   K_IDLE         = 0xFF               // vyhrazeno k internimu pouziti, prazdny cyklus cteni
};

//-----------------------------------------------------------------------------
// Pripojeni Touch Screen ADS 7846
//-----------------------------------------------------------------------------

#define TouchDCLK    TmcSCK            // hodiny DCLK
#define TouchDIN     TmcSI             // vstup dat DIN
#define TouchDOUT    TmcSO             // vystup dat DOUT
#define TouchPENIRQ  TmcPENIN          // preruseni od dotyku /PENIRQ

// chipselect :
#define TouchCS           TmcCSPAN     // chipselect /CS
#define TouchSelect()     TouchCS = 0
#define TouchDeselect()   TouchCS = 1

// technicke konstanty :
#define TOUCH_DATA_SIZE     12         // 12 bitu A/D
#define TOUCH_REPEAT_COUNT 250         // Pocet opakovani cteni souradnic
#define TOUCH_REPEAT_RANGE   5         // Maximalni odchylka souradnic v LSB pro ustaleni
#define TOUCH_DIFFERENCE     5         // Maximalni odchylka souradnic po stisknuti

// definice casovych konstant :
#define TOUCH_PEN_RISE         15      // ustaleni po stisknuti [ms]
//#define TOUCH_PEN_FALL         80      // doba pro odpadnuti [ms]
#define TOUCH_AUTOREPEAT_START 700     // prodleva prvniho autorepeat [ms]
#define TOUCH_AUTOREPEAT_SPEED 300     // kadence autorepeat [ms]

// Rozsahy souradnic :
#define TOUCH_X_RANGE     320      // rozsah souradnice X : 0..<X-1>
#define TOUCH_Y_RANGE     240      // rozsah souradnice Y : 0..<Y-1>
#define TOUCH_XL        0x16F      // surovy levy okraj
#define TOUCH_XR        0xE46      // surovy pravy okraj
#define TOUCH_YU        0xE04      // surovy horni okraj
#define TOUCH_YD        0x1DE      // surovy dolni okraj

// Pomineny preklad :

//#define TOUCH_READ_VBAT   1        // cteni vstupu Vbat
//#define TOUCH_READ_AUX    1        // cteni vstupu AUX
//#define TOUCH_READ_RAW    1        // cteni a ukladani surovych souradnic

//-----------------------------------------------------------------------------
// Pripojeni I2C sbernice
//-----------------------------------------------------------------------------

#define IicSCL  TmcSCL                 // I2C hodiny
#define IicSDA  TmcSDA                 // I2C data

// casove prodlevy

#define IIC_WAIT   {byte i; for( i = 6; i > 0; i--);}  // 4takty + doplnit NOPy na 5us

// podmineny preklad :

#define IIC_READ    1                  // cteni dat se sbernice


//-----------------------------------------------------------------------------
// Pripojeni RTC RX-8025 pres I2C sbernici
//-----------------------------------------------------------------------------

// podmineny preklad :

#define RTC_USE_DATE    1              // cti/nastavuj datum
#define RTC_RANGE_CHECK 1              // hlidej zadavane hodnoty na rozsah

//-----------------------------------------------------------------------------
// Pripojeni DA prevodniku TDA8444 pres I2C sbernici
//-----------------------------------------------------------------------------

#define DAC_ADDRESS      0x04          // adresa zadratovana piny A0..A2

// podmineny preklad :

#define DAC_SINGLE_WRITE 1             // zapis do jednoho prevodniku

//-----------------------------------------------------------------------------
// Pripojeni ADC 0838
//-----------------------------------------------------------------------------

#define AdcDIO  TmcDAD                 // DI + DO spolecna data
#define AdcCLK  TmcCLKAD               // CLK hodiny

#define Adc1Enable()   TmcCS1AD = 0    // chipselect prevodniku 1
#define Adc1Disable()  TmcCS1AD = 1    // deselect prevodniku 1

#define Adc2Enable()   TmcCS2AD = 0    // chipselect prevodniku 2
#define Adc2Disable()  TmcCS2AD = 1    // deselect prevodniku 2

// casove prodlevy

#define AdcWait() _nop_();_nop_();_nop_();_nop_();   // max. hodinovy kmitocet 400kHz - doplnit NOPy na 1.25us

// pro vice prevodniku externi chipselect :
#define AdcEnable()
#define AdcDisable()

//-----------------------------------------------------------------------------
// Pripojeni AT25256
//-----------------------------------------------------------------------------

#define EepCS    TmcCSEE               // chipselect /CS
#define EepSCK   TmcSCK                // hodiny SCK
#define EepSO    TmcSO                 // vystup dat SO
#define EepSI    TmcSI                 // vstup dat SI (muze byt shodny s SO)

// ovladani chipselectu je soucasti protokolu, nelze modifikovat

// ochrana proti vybiti aku :

#define EepAccuOk()       TmcAccuOk()  // nastavi se na funkci hlidani AKU

// parametry pameti :

#define EEP_PAGE_SIZE    64            // velikost stranky
#define EEP_SIZE      32768            // celkova kapacita

// Inverze signalu:
#define EEP_XSI_H  1
#define EEP_XSCK_H 1

// Podminena kompilace :

#define EEP_READ_BYTE    1             // cteni jednoho bytu
#define EEP_WRITE_BYTE   1             // zapis jednoho bytu

//-----------------------------------------------------------------------------
// Pripojeni externiho pametoveho modulu AT25256
//-----------------------------------------------------------------------------

#define XmemCS     TmcCSM              // chipselect /CS
#define XmemSCK    TmcSCKM             // hodiny SCK
#define XmemSO     TmcSOM              // vystup dat SO
#define XmemSI     TmcD3               // vstup dat SI (muze byt shodny s SO)

// ochrana proti vybiti aku :

#define XmemAccuOk()       TmcAccuOk() // nastavi se na funkci hlidani AKU

// parametry pameti :
#define XMEM_PAGE_SIZE    64           // velikost stranky
#define XMEM_SIZE      32768           // kapacita externiho pametoveho modulu

// Inverze signalu:
#define XMEM_XSI_H  0
#define XMEM_XSCK_H 0

//-----------------------------------------------------------------------------
// Pripojeni 1-Wire sbernic
//-----------------------------------------------------------------------------

#define MwiDQI0  TmcTP1                // T1 Data Input Channel 0
#define MwiDQI1  TmcTP2                // T2 Data Input Channel 1
#define MwiDQI2  TmcTP3                // T3 Data Input Channel 2
#define MwiDQI3  TmcTP4                // T4 Data Input Channel 3
#define MwiDQI4  TmcTP5                // T5 Data Input Channel 4
#define MwiDQI5  TmcTP6                // T6 Data Input Channel 5
#define MwiDQI6  TmcTP7                // T7 Data Input Channel 6

#define MwiDQO   TmcBUZTP              // Data Output all channels

// logicke urovne na vstupech :
#define DQI_LO    0                   // prime ovladani, pozitivni logika
#define DQI_HI    1

// logicke urovne na vystupu :

#define DQO_LO    0                    // je pres hradlo a jeste tranzistor, normalni logika
#define DQO_HI    1

// casove prodlevy :

// Volani funkce s konstantou 4 takty
// cyklus je <count + 1> * 12 taktu

// Celkove zpozdeni cyklu for 2 + 4 * n

#define MWI_DELAY_RESET          MwiMicroDelay( 122)  // Zpozdeni 480 us pro reset
#define MWI_DELAY_PRESENCE       MwiMicroDelay( 17)   // Zpozdeni 70us mezi reset a presence
#define MWI_DELAY_RESET_TIMESLOT MwiMicroDelay( 104)  // Zpozdeni 410us mezi presence a dalsim
#define MWI_DELAY_START          _nop_();_nop_();_nop_();_nop_()  // minimalni delka startbitu > 1us, 4xNOP = 1.3us pri X2 modu a 18.432MHz Xtalu
#define MWI_DELAY_READ           7                    // 7 = zpozdeni 9.8us v cyklu for, s 30m kabelem funguje od 0 do 20, pri 21 zacina blbnout
#define MWI_DELAY_READ_SLOT      MwiMicroDelay( 24)   // Zpozdeni 100us
#define MWI_DELAY_WRITE          MwiMicroDelay( 19)   // Zpozdeni 79us

// identifikace kanalu a zaroven podmineny preklad :

#define MWI_CH0                  0     // preklada se kanal 0
#define MWI_CH1                  1     // preklada se kanal 1
#define MWI_CH2                  2     // preklada se kanal 2
#define MWI_CH3                  3     // preklada se kanal 3
#define MWI_CH4                  4     // preklada se kanal 4
#define MWI_CH5                  5     // preklada se kanal 5
//#define MWI_CH6                  6     // preklada se kanal 6

// podmineny preklad narocnych funkci :

#define MWI_CRC8                 1     // preklada se kod pro vypocet CRC
#define MWI_FAST_CRC8            1     // vypocet CRC tabulkou

//-----------------------------------------------------------------------------
// Teplomery DS18B20
//-----------------------------------------------------------------------------

#define TempUP   TmcPULLTP        // Strong pull-up

//#define TEMP_DS18S20     1      // definice typu DS18S20 9 bitovy
#define TEMP_DS18B20       1      // definice typu DS18B20 12 bitovy

#define TEMP_SECURE_READ   1      // bezpecne cteni (vcetne CRC) (musi byt definovan MWI_CRC8)
#define TEMP_STRONG_PULLUP 1      // Pouziva se strong pull-up
#define TEMP_PULLUP_ON     0      // Hodnota portu, pri ktere se sepne pullup

//-----------------------------------------------------------------------------
// Teplomery
//-----------------------------------------------------------------------------

#define _THERMO_LAST    (MWI_CH5 + 1)  // celkovy pocet teplomeru (zatim nechavam 1 rezervu)
#define THERMO_TRIALS   2              // pocet pokusu na cteni z jednoho teplomeru - dalsi pokus se opakuje okamzite v cyklu, ne az za 1sec periodu

// Podminena kompilace :
#define THERMO_CONVERT_1C       1      // Prevod na cele stupne
#define THERMO_CONVERT_01C      1      // Prevod na desetiny stupne
#define THERMO_PERIODIC_INIT    1      // Periodicka inicializace cidel

#define THERMO_PERIODIC_INIT_ON     60 // Pocet period mereni
#define THERMO_PERIODIC_INIT_OFF    1  // Pocet period, kdy jsou teplomery vypnute

void AlogTemperature( byte channel, int value);  //!!!
#define AUTO_TEMP_ERROR   (-100<<8)               //!!! chyba mereni -100C misto merene hodnoty

//-----------------------------------------------------------------------------
// Digitalni vstup 74HC165
//-----------------------------------------------------------------------------

#define DinCP       TmcCLKDI           // hodiny CP
#define DinPL       TmcD2              // parallel load /PL
#define DinQ7       TmcOUTDI           // vystup registru Q7

//-----------------------------------------------------------------------------
// Digitalni vystup MC33298
//-----------------------------------------------------------------------------

#define DoutSI      TmcSI              // serial input SI
#define DoutSO      TmcSO              // serial output SO
#define DoutSCLK    TmcSCK             // serial clock SCLK
#define DoutCS      TmcCSDO            // chipselect /CS

// Je-li definovan nasledujici symbol, funkce DoutWrite
// vraci aktualni stav vystupu

//#define DOUT_READBACK    1

// Maska kontroly pullup a spravne hodnoty
// na uvedenych vystupech
// 0x01 je DO0 .. 0x80 je DO7

#define DOUT_MASK   0xFF               // maska aktivnich vystupu


//-----------------------------------------------------------------------------
// Parametry FIFO - datovy logger
//-----------------------------------------------------------------------------

struct SLog;                           // dopredna deklarace

typedef struct SLog  TFifoData;        // Typ ukladane struktury (max 255 bytu)
#define FifoData        Log            // Globalni buffer ukladanych dat

#define FIFO_START            TMC_FIFO_START          // Pocatecni adresa FIFO v EEPROM
#define FIFO_CAPACITY         TMC_FIFO_CAPACITY       // Maximalni pocet ukladanych polozek
#define FIFO_MARKER_EMPTY     TMC_FIFO_MARKER_EMPTY   // Znacka konce pri neuplnem zaplneni
#define FIFO_MARKER_FULL      TMC_FIFO_MARKER_FULL    // znacka konce pri prepisovani

#define FIFO_INVALID_ADDRESS 0xFFFF                    // neplatna adresa do FIFO - MUSI korespondovat s XFIFO_INVALID_ADDRESS ve fifo.tpl!!!!!

// Podminena kompilace :
#define FIFO_FAST            1         // Zapamatovani pozice markeru
#define FIFO_VERIFY          1         // Verifikace po zapisu dat
#define FIFO_READ            1         // Cteni obsahu po polozkach
//#define FIFO_FIND_FIRST      1       // Nalezeni adresy prvniho zaznamu
//#define FIFO_FIND_LAST       1       // Nalezeni adresy posledniho zaznamu

//-----------------------------------------------------------------------------
// Modul tiskarny
//-----------------------------------------------------------------------------

#define PRINT_COM3          1          // presmeruj na COM3

#define PrintPowerOn()                 // zapnuti napajeni budice tiskarny
#define PrintPowerOff()                // zapnuti napajeni budice tiskarny

// Podmineny preklad funkci :
#define PRT_HEX             1          // PrtHex
#define PRT_DEC             1          // PrtDec
#define PRT_CHARS           1          // PrtChars, PrtPage, PrtSeparator...
//#define PRT_DEBUG           1          // tiskni vsechny platne cislice bez ohledu na sirku

//-----------------------------------------------------------------------------
// Parametry kruhoveho pole
//-----------------------------------------------------------------------------

// Podminena komplilace :
#define FAR_RESET            1         // Preklada se funkce pro reset

// znacka ve FIFO :
// ukladana hodnota se ji nesmi rovnat !
// pro byte  0xMM
// pro word  0xMM00
// pro dword 0xMM000000

#define FAR_MARKER     0x80           // hodnota MM

//-----------------------------------------------------------------------------
// Paketova komunikace - terminal
//-----------------------------------------------------------------------------

#define PACKET_MAX_DATA      52        // maximalni delka dat paketu = sizeof(TRasData)
//#define PACKET_RX_TIMEOUT    1..0xFE   // ceka na prvni znak paketu * 0.1s
#define PACKET_RX_TIMEOUT    0         // bez cekani na prvni znak (je-li k dispozici znak, zahaji prijem)
//#define PACKET_RX_TIMEOUT    0xFF      // ceka na prvni znak COM_RX_TIMEOUT

// Podminena kompilace :
#define PACKET_TX            1         // vysilani paketu
#define PACKET_RX            1         // prijem   paketu
#define PACKET_TX_BLOCK      1         // vysilani dlouhych paketu
#define PACKET_COM1          1         // komunikace pres COM1


//-----------------------------------------------------------------------------
// COM_util
//-----------------------------------------------------------------------------

// Podminena kompilace :

//#define COM_TX_DIGIT        1
//#define COM_TX_HEX          1
//#define COM_TX_DEC          1
//#define COM_RX_HEX          1
//#define COM_SKIP_CHARS      1

//-----------------------------------------------------------------------------
// Klouzavy prumer
//-----------------------------------------------------------------------------

typedef byte   TAverageCount;     // Pocet vzorku
typedef byte   TAverageValue;     // Hodnota vzorku
typedef word   TAverageValueSum;  // Hodnota sumy vzorku TAverageCount * TAverageValue

// Meri se po 10sec, tj. prumeruju 1 minutu. Pozor, pouziva se i pro prumerovani tlaku ventilatoru a filtru, pokud neco zmenim, projevi se to i tam!
#define AVERAGE_CAPACITY 6        // Kapacita (maximalni pocet prvku) klouzaveho prumeru

// Podmineny preklad
//#define AVERAGE_FULL    1         // Test zaplneni
//#define AVERAGE_STEADY  1         // Test zaplneni


//-----------------------------------------------------------------------------

#define uDelay( us)  \
                 {byte __count; __count = ((us) * 3) / 2; while( --__count);}

//-----------------------------------------------------------------------------


#endif // __Hardware_H__
