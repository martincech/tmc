//*****************************************************************************
//
//    Co2.c - Mereni koncentrace CO2
//    Version 1.0
//
//*****************************************************************************


#ifndef __Co2_H__
   #define __Co2_H__

#include "Hardware.h"     // pro podminenou kompilaci
#include "Pid\Pid.h"        // PID regulator

// Struktura CO2
typedef struct {
  byte IsValueValid;        // YES/NO hodnota Value obsahuje platnou koncentraci CO2 (po spusteni ventilace cidlo 10 sekund nemeri)
  byte Value;               // Koncentrace CO2 s dilkem CO2_LSB ppm
  byte OverLimit;           // Prilis vysoka koncentrace

  byte Failure;             // YES/NO porucha cidla

  TPid Pid;                 // PID regulace
  byte  FreshAir;           // Procenta cerstveho vzduchu podle regulace CO2
} TCo2;
extern TCo2 __xdata__ Co2;

#define CO2_LSB     100                 // Hodnotu ukladam s dilkem 100 ppm, rozsah 10.000ppm tak vyjde na 7 bitu

// Konstanty pro poruchu cidla
#define CO2_FAILURE_MIN     200         // Pod 200 ppm znamena poruchu cidla
#define CO2_FAILURE_MAX     9000        // Nad 9000 ppm znamena poruchu cidla


void Co2Init();
  // Inicializace

void Co2Calc();
  // Vypocte CO2

void Co2RegulatorInit();
  // Inicializace regulatoru

void Co2RegulatorUpdateParameters();
  // Preberu parametry z configu do PID regulatoru

void Co2RegulatorStep();
  // Krok regulatoru CO2

#endif
