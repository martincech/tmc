//*****************************************************************************
//
//    Co2.c - Mereni koncentrace CO2
//    Version 1.0
//
//*****************************************************************************

#include "Tmc.h"
#include "Co2.h"
#include "Ain.h"               // Analogove vstupy
#include "Alarm.h"             // Indikace poruch

TCo2 __xdata__ Co2;

// Konstanty PID regulatoru :
#define     REG_TS        10             // vzorkovaci perioda v sekundach


#ifndef __TMCREMOTE__

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void Co2Init() {
  // Inicializace
  Co2.IsValueValid = NO;                // Zatim nemam zmereno
  Co2.Value        = 0;
  Co2.OverLimit    = NO;
  Co2.Failure      = NO;
  Co2RegulatorInit();
}

//-----------------------------------------------------------------------------
// Vypocet
//-----------------------------------------------------------------------------

#ifdef USE_CO2

#define CO2_TABLE_COUNT    2
static byte code AinTable[CO2_TABLE_COUNT] = {0, 0xFF};         // Napeti Ain 0 a 10V
static byte code Co2Table[CO2_TABLE_COUNT] = {0, 100};          // Koncentrace CO2 0 a 10000ppm

#endif // USE_CO2

void Co2Calc() {
  // Vypoctu CO2
#ifdef USE_CO2

#ifdef EMC_TEST_MODE
  Co2.Value = 5;        // 500 ppm
  Co2.OverLimit = NO;
  Co2.Failure   = NO;
  return;
#endif

  Co2.Value = AinCalcValue(AinCo2, AinTable, Co2Table, CO2_TABLE_COUNT);
  Co2.IsValueValid = YES;       // Mam zmereno

  // Porucha cidla
  if (Co2.Value <= CO2_FAILURE_MIN / CO2_LSB || Co2.Value >= CO2_FAILURE_MAX / CO2_LSB ) {
    Co2.Failure = YES;
    AlarmSet();
    Co2.OverLimit = NO;         // Pri poruse nemuze byt prekrocena mez
    return;
  } else {
    Co2.Failure = NO;
    // Bez poruchy, pokracuju na test prekroceni meze
  }

  // Prekroceni mezi
  if ((word)Co2.Value * CO2_LSB > Config.MaxCo2) {
    Co2.OverLimit = YES;
    AlarmSet();
  } else {
    Co2.OverLimit = NO;
  }

#else
  Co2.Value = 0;
  Co2.OverLimit = NO;
  Co2.Failure   = NO;
#endif // USE_CO2
}

//-----------------------------------------------------------------------------
// Inicializace regulatoru
//-----------------------------------------------------------------------------

void Co2RegulatorInit() {
  // Vyplnim strukturu PID regulatoru
  Co2.Pid.SamplingPeriod = REG_TS;
  Co2.Pid.KpDenominator  = 1000;       // Kp je na tisiciny
  Co2.Pid.TiMax          = 1000;
  Co2.Pid.LsbDenominator = 1;
  Co2RegulatorUpdateParameters();
  // Target do PID ukladam az v Co2RegulatorStep(), tady neni treba

  PidInit(&Co2.Pid);

  Co2.FreshAir = 20;                // Radsi aspon trosku
}

//-----------------------------------------------------------------------------
// Predani parametru
//-----------------------------------------------------------------------------

void Co2RegulatorUpdateParameters() {
  // Preberu parametry z configu do PID regulatoru
  Co2.Pid.Kp = -(int)Config.Co2RegKp;        // Otocene, pri vzrustajicim CO2 chci otevirat napor
  Co2.Pid.Ti =  Config.Co2RegTi;
  Co2.Pid.Td =  Config.Co2RegTd;
}

//-----------------------------------------------------------------------------
// Krok regulatoru
//-----------------------------------------------------------------------------

#ifdef USE_CO2

void Co2RegulatorStep() {
  // Krok regulatoru CO2

  // Pokud nemam zmerenou platnou koncentraci CO2, regulaci neprovadim
  if (!Co2.IsValueValid) {
    return;
  }

  // Krok regulatoru
  Co2.Pid.Target = Config.Co2Target / CO2_LSB;          // Update cilove hodnoty, mohla se zmenit
  PidStep(&Co2.Pid, Co2.Value);
  Co2.FreshAir = Co2.Pid.Output;                        // Preberu vystup regulatoru
}

#endif // USE_CO2


#endif // __TMCREMOTE__
