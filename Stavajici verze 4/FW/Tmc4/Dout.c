//*****************************************************************************
//
//    Dout.c - Digitalni vystupy
//    Version 1.0
//
//*****************************************************************************

#include "Dout.h"
#include "Mc33298\Mc33298.h"     // digital output

// Definice jednotlivych vystupu
byte __xdata__ DoutCooling;
byte __xdata__ DoutHeatingAndPump;
byte __xdata__ DoutVentilation;
byte __xdata__ DoutHeatingPump;
byte __xdata__ DoutExternalSiren;
byte __xdata__ DoutDisinfectionPower;
byte __xdata__ DoutDisinfectionBody;
byte __xdata__ DoutDisinfectionWheels;

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void DoutInit() {
  // Inicializuje digitalnich vystupu
  DigitalOutInit();     // Inicializace
  // Vypnu vsechny vystupy
  DoutCooling            = NO;
  DoutHeatingAndPump     = NO;
  DoutVentilation        = NO;
  DoutHeatingPump        = NO;
  DoutExternalSiren      = NO;
  DoutDisinfectionPower  = NO;
  DoutDisinfectionBody   = NO;
  DoutDisinfectionWheels = NO;
}

//-----------------------------------------------------------------------------
// Nastaveni vsech vystupu
//-----------------------------------------------------------------------------

#define SetOutput(Mask, Value) if (Value) OutputValue |= Mask; else OutputValue &= ~Mask

static void SetOutputs() {
  // Nastavi
  byte OutputValue = 0;    // Vysledna hodnota
  SetOutput(TmcDVY1, DoutDisinfectionPower);
  SetOutput(TmcDVY2, DoutDisinfectionBody);
  SetOutput(TmcDVY3, DoutDisinfectionWheels);
  SetOutput(TmcDVY4, DoutExternalSiren);
  SetOutput(TmcDVY5, DoutCooling);
  SetOutput(TmcDVY6, DoutHeatingAndPump);
  SetOutput(TmcDVY7, DoutVentilation);
  SetOutput(TmcDVY8, DoutHeatingPump);
  DigitalOutWrite(OutputValue);
}

//-----------------------------------------------------------------------------
// Nastaveni jednotlivych vystupu, YES = zapnout, NO = vypnout zarizeni pripojene na dany vystup
//-----------------------------------------------------------------------------

void DoutSetCooling(TYesNo Value) {
  // Zapne / vypne chlazeni
  DoutCooling = Value;
  SetOutputs();
}

void DoutSetHeatingAndPump(TYesNo Value) {
  // Zapne / vypne topeni vcetne cerpadla
  DoutHeatingAndPump = Value;
  SetOutputs();
}

void DoutSetVentilation(TYesNo Value) {
  // Zapne / vypne ventilatory
  DoutVentilation = Value;
  SetOutputs();
}

#ifndef USE_ON_OFF_HEATING

void DoutSetHeatingPump(TYesNo Value) {
  // Zapne / vypne cerpadlo topeni, samotne topeni je vypnute
  DoutHeatingPump = Value;
  SetOutputs();
}

#endif // USE_ON_OFF_HEATING

void DoutSetExternalSiren(TYesNo Value) {
  // Zapne / vypne externi sirenu
  DoutExternalSiren = Value;
  SetOutputs();
}

void DoutSetDisinfectionPower(TYesNo Value) {
  // Zapne / vypne LOGO
  DoutDisinfectionPower = Value;
  SetOutputs();
}

void DoutSetDisinfectionWheels(TYesNo Value) {
  // Zapne / vypne dezinfekci kol
  DoutDisinfectionWheels = Value;
  SetOutputs();
}

void DoutSetDisinfectionBody(TYesNo Value) {
  // Zapne / vypne dezinfekci skrine
  DoutDisinfectionBody = Value;
  SetOutputs();
}

