//*****************************************************************************
//
//    Time.c - Datum a cas
//    Version 1.0
//
//*****************************************************************************


#ifndef __Time_H__
   #define __Time_H__

#include "Hardware.h"     // pro podminenou kompilaci

// Uplny datum, vcetne celeho roku a na cele bajty (v pripade vicenasobneho ukladani predelat na bity)
typedef struct {
  byte Min;
  byte Hour;
  byte Day;
  byte Month;
  word Year;
  byte Changed;     // YES/NO v tomto kroku doslo k zmene nastaveni hodin, nuluje se po zapisu do Logu
} TTime;  // 6 bajtu

// Datum a cas maximalne zkraceny na 4 bajty pro prenos do serveru
typedef union {
  dword Dword;
  struct {
    word  Year    : 12,
          Month   : 4;
    word  Day     : 5,
          Hour    : 5,
          Min     : 6;
  } Time;
} TTransferTime;


// Globalni datum a cas
extern TTime __xdata__ Time;

void TimeInit();
  // Inicializuje realny cas

void TimeRead();
  // Nacte aktualni datum a cas a uloze je do lokalnich promennych

void TimeSetNewTime(TTransferTime data *NewTime);
  // Nastavi novy datum a cas zadany strukturou TTransferTime

#endif
