//*****************************************************************************
//
//    Ain.c - Analogove vstupy
//    Version 1.0
//
//*****************************************************************************

#include "Ain.h"

#ifndef __TMCREMOTE__

// Definice jednotlivych vstupu
byte __xdata__ AinServo[_SERVO_COUNT]; // Vystupy jednotlivych serv
byte __xdata__ AinDieselTemperature;   // Teplota dieselu
byte __xdata__ AinFan;                 // Napeti ventilatoru
byte __xdata__ AinVoltage;             // Napeti akumulatoru
byte __xdata__ AinSuctionPressure;     // Tlak sani kompresoru
byte __xdata__ AinDischargePressure;   // Tlak vytlaku kompresoru
byte __xdata__ AinWaterTemperature;    // Teplota vody v topnem okruhu
byte __xdata__ AinCo2;                 // Koncentrace CO2
byte __xdata__ AinHumidity;            // Vlhkost vzduchu
byte __xdata__ AinFiltersPressure;     // Tlakova ztrata na vzduchovych filtrech
byte __xdata__ AinLogoReady;           // Siemens LOGO je pripraveno (nemame uz digitalni vstupy, musim pouzit analogovy)
byte __xdata__ AV16;           // Offset zeme pri zatizenem motoru


//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void AinInit () {
   // Inicializuje digitalnich vstupu
   Adc1Disable ();            // chipselect ADC 1
   Adc2Disable ();            // chipselect ADC 2
   AdcInit ();   // Inicializace prevodniku
}

//-----------------------------------------------------------------------------
// Cteni vstupu
//-----------------------------------------------------------------------------

static byte ReadAdc1 (byte channel) {
   byte value;
   Adc1Enable ();
   value = AdcRead (channel);
   Adc1Disable ();
   return value;
}

static byte ReadAdc2 (byte channel) {
   byte value;
   Adc2Enable ();
   value = AdcRead (channel);
   Adc2Disable ();
   return value;
}


void AinRead () {
   // Nacte vsechny analogove vstupy a ulozi je do lokalnich promennych
   // Prevodnik 1
   AinSuctionPressure = ReadAdc1 (TmcAV1);
   AinServo[SERVO_FLOOR] = ReadAdc1 (TmcAV2);
   AinDischargePressure = ReadAdc1 (TmcAV3);
   AinWaterTemperature = ReadAdc1 (TmcAV4);
   AinCo2 = ReadAdc1 (TmcAV5);
   AinHumidity = ReadAdc1 (TmcAV6);
   AinDieselTemperature = ReadAdc1 (TmcAV7);
   AinFan = ReadAdc1 (TmcAV8);

   // Prevodnik 2
   AinLogoReady = ReadAdc2 (TmcAV9);
   AinFiltersPressure = ReadAdc2 (TmcAV10);
   AinVoltage = ReadAdc2 (TmcAV11);
   AinServo[SERVO_HEATING] = ReadAdc2 (TmcAV12);
   AinServo[SERVO_INDUCTION1] = ReadAdc2 (TmcAV13);
   AinServo[SERVO_INDUCTION2] = ReadAdc2 (TmcAV14);
   AinServo[SERVO_RECIRCULATION] = ReadAdc2 (TmcAV15);
   AV16 = ReadAdc2 (TmcAV16);
}
#ifndef NO_DIESEL
byte AinMapValue (byte ainValue, byte code *ainTable, byte code *valueTable, byte tableCount) {
// jako aincalcvalue ale bez aproximace, pouze mapuje
   byte j;
   for (j = 0; j < tableCount; j++) {
      if (ainValue <= ainTable[j]) {
         return valueTable[j];             // Nasel jsem primo bod ve krivce
      }
   }
   return valueTable[tableCount-1];
}
#endif
#endif // __TMCREMOTE__


byte AinCalcValue (byte ainValue, byte code *ainTable, byte code *valueTable, byte tableCount) {
   // Prepocte hodnotu analogoveho vstupu <ainValue> na hodnotu podle prevodni tabulky.
   // Hodnoty v ainTable musi byt vzrustajici, ve valueTable mohou i klesat
   byte j;

   // Omezim min a max hodnotu
   if (ainValue < ainTable[0]) {
      ainValue = ainTable[0];
   }
   if (ainValue > ainTable[tableCount - 1]) {
      ainValue = ainTable[tableCount - 1];
   }

   // Hledam koncovy bod
   for (j = 0; j < tableCount; j++) {
      if (ainValue == ainTable[j]) {
         return valueTable[j];             // Nasel jsem primo bod ve krivce
      }
      if (ainTable[j] > ainValue) {
         // Nasel jsem prvni bod napravo od ainValue, vypoctu hodnotu
         // 17.4.2013: Vysledek je nutne pocitat jako long, puvodne bylo jako int. Muzu zde nasobit 255*255=65025 a potrebuju znamenko.
         return (long)(valueTable[j - 1]
            + (long)((long)(ainValue - ainTable[j - 1])
               * (long)((int)valueTable[j] - (int)valueTable[j - 1])      // valueTable nemusi byt vzrustajici, tj. rozdil muze byt zaporny
               / (long)(ainTable[j] - ainTable[j - 1])));
      }
   }
}
