//*****************************************************************************
//
//   Ras.h       Remote Terminal access
//   Version 1.0 (c) Vymos
//
//*****************************************************************************

#ifndef __Ras_H__
   #define __Ras_H__

#include "Hardware.h"          // zakladni datove typy
#include "Tmc.h"               // Rozsahy
#include "Time.h"              // Datum a cas


// Prikazy z terminalu:
#define RAS_CMD_SET_MODE                1           // Nastavi rezim
#define RAS_CMD_SET_TARGET_TEMPERATURE  2           // Nastavi cilovou teplotu
#define RAS_CMD_SET_LOCALIZATION        3           // Nastavi lozeni
#define RAS_CMD_SET_FRESH_AIR_MODE      4           // Nastavi rezim cerstveho vzduchu
#define RAS_CMD_FRESH_AIR_INCREASE      5           // Rucni pridani cerstveho vzduchu
#define RAS_CMD_FRESH_AIR_DECREASE      6           // Rucni ubrani cerstveho vzduchu
#define RAS_CMD_SET_FLOOR_FLAP_MODE     7           // Nastavi rezim klapek v podlaze
#define RAS_CMD_FLOOR_FLAP_INCREASE     8           // Rucni pridani klapek v podlaze
#define RAS_CMD_FLOOR_FLAP_DECREASE     9           // Rucni ubrani klapek v podlaze
#define RAS_CMD_SOUND                   10          // Zapne / vypne hlaseni alarmu
#define RAS_CMD_TIME                    11          // Nastavi novy datum a cas
#define RAS_CMD_LOGIN                   12          // Provede login ridice
#define RAS_GET_DATA                    13          // Posle zpet periodicka data TRasData
#define RAS_CMD_DISINFECTION_WHEELS     14          // Spusti dezinfekci kol
#define RAS_CMD_DISINFECTION_BODY       15          // Spusti dezinfekci skrine

// Odpovedi ze serveru:
#define RAS_CMD_NAK                     0xF0        // Zaslany parametr je neplatny, nastaveni se neprovedlo (hodnota mimo rozsah, jednotka neni ve spravnem rezimu atd)


// Struktura pro periodicka data
typedef struct {
  byte VersionMajor;                    // Cislo verze TMC, posilat vzdy na prvni pozici
  byte VersionMinor;                    // Cislo verze TMC

  byte AccuVoltage              : 5,    // Napeti ve voltech
       ConfigLocalization       : 3;    // TLocalizationStatus: lozeni prepravek ve skrini

  byte ControlMode              : 2,    // Rezim, ve kterem regulator pracuje TControlMode
       ControlAutoMode          : 1,    // Cinnost v automatickem rezimu - bud se topi, nebo chladi TAutoMode
       ControlEmergencyMode     : 1,    // YES/NO nouzove ovladani z rozvadece
       ControlOldEmergencyMode  : 1,    // Nouzove ovladani v predchozim kroku
       ControlFrontTempFault    : 1,    // Porucha predniho cidla (nedojeti do pasma cilove teploty)
       ControlMiddleTempFault   : 1,    // Porucha zadniho cidla (nedojeti do pasma cilove teploty)
       ControlRearTempFault     : 1;    // Porucha zadniho cidla (nedojeti do pasma cilove teploty)

  byte CoolingOn                : 1,    // YES/NO prave sepnute chlazeni
       CoolingFailure           : 1,    // YES/NO porucha chlazeni
       DieselUse                : 1,    // YES/NO zda se diesel motor pouziva
       DieselOn                 : 1,    // YES/NO bezi diesel
       DieselTemperature        : 4;    // Teplota motoru

  byte ServoFloorServoStatus    : 3,    // Stav serva v podlaze
       DieselChangeOil          : 1,    // YES/NO ma se vymenit olej s filtrem
       DieselOverheated         : 1,    // YES/NO prehraty diesel
       ElMotorOn                : 1,    // YES/NO zapnuty elektromotor
       HeatingFailure           : 1,    // YES/NO porucha samotneho topeni
       HeatingServoFailure      : 1;    // YES/NO porucha serva topeni

  byte FreshAirMode             : 1,    // Typ rizeni TFreshAirMode
       FreshAirPercent          : 7;    // Aktualni hodnota cersrtveho vzduchu v procentech

  byte FreshAirPosition          : 4,  // Poloha klapky pro zobrazeni a logovani
       FreshAirFloorFlapPosition : 4;  // Poloha klapky pro zobrazeni a logovani

  byte FreshAirFloorFlapMode    : 1,   // Typ rizeni
       FreshAirFloorFlapPercent : 7;   // Aktualni hodnota otevreni klapky v procentech

  byte HeatingPosition          : 4,   // Poloha serva topeni pro logovani
       RecirculationServoStatus : 3,    // Stav serva recirkulace
       HeatingFlame1            : 1;    // YES/NO topeni 1 prave hori

  byte HeatingPercent           : 7,    // Aktualni hodnota vykonu topeni v procentech
       FanOn                    : 1;    // YES/NO zapnute ventilatory

  byte ServoTopInductionServoStatus     : 3,    // Stav horniho naporoveho serva
       ServoBottomInductionServoStatus  : 3,    // Stav spodniho naporoveho serva
       FanAutoMode                      : 1,    // YES/NO ventilatory prepnute na automaticky regulator vykonu
       FanFailure                       : 1;    // YES/NO porucha ventilatoru

  byte LogDriver                : 7,    // Cislo nalogovaneho ridice
       AccuLowVoltage           : 1;    // YES/NO nizke napeti akumulatoru

  byte Co2                      : 7,    // Koncentrace CO2 s dilkem CO2_LSB ppm
       GsmDummy                 : 1;    // GSM dummy

  byte Humidity                 : 7,                           // Relativni vlhkost v %
       GsmDummy2                : 1;    // GSM dummy2

  byte FiltersAirPressure       : 7,    // Tlakova ztrata na filtrech s dilkem AIR_PRESSURE_LSB Pa
       HeatingFlame2            : 1;    // YES/NO topeni 2 prave hori

  byte DisinfectionMode         : 2,    // Rezim dezinfekce
       DisplayMode              : 2,    // Rezim zobrazeni, kvuli dezinfekci musim propagovat az do TMCR
       ChargingOn               : 1,    // YES/NO dobiji se
       DisinfectionIsInstalled  : 1,    // YES/NO dezinfekce je nainstalovana
       Dummy3                   : 2;

  char Temp1C[TEMP_COUNT];    // Teploty zaokrouhlene na 1 stupen
  int  Temp01C[TEMP_COUNT];   // Teploty zaokrouhlene na desetinu stupne

  TTransferTime Time;         // 4 bajty

  char ConfigTargetTemperature; // Cilova teplota, cele stupne

  byte ConfigMaxOffsetAbove     : 7,    // Maximalni odchylka teploty ve skrini nad cilovou teplotou
       AlarmShowAlarm           : 1;    // YES/NO zda se prave indikuje alarm

  byte ConfigMaxOffsetBelow     : 7,    // Maximalni odchylka teploty ve skrini pod cilovou teplotou
       AlarmQuiet               : 1;    // YES/NO potlaceny reproduktor pri alarmu

  byte ChargingFailure          : 1,    // YES/NO porucha dobijeni
       HeatingCircuitState      : 3,    // Stav ohrevu vody ve spodnim okruhu
       FiltersPressureOverLimit : 1,    // YES/NO tlakova ztrata na vzduchovych filtrech je prilis vysoka
       Co2OverLimit             : 1,    // YES/NO koncentrace CO2 je prilis vysoka
       Co2Failure               : 1,    // YES/NO porucha cidla CO2
       TempUnits                : 1;    // Jednotky teploty TTempUnits

  byte FanPower                 : 7,    // Vykon ventilatoru 0-100%
       TruckEngineRun           : 1;    // Pouze pro GPS: YES/NO Beh motoru vozidla

  byte DischargePressure;               // Pouze pro GPS: Tlak vytlaku kompresoru 0-25.5 bar

  byte HeatingFlame1DutyCycle   : 4,    // Pouze pro GPS: Strida horeni plamene topeni 1 s dilkem LOG_DUTY_CYCLE_LSB
       HeatingFlame2DutyCycle   : 4;    // Pouze pro GPS: Strida horeni plamene topeni 2 s dilkem LOG_DUTY_CYCLE_LSB

  byte CoolingDutyCycle         : 4,    // Pouze pro GPS: Strida chodu kompresoru s dilkem LOG_DUTY_CYCLE_LSB
       SuctionPressureMsb       : 4;    // Pouze pro GPS: MSB Tlak sani kompresoru 0-6.3 bar

  byte SuctionPressureLsb       : 2,    // Pouze pro GPS: LSB Tlak sani kompresoru 0-6.3 bar
       HeatingWaterTemperature  : 6;    // Pouze pro GPS: Teplota vody na vstupu Webasta ve stupnich Celsia s krokem LOG_HEATING_WATER_TEMP_LSB

  byte Dummy1                   : 7,
       EcuError                 : 1;

  byte Dummy2[3];                       // Vypln na 52 bajtu, s velikosti 44 bajtu blbla editace cisel v TMCR

} TRasData;   // 52 bajtu - upravit vzdy i PACKET_MAX_DATA v Hardware.h


void RasInit( void);
// Inicializace terminalu

void RasExecute( void);
// Periodicka obsluha - volat 1x za sekundu

#endif


