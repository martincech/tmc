
#include "Log.h"
#include "LogData.h"           // Struktura pro ukladani
#include "Temp.h"              // Teploty
#include "Control.h"           // Automaticke rizeni topeni/chlazeni
#include "FAir.h"              // Regulace cerstrveho vzduchu
#include "Heating.h"           // Regulace topeni
#include "Cooling.h"           // Regulace chlazeni
#include "Servo.h"             // Kontrola stavu serv
#include "Accu.h"              // Napeti a proud akumulatoru
#include "Charging.h"          // Dobijeni
#include "ElMotor.h"           // Elektromotor
#include "TruckEn.h"           // Motor vozidla
#include "Alarm.h"             // Indikace poruch
#include "Fan.h"               // Ventilatory
#include "Diesel.h"            // Diesel motor
#include "Time.h"              // Aktualni datum a cas
#include "Co2.h"               // Koncentrace CO2
#include "Humidity.h"          // Relativni vlhkost
#include "AirPress.h"          // Tlak radiatoru a filtru
#include "System.h"     // "operacni system" (SysDelay)
#include "Fifo\Fifo.h"       // obsluha FIFO
#include "AT25256\EEP.h"     // zabudovana EEPROM
#include "Xmem\Xmem.h"       // externi pametovy modul
#include <datconv.h>           // casem BCD aritmetika
#include <string.h>            // memset
#include <stddef.h>            // makro offsetof
#include <math.h>              // abs

TLog   __xdata__ Log;          // buffer logu v externi RAM
TStamp __xdata__ Stamp;        // buffer razitka v externi RAM
TEvent __xdata__ Event;        // komunikacni struktura v externi RAM - pro zaznam udalosti

byte __xdata__ Driver;                   // Cislo nalogovaneho ridice
TYesNo                 _UkladatZaznamy;  // Flag, zda se pri mereni ma ukladat do pameti - napr. behem tisku protokolu chci merit a regulovat, ale nesmi se ukladat
static byte __xdata__  last_min = -1;    // posledni zapis v minutach

#define LogArray ((byte __xdata__ *)&Log)            // umozni indexovat po bytu
#define EventArray ((byte __xdata__ *)&Event)        // umozni indexovat po bytu

static byte sum;              // Checksum pri zapisu a verifikaci, vyuziva vice fci





//-----------------------------------------------------------------------------
// Inicializace modulu
//-----------------------------------------------------------------------------

void LogInit () {
   // Inicializuje modul
   LogClear ();         // nulovani kumulovanych velicin
   FifoInit ();          // start FIFO
   _UkladatZaznamy = YES;

   TempClearAverages ();  // Teploty prumeruju od zacatku
}  // LogInit

//-----------------------------------------------------------------------------
// Zapis do FIFO
//-----------------------------------------------------------------------------

static TServoStatusLog PrepocitejServoStatusNaLog (TServoStatus Status) {
   // Prepocita promennou typu TServoStatus na promennou typu TServoStatusLog (pro logovani)
   switch (Status) {
   case SERVO_STATUS_OK:
   case SERVO_STATUS_RUNNING:
      return SERVO_LOG_OK;
   case SERVO_STATUS_TIMEOUT: return SERVO_LOG_TIMEOUT;
   case SERVO_STATUS_CONTROL: return SERVO_LOG_CONTROL;
   case SERVO_STATUS_OFF: return SERVO_LOG_MANUAL;
   }//switch
}

void LogExecute () {
   // Prekopiruje aktualni stav do FifoData a v urceny cas zapise
   // Volat po mereni
   int Temp;

   // Cilova teplota
   Log.TargetTemperature = Config.TargetTemperature;

   Temp = TempCountAverage (TEMP_FRONT);
   Log.FrontTemp = abs (Temp);
   Log.FrontTempSign = (TYesNo)(Temp < 0);
   if (Control.TempFaults[TEMP_FRONT].Fault) {
      Log.FrontTempOffset = YES;  // Kumulace
   }
   Log.FreshAirLsb = FreshAir.Percent & 0x0F;    // Spodni 4 bity

   Temp = TempCountAverage (TEMP_MIDDLE);
   Log.MiddleTemp = abs (Temp);
   Log.MiddleTempSign = (TYesNo)(Temp < 0);
   if (Control.TempFaults[TEMP_MIDDLE].Fault) {
      Log.MiddleTempOffset = YES;  // Kumulace
   }
   Log.FloorPosition = FreshAir.FloorFlap.Position;

   Temp = TempCountAverage (TEMP_REAR);
   Log.RearTemp = abs (Temp);
   Log.RearTempSign = (TYesNo)(Temp < 0);
   if (Control.TempFaults[TEMP_REAR].Fault) {
      Log.RearTempOffset = YES;  // Kumulace
   }
   Log.CoolingDutyCycle = Cooling.DutyCycle.Percent / LOG_DUTY_CYCLE_LSB;

   Temp = TempCountAverage (TEMP_CHANNEL);
   Log.ChannelTemp = abs (Temp);
   Log.ChannelTempSign = (TYesNo)(Temp < 0);
   Log.Voltage = Accu.Voltage;

   Temp = TempCountAverage (TEMP_OUTSIDE);
   Log.OutsideTemp = abs (Temp);
   Log.OutsideTempSign = (TYesNo)(Temp < 0);
   Log.HeatingFlame1DutyCycle = Heating.Flame1DutyCycle.Percent / LOG_DUTY_CYCLE_LSB;
   if (Accu.LowVoltage) {
      Log.LowVoltage = YES;       // Kumulace
   }

   Temp = TempCountAverage (TEMP_RECIRCULATION);
   Log.RecirculationTemp = abs (Temp);
   Log.RecirculationTempSign = (TYesNo)(Temp < 0);
   Log.DieselTemperature = Diesel.Temperature;
#ifndef NO_DIESEL
   Log.DieselUse = NO;
#else 
   Log.DieselUse = YES;
#endif

   Log.HeatingPosition = Heating.Position;
   Log.HeatingServoStatus = PrepocitejServoStatusNaLog (ServoStatus[SERVO_HEATING]);
   Log.RecirculationServoStatus = PrepocitejServoStatusNaLog (ServoStatus[SERVO_RECIRCULATION]);

   Log.TopInductionServoStatus = PrepocitejServoStatusNaLog (ServoStatus[SERVO_INDUCTION1]);
   Log.BottomInductionServoStatus = PrepocitejServoStatusNaLog (ServoStatus[SERVO_INDUCTION2]);
   Log.HeatingFlame2DutyCycle = Heating.Flame2DutyCycle.Percent / LOG_DUTY_CYCLE_LSB;

   Log.Mode = ControlModeLog ();
   Log.Humidity = Humidity.Value / LOG_HUMIDITY_LSB;

   Log.FloorServoStatus = PrepocitejServoStatusNaLog (ServoStatus[SERVO_FLOOR]);
   Log.HeatingWaterTemperature = Heating.WaterTemperature / LOG_HEATING_WATER_TEMP_LSB;

   Log.Driver = Driver;

   Log.FiltersAirPressure = FiltersAirPressure;          // Neprepocitavam, dilek pro ukladani je stejny jako dilek mereni
   Log.ElMotorOn = ElMotorOn;

   Log.FansAirPressure = 0;                // Zruseno
   if (Fan.Failure) {
      Log.FanFailure = YES;          // Kumulace
   }

   Log.Co2 = Co2.Value;                                  // Neprepocitavam, dilek pro ukladani je stejny jako dilek mereni
   if (Cooling.Failure) {
      Log.CoolingFailure = YES;      // Kumulace
   }

   Log.SuctionPressure = Cooling.SuctionPressureAverage;
   if (Charging.Failure) {
      Log.ChargingFailure = YES;  // Kumulace
   }
   Log.DieselOn = Diesel.On;

   Log.DischargePressure = Cooling.DischargePressureAverage;

   Log.Localization = Config.Localization;
   Log.HeatingCircuitState = Heating.HeatingCircuitState;
   Log.ChangeOil = Diesel.ChangeOil;
   if (Heating.Failure) {
      Log.HeatingFailure = YES;      // Kumulace
   }

   if (Alarm.ShowAlarm) {
      Log.Alarm = YES;    // Kumulace
   }
   Log.ManualFreshAir = FreshAir.Mode;
   Log.ManualFloor = FreshAir.FloorFlap.Mode;
   Log.EmergencyMode = Control.EmergencyMode;
   Log.TruckEngineRun = TruckEngineOn;
   Log.FanAutoMode = Fan.AutoMode;
   if (FiltersHighAirPressure) {
      Log.FiltersPressureOverLimit = YES;         // Kumulace
   }
   if (Co2.OverLimit) {
      Log.Co2OverLimit = YES;     // Kumulace
   }

   Log.FanPower = Fan.Power;
   if (Co2.Failure) {
      Log.Co2Failure = YES;       // Kumulace
   }

   Log.FreshAirMsb = (FreshAir.Percent >> 4) & 0x0F;    // Horni 3 bity

   //---------------------------------------------------------
   // Kontrola podminek pro zapis :
   if (!_UkladatZaznamy) {
      return;    // Mam nastaveno, ze nemam ukaldat do pameti - napr. behem tisku protokolu chci merit a regulovat, ale nesmi se ukladat
   }
   if (Time.Min == last_min) {
      return;        // tato minuta je jiz zapsana
   }
   if (Time.Min % Config.SavingPeriod != 0) {
      return;        // neni splnena perioda
   }
   // Aktualni datum a cas :
   Log.DateTime.Min = Time.Min;
   mkhour (Log.DateTime, Time.Hour);
   Log.DateTime.Day = Time.Day;
   Log.DateTime.Month = Time.Month;
   mkyear (Log.DateTime, Time.Year);
   Log.DateTime.Changed = Time.Changed;
   Time.Changed = NO;    // vzali jseme na vedomi, muzeme shodit
   if (!FifoWrite ()) {
      return;        // nepodaril se zapis, zkusime priste
   }//if
   last_min = Time.Min;   // cas posledniho zapisu
   LogClear ();      // nulovani kumulovanych velicin
   TempClearAverages ();          // Vynulovani prumeru teplot
   ControlResetDutyCycle ();      // Vynulovani mereni stridy topeni a chlazeni
   CoolingResetPressure ();       // Vynulovani prumeru tlaku chlazeni
} // AlogExecute

//-----------------------------------------------------------------------------
// Protokolovani udalosti
//-----------------------------------------------------------------------------

void LogWriteProtocol (byte type, dword parameter) {
   // Zapis do protokolu, <type> je typ operace s parametrem <parameter>
   byte __xdata__ *p1;
   byte __xdata__ *p2;

   if (!_UkladatZaznamy) {
      return;    // Mam nastaveno, ze nemam ukladat do pameti
   }
   // Do prvniho bajtu ulozim, ze jde o zaznam udalosti, ne o normalni zaznam teplot
   Event.RecordType = TMC_FIFO_PROTOCOL_TYPE;
   Event.Type = type;
   Event.Parameter = parameter;
   Event.Driver = Driver;

   // Fce FifoWrite zapisuje strukturu Log, ktera ma stejnou delku jako Event. Musim tedy zapisovana data ulozit do struktury Log
   // Vyuziju promennou type
   p1 = LogArray;
   p2 = EventArray;
   for (type = 0; type < sizeof (TLog); type++) {
      *p1 = *p2;
      p1++;
      p2++;
   }//for
   // aktualni datum a cas : vyplnim uz do Log
   Log.DateTime.Min = Time.Min;
   mkhour (Log.DateTime, Time.Hour);
   Log.DateTime.Day = Time.Day;
   Log.DateTime.Month = Time.Month;
   mkyear (Log.DateTime, Time.Year);
   Log.DateTime.Changed = Time.Changed;
   Time.Changed = NO;   // vzali jseme na vedomi, muzeme shodit
   if (!FifoWrite ()) {
      return;        // nepodaril se zapis, zkusime priste
   }
   LogClear ();      // nulovani kumulovanych velicin - musim to tady udelat, jinak by tam neco zbylo a ulozilo by se to v dalsim zaznamu
   return;
}// LogWriteProtocol

//-----------------------------------------------------------------------------
// Mazani FIFO
//-----------------------------------------------------------------------------

TYesNo LogReset (void)
// Smaze logger
{
   LogClear ();            // nuluj buffer
   return (FifoReset ());  // nove, prazdne FIFO
} // LogReset

//-----------------------------------------------------------------------------
// Mazani bufferu FIFO
//-----------------------------------------------------------------------------

void LogClear (void)
// Nulovani kumulovanych velicin loggeru
{
   memset (&Log, 0, sizeof (Log));
} // LogClear


void LogCreateStamp () {
   // Vyplneni razitka
   byte j;

   Stamp.DatumPorizeni.Min = Time.Min;
   mkhour (Stamp.DatumPorizeni, Time.Hour);
   Stamp.DatumPorizeni.Day = Time.Day;
   Stamp.DatumPorizeni.Month = Time.Month;
   Stamp.DatumPorizeni.Year = Time.Year;
   Stamp.MotoHodiny = Diesel.MotoHours;    // Do razitka ulozim i celkovy pocet nabehanych motohodin v okamziku kopirovani

#ifdef USE_TWO_SENSORS
   Stamp.UseTwoSensors = YES;
#else
   Stamp.UseTwoSensors = NO;
#endif // USE_TWO_SENSORS

#ifdef USE_FLOOR_FLAPS
   Stamp.UseFloorFlaps = YES;
#else
   Stamp.UseFloorFlaps = NO;
#endif // USE_FLOOR_FLAPS

#ifdef USE_ON_OFF_HEATING
   Stamp.UseOnOffHeating = YES;
#else
   Stamp.UseOnOffHeating = NO;
#endif // USE_ON_OFF_HEATING

#ifdef USE_COOLING
   Stamp.UseCooling = YES;
#else
   Stamp.UseCooling = NO;
#endif // USE_COOLING

   Stamp.KontrolniSuma = 0;         // jen pro vypocet sumy
   for (j = 0; j < sizeof (TStamp); j++) {
      sum += ((byte __xdata__ *)&Stamp)[j];
   }
   Stamp.KontrolniSuma = -sum;             // dohromady da nulu
}

//-----------------------------------------------------------------------------
// Kopie FIFO do externiho modulu
//-----------------------------------------------------------------------------

// makro na kopirovani dat do Xmem :
#define SaveData( item, size, data, stop)                \
   new_page = YES;                                       \
   addr     = offsetof( TXmem, item);                    \
   for( i = 0; i < size; addr++, i++){                   \
      WatchDog();                                        \
      if( new_page){                                     \
         new_page = NO;                                  \
         if( !XmemPageWriteStart( addr)){                \
            stop;                                        \
            return( NO);                                 \
         }                                               \
      }                                                  \
      d    = data;                                       \
      sum += d;                                          \
      XmemPageWriteData( d);                             \
      if( ((addr + 1) & (XMEM_PAGE_SIZE - 1)) == 0){     \
         XmemPageWritePerform();                         \
         new_page = YES;                                 \
      }                                                  \
   }                                                     \
   stop;                                                 \
   if( !new_page){                                       \
      XmemPageWritePerform();                            \
   }                                                     \

// makro na verifikaci Xmem :
#define VerifyData( item, size, data, stop)              \
   XmemBlockReadStart( offsetof( TXmem, item));          \
   for( i = 0; i < size; i++){                           \
      WatchDog();                                        \
      d    = XmemBlockReadData();                        \
      sum += d;                                          \
      if( d != data){                                    \
         XmemBlockReadStop();                            \
         stop;                                           \
         return( NO);                                    \
      }                                                  \
   }                                                     \
   stop;                                                 \
   XmemBlockReadStop();                                  \


//-----------------------------------------------------------------------------

TYesNo LogCopy (void)
// Prekopiruje interni EEPROM na externi modul
{
   word i, addr;
   byte d;
   byte new_page;                          // zacatek nove stranky

   if (!XmemIsPresent ()) {
      return(NO);                      // modul nepritomen
   }
   XmemInit ();                          // inicializace modulu
   sum = 0;                             // kontrolni soucet
  //---------------------------------------------------------------------------
  // kopirovani Logu z mistni EEPROM :
   EepBlockReadStart (TMC_FIFO_START); // zacatek Logu v Eep
   SaveData (Log, sizeof (TLogArray), EepBlockReadData (), EepBlockReadStop ());
   // kopirovani konfigurace z AutoConfig :
   SaveData (Config, sizeof (TConfig), ((byte __xdata__ *)&Config)[i], _nop_ ());
   // zapis nul do rezervy :
   SaveData (Spare, XMEM_SPARE, 0, _nop_ ());
   // Zapis razitka :
   LogCreateStamp ();
   SaveData (Stamp, sizeof (TStamp), ((byte __xdata__ *)&Stamp)[i], _nop_ ());

   //--------------------------------------------------------------
   // verifikace Logu :
   sum = 0;
   EepBlockReadStart (TMC_FIFO_START);        // zacatek Logu v Eep
   VerifyData (Log, sizeof (TLogArray), EepBlockReadData (), EepBlockReadStop ());
   // Verifikace Config :
   VerifyData (Config, sizeof (TConfig), ((byte __xdata__ *)&Config)[i], _nop_ ());
   // Verifikace rezervy :
   VerifyData (Spare, XMEM_SPARE, 0, _nop_ ());
   // Verifikace razitka :
   VerifyData (Stamp, sizeof (TStamp), ((byte __xdata__ *)&Stamp)[i], _nop_ ());
   if (sum != 0) {
      return(NO);
   }
   return(YES);
} // LogCopy

