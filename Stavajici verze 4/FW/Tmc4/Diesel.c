//*****************************************************************************
//
//    Diesel.c - Obsluha diesel motoru
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#include "Diesel.h"
#include "Tmc.h"
#include "Din.h"               // Digitalni vstupy
#include "Ain.h"               // Analogove vstupy
#include "Time.h"              // Datum a cas
#include "Cfg.h"               // Konfigurace
#include "Alarm.h"             // Indikace poruch
#include "Cooling.h"           // Regulace chlazeni
#include "Fifo\FArray.h"     // FIFO array
#include "Iep.h"        // Internal EEPROM
#include <stddef.h>            // makro offsetof

// POZOR, interne modul pracuje s motominutami
// nejmensi evidovane kvantum je minuta,
// mensi hodnota je povazovana za 0
// Mozna by bylo lepe pracovat jako LSB=6min,
// Potom lze zobrazovat jako desetinne cislo
// pred carkou hodnota/10 - motohodiny
// za carkou hodnota % 10 - desetiny hodiny (6min)


TDiesel __xdata__ Diesel;

#if defined(NO_DIESEL) && !defined(__TMCREMOTE__)
void DieselInit (){
   Diesel.On = NO;
   Diesel.MotoHours = 0;
   Diesel.ChangeOil = NO;
   Diesel.Temperature = 0;
   Diesel.Overheated = NO;
   Diesel.EcuError = NO;
}

#endif

#if !defined(__TMCREMOTE__) && !defined(NO_DIESEL)

// Staticke promenne
static   byte __xdata__ last_min;        // posledni zpracovana minuta
static   byte __xdata__ MotoMin;         // dilci pocet motohodin - motominuty, zapisuji se kazdych DSL_WRITE_PERIOD = 6 minut, tj. staci na bajt

// Prumerovani teploty dieselu
#define PRUMEROVANI_TEPLOTY_POCET 10   // Kolik prvku prumerovat (meri se kazdou sekundu, tj. 60=1x za minutu se vypocte prumer) (nezvysovat - pozor na preteceni)
typedef struct {
   unsigned int Suma;                   // Suma jednotlivych mereni
   unsigned char Pocet;                 // Pocet prvku v sume
   unsigned char PrvniMereni;           // Flag, ze se meri poprve a ma se pouzit prvni namerena hodnota (po zapnuti, aby se necekalo)
} TPrumerTeploty;
static TPrumerTeploty __xdata__ PrumerTeploty;  // Struktura pro prumerovani mnozstvi nafty

// Mereni teploty dieselu:
// Kalibracni konstanty (meri se 0-10V), max hodnota je v pulce prevodniku (0X7F):
#define MV_PER_ADC_UNIT       20
#define GND_OFFSET_CONST_MV   250
#define GND_OFFSET ((GND_OFFSET_CONST_MV/2)/MV_PER_ADC_UNIT)   

#define TEMP_AV_ERROR 0x0E     // chyba hlasena od autostartu

static byte code AinTable[]   = {
0x15, //>105�C
0x17, //~105�C
0x19, //~100�C
0x20, //~95�C
0x24, //90�C
0x49, //70�C
0x58, //60�C
0x74, //50�C
0x82, //40�C
0xB7, //~20�C
0xB8  //<20�C
};  // Napeti Ain

static byte code TempScaleTable[] = {
10,
9,
8,
7,
6,
5,
4,
3,
2,
1,
0
};   // pocet dilku stupnice 

// Pro oba motory
#define DSL_TEMP_AV_NOT_USED  0x01     // Pokud se cidlo nevyuziva, vstup se uzemni a dava 0V
#define DSL_TEMP_AV_ERROR     0xF5     // Pri poruse (odpojene cidlo) je na vstupu plne napeti

#define DISPLAY_MOTOHOUR( mh)   Diesel.MotoHours = (mh);                             \
                                Diesel.ChangeOil = ((mh) > Config.ChangeOilAtHours)

#define DSL_SIZE          sizeof( TDieselMotoHour)     // velikost polozky pole
#define DSL_ADDRESS       offsetof( TIep, DieselArray) // na konci EEPROM
#define DSL_WRITE_PERIOD  6                            // perioda zapisu do EEPROM - min


// Lokalni funkce :

static TYesNo MotoHourAdd (byte MotoMin);
// Zvysi pocet motohodin o <MotoMin> minut

//-----------------------------------------------------------------------------
// Kontrola motohodin
//-----------------------------------------------------------------------------

void DieselInit ()
// Nastaveni inicialniho stavu pocitadla motohodin
{
   word mh;
   last_min = -1;
   MotoMin = 0;

   Diesel.On = NO;
   Diesel.Temperature = 0;
   Diesel.Overheated = NO;
   Diesel.EcuError = NO;

   // Tato fce se vola jen 1x po zapnuti jednotky => vyuziji ji i pro inicializaci prumerovani stavu nafty
   PrumerTeploty.Suma = 0;
   PrumerTeploty.Pocet = 0;
   PrumerTeploty.PrvniMereni = YES;  // Nastavim flag, aby ihned po zmereni prvni hodnoty zobrazil nejakou teplotu, aby to tam nesvitilo prazdne (to delam jen po zapnuti jednotky)

   // Nactu motohodiny a nastavim flag pro vymenu oleje
   mh = DieselRead ();
   DISPLAY_MOTOHOUR (mh);
} // DieselInit

//-----------------------------------------------------------------------------
// Kontrola motohodin
//-----------------------------------------------------------------------------

void DieselExecute (void)
// Detekce behu motoru a aktualizace motohodin
{
   byte gndOffset = 0;
   byte dieselTemperature = 0;
   if (Cooling.On) {
      gndOffset = GND_OFFSET;
   }
   Diesel.EcuError = NO;

   // Teplota dieselu
   if (AinDieselTemperature >= DSL_TEMP_AV_ERROR) {
      // Cidlo je odpojene (porucha)
      Diesel.Temperature = DSL_TEMP_STEPS - 1;    // Maximalni teplota, aby to hlasilo chybu
   }
   else if (AinDieselTemperature <= TEMP_AV_ERROR) {
      // Diesel Deutz indikuje n�jakou chybu (ne p�eh��t�), v takov�m p��pad� indikuje blikaj�c�m motorem ne overheated
      Diesel.EcuError = YES;
      Diesel.Overheated = NO;
      Diesel.Temperature = 0;
      AlarmSet ();
   }
   else {     
      dieselTemperature = ((word)((word)AinDieselTemperature + (word)gndOffset) > 0xFF) ? 0xFF : AinDieselTemperature + gndOffset;
      PrumerTeploty.Suma += AinMapValue(dieselTemperature, AinTable, TempScaleTable, ArrayLength(AinTable));
      PrumerTeploty.Pocet++;
      if (PrumerTeploty.Pocet >= PRUMEROVANI_TEPLOTY_POCET || PrumerTeploty.PrvniMereni) {
         // Uz jsem nasumoval dost vzorku nebo jde o prvni mereni
         PrumerTeploty.PrvniMereni = NO;  // Kazdopadne shodim flag, ktery uz se v prubehu behu jednotky nikdy nenahodi (az po dalsim zapnuti)
         PrumerTeploty.Suma /= PrumerTeploty.Pocet;   // Vypoctu prumer
         Diesel.Temperature = PrumerTeploty.Suma;
         // Vynuluju sumu
         PrumerTeploty.Suma = 0;
         PrumerTeploty.Pocet = 0;
      }//if
   }//else


   // Beh dieselu
   Diesel.On = (TYesNo)(DinDieselRun == DIN_DIESEL_RUNNING);

   // Prehrati dieselu
   if (Diesel.On && Diesel.Temperature == DSL_TEMP_STEPS - 1) {
      // Dosazena maximalni teplota
      Diesel.Overheated = YES;
      AlarmSet ();
   }
   else {
      Diesel.Overheated = NO;
   }

   // Motohodiny
   if (!Diesel.On) {
      if (MotoMin != 0) {
         // vypnuti dieselu, zapis rozpracovane minuty
         MotoHourAdd (MotoMin);
      }
      last_min = -1;     // dalsi kontrola pri zapnuti dieselu
      MotoMin = 0;      // dilci pocet motohodin
      return;            // neni nastartovan
   }

   // Kontrola podminek pro zvyseni poctu motohodin :
   if (Time.Min == last_min) {
      return;            // tato minuta je jiz aktualizovana
   }
   last_min = Time.Min; // zapamatuj pro priste
   MotoMin++;          // uplynula dalsi minuta
   // Kontrola podminek pro zapis poctu motohodin :
   if (Time.Min % DSL_WRITE_PERIOD) {
      return;            // neni zapisova perioda
   }

   // Zrychlene:
 //  MotoMin += 240;

   if (!MotoHourAdd (MotoMin)) {
      return;            // nepodarilo se zapsat
   }
   MotoMin = 0;        // zapis se povedl, vynuluj dilci pocet
} // DieselExecute

//-----------------------------------------------------------------------------
// Nulovani motohodin
//-----------------------------------------------------------------------------

void DieselSet (word Motohours, word Oil)
// Nastaveni poctu motohodin a vymeny oleje
{
   DieselReset ();                                              // vymazani celeho pole
   DieselWrite (Motohours);                                     // Nastaveni pocitadla motominut
   // nastaveni novych kontrolnich bodu :
   Config.ChangeOilAtHours = Oil;
   CfgSave ();                                                  // ulozit do EEPROM
   DISPLAY_MOTOHOUR (Motohours);                                // zobrazit
} // DieselClear

//-----------------------------------------------------------------------------
// Kvitace Oleje
//-----------------------------------------------------------------------------

void DieselQuitOil (void)
// Odkvituje vymenu oleje
{
   // nastaveni noveho kontrolnich bodu :
   Config.ChangeOilAtHours = Diesel.MotoHours + Config.ChangeOilPeriod;    // Pocitam periodu az od okamziku, kdy to vymeni, ne od okamziku rozblikani na displeji
   CfgSave ();                                                  // ulozit do EEPROM
   Diesel.ChangeOil = NO;                                 // zruseni alarmu
} // DieselQuitOil

//-----------------------------------------------------------------------------
// Cteni motohodin
//-----------------------------------------------------------------------------

word DieselRead (void)
// Cteni motohodin
{
   TFarData fd;

   fd.dw = FarRead (DSL_ADDRESS, DSL_COUNT, DSL_SIZE);
   return(fd.dw / 60);      // prepocitej motominuty na motohodiny
} // DieselRead

//-----------------------------------------------------------------------------
// Nastaveni motohodin
//-----------------------------------------------------------------------------

TYesNo DieselWrite (word MotoHour)
// Zapis motohodin
{
   TFarData fd;

   fd.dw = (dword)MotoHour * 60L;       // prepocitej motohodiny na motominuty
   fd.dw &= 0x00FFFFFF;         // Ukladam jen na 3 bajty, horni byte je marker
   if (!FarWrite (fd.dw, DSL_ADDRESS, DSL_COUNT, DSL_SIZE)) {
      return(NO);
   }
   return(YES);
} // DieselWrite

//-----------------------------------------------------------------------------
// Reset
//-----------------------------------------------------------------------------

TYesNo DieselReset (void)
// Vymaze celou oblast
{
   if (!FarReset (DSL_ADDRESS, DSL_COUNT, DSL_SIZE)) {
      return(NO);
   }
   return(YES);
} // DieselReset

//-----------------------------------------------------------------------------
// Aktualizace motohodin
//-----------------------------------------------------------------------------

static TYesNo MotoHourAdd (byte MotoMin)
// Zvysi pocet motohodin o <MotoMin> minut
{
   TFarData fd;

   // Pokud je motor typu Kubota a pokud se prave nechladi (tj. diesel jede pomalu), prictu pouze 2/3 pozadovanych minut
   // Testuju primo sepnuti spojky, protoze v rezimu chlazeni muze byt dlouhodobe a chladit se pritom nemusi. Pokud se spojka sepne jen na par
   // sekund (napr. minimalni manualni chlazeni), sem-tam se spojka nepochyti, ale chladi se stejne tak malo, ze to je jakoby nechladil vubec.
   // U motoru Deutz se motohodiny pocitaji 1:1 nehlede na otacky
   if (Config.DieselType == DIESEL_TYPE_KUBOTA && !Cooling.On) {
      // Je osazen motor Kubota a nechladi se => snizim motominuty
      if (MotoMin > 1) {
         // Snizuju jen hodnotu vyssi nez 1, jinak by mne vysla 0. 1 minutu ulozim jako 1 minutu - melo by nastat jen pri zapnuti nebo vypnuti dieselu,
         // nikdy ne v prubehu chodu.
         MotoMin = (word)((word)MotoMin * 2 / 3);           // Za normalniho chodu jsou MotoMin max. 6, ale pri testech pouzivam 240 => pocitam na word.
      }
   }
   // precti stavajici hodnotu :
   fd.dw = FarRead (DSL_ADDRESS, DSL_COUNT, DSL_SIZE);
   fd.dw += (dword)MotoMin;     // pozor na datovy typ TDieselMotoHour
   fd.dw &= 0x00FFFFFF;         // Ukladam jen na 3 bajty, horni byte je marker
   if (!FarWrite (fd.dw, DSL_ADDRESS, DSL_COUNT, DSL_SIZE)) {
      return(NO);
   }
   DISPLAY_MOTOHOUR (fd.dw / 60);  // zobrazit
   return(YES);
}  // MotoHourAdd

#endif // __TMCREMOTE__

