//*****************************************************************************
//
//    AirPress.c - Mereni tlakove ztraty vzduchu
//    Version 1.0
//
//*****************************************************************************


#ifndef __AirPress_H__
   #define __AirPress_H__

#include "Hardware.h"     // pro podminenou kompilaci

extern byte __xdata__ FiltersAirPressure;       // Tlakova ztrata na filtrech s dilkem AIR_PRESSURE_LSB Pa
extern byte __xdata__ FiltersHighAirPressure;   // YES/NO Prilis vysoka tlakova ztrata na filtrech

#define AIR_PRESSURE_LSB     5                  // Hodnotu ukladam s dilkem 5 Pa, i nejvyssi rozsah 500 Pa tak vyjde na 7 bitu

void AirPressureInit();
  // Inicializace

void AirPressureCalc();
  // Vypocte tlakove ztraty

#endif
