//*****************************************************************************
//
//    Cfg.h - Configuration load/save
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

//#define __DEBUG__

#include "Tmc.h"
#include "Cfg.h"
#include "iep.h"
#include "Thermo\Thermo.h"      // TempMk1C()
#include <stddef.h>       // makro offsetof

#define CFG_IEP_START   offsetof( TIep, Config)    // adresa zacatku parametru v EEPROM

// Lokalni funkce :

static void SetDefaults( void);
// Nastavi implicitni hodnoty parametru

static byte CalcChecksum( void);
// Spocita a vrati zabezpeceni

//-----------------------------------------------------------------------------
// Load
//-----------------------------------------------------------------------------

TYesNo CfgLoad( void)
// Nacte konfiguraci z EEPROM
// Vraci NO, pro neplatnou konfiguraci (dosazeni default hodnot)
{
word           address;
byte __xdata__ *cfg;
byte           i;

#ifdef __DEBUG__
   DisplayGotoRC( 29, 0);
#endif
   address = CFG_IEP_START;      // startovni adresa v EEPROM
   cfg = (byte *)&Config;     // pretypovani na byte array
   // Pozor, prenos XDATA->XDATA nelze pouzit blokove cteni :
   for( i = 0; i < sizeof( TConfig); i++){
      *cfg = IepRead( address++);
      cfg++;
   }
   // kontrola verze a sumy
   if (Config.VersionMajor != VERSION_MAJOR || Config.VersionMinor != VERSION_MINOR || CalcChecksum() != Config.CheckSum) {
      SetDefaults();
      CfgSave();
      return( NO);
   }
   return( YES);
} // CfgLoad

//-----------------------------------------------------------------------------
// Save
//-----------------------------------------------------------------------------

TYesNo CfgSave(void)
// Ulozi vsechny polozky do EEPROM
// Vraci NO, nepovedl-li se zapis
{

word           address;
byte __xdata__ *cfg;
byte            i;

   if (!IepAccuOk()) {
     return NO;  // Slabe napajeni (dal uz se nekontroluje)
   }

   // sluzebni informace :
   Config.VersionMajor = VERSION_MAJOR;
   Config.VersionMinor = VERSION_MINOR;
   Config.VersionBuild = VERSION_BUILD;
   Config.VersionHw    = VERSION_HW;
   Config.CheckSum = CalcChecksum();
   // zapis celou konfiguraci :
   cfg = (byte *)&Config;     // pretypovani na byte array
   address = CFG_IEP_START;      // startovni adresa v EEPROM
   if( !IepPageWriteStart()){
        return( NO);
   }
   for( i = 0; i < sizeof( TConfig); i++){
        IepPageWriteData( address, *cfg);
        address++;
        cfg++;
   }
   IepPageWritePerform();
   return( YES);
} // CfgSave

//-----------------------------------------------------------------------------
// Default
//-----------------------------------------------------------------------------

static void SetDefaults( void)
// Nastavi implicitni hodnoty parametru
{
   byte j;

   Config.VersionMajor         = VERSION_MAJOR;
   Config.VersionMinor         = VERSION_MINOR;
   Config.VersionBuild         = VERSION_BUILD;
   Config.VersionHw            = VERSION_HW;
   Config.IdentificationNumber = 1;
   Config.TargetTemperature    = DEFAULT_TARGET_TEMPERATURE_C;
   Config.Localization         = LOC_CELE;
   Config.AutoHysteresis       = TempMk01C(1, 0);
   Config.RegKp                = 60;
   Config.RegTi                = 1000;
   Config.RegTd                = 40;
   Config.MaxOvershootSum      = 30;    // 30 C*s, tj. rozdil 1C po dobu max. 30sec
   Config.MaxOffsetAbove       = DEFAULT_MAX_OFFSET_ABOVE_C;
   Config.MaxOffsetBelow       = DEFAULT_MAX_OFFSET_BELOW_C;
   Config.ChangeOilPeriod      = DEFAULT_OIL_PERIOD_KUBOTA;
   Config.ChangeOilAtHours     = DEFAULT_OIL_PERIOD_KUBOTA;   // Beru jakoby chod od nuly, tj. prvni vymena v 350 motohodinach
   Config.SavingPeriod         = 10;
   Config.Login                = NO;
   Config.MaxFiltersAirPressure = 160;
   Config.MaxCo2               = 3500;

   Config.FreshAirRegKp        = 100;   // 1C rozdil = 100% (tak fungovalo TMC postaru)
   Config.FreshAirRegTi        = 1200;  // 1C rozdil po dobu 60sec a Kp=100 vyvola zmenu klapek 5%
   Config.FreshAirRegTd        = 5;     // Jen velmi jemne, nesmi oscilovat jako servo topeni

   Config.Co2Target            = 2500;
   Config.Co2RegKp             = 2000;  // 0.020, ale pozor, CO2 mam s dilkem CO2_LSB = 100, tj. Kp posouvam o 2 rady a v TMC se to zobrazuje jako 2.000
   Config.Co2RegTi             = 1000;
   Config.Co2RegTd             = 40;
   Config.TempUnits            = UNITS_CELSIUS;

   Config.MinFreshAirForbidden = 0;     // Zakazane pasmo cerstveho vzduchu defaultne nepouzivam
   Config.MaxFreshAirForbidden = 0;

   Config.DieselType           = DIESEL_TYPE_KUBOTA;

   // Reserved bajty nastavim default na 0
   for (j = 0; j < CONFIG_RESERVED_SIZE; j++) {
     Config.Reserved[j] = 0;
   }

   Config.CheckSum             = CalcChecksum();
   CfgSave();
} // SetDefaults

//-----------------------------------------------------------------------------
// Checksum
//-----------------------------------------------------------------------------

static byte CalcChecksum( void)
// Spocita a vrati zabezpeceni
{
byte __xdata__ *cfg;
byte            i;
byte            sum;

   cfg = (byte *)&Config;     // pretypovani na byte array
   sum = 0;
   // bez koncove sumy :
   for( i = 0; i < sizeof( TConfig) - 1; i++){
      sum += cfg[ i];
   }
   return( sum);
} // CalcChecksum
