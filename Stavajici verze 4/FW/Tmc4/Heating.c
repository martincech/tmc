//
//    Heating.c - Ovladani topeni
//    Version 1.0
//
//*****************************************************************************

#include "Heating.h"
#include "Servo.h"             // Kontrola stavu serv
#include "Din.h"               // Digitalni vstupy
#include "Dout.h"              // Digitalni vystupy
#include "Aout.h"              // Analogove vystupy
#include "Alarm.h"             // Indikace poruch
#include "FAir.h"              // Regulace cerstrveho vzduchu - prepocet procent na polohu serva
#include "Temp.h"              // Teploty
#include "Thermo\Thermo.h"     // Prevod teplot

THeating __xdata__ Heating;

#ifndef __TMCREMOTE__

#include "Stat\Average.h"    // Klouzavy prumer

// Promenne pro poruchu
static byte __xdata__ HeatingCounter = 0;     // Pocitadlo doby behu topeni od nahozeni
#define WAIT_AFTER_HEATING_START 10           // Kolik sekund cekat po zapnuti topeni na vyhodnocovani chyby topeni

static byte __xdata__ CounterErrorFree;       // Pocitadlo doby po odezneni chyby topeni (LEDka blika, tj. chytnu ji jen nahodne)
#define WAIT_AFTER_HEATING_ERROR 5            // Kolik sekund pockat po odezneni chyby topeni, nez se vyhlaseni poruchy zrusi

#define HEATING_MIN_DAC 0x0B                  // Vystup DA prevodniku, aby dal 2V (zcela zavrene servo)
#define HEATING_MAX_DAC 0x3E                  // Vystup DA prevodniku, aby dal 10V (zcela otevrene servo)

// Hystereze pro bufiky
#ifdef USE_ON_OFF_HEATING
  #define HEATING_ON_OFF_HYSTERESIS        TempMk01C(0, 3)      // Hystereze pro spinani bufiku +/- 0.3C
#endif // USE_ON_OFF_HEATING

// Teplota vody v okruhu
#define HEATING_WATER_TEMP_INVALID              0       // Neplatna (zatim nezmerena) teplota - pozor, at se vleze do logu

#define HEATING_POWER_THRESHOLD1                30      // Pri otevreni serva nad 30% zase nahodim topeni, aby vodu zahralo
#define HEATING_WATER_TEMP_MIN_THRESHOLD1       50      // Pri poklesu teploty vody pod 50C zase nahodim topeni, aby vodu zahralo
#define HEATING_POWER_THRESHOLD2                99      // Pri otevreni serva nad 99% (tj. naplno) zvednu minimalni teplotu topeni na 65C
#define HEATING_WATER_TEMP_MIN_THRESHOLD2       65      // Pri poklesu teploty vody pod 65C zase nahodim topeni, aby vodu zahralo

// Vypnuti cerpadla po delsi dobe necinnosti topeni
static word __xdata__ ServoClosedCounter = 0;      // Pocitadlo sekund, kdy bezi pouze cerpadlo bez topeni
#define SERVO_CLOSED_TIMEOUT            (15 * 60)  // Pokud bezi 30min pouze cerpadlo bez topeni, tak cerpadlo vypnu, bezi zbytecne

static byte __xdata__ FailureCheckCounter = 0;  // Pocitadlo doby po zhasnuti plamene
#define WAIT_AFTER_FLAME_OFF            30      // Po jakou dobu od zhasnuti plamene provadet kontrolu poruchy topeni

// Prepocet teploty na interni format regulatoru :
extern int __xdata__ RequestedTemperature;

// Vypocet prumeru vykonu topeni
static TAverage __xdata__ HeatingPercentAverage;      // Klouzavy prumer

static byte PrepoctiProcentaNaDAC(byte Procenta);
  // Prepocte procenta 0..HEATING_MAX na analogovy vystup (DAC) serva topeni, vcetne linearizace vykonu


//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void HeatingInit() {
  // Inicializace
  Heating.Percent = 0;
  Heating.Position = 0;
  Heating.Flame1 = NO;
  Heating.Flame2 = NO;
  Heating.Failure = NO;
  Heating.ServoFailure = NO;
  AoutSetHeatingServo(PrepoctiProcentaNaDAC(0));       // Zavru ventil (po inicializaci je na Aout=0.3V a zacne to hlasit poruchu serva topeni)
  HeatingResetDutyCycle();
  Heating.WaterTemperature = HEATING_WATER_TEMP_INVALID;

  // Inicializace klouzaveho prumeru podle konfigurace
  AverageSetLength(&HeatingPercentAverage, AVERAGE_CAPACITY);
  AverageClear(&HeatingPercentAverage);
  Heating.AveragePercent = 0;

// U bufiku vyresim warningy a dal se tim nemusim zabyvat
#ifdef USE_ON_OFF_HEATING
  AverageAdd(&HeatingPercentAverage, 0);
  AverageGetAverage(&HeatingPercentAverage);
#endif // USE_ON_OFF_HEATING
}

//-----------------------------------------------------------------------------
// Spinani napajeni topeni
//-----------------------------------------------------------------------------

#ifdef USE_ON_OFF_HEATING

// U bufiku zapnu napajeni a zapamatuju si stav kvuli hysterezi
static void SwitchPower(TYesNo State) {
  DoutSetHeatingAndPump(State);
  if (State && !Heating.On) {
    HeatingCounter = 0;         // Kdyz zapinam bufik, zapinam mu napajeni, poruchu budu opet pocitat od nuly
  }
  Heating.On = State;
}

#else

// U teplovodniho topeni jen zapnu
static void SwitchPower(TYesNo State) {
  if (State) {
    return;     // Tento stav neobsluhuju
  }
  // Vypnu oba vystupy
  DoutSetHeatingAndPump(NO);
  DoutSetHeatingPump(NO);
}

#endif // USE_ON_OFF_HEATING


//-----------------------------------------------------------------------------
// Vypnuti topeni
//-----------------------------------------------------------------------------

void HeatingStop() {
  // Vypne topeni a vynuluje poruchu
  SwitchPower(NO);                      // Vypnu cerpadlo
  HeatingInit();
  HeatingCounter = 0;                   // Pri pristim zapnuti budu opet pocitat od nuly
  Heating.WaterTemperature = HEATING_WATER_TEMP_INVALID;         // Aby tam pri chlazeni netrcela posledni hodnota
}

//-----------------------------------------------------------------------------
// Test zavreneho topeni
//-----------------------------------------------------------------------------

#ifdef USE_COOLING

TYesNo HeatingFullyClosed() {
  // Vrati YES, pokud je ventil topeni plne uzavren nebo je servo odpojeno/v chybe
  return (TYesNo)(ServoFullyClosed(SERVO_HEATING) || ServoStatus[SERVO_HEATING] > SERVO_STATUS_RUNNING);
}

#endif // USE_COOLING


//-----------------------------------------------------------------------------
// Kontrola poruchy
//-----------------------------------------------------------------------------

static void HeatingCheckFailure() {
  // Zkontroluje poruchu topeni a poruchu serva topeni

#ifdef USE_ON_OFF_HEATING

  // Poruchu bufiku kontroluju jen pokud je opravdu zapnuty
  Heating.ServoFailure = NO;    // Servo neni osazene, tj. porucha nemuze byt
  if (!Heating.On) {
    // Topeni netopi, porucha nemuze byt
    Heating.Failure = NO;
    return;
  }

#else

  // Servo topeni
  Heating.ServoFailure = NO;
  if (ServoStatus[SERVO_HEATING] == SERVO_STATUS_TIMEOUT || ServoStatus[SERVO_HEATING] == SERVO_STATUS_CONTROL || ServoStatus[SERVO_HEATING] == SERVO_STATUS_OFF) {
    Heating.ServoFailure = YES;
    AlarmSet();         // Vyhlasim alarm
  }
  // Samotne Webasto - tuto poruchu zacnu sledovat az po nekolika sekundach po zapnuti topeni a poruchu vyhlasim po prvnim vyskytu chyby.
  // V pripade zmizeni chyby cekam nejakou dobu a az potom chybu zrusim (LED poruchy blika, tj. nekdy ji chytnu a nekdy ne)

  // Poruchu Webasta kontroluju jen pokud je Webasto opravdu zapnute
  if (Heating.HeatingCircuitState != HEATING_CIRCUIT_START_HEATING
   && Heating.HeatingCircuitState != HEATING_CIRCUIT_HEATING
   && Heating.HeatingCircuitState != HEATING_CIRCUIT_CHECK_FAILURE) {
    // Topeni netopi, porucha nemuze byt
    Heating.Failure = NO;
    return;
  }

#endif // USE_ON_OFF_HEATING

  // Topeni prave topi, chybu kontroluju
  if (HeatingCounter < WAIT_AFTER_HEATING_START) {
    HeatingCounter++;   // Jeste cekam
  } else {
    // Od startu topeni uz ubehla nejaka doba, muzu testovat poruchu Webasta
    if (DinHeatingCoolingError == DIN_COOLING_FAILURE) {
      Heating.Failure = YES;
      CounterErrorFree = 0;             // Snuluju pocitadlo odezneni chyby
    } else {
      // Pocitam dobu odezneni poruchy a az pak poruchu zrusim
      CounterErrorFree++;
      if (CounterErrorFree > WAIT_AFTER_HEATING_ERROR) {
        CounterErrorFree = WAIT_AFTER_HEATING_ERROR;            // Aby counter nepretekl
        Heating.Failure = NO;
      }
    }
    if (Heating.Failure) {
      // Chyba se vyskytla bud v tomto kroku, nebo jeste trva z minula, alarm kazdopadne stale trva az do prepnuti rezimu
      AlarmSet();         // Vyhlasim alarm
    }
  }//else
}

//-----------------------------------------------------------------------------
// Prepocet procent na analogovy vystup serva
//-----------------------------------------------------------------------------

//#define SERVO_TYPE_1    1       // Stare servo s nelinearnim prubehem vykonu versus poloha serva
//#define SERVO_TYPE_2    1       // Nove servo od 28.4.2008 s castecnou linearizaci
//#define SERVO_TYPE_3    1       // Tricestny ventil od 23.9.2009 (Volvo)
#define SERVO_TYPE_4    1       // Tricestny ventil od 29.3.2012 (Novy typ radiatoru)

// Prevodni tabulka topny vykon versus poloha serva topeni, musi zacinat 0 a koncit 100. Namereno 30.1.2008 na Volvu Svedsko.
#ifdef SERVO_TYPE_1
  #define POWER_TABLE_COUNT     8                                               // Pocet bodu v prevodni tabulce
  byte code PowerTable[POWER_TABLE_COUNT]    = {0, 6, 8, 20, 32, 43, 95, 100};  // Vykon topeni v %
  byte code PositionTable[POWER_TABLE_COUNT] = {0, 3, 9, 14, 17, 19, 33, 100};  // Poloha serva v %
#elif defined SERVO_TYPE_2
  #define POWER_TABLE_COUNT     6                                               // Pocet bodu v prevodni tabulce
  byte code PowerTable[POWER_TABLE_COUNT]    = {0, 10, 35, 76, 95, 100};        // Vykon topeni v %
  byte code PositionTable[POWER_TABLE_COUNT] = {0, 15, 29, 38, 46, 100};        // Poloha serva v %
#elif defined SERVO_TYPE_3
  #define POWER_TABLE_COUNT     6                                               // Pocet bodu v prevodni tabulce
  byte code PowerTable[POWER_TABLE_COUNT]    = {0, 3, 12, 44, 59, 100};         // Vykon topeni v %
  byte code PositionTable[POWER_TABLE_COUNT] = {0, 9, 15, 25, 33, 100};         // Poloha serva v %
#elif defined SERVO_TYPE_4
  #define POWER_TABLE_COUNT     4                                               // Pocet bodu v prevodni tabulce
  byte code PowerTable[POWER_TABLE_COUNT]    = {0, 50, 80, 100};                // Vykon topeni v %
  byte code PositionTable[POWER_TABLE_COUNT] = {0, 13, 19, 100};                // Poloha serva v %
#endif // SERVO_TYPE_4

static byte PrepoctiProcentaNaDAC(byte Procenta) {
  // Prepocte procenta 0..HEATING_MAX na analogovy vystup (DAC) serva topeni, vcetne linearizace vykonu

  // Korekce nelinearni zavislosti vykonu topeni na poloze serva:
  Procenta = AinCalcValue(Procenta, PowerTable, PositionTable, POWER_TABLE_COUNT);

  // Nastaveni DAC podle zkorigovaneho vykonu:
  return (word)((word)HEATING_MIN_DAC + (word)Procenta * (word)(HEATING_MAX_DAC - HEATING_MIN_DAC) / (word)HEATING_MAX);
}

//-----------------------------------------------------------------------------
// Start topeni v automatickem rezimu
//-----------------------------------------------------------------------------

void HeatingAutoStart() {
  // Zahaji topeni v automatickem rezimu
#ifdef USE_ON_OFF_HEATING
  SwitchPower(NO);              // Bufik necham zatim vypnuty
  HeatingCounter = 0;
#endif // USE_ON_OFF_HEATING
  ServoResetTimer(SERVO_HEATING);  // Nuluju pocitadlo chyby serva
  Heating.ServoFailure = NO;    // Porucha nemuze byt
  // Ohrev vody ve spodnim okruhu
  Heating.WaterTemperature    = HEATING_WATER_TEMP_INVALID;
  Heating.HeatingCircuitState = HEATING_CIRCUIT_INIT;
  // Prumerovani vykonu
  AverageClear(&HeatingPercentAverage);
  Heating.AveragePercent = 0;

  // Vyplnim strukturu PID regulatoru
  Heating.Pid.SamplingPeriod = REG_TS;
  Heating.Pid.KpDenominator  = 1;       // Kp je cele cislo
  Heating.Pid.TiMax          = 1000;
  HeatingRegulatorUpdateParameters();

  Heating.Pid.LsbDenominator = 16;      // LSB teploty je 1/16
  // Target do PID ukladam az v HeatingRegulatorStep(), tady neni treba

  PidInit(&Heating.Pid);
}

//-----------------------------------------------------------------------------
// Predani parametru
//-----------------------------------------------------------------------------

void HeatingRegulatorUpdateParameters() {
  // Preberu parametry z configu do PID regulatoru
  Heating.Pid.Kp = Config.RegKp;
  Heating.Pid.Ti = Config.RegTi;
  Heating.Pid.Td = Config.RegTd;
}


//-----------------------------------------------------------------------------
// Krok topeni v automatickem rezimu
//-----------------------------------------------------------------------------

#ifdef USE_ON_OFF_HEATING

void HeatingAutoExecute(int Average) {
  // Provede krok regulatoru ON/OFF v rezimu auto topeni, volat 1x za sekundu. <Average> je prumerna teplota podle lozeni ve vnitrnim formatu
  TYesNo NewHeatingOn;

  if (Average == AUTO_TEMP_ERROR) {
    SwitchPower(NO);            // Teplotni cidlo je odpojene, vypnu topeni
    Heating.Percent = 0;        // Indikace vykonu
    return;
  }

  // Hystereze
  NewHeatingOn = Heating.On;
  if (Heating.On) {
    // Topeni je zapnuto
    if (Average > RequestedTemperature + HEATING_ON_OFF_HYSTERESIS) {
      // Teplota nad nastavenou => vypnu
      NewHeatingOn = NO;
    }
  } else {
    if (Average < RequestedTemperature - HEATING_ON_OFF_HYSTERESIS) {
      // Teplota pod nastavenou => zapnu
      NewHeatingOn = YES;
    }
  }

  // Zapnu nebo vypnu topeni
  SwitchPower(NewHeatingOn);    // Fce detekuje prechod, musim ji predat extra parrametr, ne primo Heating.On

  // Vypoctu stridu horeni plamene bufiku (bufik beru jako topeni cislo 1, topeni 2 zustava vypnute)
  // Pozor, u bufiku nelze vypocitat presnou stridu plamene, protoize bufik nema vystup horeni plamene. Pocitam jen stridu zapnuti bufiku,
  // coz neodpovida plamenu. Po zapnuti bufiku se nahodi plamen a nasledne se sam vypina a zapina pro udrzeni teploty. O zhasnuti plamene
  // bufik nijak neinformuje, vse se deje jen vevnitr. Stridu nyni pocitam jen kvuli indikaci vykonu topeni, stridu plamene musim do zaznamu
  // ulozit nulovou!
  DutyCycleExecute(&Heating.Flame1DutyCycle, Heating.On);

  // Vykon topeni je vlastne zmerena strida zapnuti bufiku
  Heating.Percent = Heating.Flame1DutyCycle.Percent;

  // Nastavim stridu plamene na nulu a tim to mam vyresene, do logu se ulozi nula, coz potrebuji. Pocitadla stridy zustavaji beze zmeny, takze
  // v dalsim kroku se opet vypocte spravna strida. Neni to moc systemove reseni, ale aspon nemusim resit ukladani stridy plamene do logu zvlast
  // pro bufik.
  Heating.Flame1DutyCycle.Percent = 0;

  // Kontrola poruchy
  HeatingCheckFailure();
}

#else

static void HeatingRegulatorStep(int temp) {
  // Krok regulatoru topeni, prumerna teplota ve skrini je <temp> (ve vnitrnim formatu)

  // Vypoctu stridu horeni plamene u obou topeni, to muzu udelat nezavisle na regulaci a hlavne 1x za sekundu
  DutyCycleExecute(&Heating.Flame1DutyCycle, Heating.Flame1);
  DutyCycleExecute(&Heating.Flame2DutyCycle, Heating.Flame2);

  if (temp == AUTO_TEMP_ERROR) {
    // zadna pozadovana sonda nemeri
    Heating.Percent = 0;    // Zavru ventil
    return;
  }

  // Krok regulatoru
  Heating.Pid.Target = RequestedTemperature >> 4;       // Update cilove teploty, mohla se zmenit. Spodni 4 bity se nemeri
  PidStep(&Heating.Pid, temp >> 4);                     // Spodni 4 bity se nemeri
  Heating.Percent = Heating.Pid.Output;                 // Preberu vystup regulatoru

  // Po vypoctu vykonu pridam vysledek do klouzaveho prumeru (1x za 10sec)
  AverageAdd(&HeatingPercentAverage, Heating.Percent);
  Heating.AveragePercent = AverageGetAverage(&HeatingPercentAverage);
} // HeatingRegulatorStep

// Teplota vody v okruhu
#define WATER_TABLE_COUNT    8
static byte code AinWaterTemperatureTable[WATER_TABLE_COUNT] = {0x6B, 0x80, 0x91, 0xB7, 0xD8, 0xE2, 0xEC, 0xF4};      // Napeti Ain
static byte code WaterTemperatureTable[WATER_TABLE_COUNT] = {90, 85, 80, 60, 40, 30, 20, 0};                   // Teplota v C
#define WATER_AIN_LOW_LIMIT     0x0B            // Odpovida napeti 0.5V, pod tuto hodnotu povazuju cidlo za zkratnute

static bool IsWaterTempLow() {
  // U Mika se stalo, ze regulator byl v koncich: teplota vody byla 53C, takze topeni bylo vypnute, ale servo bylo na 100%
  // a voda o teplote 53C nestacila na vytopeni. Pro urceni meze teploty vody se nyni vyuziva poloha serva topeni jako jasny
  // ukazatel toho, zda topeni stiha nebo ne. Pokud je servo topeni otevrene naplno, je treba nastavit vyssi teplotu vody
  // v okruhu, protoze topeni nestiha.

  // Pokud je servo topeni otevrene na 100%, znamena to, ze topeni nestiha, resp. neni uz zadna rezerva pro regulaci
  // teploty. V tom pripade nastavim minimalni teplotu vody v okruhu na 65C.
  if (Heating.AveragePercent > HEATING_POWER_THRESHOLD2 && Heating.WaterTemperature < HEATING_WATER_TEMP_MIN_THRESHOLD2) {
    return YES;
  }

  // Pokud je servo otevrene na 30-99%, staci minimalni teplota vody 50C.
  if (Heating.AveragePercent > HEATING_POWER_THRESHOLD1 && Heating.WaterTemperature < HEATING_WATER_TEMP_MIN_THRESHOLD1) {
    return YES;
  }

  // Pokud je servo otevrene na mene nez 30%, topeni zatim nezapinam, teplo od motoru dostacuje
  return NO;
}

static void WaterTempStep() {
  // Vypoctu teplotu vody v okruhu (na vstupu do topeni)
  if (AinWaterTemperature < WATER_AIN_LOW_LIMIT) {
    Heating.WaterTemperature = HEATING_WATER_TEMP_INVALID;      // Cidlo je zkratovane na zem, nastavim nizkou teplotu, aby topeni horelo
  } else {
    Heating.WaterTemperature = AinCalcValue(AinWaterTemperature, AinWaterTemperatureTable, WaterTemperatureTable, WATER_TABLE_COUNT);
  }

  switch (Heating.HeatingCircuitState) {
    case HEATING_CIRCUIT_INIT:
        // Jdu vzdy na cerpadlo, o zapnuti topeni rozhodnu az tam (tam uz beru v uvahu i polohu serva topeni, takze nebudu startovat topeni zbytecne)
        Heating.HeatingCircuitState = HEATING_CIRCUIT_PUMP;
        ServoClosedCounter  = 0;
        FailureCheckCounter = 0;
        break;

      case HEATING_CIRCUIT_START_HEATING:
        // Nahodim topeni
        DoutSetHeatingPump(NO);
        DoutSetHeatingAndPump(YES);

        // Cekam na plamen
        if (Heating.Flame1) {
          Heating.HeatingCircuitState = HEATING_CIRCUIT_HEATING;
        }
        break;

      case HEATING_CIRCUIT_HEATING:
        // Cekam na zhasnuti plamene

        // Pokud se objevi chyba, jdu zpet na start topeni
        if (Heating.Failure) {
          Heating.HeatingCircuitState = HEATING_CIRCUIT_START_HEATING;
          break;
        }

        if (!Heating.Flame1) {
          // Plamen zhasnul, voda se prave zahrala na 85C (na vystupu topeni). Necham topeni zapnute a cekam, zda se neobjevi porucha topeni.
          Heating.HeatingCircuitState = HEATING_CIRCUIT_CHECK_FAILURE;
          FailureCheckCounter = 0;
        }
        break;

      case HEATING_CIRCUIT_CHECK_FAILURE:
        // Nejakou dobu cekam, zda se neobjevi porucha topeni. Pokud topeni vypadne na poruchu, tak se automaticky take zhasne plamen, tj.
        // vlastne nerozlisim zda zhasnul plamen kvuli dotopeni na 85C nebo kvuli poruse. Pokud bych po zhasnuti plamene hned topeni vypnul,
        // poruchu bych vubec nezjistil.

        // Pokud se objevi plamen, topeni opet zacalo topit (napr. se mezitim naplno otevrelo servo topeni), tj. jdu zpet
        if (Heating.Flame1) {
          Heating.HeatingCircuitState = HEATING_CIRCUIT_HEATING;
          break;
        }

        // Pokud se objevi chyba, jdu zpet na start topeni
        if (Heating.Failure) {
          Heating.HeatingCircuitState = HEATING_CIRCUIT_START_HEATING;
          break;
        }

        // Kontrola doby
        if (FailureCheckCounter < WAIT_AFTER_FLAME_OFF) {
          // Jeste cekam
          FailureCheckCounter++;
          break;
        }

        // Porucha po celou dobu od zhasnuti plamene nenastala, muzu topeni vypnout
        Heating.HeatingCircuitState = HEATING_CIRCUIT_PUMP;
        ServoClosedCounter = 0;
        break;

      case HEATING_CIRCUIT_PUMP:
      case HEATING_CIRCUIT_OFF:
        // Nyni je topeni vypnute. Pokud teplota vody hodne poklesne a zaroven je servo topeni po urcitou dobu otevrene, zapnu zase topeni
        // Servo prumeruju klouzavym prumerem, protoze dost osciluje kvuli derivacni slozce
        if (IsWaterTempLow()) {
          Heating.HeatingCircuitState = HEATING_CIRCUIT_START_HEATING;
          // Snuluju poruchu topeni, az ted ho startuju
          Heating.Failure = NO;
          HeatingCounter = 0;           // Dobu behu topeni pocitam od nuly (prave jsem zapnul)
          break;
        }

        // Topeni vypnu
        DoutSetHeatingAndPump(NO);

        // Cerpadlo zapnu nebo vypnu podle toho, jak dlouho je servo topeni zavrene
        if (Heating.AveragePercent > HEATING_POWER_THRESHOLD1) {
          // Servo topeni je otevrene, zapnu cerpadlo
          ServoClosedCounter = 0;
          DoutSetHeatingPump(YES);
          Heating.HeatingCircuitState = HEATING_CIRCUIT_PUMP;   // Rozjel jsem cerpadlo, zmenim stav (mohl byt dosud HEATING_CIRCUIT_OFF)
          break;
        }

        // Servo je zavrene (resp. pod HEATING_POWER_THRESHOLD procent)

        if (Heating.HeatingCircuitState == HEATING_CIRCUIT_OFF) {
          break;        // Cerpadlo je jiz odstavene, nemusim kontrolovat dobu, po kterou je servo zavrene
        }

        // Servo topeni je zavrene, neni treba topit.
        // Pocitam dobu, po kterou je servo zavrene a po uplynuti delsiho casu muzu cerpadlo odstavit.
        if (ServoClosedCounter < SERVO_CLOSED_TIMEOUT) {
          // Jeste cekam, cerpadlo ponecham zapnute
          ServoClosedCounter++;
          DoutSetHeatingPump(YES);
        } else {
          // Cerpadlo bezi dlouhou dobu naprazdno (bez topeni), muzu ho vypnout a zapnu ho az bude zase treba topit
          DoutSetHeatingPump(NO);
          Heating.HeatingCircuitState = HEATING_CIRCUIT_OFF;
        }
  }
}

void HeatingAutoExecute(int Average) {
  // Provede krok PID regulatoru v rezimu auto topeni, volat 1x za sekundu. <Average> je prumerna teplota podle lozeni ve vnitrnim formatu

  // Vypoctu teplotu vody v okruhu a provedu krok automatu horeni plamene
  WaterTempStep();

  HeatingRegulatorStep(Average);
  Heating.Position = FAirCalculateServoPosition(Heating.Percent);
  Heating.Flame1 = DinHeatingFlame1;
  Heating.Flame2 = DinHeatingFlame2;
  AoutSetHeatingServo(PrepoctiProcentaNaDAC(Heating.Percent));       // Otevru ventil podle navolenych procent
  HeatingCheckFailure();        // Kontrola poruchy
}

#endif // USE_ON_OFF_HEATING

//-----------------------------------------------------------------------------
// Snulovani stridy
//-----------------------------------------------------------------------------

void HeatingResetDutyCycle() {
  DutyCycleReset(&Heating.Flame1DutyCycle);
  DutyCycleReset(&Heating.Flame2DutyCycle);
}


#endif // __TMCREMOTE__

