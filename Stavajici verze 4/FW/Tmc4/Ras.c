//*****************************************************************************
//
//   Ras.c       Remote Terminal access
//   Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Ras.h"
#include "Temp.h"              // Teploty
#include "Menu.h"              // Zobrazeni
#include "Control.h"           // Automaticke rizeni topeni/chlazeni
#include "Heating.h"           // Regulace topeni
#include "Cooling.h"           // Regulace chlazeni
#include "FAir.h"              // Regulace cerstrveho vzduchu
#include "Alarm.h"             // Indikace poruch
#include "Accu.h"              // Napeti a proud akumulatoru
#include "Diesel.h"            // Diesel motor
#include "ElMotor.h"           // Elektromotor
#include "Fan.h"               // Ventilatory
#include "Servo.h"             // Stav serv
#include "Charging.h"          // Dobijeni
#include "Log.h"               // Logovani do pameti
#include "Co2.h"               // Koncentrace CO2
#include "Humidity.h"          // Relativni vlhkost
#include "AirPress.h"          // Tlak radiatoru a filtru
#include "TruckEn.h"           // Motor vozidla
#include "Disinf.h"            // Dezinfekce
#include "RS232\Com.h"        // COM interface
#include "Packet\ComPkt.h"     // Packet interface
#include <string.h>            // memset
#include "System.h"     // "operacni system" - SysSetTimeout()

//#define __DEBUG__

#ifdef __DEBUG__
#include "conio.h"           // jednoduchy display

#define TRACE( s)          cputs( s);putchar('\n')
#define TRACECMD( s, c, d) cputs( s);cbyte( c);putchar( ' ');cdword( d);putchar('\n')
#else
#define TRACE( s)
#define TRACECMD( s, c, d)
#endif

static TRasData __xdata__ RasData;   // Periodicka data

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void RasInit (void)
// Inicializace terminalu
{
   ComInit ();
   // nastaveni kontextu
} // RasInit

//-----------------------------------------------------------------------------
// Zakodovani periodickych dat
//-----------------------------------------------------------------------------

#define Uloz(Kam, Co) if (Co) Kam = YES                 // Ulozi bitovou promennou

static void EncodeData () {
   // Vytvori stukturu RasData. Celou strukturu nejprve nastavim na 0 a bitove promenne ukladam pomoci if - usetri to cca 200 bajtu kodu proti primemu ukladani.
   // POZOR, musi odpovidat dekodovani na strane terminalu
   byte j;

   memset (&RasData, 0, sizeof (RasData));         // Snuluju celou strukturu

   RasData.VersionMajor = VERSION_MAJOR;
   RasData.VersionMinor = VERSION_MINOR;

   RasData.AccuVoltage = Accu.Voltage;
   RasData.ConfigLocalization = Config.Localization;

   RasData.ControlMode = Control.Mode;
   RasData.ControlAutoMode = Control.AutoMode;
   Uloz (RasData.ControlEmergencyMode, Control.EmergencyMode);
   Uloz (RasData.ControlOldEmergencyMode, Control.OldEmergencyMode);
   Uloz (RasData.ControlFrontTempFault, Control.TempFaults[TEMP_FRONT].Fault);
   Uloz (RasData.ControlMiddleTempFault, Control.TempFaults[TEMP_MIDDLE].Fault);
   Uloz (RasData.ControlRearTempFault, Control.TempFaults[TEMP_REAR].Fault);

   Uloz (RasData.CoolingOn, Cooling.On);
   Uloz (RasData.CoolingFailure, Cooling.Failure);
#ifndef NO_DIESEL
   Uloz (RasData.DieselUse, YES);
#else 
   Uloz (RasData.DieselUse, NO);
#endif
   Uloz (RasData.DieselOn, Diesel.On);
   RasData.DieselTemperature = Diesel.Temperature;

   RasData.ServoFloorServoStatus = ServoStatus[SERVO_FLOOR];
   Uloz (RasData.DieselChangeOil, Diesel.ChangeOil);
   Uloz (RasData.DieselOverheated, Diesel.Overheated);
   Uloz (RasData.EcuError, Diesel.EcuError);
   Uloz (RasData.ElMotorOn, ElMotorOn);
   Uloz (RasData.HeatingFailure, Heating.Failure);
   Uloz (RasData.HeatingServoFailure, Heating.ServoFailure);


   RasData.FreshAirMode = FreshAir.Mode;
   RasData.FreshAirPercent = FreshAir.Percent;

   RasData.FreshAirPosition = FreshAir.Position;
   RasData.FreshAirFloorFlapPosition = FreshAir.FloorFlap.Position;

   RasData.FreshAirFloorFlapMode = FreshAir.FloorFlap.Mode;
   RasData.FreshAirFloorFlapPercent = FreshAir.FloorFlap.Percent;

   RasData.HeatingPosition = Heating.Position;
   RasData.RecirculationServoStatus = ServoStatus[SERVO_RECIRCULATION];
   Uloz (RasData.HeatingFlame1, Heating.Flame1);

   RasData.HeatingPercent = Heating.Percent;
   Uloz (RasData.FanOn, Fan.On);

   RasData.ServoTopInductionServoStatus = ServoStatus[SERVO_INDUCTION1];
   RasData.ServoBottomInductionServoStatus = ServoStatus[SERVO_INDUCTION2];
   Uloz (RasData.FanAutoMode, Fan.AutoMode);
   Uloz (RasData.FanFailure, Fan.Failure);

   RasData.LogDriver = Driver;
   Uloz (RasData.AccuLowVoltage, Accu.LowVoltage);

   RasData.Co2 = Co2.Value;

   RasData.Humidity = Humidity.Value;

   RasData.FiltersAirPressure = FiltersAirPressure;
   Uloz (RasData.HeatingFlame2, Heating.Flame2);

   RasData.DisinfectionMode = Disinfection.Mode;
   RasData.DisplayMode = DisplayMode;
   Uloz (RasData.ChargingOn, Charging.On);
   Uloz (RasData.DisinfectionIsInstalled, Disinfection.IsInstalled);

   for (j = 0; j < TEMP_COUNT; j++) {
      RasData.Temp1C[j] = Temp1C[j];
      RasData.Temp01C[j] = Temp01C[j];
   }

   RasData.Time.Time.Day = Time.Day;
   RasData.Time.Time.Month = Time.Month;
   RasData.Time.Time.Year = Time.Year;
   RasData.Time.Time.Hour = Time.Hour;
   RasData.Time.Time.Min = Time.Min;

   RasData.ConfigTargetTemperature = Config.TargetTemperature;

   RasData.ConfigMaxOffsetAbove = Config.MaxOffsetAbove;
   Uloz (RasData.AlarmShowAlarm, Alarm.ShowAlarm);

   RasData.ConfigMaxOffsetBelow = Config.MaxOffsetBelow;
   Uloz (RasData.AlarmQuiet, Alarm.Quiet);

   Uloz (RasData.ChargingFailure, Charging.Failure);
   RasData.HeatingCircuitState = Heating.HeatingCircuitState;
   Uloz (RasData.FiltersPressureOverLimit, FiltersHighAirPressure);
   Uloz (RasData.Co2OverLimit, Co2.OverLimit);
   Uloz (RasData.Co2Failure, Co2.Failure);
   Uloz (RasData.TempUnits, Config.TempUnits == UNITS_FAHRENHEIT);

   RasData.FanPower = Fan.Power;
   Uloz (RasData.TruckEngineRun, TruckEngineOn);

   RasData.DischargePressure = Cooling.DischargePressureAverage;         // Pouziju radeji prumernou hodnotu za periodu ukladani, i kdyz periody ukladani v TMC a v GPS nejsou synchronizovane

   RasData.HeatingFlame1DutyCycle = Heating.Flame1DutyCycle.Percent / LOG_DUTY_CYCLE_LSB;
   RasData.HeatingFlame2DutyCycle = Heating.Flame2DutyCycle.Percent / LOG_DUTY_CYCLE_LSB;

   RasData.CoolingDutyCycle = Cooling.DutyCycle.Percent / LOG_DUTY_CYCLE_LSB;
   RasData.SuctionPressureMsb = (Cooling.SuctionPressureAverage >> 2) & 0x0F;    // Horni 4 bity (celkem 6 bitu)

   RasData.SuctionPressureLsb = Cooling.SuctionPressureAverage & 0x03;           // Spodni 2 bity
   RasData.HeatingWaterTemperature = Heating.WaterTemperature / LOG_HEATING_WATER_TEMP_LSB;
}

//-----------------------------------------------------------------------------
// Radic
//-----------------------------------------------------------------------------

void RasExecute (void)
// Periodicka obsluha - volat 1x za sekundu
{
   byte  cmd;
   dword arg;
   word  i;
   byte __xdata__ *Data;

   if (!ComRxPacket (&cmd, &arg)) {
      return;
   }
   TRACECMD ("CMD : ", cmd, arg);
   switch (cmd) {
   case RAS_CMD_SET_MODE: {
      // Nastaveni rezimu
      if (arg <= _MODE_LAST) {
         ControlSetNewMode (arg);
         Redraw = REDRAW_ALL;
      }
      else {
         cmd = RAS_CMD_NAK;    // Neplatny rozsah
      }
      SysSetTimeout ();        // Pokud je v nejakem menu, vypadne do zakladniho zobrazeni
      break;
   }
   case RAS_CMD_SET_TARGET_TEMPERATURE: {
      // Nastavi cilovou teplotu
      if (TempIsTargetOk ((char)arg)) {
         // Zadana teplota je v poradku, ulozim ji
         MenuNewTargetTemperature (arg);
         Redraw = REDRAW_ALL;
      }
      else {
         cmd = RAS_CMD_NAK;    // Spatny rozsah
      }
      break;
   }
   case RAS_CMD_SET_LOCALIZATION: {
      // Nastavi lozeni
      if (arg <= _LOC_LAST) {
         // Zadane lozeni je v poradku, ulozim je
         MenuNewLocalization (arg);
         Redraw = REDRAW_ALL;
      }
      else {
         cmd = RAS_CMD_NAK;    // Spatny rozsah
      }
      break;
   }
   case RAS_CMD_SET_FRESH_AIR_MODE: {
      // Nastaveni rezimu cersveho vzduchu
      if (arg <= _FAIR_LAST) {
         FAirNewMode (arg);
         Redraw = REDRAW_ALL;
      }
      else {
         cmd = RAS_CMD_NAK;    // Neplatny rozsah
      }
      SysSetTimeout ();        // Pokud je v nejakem menu, vypadne do zakladniho zobrazeni
      break;
   }
   case RAS_CMD_FRESH_AIR_INCREASE: {
      // Rucni pridani cerstveho vzduchu
      if (FreshAir.Mode != FAIR_MANUAL) {
         cmd = RAS_CMD_NAK;    // Cerstvy vzuch se neovlada rucne
      }
      else {
         FAirManualIncrease ();
         arg = FreshAir.Percent;
      }
      SysSetTimeout ();        // Pokud je v nejakem menu, vypadne do zakladniho zobrazeni
      break;
   }
   case RAS_CMD_FRESH_AIR_DECREASE: {
      // Rucni ubrani cerstveho vzduchu
      if (FreshAir.Mode != FAIR_MANUAL) {
         cmd = RAS_CMD_NAK;    // Cerstvy vzuch se neovlada rucne
      }
      else {
         FAirManualDecrease ();
         arg = FreshAir.Percent;
      }
      SysSetTimeout ();        // Pokud je v nejakem menu, vypadne do zakladniho zobrazeni
      break;
   }
   case RAS_CMD_SET_FLOOR_FLAP_MODE: {
      // Nastaveni rezimu klapky v podlaze
      if (arg <= _FLAP_LAST) {
         FAirNewFloorFlapMode (arg);
         Redraw = REDRAW_ALL;
      }
      else {
         cmd = RAS_CMD_NAK;    // Neplatny rozsah
      }
      SysSetTimeout ();        // Pokud je v nejakem menu, vypadne do zakladniho zobrazeni
      break;
   }
   case RAS_CMD_FLOOR_FLAP_INCREASE: {
      // Rucni pridani klapek v podlaze
      if (FreshAir.FloorFlap.Mode != FLAP_MANUAL) {
         cmd = RAS_CMD_NAK;    // Klapka se neovlada rucne
      }
      else {
         FAirFloorFlapManualIncrease ();
         arg = FreshAir.FloorFlap.Percent;
      }
      SysSetTimeout ();        // Pokud je v nejakem menu, vypadne do zakladniho zobrazeni
      break;
   }
   case RAS_CMD_FLOOR_FLAP_DECREASE: {
      // Rucni ubrani klapek v podlaze
      if (FreshAir.FloorFlap.Mode != FLAP_MANUAL) {
         cmd = RAS_CMD_NAK;    // Klapka se neovlada rucne
      }
      else {
         FAirFloorFlapManualDecrease ();
         arg = FreshAir.FloorFlap.Percent;
      }
      SysSetTimeout ();        // Pokud je v nejakem menu, vypadne do zakladniho zobrazeni
      break;
   }
   case RAS_CMD_SOUND: {
      // Zapne / vypne hlaseni alarmu
      AlarmChange ();
      arg = Alarm.Quiet;
      SysSetTimeout ();        // Pokud je v nejakem menu, vypadne do zakladniho zobrazeni
      break;
   }
   case RAS_CMD_TIME: {
      // Nastavi novy datum a cas
      MenuNewTime ((TTransferTime *)&arg);
      break;
   }
   case RAS_CMD_LOGIN: {
      // Login ridice - zatim se nevyuziva
      if (arg >= MIN_LOGIN && arg <= MAX_LOGIN) {
         // Zadane cislo ridice je v poradku, ulozim je
         MenuNewLogin (arg);
      }
      else {
         cmd = RAS_CMD_NAK;    // Spatny rozsah
      }
      break;
   }
   case RAS_GET_DATA: {
      // data request
      WatchDog ();
      EncodeData ();           // Vytvorim strukturu
      Data = (byte *)&RasData;
      ComTxBlockStart (PACKET_MAX_DATA);
      for (i = 0; i < PACKET_MAX_DATA; i++) {
         ComTxBlockByte (*Data);
         Data++;
      }
      ComTxBlockEnd ();
      TRACE ("Long Reply");
      ComFlushChars ();              // odstranit (pripadne) dalsi pakety
      return;
   }
   case RAS_CMD_DISINFECTION_WHEELS: {
      // Zahajeni dezinfekce kol
      DisinfectionWheelsStart ();
      SysSetTimeout ();        // Pokud je v nejakem menu, vypadne do zakladniho zobrazeni
      break;
   }
   case RAS_CMD_DISINFECTION_BODY: {
      // Zahajeni dezinfekce skrine
      if (!DisinfectionBodyCanBeStarted ()) {
         // Dezinfekci skrine mohu zahajit jen pokud je vypnuta ventilace, tj. nejsou tam kurata
         // Sice to kontroluje uz TMCR, ale radeji i zde, zapnuti dezinfekce s kuraty by byl prusvih
         break;
      }
      DisinfectionBodyStart ();
      SysSetTimeout ();        // Pokud je v nejakem menu, vypadne do zakladniho zobrazeni
      break;
   }
   }
   ComTxPacket (cmd, arg);
   TRACE ("Reply");
   ComFlushChars ();                    // odstranit (pripadne) dalsi pakety
} // RasExecute
