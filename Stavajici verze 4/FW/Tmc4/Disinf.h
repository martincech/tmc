//*****************************************************************************
//
//    Disinf.c - Dezinfekce skrine a kol
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Disinf_H__
   #define __Disinf_H__

#include "Hardware.h"     // pro podminenou kompilaci
#include "Control.h"           // Automaticke rizeni

// Rezim
typedef enum {
  DISINFECTION_OFF,
  DISINFECTION_BODY,
  DISINFECTION_WHEELS
} TDisinfectionMode;

// Stav automatu dezinfekce
typedef enum {
  DISINFECTION_STATUS_INIT,
  DISINFECTION_STATUS_WAIT_FOR_LOGO,
  DISINFECTION_STATUS_ERROR,
  DISINFECTION_STATUS_RUNNING,
  DISINFECTION_STATUS_FINISHED
} TDisinfectionStatus;

// Struktura dezinfekce
typedef struct {
  byte IsInstalled;                  // YES/NO dezinfekce je nainstalovana
  TDisinfectionMode Mode;            // Rezim, ve kterem dezinfekce pracuje
  TDisinfectionStatus Status;        // Stav automatu
} TDisinfection;
extern TDisinfection __xdata__ Disinfection;

// Beh libovolne dezinfekce
#define IsDisinfectionStarted() (Disinfection.Mode != DISINFECTION_OFF)

// Dezinfekci skrine mohu zahajit jen pokud je vypnuta ventilace, tj. nejsou tam kurata
#define DisinfectionBodyCanBeStarted() (Disinfection.IsInstalled && Control.Mode == MODE_OFF && !IsDisinfectionStarted())

#define DisinfectionWheelsCanBeStarted() (Disinfection.IsInstalled && !IsDisinfectionStarted())

void DisinfectionInit();
// Inicializace

void DisinfectionBodyStart();
// Start dezinfekce skrine

void DisinfectionWheelsStart();
// Start dezinfekce kol

void DisinfectionExecute();
// Krok automatu, volat kazdou sekundu

#endif
