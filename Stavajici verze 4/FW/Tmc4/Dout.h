//*****************************************************************************
//
//    Dout.c - Digitalni vystupy
//    Version 1.0
//
//*****************************************************************************


#ifndef __Dout_H__
   #define __Dout_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

// Definice jednotlivych vystupu
extern byte __xdata__ DoutCooling;
extern byte __xdata__ DoutHeatingAndPump;
extern byte __xdata__ DoutVentilation;
extern byte __xdata__ DoutHeatingPump;
extern byte __xdata__ DoutExternalSiren;
extern byte __xdata__ DoutDisinfectionPower;
extern byte __xdata__ DoutDisinfectionBody;
extern byte __xdata__ DoutDisinfectionWheels;


void DoutInit();
  // Inicializuje digitalnich vystupu

void DoutSetCooling(TYesNo Value);
  // Zapne / vypne chlazeni

void DoutSetHeatingAndPump(TYesNo Value);
  // Zapne / vypne topeni vcetne cerpadla

void DoutSetVentilation(TYesNo Value);
  // Zapne / vypne ventilatory

void DoutSetHeatingPump(TYesNo Value);
  // Zapne / vypne cerpadlo topeni (topeni samotne je vypnute)

void DoutSetExternalSiren(TYesNo Value);
  // Zapne / vypne externi sirenu

void DoutSetDisinfectionPower(TYesNo Value);
  // Zapne / vypne LOGO

void DoutSetDisinfectionWheels(TYesNo Value);
  // Zapne / vypne dezinfekci kol

void DoutSetDisinfectionBody(TYesNo Value);
  // Zapne / vypne dezinfekci skrine

#endif
