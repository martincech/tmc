//*****************************************************************************
//
//    MenuPos.h - Souradnice pro zobrazeni
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __MenuPos_H__
   #define __MenuPos_H__

#include "Hardware.h"     // pro podminenou kompilaci

//-----------------------------------------------------------------------------
// Celkovy pocet radku senzoru
//-----------------------------------------------------------------------------

#if (defined(USE_HUMIDITY) && defined(USE_CO2))
  #define SENSORS_NUMBER_OF_ROWS        2
#else
  #if (defined(USE_FILTER_PRESSURE) || defined(USE_HUMIDITY) || defined(USE_CO2))
    #define SENSORS_NUMBER_OF_ROWS      1
  #else
    #define SENSORS_NUMBER_OF_ROWS      0
  #endif
#endif

//-----------------------------------------------------------------------------
// Teploty
//-----------------------------------------------------------------------------

#if (SENSORS_NUMBER_OF_ROWS == 0)
  #define TEMP_Y1 11
  #define TEMP_Y2 99
  #define TEMP_Y3 192
#elif (SENSORS_NUMBER_OF_ROWS == 1)
  #define TEMP_Y1 8
  #define TEMP_Y2 81
  #define TEMP_Y3 156
#else
  #define TEMP_Y1 5
  #define TEMP_Y2 70
  #define TEMP_Y3 134
#endif

//-----------------------------------------------------------------------------
// Teploty ve skrini
//-----------------------------------------------------------------------------

#ifdef USE_TWO_SENSORS
  #if (SENSORS_NUMBER_OF_ROWS == 0)
    #define TEMP_BODY_Y1 35
    #define TEMP_BODY_Y2 164
  #elif (SENSORS_NUMBER_OF_ROWS == 1)
    #define TEMP_BODY_Y1 32
    #define TEMP_BODY_Y2 135
  #else
    #define TEMP_BODY_Y1 25
    #define TEMP_BODY_Y2 112
  #endif
#else
  #define TEMP_BODY_Y1 TEMP_Y1
  #define TEMP_BODY_Y2 TEMP_Y2
  #define TEMP_BODY_Y3 TEMP_Y3
#endif // USE_TWO_SENSORS

//-----------------------------------------------------------------------------
// Klapky
//-----------------------------------------------------------------------------

// Poloha horni a spodni rady klapek
#if (SENSORS_NUMBER_OF_ROWS == 0)
  #define FLAPS_Y1   64
  #define FLAPS_Y2   155
#elif (SENSORS_NUMBER_OF_ROWS == 1)
  #define FLAPS_Y1   55
  #define FLAPS_Y2   131
#else
  #define FLAPS_Y1   48
  #define FLAPS_Y2   113
#endif

#ifdef USE_TWO_SENSORS
  #if (SENSORS_NUMBER_OF_ROWS == 0)
    #define FLOOR_FLAPS_Y1   110
  #elif (SENSORS_NUMBER_OF_ROWS == 1)
    #define FLOOR_FLAPS_Y1   94
  #else
    #define FLOOR_FLAPS_Y1   80
  #endif
#else
  #define FLOOR_FLAPS_Y1   FLAPS_Y1
  #define FLOOR_FLAPS_Y2   FLAPS_Y2
#endif // USE_TWO_SENSORS

//-----------------------------------------------------------------------------
// Vypocet souradnic cidel ve spodni casti displeje
//-----------------------------------------------------------------------------

#if (SENSORS_NUMBER_OF_ROWS == 2)
  #define SENSORS_Y1      188     // Prvni radek
  #define SENSORS_Y2      215     // Druhy radek
#elif (SENSORS_NUMBER_OF_ROWS == 1)
  #define SENSORS_Y1      211     // Jen prvni radek
#endif

// Tlak ventilatoru a filtru
#ifdef USE_FILTER_PRESSURE
  #define AIR_PRESSURE_Y2  SENSORS_Y1
#endif

// CO2 a vlhkost
#ifdef USE_CO2
  #define CO2_Y   SENSORS_Y1
  #ifdef USE_HUMIDITY
    #define HUMIDITY_Y  SENSORS_Y2
  #endif
#else
  #ifdef USE_HUMIDITY
    #define HUMIDITY_Y  SENSORS_Y1
  #endif
#endif

//-----------------------------------------------------------------------------
// Prekresleni poruch
//-----------------------------------------------------------------------------

#define FAULTS_Y                178

//-----------------------------------------------------------------------------
// Tlacitko Menu
//-----------------------------------------------------------------------------

#define MENU_Y                200

#endif
