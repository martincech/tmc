//*****************************************************************************
//
//    Fan.c - Kontrola behu a poruchy ventilatoru
//    Version 1.0
//
//*****************************************************************************

#include "Fan.h"
#include "Din.h"               // Digitalni vstupy
#include "Dout.h"              // Digitalni vystupy
#include "Ain.h"               // Analogove vstupy
#include "Alarm.h"             // Indikace poruch
#include "Control.h"           // Automaticke rizeni
#include "System.h"     // SysDelay()

TFan __xdata__ Fan;

#ifndef __TMCREMOTE__

// Promenne pro poruchu
static byte __xdata__ FaultCounter;     // Pocitadlo trvani poruchy

// Automaticky reset rele pri poruse
#define FAULT_RESET_TIME 15             // Doba trvani poruchy v sekundach, po ktere se rele resetuje (vypne a hned znovu zapne)
#define FAULT_OFF_TIME   100            // Doba vypnuti rele v milisekundach

#endif // __TMCREMOTE__

// Kalibracni konstanty:
// Predelano na automaticky regulator vykonu ventilatoru
#if (ACCU_MIN_VOLTAGE  < 15)
  // Dodavka na 12V
  #define FAN_MANUAL_100          0x2E  // U > 7V = 100%, U < 7V = 50% - puvodne bylo 10V, ale kvuli rele ETA jsem stahovali proud z 20 na 19A a pak to hlasilo 50%
#else
  // Auto nebo naves na 24V
  #define FAN_MANUAL_100          0x85  // Ventilace rucne prepnuta na 100%, U > 20V
  #define FAN_MANUAL_50           0x29  // Ventilace rucne prepnuta na 50%, U < 6V, zustava stejne pro dodavku i auto
#endif // (ACCU_MIN_VOLTAGE  < 15)
#define FAN_FAULT               0x0C    // Porucha ventilatoru (pojistka), U < 2V

#define FAN_AUTO_AIN_MIN        0x35    // Min. vykon regulatoru je pri 8V
#define FAN_AUTO_AIN_MAX        0x42    // Max. vykon regulatoru je pri 10V
#define FAN_AUTO_MIN            80      // Min. vykon regulatoru odpovida 80% vykonu ventilace
#define FAN_AUTO_MAX            100     // Max. vykon regulatoru odpovida 100% vykonu ventilace

#ifndef __TMCREMOTE__

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void FanInit() {
  // Inicializace ventilatoru
  Fan.On       = NO;
  Fan.Failure  = NO;
  FaultCounter = 0;
}

//-----------------------------------------------------------------------------
// Start ventilatoru
//-----------------------------------------------------------------------------

void FanStart() {
  // Start behu ventilatoru a nulovani chyby
  DoutSetVentilation(YES);
  Fan.On       = YES;
  Fan.Failure  = NO;
  FaultCounter = 0;
}

//-----------------------------------------------------------------------------
// Stop ventilatoru
//-----------------------------------------------------------------------------

void FanStop() {
  // Stop behu ventilatoru a nulovani chyby
  DoutSetVentilation(NO);
  Fan.On       = NO;
  Fan.Failure  = NO;
  FaultCounter = 0;
}

#endif // __TMCREMOTE__

//-----------------------------------------------------------------------------
// Vypocet vykonu ventilatoru
//-----------------------------------------------------------------------------

#define POWER_TABLE_COUNT    2
static byte code AinTable[POWER_TABLE_COUNT]   = {FAN_AUTO_AIN_MIN, FAN_AUTO_AIN_MAX};  // Napeti Ain
static byte code PowerTable[POWER_TABLE_COUNT] = {FAN_AUTO_MIN, FAN_AUTO_MAX};          // Vykon ventilatoru
static byte code PowerDisplayTable[POWER_TABLE_COUNT] = {0, 100};                       // Vykon ventilatoru pro zobrazeni 0..100%

#if !(ACCU_MIN_VOLTAGE  < 15)
#ifndef __TMCREMOTE__

static void PowerCalc() {
  // Vypoctu vykon ventilatoru
  Fan.Power = AinCalcValue(AinFan, AinTable, PowerTable, POWER_TABLE_COUNT);
}

#endif // __TMCREMOTE__
#endif // !(ACCU_MIN_VOLTAGE  < 15)

byte FanCalcPowerForDisplay(byte Power) {
  // Prepocte vykon z rozsahu FAN_AUTO_MIN..FAN_AUTO_MAX na 0..100% pro zobrazeni
  return AinCalcValue(Power, PowerTable, PowerDisplayTable, POWER_TABLE_COUNT);
}

#ifndef __TMCREMOTE__

//-----------------------------------------------------------------------------
// Kontrola ventilatoru
//-----------------------------------------------------------------------------

static TYesNo VentilationRunning() {
   return Control.Mode != MODE_OFF && Fan.On;
}

static TYesNo CalculateFault() {
  // Pocita cas poruchy ventilatoru
  TYesNo isFailure = VentilationRunning() && (DinFan == DIN_FAN_ERROR || AinFan < FAN_FAULT);

  if(AlarmCheckFaultCounter(isFailure, &FaultCounter, &Fan.Failure)){
    return YES;
  }
  if(!isFailure){
    return NO;
  }

  // Jeste cekam, nez poruchu opravdu vyhlasim. Behem teto doby periodicky resetuju rele ETA.
  if (FaultCounter % FAULT_RESET_TIME == 0) {
    // Pokud rele indikuje poruchu, kazdych FAULT_RESET_TIME sekund rele na kratko vypnu a hned zapnu, treba se chyti.
    // U dvou rele paralelne se stavalo, ze po zapnuti ventilace hlasilo jedno z rele poruchu a pomohlo jen zastaveni a novy start ventilace.
    // Potvrzeno i z ETA, ze v tomto stavu pomuze jen reset rele.
    DoutSetVentilation(NO);
    SysDelay(FAULT_OFF_TIME);
    DoutSetVentilation(YES);
  }
   return YES;
}

#define ClearFaultCounter()     FaultCounter = 0        // Vynuluje pocitadlo poruchy

void FanCheck() {
  // Zkontroluje beh a poruchu ventilatoru, volat 1x za sekundu.
  Fan.AutoMode = YES;
  Fan.Power    = 0;
  Fan.Failure  = NO;

  if(CalculateFault() || !VentilationRunning()){
    return;
  }
  // Zjistim vykon ventilatoru - zvlast pro dodavku (12V) a auto (24V)

#if (ACCU_MIN_VOLTAGE  < 15)

  // U dodavky (ACCU_MIN_VOLTAGE = 11V) je vykon vzdy rucne 50 nebo 100%, neni tam automat. Nad 10V 100%, pod 10V 50%.
  Fan.AutoMode = NO;
  if (AinFan > FAN_MANUAL_100) {
    // Rucne na 100% vykon
    Fan.Power = 100;
  } else {
    // Rucne na 50% vykon
    Fan.Power = 50;
  }

#else

  // Auto na 24V muze mit i automat

  if (AinFan > FAN_MANUAL_100) {
    // Rucne na 100% vykon
    Fan.AutoMode = NO;
    Fan.Power    = 100;
    return;
  }

  if (AinFan < FAN_MANUAL_50) {
    // Rucne na 50% vykon
    Fan.AutoMode = NO;
    Fan.Power    = 50;
    return;
  }

  // Automaticky regulator, vykon musim vypocitat
  Fan.AutoMode = YES;
  PowerCalc();

#endif // (ACCU_MIN_VOLTAGE  < 15)

}

#endif // __TMCREMOTE__
