//*****************************************************************************
//
//    XmemAT.c     External EEPROM module using AT25256 template
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Xmem\Xmem.h"

//-----------------------------------------------------------------------------
// Makra pro generovani zdrojoveho textu
//-----------------------------------------------------------------------------

// Porty :

#define XCS    XmemCS
#define XSCK   XmemSCK
#define XSO    XmemSO
#define XSI    XmemSI

// Inverze signalu:
#define XSI_H  XMEM_XSI_H
#define XSCK_H XMEM_XSCK_H

// Funkce :

#define XInit           XmemInit
#define XReadByte       XmemReadByte
#define XBlockReadStart XmemBlockReadStart
#define XRawReadByte    XmemRawReadByte
#define XStop           XmemStop
#define XWriteByte      XmemWriteByte
#define XPageWriteStart XmemPageWriteStart
#define XRawWriteByte   XmemRawWriteByte
#define XAccuOk         XmemAccuOk

// Podminena kompilace :

#ifdef XMEM_READ_BYTE
   #define X_READ_BYTE     1
#endif
#ifdef XMEM_WRITE_BYTE
   #define X_WRITE_BYTE    1
#endif

#include "..\AT25256\AT25256.tpl"      // zdrojovy text

#ifndef XMEM_USE_INS                   // SW testovani pritomnosti

//-----------------------------------------------------------------------------
// Instrukce WRDI
//-----------------------------------------------------------------------------

static void WrdiCmd( void)
// Posle instrukci WRDI
{
  Select();                           // select CS
  XRawWriteByte( EEP_WRDI);
  Deselect();                         // deselect CS
} // WrdiCmd

//-----------------------------------------------------------------------------
// Zjisteni pripojeni modulu
//-----------------------------------------------------------------------------

TYesNo XmemIsPresent( void)
// Vraci YES, je-li pritomen pametovy modul
{
   SetupCS()                          // vychozi urovne pred CS
   WrenCmd();                         // Povoleni zapisu - nastavi se WEN ve status registru
   if( !(RdsrCmd() & EEP_MASK_WEN)){
      CleanupCS();
      return( NO);                    // modul neni vlozen
   }
   WrdiCmd();                         // Zakaz zapisu - shodi se bit WEN
   if(   RdsrCmd() & EEP_MASK_WEN){
      CleanupCS();
      return( NO);                    // modul neni vlozen
   }
   CleanupCS();
   return( YES);                      // modul vlozen
} // XmemIsPresent

#endif // XMEM_USE_INS
