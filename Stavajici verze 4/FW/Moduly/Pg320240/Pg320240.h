//*****************************************************************************
//
//    Pg320240.c - LCD displej PG320240 s radicem SED1335 (320 x 240 bodu)
//    Version 1.0
//
//*****************************************************************************

#ifndef __Pg320240_H__
   #define __Pg320240_H__

#include "Hardware.h"     // zakladni datove typy


// Blikani v editaci atd.
typedef struct {
  byte Show;                            // Zda se ma prave zobrazit nebo skryt
  byte Change;                          // Flag, ze se zmenil stav Show a ma se tedy roznout nebo zhasnout => ma se zmenit stav
} TBlink;
extern TBlink __xdata__ Blink;

// Numericke neproporcionalni fonty
extern TFontDescriptor code FontNumericMyriad40;
extern TFontDescriptor code FontNumericMyriad32;
extern TFontDescriptor code FontNumericTahoma18;

extern byte code FONT_NUMERIC_TAHOMA18[];

// Tlacitka, definovana zde a vyuzitelna i jinde
extern byte code SYMBOL_OK[];
extern byte code SYMBOL_ZRUSIT[];

#define DISPLAY_EDIT_DATE 9   // Delka, ktera ve fci Editace() znaci editaci datumu
#define DISPLAY_EDIT_TIME 10  // Delka, ktera ve fci Editace() znaci editaci casu

#define NUMPAD_1ROW_Y 20  // Pozice jednoho radku
#define NUMPAD_2ROWS_Y1 5  // Pozice 1. radku pri celkem dvou radcich
#define NUMPAD_2ROWS_Y2 35 // Pozice 2. radku pri celkem dvou radcich

#define TLACITKO_Y 196                          // Standardni pozice radku tlacitek
#define TLACITKO_X1 1                           // Standardni pozicce tlacitka vlevo
#define TLACITKO_X2 23                          // Standardni pozicce tlacitka vpravo
#define TLACITKO_SIRKA 16                       // Standardni sirka tlacitka
#define TLACITKO_VYSKA 41                        // Standardni vyska tlacitka

// Funkce:

void DisplayVertLine(byte X, byte Y, byte Vyska, byte Value);
  // Nakresli svislou caru osmi bitu do nastavene roviny od nastavene pozice.
  // Vyska je vyska v pixelech, <Value> je hodnota osmice

void DisplayVertDotLine(byte X, byte Y, byte Vyska, byte Value);
  // Nakresli svislou caru osmi bitu do nastavene roviny od nastavene pozice.
  // Vyska je vyska v pixelech, <Value> je hodnota osmice

void DisplayHorLineValue(byte X, byte Y, byte Sirka, byte Hodnota);
  // Vodorovna cara s definovanou hodnotou

#define DisplayHorLine(X, Y, Sirka)    DisplayHorLineValue(X, Y, Sirka, 0xFF)
  // Vodorovna plna cara

#define DisplayHorDotLine(X, Y, Sirka) DisplayHorLineValue(X, Y, Sirka, 0x55)
  // Vodorovna teckovana cara

void DisplaySymbol(byte X, byte Y, byte code *Symbol);
  // Zobrazi symbol na pozici X, Y (X je osmice)

void DisplayCharTahoma18(word X, byte Y, byte Znak);
  // Zobrazi znak Znak na pozici X,Y - proporcionalni


void DisplayStringTahoma18(word x, byte y, byte *s);

void DisplayStringCenterTahoma18(word x, byte y, byte *s);
  // Vykresli vetu zvolenym fontem zarovnanou nastred
  // String je zakonceny nulou nebo FF (FF=konec radku)

void DisplayButtonFrame(byte X, byte Y, byte Width, byte Height, TYesNo Thick);
  // Zobrazi obrys tlacitka, X a Width je v osmicich pixelu. Pokud je <Thick> = YES, vykresli obrys tucne (3px).
  // Sirka musi byt vetsi nez 2 (nekontroluju)

void DisplayButtonText(byte X, byte Y, byte Delka, byte code *Text);
  // Zobrazi tlacitko s textem uprostred, Delka je v osmicich pixelu, vyska konstantni. X je v osmicich, Y je v pixelech

#define DisplayOk() DisplayButtonText(TLACITKO_X2, TLACITKO_Y, TLACITKO_SIRKA, STR_OK)
// Zobrazi tlacitko OK vespodu displeje

void DisplayRetryCancel();
  // Zobrazi tlacitka Opakovat a Zrusit vespodu displeje

void DisplayOkCancel();
  // Zobrazi tlacitka OK a Zrusit vespodu displeje

void DisplayYesNo();
  // Zobrazi tlacitka Ano a Ne vespodu displeje

TYesNo DisplayExecuteButtons(byte Count);
  // Ceka na stisk jednoho z max 2 tlacitek v menu, pri stisku leveho vrati 1, pri stisku praveho nebo timeoutu vrati 0.
  // Count muze byt 1 nebo 2

void DisplayDrawNumPad(TYesNo Znamenko);
  // Nakresli na displej cislicovou klavesnici na pevne pozici
  // Pokud je Znamenko=YES, nakresli i klavesu +/-

TYesNo DisplayExecuteNumPad(byte X, byte Y, byte Delka, TYesNo Znamenko);
  // Edituje cislo UN.DT, Y je cislo radku. Vyuziva font Tahoma18 s neproporcionalni sirkou.
  // Pokud je Delka=1-8, edituje se cislo o zadane delce bez desetinne carky.
  // Pokud je Delka=DISPLAY_EDIT_DATE, edituje se datum. Pokud je Delka=DISPLAY_EDIT_TIME, edituje se cas.
  // Pokud je delka>10, je v MSB delka a v LSB pocet desetin, takze napr. 0x42 edituje cislo o delce 4 se 2 des. carkami
  // Pokud je Znamenko=YES, edituje se cislo se znamenkem (nelze pouzit u data a casu)


#endif
