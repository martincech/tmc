//*****************************************************************************
//
//    Printer.h    - COM Printer services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Printer_H__
   #define __Printer_H__


#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Fmt_H__
   #include "Fmt.h"
#endif

#define PRT_ROW_WIDTH      24           // pocet znaku na radku
#define PRT_FF             '\x0C'       // znak formfeed - nova stranka

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void PrtInit( void);
// Inicializace po resetu

void PrtStart( void);
// Inicializuje linku tiskarny

void PrtStop( void);
// Uvolni linku tiskarny

char PrtChar( char c);
// Tiskne znak

void PrtString( char code *Text);
// Tiskni retezec z kodove pameti

void PrtHex( dword x, byte width);
// Tiskne hexa cislo

void PrtDec( int32 x, byte width);
// Tiskne dekadicke cislo

void PrtPrintf( char code *format, ...);
// Formatovany tisk

#define PrtByte( x)  PrtHex( x, 2 | FMT_LEADING_0)
// Tiskne byte jako dve hexa cislice (pro BCD)

#define PrtWord( x)  PrtHex( x, 4 | FMT_LEADING_0)
// Tiskne word jako ctyri hexa cislice (pro BCD)

#define PrtDword( x) PrtHex( x, 0 | FMT_LEADING_0)
// Tiskne dword jako osm hexa cislic (pro BCD)

#define PrtInt8( x)   PrtDec( x, 3);
// Tiskne byte jako cislo se znamenkem

#define PrtInt16( x)  PrtDec( x, 5);
// Tiskne int jako cislo se znamenkem

#define PrtInt32( x)  PrtDec( x, 0);
// Tiskne long jako cislo se znamenkem
// (max 8 platnych cislic)

void PrtChars( char c, byte cols);
// Prida sloupec znaku <c> o sirce <cols>

#define PrtSpace( cols)    PrtChars( ' ', cols)
// Prida sloupec o <cols> mezer

#define PrtNewline() PrtChar( '\r');
// Novy radek

#define PrtNewlines( rows) PrtChars( '\r', rows)
// Odradkuje o <rows> radku

#define PrtSeparator()     PrtChars( '-', PRT_ROW_WIDTH); PrtNewline()
// Vytiskne vodorovnou caru

#endif
