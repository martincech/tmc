;------------------------------------------------------------------------------
;
; Wbin2bcd.A51 generated from: BCD.C51
;
;------------------------------------------------------------------------------


NAME    WBIN2BCD

?PR?_wbin2bcd?WBIN2BCD    SEGMENT CODE
	PUBLIC	_wbin2bcd

; //-----------------------------------------------------------------------------
; // wbin2bcd
; //-----------------------------------------------------------------------------

; dword wbin2bcd( word x) //  R6:R7

        RSEG  ?PR?_wbin2bcd?WBIN2BCD
	USING	0
_wbin2bcd:
; // prevede 0..65535 do bcd
; {
			; SOURCE LINE # 39
	  mov     r2, ar7         ; X - zdrojovy operand R1:R2
	  mov     r1, ar6
	  clr     a               ; nuluj vysledek
	  mov     r7, a
	  mov     r6, a
	  mov     r5, a
	  mov     r4, a

	  mov     b, #16          ; vsechny bity zdroje
bcd00b:
	  clr     c               ; posun zdroj doleva
	  mov     a, r2
	  rlc     a
	  mov     r2, a
	  mov     a, r1
	  rlc     a
	  mov     r1, a

	  mov     a, r7
	  addc    a, r7           ; vynasob 2* pricti rotovany bit
	  da      a
	  mov     r7, a

	  mov     a, r6
	  addc    a, r6           ; vynasob 2* pricti prenos
	  da      a
	  mov     r6, a

	  mov     a, r5
	  addc    a, r5           ; vynasob 2* pricti prenos
	  da      a
	  mov     r5, a

	  djnz    b, bcd00b
; } // wbin2bcd
			; SOURCE LINE # 76
	RET

        END
