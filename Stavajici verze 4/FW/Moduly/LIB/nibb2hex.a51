;------------------------------------------------------------------------------
;
; nibb2hex.a51 generated from: BCD.C51 + MODIFIED
;
;------------------------------------------------------------------------------


NAME    NIBBLE2HEX

?PR?_nibble2hex?NIBBLE2HEX     SEGMENT CODE

        PUBLIC  _nibble2hex

; //-----------------------------------------------------------------------------
; // nibble2hex
; //-----------------------------------------------------------------------------
;
; byte nibble2hex( byte x)

        RSEG  ?PR?_nibble2hex?NIBBLE2HEX
	USING	0
_nibble2hex:
			; SOURCE LINE # 330
; // prevede dolni nibble na '0'..'F'
; {
			; SOURCE LINE # 332
	  mov     a, r7                   ; Get parameter
	  anl     a,#00fh                 ; Keep Only Low Bits
	  add	a,#090h 		; Add 144
	  da	a			; Decimal Adjust
	  addc	a,#040h 		; Add 64
	  da	a			; Decimal Adjust
	  mov     r7, a                   ; Return parameter
; } // nibble2hex
			; SOURCE LINE # 342
	RET

        END
