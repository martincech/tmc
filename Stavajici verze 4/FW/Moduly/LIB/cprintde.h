//*****************************************************************************
//
//    Cprintde.c - simple display services
//    Version 1.1 (c) VymOs
//
//*****************************************************************************

#ifndef __cprintde_h__
#define __cprintde_h__
#include "cprint.h"

//-----------------------------------------------------------------------------
// Dekadicke se znamenkem
//-----------------------------------------------------------------------------
void coutdec( int32 x, byte width, PrintCharFnc printFnc);
void cprintdec( int32 x, byte width);

#endif