;------------------------------------------------------------------------------
;
; bbcdinc.a51 generated from: BCD.C51 + MODIFIED
;
;------------------------------------------------------------------------------


NAME    BBCDINC

?PR?_bbcdinc?BBCDINC     SEGMENT CODE

        PUBLIC  _bbcdinc

; //-----------------------------------------------------------------------------
; // bbcdinc
; //-----------------------------------------------------------------------------

; word bbcdinc( byte x)

        RSEG  ?PR?_bbcdinc?BBCDINC
	USING	0
_bbcdinc:
			; SOURCE LINE # 171
; // vrati x+1
; {
			; SOURCE LINE # 173
	  mov     a, r7                   ; parametr X
	  add     a, #1                   ; pricti 1
	  da      a                       ; BCD korekce
	  mov     r7, a                   ; navratova hodnota
	  clr     a                       ; nuluj prenos
	  addc    a, acc                  ; jen prenos
	  mov     r6, a                   ; navratova hodnota
; } // bbcdinc
			; SOURCE LINE # 183
	RET

        END
