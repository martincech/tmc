;------------------------------------------------------------------------------
;
; wbcddec.a51 generated from: BCD.C51 + MODIFIED
;
;------------------------------------------------------------------------------


NAME    WBCDDEC

?PR?_wbcddec?WBCDDEC     SEGMENT CODE

        PUBLIC  _wbcddec

; //-----------------------------------------------------------------------------
; // wbcddec
; //-----------------------------------------------------------------------------
;
; word wbcddec( word x) // R6:R7

        RSEG  ?PR?_wbcddec?WBCDDEC
	USING	0
_wbcddec:
			; SOURCE LINE # 227
; // vrati x-1
; {
			; SOURCE LINE # 229
	  mov a, #99H                     ; doplnek
	  add a, r7                       ; plus X
	  da  a                           ; BCD korekce
	  mov r7, a                       ; navrat LSB
	  mov a, #99H
	  addc a, #0                      ; prenos z dolniho
	  subb a, #0                      ; doplnek MSB Y
	  add  a, r6                      ; plus MSB X
	  da a                            ; BCD korekce
	  mov r6, a                       ; navrat MSB
; } // wbcddec
			; SOURCE LINE # 242
	RET

        END

