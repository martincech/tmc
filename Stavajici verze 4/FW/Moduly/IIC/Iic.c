//*****************************************************************************
//
//    Iic.c  -  I2C bus services
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#include "Iic\Iic.h"

static IicWait( void);
// prodleva min 5us

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

TYesNo IicInit( void)
// Zkontroluje funkci sbernice a provede inicializaci.
// Vrati 0 pri zkratu SDA
{
   IicStart();        // ukonceni predchozi akce, SCL=0, SDA=0
   // zacvicime s datovym vodicem :
   if( IicSDA){
      return( NO);    // zkrat na napajeni
   }
   IicSDA = 1;
   IicWait();
   if( !IicSDA){
      return( NO);    // zkrat na zem
   }
   IicStop();         // ukonceni do vychoziho stavu SCL=1, SDA=1
   return( YES);
} // IicInit

//-----------------------------------------------------------------------------
// Start Bit
//-----------------------------------------------------------------------------

void IicStart( void)
// Vygeneruje na sbernici startovaci sekvenci. Lze volat
// z libovolne pozice, zajisti klidovy stav sbernice, prodlevu
// a nasledujici start
{
   IicSDA = 1;        // klidovy stav sbernice
   IicSCL = 1;
   IicWait();         // setup pred startbitem
   IicSDA = 0;        // start
   IicWait();         // setup hodin
   IicSCL = 0;        // atakovana sbernice SCL=0, SDA=0
   IicWait();         // dokonceni periody hodin
} // IicStart

//-----------------------------------------------------------------------------
// Stop Bit
//-----------------------------------------------------------------------------

void IicStop( void)
// Vygeneruje na sbernici Ukoncovaci sekvenci. Lze volat
// z libovolne pozice, zajisti klidovy stav sbernice bez
// koncoveho cekani (je soucasti IicStart)
{
   IicSDA = 0;
   IicSCL = 1;
   IicWait();         // setup pred stopbitem
   IicSDA = 1;        // stopbit, klidovy stav sbernice SCL=1 SDA=1
} // IicStop

//-----------------------------------------------------------------------------
// Vyslani bytu
//-----------------------------------------------------------------------------

TYesNo IicSend( byte value)
// Vysle byte <value> po sbernici, precte odpoved ACK.
// Vraci YES pri ACK a NO pri NAK
{
byte i;
bit  ack;

   for( i = 0; i < 8; i++){
      IicSDA = value & 0x80;
      IicSCL = 1;
      IicWait();
      IicSCL = 0;
      IicWait();       // nemusi byt, lze nahradit rezii cyklu
      value <<= 1;
   }
   // prijem ACK/NAK :
   IicSDA = 1;         // data do vysoke urovne
   IicSCL = 1;
   IicWait();
   ack = !IicSDA;      // pozitivni potvrzeni je 0
   IicSCL = 0;
   IicWait();          // dokonceni periody hodin
   return( ack);
} // IicSend

#ifdef IIC_READ

//-----------------------------------------------------------------------------
// Cteni bytu
//-----------------------------------------------------------------------------

byte IicReceive( TYesNo ack)
// Prijme byte se sbernice, vrati jej jako navratovou hodnotu.
// Odesle potvrzeni <ack>, YES je ACK NO je NAK
{
byte value;
byte i;

   value  = 0;
   IicSDA = 1;          // data do vysoke urovne
   for( i = 0; i < 8; i++){
      value <<= 1;
      IicSCL = 1;
      IicWait();
      if( IicSDA){
         value++;
      }
      IicSCL = 0;
      IicWait();        // nemusi byt, lze nahradit rezii cyklu
   }
   // vyslani ACK/NAK :
   IicSDA = !ack;      // pozitivni potvrzeni je 0
   IicSCL = 1;
   IicWait();
   IicSCL = 0;
   IicWait();          // dokonceni periody hodin
   return( value);
} // IicReceive

#endif

//-----------------------------------------------------------------------------
// Cekani
//-----------------------------------------------------------------------------

static IicWait( void)
// prodleva min 5us (vcetne volani)
{
   IIC_WAIT;
} // IicWait

