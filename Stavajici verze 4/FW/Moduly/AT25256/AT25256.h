//*****************************************************************************
//
//    AT25256.h    AT25256 EEPROM services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __X25645_H__
   #define __X25645_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// POZOR : u blokoveho cteni/zapisu nelze vkladat obsluhu
// zarizeni, ktera pouzivaji sdilene vodice EEPROM

void EepInit( void);
// Nastavi klidove hodnoty na sbernici, inicializuje pamet

TYesNo EepCheckConnection();
// Otestuje pritomnost pametoveho modulu i bez HW pripojeni signalu INS

byte EepReadByte( word address);
// Precte byte z EEPROM <address>

TYesNo EepWriteByte( word address, byte value);
// Zapise byte <value> na <address> v EEPROM
// Vraci NO neni-li zapis mozny

//------ Blokove cteni -----------------------------

void EepBlockReadStart( word address);
// Zahaji blokove cteni z EEPROM <address>

#define EepBlockReadData() EepRawReadByte()
// Cteni bytu bloku

#define EepBlockReadStop() EepStop()
// Ukonceni cteni bloku

//------ Strankovy zapis -----------------------------
// Pozor, hranice stranek hlida uzivatel API
// EEP_PAGE_SIZE je v Hardware.h

TYesNo EepPageWriteStart( word address);
// Zahaji zapis stranky od <address> v EEPROM.
// Vraci NO neni-li zapis mozny

#define EepPageWriteData( value) EepRawWriteByte( value)
// Zapis bytu do stranky

#define EepPageWritePerform() EepStop()
// Odstartuje fyzicky zapis stranky do EEPROM

//------ Interni funkce, nepouzivat : ----------------

void EepRawWriteByte( byte value);
// Zapise byte

byte EepRawReadByte( void);
// Precte a vrati byte

void EepStop( void);
// Ukonci operaci

// Pozor, XSO je vystup z pameti, budeme jej cist
// XSI je vstup do pameti, do nej zapisujeme

// Instrukce AT25256 :

#define EEP_WREN    0x06           // Set Write Enable Latch (Enable Write)
#define EEP_WRDI    0x04           // Reset Write Enable/Reset Flag Bit
#define EEP_RDSR    0x05           // Read Status Register
#define EEP_WRSR    0x01           // Write Status Register
#define EEP_READ    0x03           // Read data
#define EEP_WRITE   0x02           // Write data

// Status register format AT25256 :

#define EEP_MASK_RDY  0x01         // -READY, je v 1 probiha-li zapis
#define EEP_MASK_WEN  0x02         // je v 1 je-li zarizeni Write Enabled
#define EEP_MASK_BP0  0x04         // ochranny bit 0 (1/4 kapacity)
#define EEP_MASK_BP1  0x08         // ochranny bit 1 (1/2 kapacity)
#define EEP_MASK_WPEN 0x80         // ovladani Write Protect /WP pinu

// Lokalni funkce :

void WrenCmd (void);
// Posle instrukci WREN

byte RdsrCmd (void);
// Precte a vrati status registr

// porty :

#define Select()   XCS  = 0
#define Deselect() XCS  = 1
#define SetSCK()   XSCK = XSCK_H
#define ClrSCK()   XSCK = !XSCK_H
#define SetSI()    XSI  = XSI_H
#define ClrSI()    XSI  = !XSI_H
#define GetSO()    XSO
#define EnableSO() XSO  = 1


// Urovne pred povolenim CS :
#define SetupCS()   ClrSCK(); EnableSO();
// Urovne po ukonceni operace :
#define CleanupCS() XSCK=1; XSI=1; XSO=1;       // vsechny porty do H

#endif
