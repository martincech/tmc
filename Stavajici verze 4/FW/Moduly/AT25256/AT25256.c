//*****************************************************************************
//
//    AT25256.c    AT25256 EEPROM services via template
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "AT25256\AT25256.h"

//-----------------------------------------------------------------------------
// Makra pro generovani zdrojoveho textu
//-----------------------------------------------------------------------------

// Porty :

#define XCS    EepCS
#define XSCK   EepSCK
#define XSO    EepSO
#define XSI    EepSI

// Inverze signalu:
#define XSI_H  EEP_XSI_H
#define XSCK_H EEP_XSCK_H

// Funkce :

#define XInit           EepInit
#define XReadByte       EepReadByte
#define XBlockReadStart EepBlockReadStart
#define XRawReadByte    EepRawReadByte
#define XStop           EepStop
#define XWriteByte      EepWriteByte
#define XPageWriteStart EepPageWriteStart
#define XRawWriteByte   EepRawWriteByte
#define XAccuOk         EepAccuOk


// Podminena kompilace :

#ifdef EEP_READ_BYTE
   #define X_READ_BYTE     1
#endif
#ifdef EEP_WRITE_BYTE
   #define X_WRITE_BYTE    1
#endif

#include "AT25256.tpl"          // zdrojovy text
