//*****************************************************************************
//
//    Hc165.h    -  74HC165 shift register as digital input
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Hc165_H__
   #define __Hc165_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// usporadani vstupu :
// LSB je D0
// MSB je D7

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void DigitalInInit( void);
// Inicializace modulu

byte DigitalInRead( void);
// Cteni vstupu

#endif
