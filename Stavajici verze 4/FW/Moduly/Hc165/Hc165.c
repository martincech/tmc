//*****************************************************************************
//
//    Hc165.c    -  74HC165 shift register as digital input
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "Hc165\Hc165.h"

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void DigitalInInit( void)
// Inicializace modulu
{
   DinCP = 0;                          // vychozi hodiny
   DinPL = 1;                          // shift
   DinQ7 = 1;                          // vstup
} // DinInit

//-----------------------------------------------------------------------------
// Cteni
//-----------------------------------------------------------------------------

byte DigitalInRead( void)
// Cteni vstupu
{
byte i;
byte Value;
   DinPL = 1;                          // shift
   DinCP = 0;                          // vychozi hodiny
   DinPL = 0;                          // strobovani vstupu
   DinQ7 = 1;                          // inicializace cteni
   DinPL = 1;                          // konec strobovani
   // shift dat :
   Value = 0;
   for( i = 0; i < 8; i++){
      DinCP = 0;                       // neaktivni hrana hodin
      Value <<= 1;
      if( DinQ7){
         Value |= 0x01;
      }
      DinCP = 1;                       // posun registru
   }
   DinCP = 0;
   return( Value);
} // DinRead
