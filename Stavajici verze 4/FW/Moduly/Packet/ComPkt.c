//*****************************************************************************
//
//   ComPkt.c    COM packet interface
//   Version 1.0 (c) Vymos
//
//*****************************************************************************

#include "Packet\ComPkt.h"
#include "Packet\CPktDef.h"            // format paketu

#ifdef PACKET_TX
   #define _TX
#endif
#ifdef PACKET_RX
   #define _RX
#endif
#ifdef PACKET_TX_BLOCK
   #define _TX_BLOCK
#endif
#ifdef PACKET_RX_BLOCK
   #define _RX_BLOCK
#endif
#ifdef PACKET_COM1
   #define _COM1
#endif

#define _RX_TIMEOUT   PACKET_RX_TIMEOUT
#define _MAX_DATA     PACKET_MAX_DATA

#ifdef _COM1
   #include "RS232\Com.h"
#else
   #error "Unknowm COM interface"
#endif

//-----------------------------------------------------------------------------
// Prijem
//-----------------------------------------------------------------------------

#define DW_BYTE(dw, offset) ((byte)(dw >> (offset*8)) & 0xFF)
#define BYTE_DW(dw, byte, offset) dw |= (byte & 0xFF) << (offset * 8)
#ifdef _RX

TYesNo ComRxPacket( byte data *cmd, dword data *arg)
// Prijme paket, vrati typ prikazu <cmd> a parametr prikazu <arg>.
// Vrati NO, pokud paket neprisel
{
byte b;
byte crc;

   *arg = 0;
   #if (_RX_TIMEOUT != 0xFF)
      // s uvodnim cekanim
      if( !ComRxWait( _RX_TIMEOUT)){
         return( NO);                // timeout
      }
      ComRxChar( &b);
   #else
      // jen meziznakovy timeout
      if( !ComRxChar( &b)){
         return( NO);
      }
   #endif
   if( b != CPKT_SHORT_START){
      ComFlushChars();                 // prijato smeti, cekat na dobeh vsech znaku
      return( NO);
   }
   if( !ComRxChar( &b)){
      return( NO);
   }
   crc  = b;
   *cmd = b;
   if( !ComRxChar( &b)){
      return( NO);
   }
   crc += b;
   BYTE_DW(*arg, b, 3);             // LSB
   if( !ComRxChar( &b)){
      return( NO);
   }
   crc += b;
   BYTE_DW(*arg, b, 2);
   if( !ComRxChar( &b)){
      return( NO);
   }
   crc += b;
   BYTE_DW(*arg, b, 1);
   if( !ComRxChar( &b)){
      return( NO);
   }
   crc += b;
   BYTE_DW(*arg, b, 0);             // MSB
   if( !ComRxChar( &b)){
      return( NO);
   }
   if( b != (byte)~crc){
      return( NO);                     // chyba zabezpeceni
   }
   if( !ComRxChar( &b)){
      return( NO);
   }
   if( b != CPKT_SHORT_END){
      return( NO);                     // neplatne zakonceni
   }
   return( YES);
} // ComRxPacket

#endif

//-----------------------------------------------------------------------------
// Vyslani
//-----------------------------------------------------------------------------

#ifdef _TX

void ComTxPacket( byte cmd, dword arg)
// Vysle paket s kodem prikazu <cmd> a parametrem <arg>
{
byte crc;

   ComTxChar( CPKT_SHORT_START);
   crc    = cmd;
   crc   += DW_BYTE(arg,0);
   crc   += DW_BYTE(arg,1);
   crc   += DW_BYTE(arg,2);
   crc   += DW_BYTE(arg,3);
   ComTxChar( cmd);
   ComTxChar( DW_BYTE(arg,3));           // LSB
   ComTxChar( DW_BYTE(arg,2));
   ComTxChar( DW_BYTE(arg,1));
   ComTxChar( DW_BYTE(arg,0));           // MSB
   ComTxChar( ~crc);
   ComTxChar( CPKT_SHORT_END);
} // ComTxPacket

#endif

#ifdef _TX_BLOCK

   static byte _com_data_crc;                 // pro interni pouziti

//-----------------------------------------------------------------------------
// Datovy blok - start
//-----------------------------------------------------------------------------

void ComTxBlockStart( word size)
// Vysle zahlavi paketu pro data o velikosti <size>
{
   ComTxChar( CPKT_DATA_START);
   ComTxChar( size & 0xFF);            // Size1 LSB
   ComTxChar( size >> 8);              // MSB
   ComTxChar( size & 0xFF);            // Size2 LSB
   ComTxChar( size >> 8);              // MSB
   ComTxChar( CPKT_DATA_START);
   _com_data_crc = 0;
} // ComTxBlockStart

void ComTxBlockByte( byte b)
// Vysle byte <b> bloku dat
{
   ComTxChar( b);
   _com_data_crc += b;
} // ComTxBlockByte


//-----------------------------------------------------------------------------
// Datovy blok - konec
//-----------------------------------------------------------------------------

void ComTxBlockEnd( void)
// Uzavre datovy blok
{
   ComTxChar( ~_com_data_crc);
   ComTxChar( CPKT_DATA_END);
} // ComTxBlockEnd
#endif // _TX_BLOCK


#ifdef _RX_BLOCK
//-----------------------------------------------------------------------------
// Prijem datoveho bloku
//-----------------------------------------------------------------------------

TYesNo ComRxBlock( void xdata *buffer, word data *size)
// Prijem datoveho bloku
{
byte b, bb;
word data_size;
byte crc;
word i;
byte xdata *p;

   #if (_RX_TIMEOUT != 0xFF)
      // s uvodnim cekanim
      if( !ComRxWait( _RX_TIMEOUT)){
         return( NO);                // timeout
      }
      ComRxChar( &b);
   #else
      // jen meziznakovy timeout
      if( !ComRxChar( &b)){
         return( NO);
      }
   #endif
   if( b != CPKT_DATA_START){
      ComFlushChars();                 // prijato smeti, cekat na dobeh vsech znaku
      return( NO);
   }
   // Size1 :
   if( !ComRxChar( &b)){
      return( NO);
   }
   if( !ComRxChar( &bb)){
      return( NO);
   }
   data_size = ((word)bb << 8) | b;
   // Size2 :
   if( !ComRxChar( &b)){
      return( NO);
   }
   if( !ComRxChar( &bb)){
      return( NO);
   }
   if( data_size != (((word)bb << 8) | b)){
      return( NO);
   }
   if( data_size > _MAX_DATA){
      return( NO);                     // delka paketu presahuje buffer
   }
   // Header :
   if( !ComRxChar( &b)){
      return( NO);
   }
   if( b != CPKT_DATA_START){
      return( NO);
   }
   // Data :
   crc = 0;
   p   = (byte xdata *)buffer;
   for( i = 0; i < data_size; i++){
      if( !ComRxChar( &b)){
         return( NO);
      }
      *p = b;
      p++;
      crc += b;
   }
   // Crc :
   if( !ComRxChar( &b)){
      return( NO);
   }
   if( b != (byte)~crc){
      return( NO);                     // chyba zabezpeceni
   }
   // End :
   if( !ComRxChar( &b)){
      return( NO);
   }
   if( b != CPKT_DATA_END){
      return( NO);                     // neplatne zakonceni
   }

   // 22.2.2013: Navrat delky presunut az na konec, nevim proc, ale kdyz byl tento radek vyse, tak
   // se zmrvila promenna data_size.
   *size = data_size;                  // navrat delky

   return( YES);
} // ComRxBlock

#endif // _RX_BLOCK
