//*****************************************************************************
//
//    Tda8444.c  -  DAC TDA8444 convertor services
//    Version 1.1 (c) VymOs
//
//*****************************************************************************

#include "Tda8444\Tda8444.h"

/*
20.3.05 - v SMD provedeni je zadratovano A2 na H, zada se v Hardware.h jako
          DAC_ADDRESS = 0x04
*/

// Prvni byte :

#define DAC_BASE_ADDRESS 0x40          // zaklad adresy prevodniku
#define DAC_MASK_ADDRESS 0x0E          // poloha adresy pinu A0..A2


// Druhy byte :

#define DAC_INSTRUCTION_SINGLE 0xF0    // instrukce pro jednorazove zadani subadresy
#define DAC_INSTRUCTION_MULTI  0x00    // instrukce pro autoinkrement subadresy
#define DAC_MASK_SUBADDRESS    0x07    // subadresa - cilovy prevodnik

// Treti a dalsi byty :

#define DAC_MASK_VALUE         0x3F    // maximalni zadavana hodnota

// Implementacni konstanty :

#define DAC_ADDRESS_BYTE (DAC_BASE_ADDRESS | (DAC_MASK_ADDRESS & (DAC_ADDRESS << 1)))
                                                     // povel pro adresaci prevodniku
#define DAC_INSTRUCTION_WRITE( channel) (DAC_INSTRUCTION_SINGLE | (channel & DAC_MASK_SUBADDRESS))
                                                     // jednorazovy zapis do kanalu
#define DAC_INSTRUCTION_WRITE_BURST      DAC_INSTRUCTION_MULTI
                                                     // zapis vsech kanalu od subadresy 0

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

TYesNo DacInit( void)
// Inicializuje sbernici a prevodnik
{
bit ack;

   if( !IicInit()){
      return( NO);
   }
   IicStart();
   ack = IicSend( DAC_ADDRESS_BYTE);
   IicStop();
   return( ack);    // odpoved adresovaneho prevodniku
} // DacInit


#ifdef DAC_SINGLE_WRITE
//-----------------------------------------------------------------------------
// Zapis kanalu
//-----------------------------------------------------------------------------

TYesNo DacWrite( byte channel, byte value)
// Zapis do kanalu prevodniku
{
   IicStart();
   if( !IicSend( DAC_ADDRESS_BYTE)){
      return( NO);       // neprislo ACK
   }
   if( !IicSend( DAC_INSTRUCTION_WRITE( channel))){
      return( NO);       // neprislo ACK
   }
   if( !IicSend( value & DAC_MASK_VALUE)){
      return( NO);
   }
   IicStop();
   return( YES);
} // DacWrite

#endif

#ifdef DAC_BURST_WRITE
//-----------------------------------------------------------------------------
// Zapis celkovy
//-----------------------------------------------------------------------------

TYesNo DacBurstWrite( byte values[])
// Zapise pole do vsech 8 kanalu prevodniku
{
byte i;

   IicStart();
   if( !IicSend( DAC_ADDRESS_BYTE)){
      return( NO);       // neprislo ACK
   }
   if( !IicSend( DAC_INSTRUCTION_WRITE_BURST)){
      return( NO);       // neprislo ACK
   }
   for( i = 0; i < DAC_COUNT; i++){
      if( !IicSend( values[ i] & DAC_MASK_VALUE)){
         return( NO);
      }
   }
   IicStop();
   return( YES);
} // DacBurstWrite

#endif


