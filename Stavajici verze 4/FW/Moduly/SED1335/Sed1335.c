//*****************************************************************************
//
//    Sed1335.c - SED 1335F LCD display services
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

// 21.1.2008: Podpora radice S1D13700

// 13.3.2008: Zlepsena podpora S1D3700, bez problemu v puvodnim makru pro zapis dat (mrseni zobrazeni)


//#define __DEBUG__

#include "System.h"
#include "Sed1335\Sed1335.h"
#include <stdio.h>

// Prikazy pro radic SED 1335F :

//-----------------------------------------------------------------------------
// SYSTEM_SET
//-----------------------------------------------------------------------------
#define DISPLAY_CMD_SYSTEM_SET 0x40          // inicializace a zakladni nastaveni
// P1
#define DISPLAY_MASK_P1         0x10          // zakladni maska, povinna !
#define DISPLAY_MASK_M0         0x01          // external CG ROM
#define DISPLAY_MASK_M2         0x04          // 16 pixel char height
#define DISPLAY_MASK_WS         0x08          // dual-panel display
#define DISPLAY_MASK_IV         0x20          // No screen top-line correction
#define DISPLAY_MASK_TL         0x40          // TV mode
#define DISPLAY_MASK_DR         0x80          // additional shift-clock cycle
// P2
#define DISPLAY_MASK_FX         0x07          // sirka znaku - 1
#define DISPLAY_MASK_WF         0x80          // two-frame AC drive waveform
// P3
#define DISPLAY_MASK_FY         0x0F          // vyska znaku - 1
// P4
#define DISPLAY_MASK_CR         0xFF          // bytu na radku - 1, 0..239
// P5
#define DISPLAY_MASK_TCR        0xFF          // celkova delka radku v "bytech" - 1, 0..255
// P6
#define DISPLAY_MASK_LF         0xFF          // pocet grafickych radku - 1, 0..255
// P7
#define DISPLAY_MASK_APL        0xFF          // dolni byte poctu bytu na virtualni radek
// P8
#define DISPLAY_MASK_APH        0xFF          // horni byte poctu bytu na virtualni radek

//-----------------------------------------------------------------------------
// SLEEP IN
//-----------------------------------------------------------------------------

#define DISPLAY_CMD_SLEEP_IN    0x53          // uspani displeje

// vzbuzeni pomoci prvnich 2 bytu SYSTEM_SET

//-----------------------------------------------------------------------------
// DISP ON/OFF
//-----------------------------------------------------------------------------

#define DISPLAY_CMD_DISP_OFF    0x58            // vypinani displeje, nastaveni kurzoru a rovin
#define DISPLAY_CMD_DISP_ON     0x59            // zapinani displeje, -"-
// P1
#define DISPLAY_MASK_CURSOR_OFF    0x00         // vypnuti kurzoru
#define DISPLAY_MASK_CURSOR_ON     0x01         // svitici kurzor
#define DISPLAY_MASK_CURSOR_FLASH2 0x02         // kurzor blikajici 2Hz
#define DISPLAY_MASK_CURSOR_FLASH1 0x03         // kurzor blikajici 1Hz

#define DISPLAY_MASK_SAD1_OFF      0x00         // vypnuti roviny SAD1
#define DISPLAY_MASK_SAD1_ON       0x04         // zapnuti roviny SAD1
#define DISPLAY_MASK_SAD1_FLASH2   0x08         // blikani roviny SAD1 2Hz
#define DISPLAY_MASK_SAD1_FLASH16  0x0C         // blikani roviny SAD1 16Hz

#define DISPLAY_MASK_SAD2_OFF      0x00         // vypnuti roviny SAD2
#define DISPLAY_MASK_SAD2_ON       0x10         // zapnuti roviny SAD2
#define DISPLAY_MASK_SAD2_FLASH2   0x20         // blikani roviny SAD2 2Hz
#define DISPLAY_MASK_SAD2_FLASH16  0x30         // blikani roviny SAD2 16Hz

#define DISPLAY_MASK_SAD3_OFF      0x00         // vypnuti roviny SAD3
#define DISPLAY_MASK_SAD3_ON       0x40         // zapnuti roviny SAD3
#define DISPLAY_MASK_SAD3_FLASH2   0x80         // blikani roviny SAD3 2Hz
#define DISPLAY_MASK_SAD3_FLASH16  0xC0         // blikani roviny SAD3 16Hz

//-----------------------------------------------------------------------------
// SCROLL
//-----------------------------------------------------------------------------

#define DISPLAY_CMD_SCROLL         0x44         // definice rolovacich oblasti
// P1
#define DISPLAY_MASK_SAD1L         0xFF         // dolni adresa oblasti SAD1
// P2
#define DISPLAY_MASK_SAD1H         0xFF         // horni adresa oblasti SAD1
// P3
#define DISPLAY_MASK_SL1           0xFF         // pocet radku oblasti SAD1 - 1

// P4
#define DISPLAY_MASK_SAD2L         0xFF         // dolni adresa oblasti SAD2
// P5
#define DISPLAY_MASK_SAD2H         0xFF         // horni adresa oblasti SAD2
// P6
#define DISPLAY_MASK_SL2           0xFF         // pocet radku oblasti SAD2 - 1

// P7
#define DISPLAY_MASK_SAD3L         0xFF         // dolni adresa oblasti SAD3
// P8
#define DISPLAY_MASK_SAD3H         0xFF         // horni adresa oblasti SAD3

// jen pro WS = 1 a dve roviny :
// P9
#define DISPLAY_MASK_SAD4L         0xFF         // dolni adresa oblasti SAD4
// P10
#define DISPLAY_MASK_SAD4H         0xFF         // horni adresa oblasti SAD4

//-----------------------------------------------------------------------------
// CSRFORM
//-----------------------------------------------------------------------------

#define DISPLAY_CMD_CSRFORM        0x5D        // nastavi velikost a tvar kurzoru
// P1
#define DISPLAY_MASK_CRX           0x0F        // sirka kurzoru v pixelech - 1
// P2
#define DISPLAY_MASK_CRY           0x0F        // vyska kurzoru v pixelech - 1
#define DISPLAY_MASK_CM            0x80        // blokovy kurzor, v grafice vzdy nastavit

//-----------------------------------------------------------------------------
// CGRAM_ADR
//-----------------------------------------------------------------------------

#define DISPLAY_CMD_CGRAM_ADR      0x5C        // nastaveni pocatecni adresy generatoru znaku
// P1
#define DISPLAY_MASK_SAGL          0xFF        // dolni cast adresy
// P2
#define DISPLAY_MASK_SAGH          0xFF        // horni cast adresy
// POZOR, adresa plati pro znak 0x00, prvni uzivatelsky znak je 0x80
// ofset prvniho uzivatelskeho znaku je 0x80 krat pocet radku na znak

//-----------------------------------------------------------------------------
// HDOT_SCR
//-----------------------------------------------------------------------------

#define DISPLAY_CMD_HDOT_SCR       0x5A        // horizontalni rolovani po pixelech
// P1
#define DISPLAY_MASK_HDOT_PIXEL    0x07        // pocet pixelu horizontalniho posunu

//-----------------------------------------------------------------------------
// OVLAY
//-----------------------------------------------------------------------------

#define DISPLAY_CMD_OVLAY          0x5B        // rizeni prekryti rovin
// P1
#define DISPLAY_MASK_OR            0x00        // OR  rovin 1 a 2, OR 3
#define DISPLAY_MASK_XOR           0x01        // XOR rovin 1 a 2, OR 3
#define DISPLAY_MASK_AND           0x02        // AND rovin 1 a 2, OR 3
#define DISPLAY_MASK_POR           0x03        // Prioritni OR rovin 1,2 a 3

#define DISPLAY_MASK_TEXT          0x00        // rovina 1 v textovem modu
#define DISPLAY_MASK_GRAPHICS      0x0C        // rovina 1 v grafickem modu
#define DISPLAY_MASK_GRAPHICS3     0x1C        // tri graficke roviny

//-----------------------------------------------------------------------------
// CSRW
//-----------------------------------------------------------------------------

#define DISPLAY_CMD_CSRW           0x46        // zapis pozice ukazatele do videopameti
// P1
#define DISPLAY_MASK_CSRL          0xFF        // dolni cast adresy ukazatele
// P2
#define DISPLAY_MASK_CSRH          0xFF        // horni cast adresy ukazatele (nepovinna)

//-----------------------------------------------------------------------------
// CSRR
//-----------------------------------------------------------------------------

#define DISPLAY_CMD_CSRR           0x47        // cteni pozice ukazatele do videopameti
// P1 viz CSRW
// P2 viz CSRW
// oba parametry se ctou (!) v datovem modu

//-----------------------------------------------------------------------------
// MREAD
//-----------------------------------------------------------------------------

#define DISPLAY_CMD_MREAD          0x43      // cteni videopameti
// P1..PN ctene byty, inkrement/dekrement adresy probiha podle CSRDIR

//-----------------------------------------------------------------------------
// GRAYSCALE
//-----------------------------------------------------------------------------
#define DISPLAY_CMD_GRAYSCALE      0x60      // Stupne sedi

//-----------------------------------------------------------------------------


// Konstanty pro displej POWERTIP PG320240 :

//#define DISPLAY_WIDTH   320                   // graficke rozliseni v pixelech vodorovne
//#define DISPLAY_HEIGHT  240                   // graficke rozliseni v pixelech svisle
#define DISPLAY_X_BYTE  (DISPLAY_WIDTH / 8)   // pocet bytu videoram na radek
#define DISPLAY_CR_SPACE 18                   // pocet bytu horizontalniho zatemneni

#ifdef DISPLAY_MODE_MIXED
   // Generovani znaku v textovem modu :

   #define DISPLAY_X_FONT 5                      // zabudovany font - sirka
   #define DISPLAY_Y_FONT 7                      // zabudovany font - vyska
   #define DISPLAY_MIN_CHAR      ' '             // minimalni znak generatoru
   #define DISPLAY_MAX_CHAR      0x7F            // maximalni zakladni znak generatoru
   #define DISPLAY_MIN_RAM1_CHAR 0x80            // prvni uzivatelsky definovany znak
   #define DISPLAY_MAX_RAM1_CHAR 0x9F            // posledni uzivatelsky definovany znak
   #define DISPLAY_MIN_RAM2_CHAR 0xE0            // prvni uzivatelsky znak v druhe casti
   #define DISPLAY_MAX_RAM2_CHAR 0xFF            // posledni uzivatelsky znak v druhe casti
   #define DISPLAY_RAM2_MASK     0x40            // kodovani RAM2 v souvisle oblasti-nuluj tento bit

   // Uzivatelske nastaveni :

   #define DISPLAY_COLS (DISPLAY_WIDTH / DISPLAY_CHAR_WIDTH)   // pocet sloupcu textu
   #define DISPLAY_ROWS (DISPLAY_HEIGHT / DISPLAY_CHAR_HEIGHT) // pocet radku textu
#endif

#define DISPLAY_CHAR_WIDTH  8                                  // sirka pole pro znak
#define DISPLAY_CHAR_HEIGHT 8                                  // vyska pole pro znak

// Rozdeleni oblasti videopameti :

#define DISPLAY_SAD1_START  0                   // startovaci adresa prvni roviny
#define DISPLAY_SAD1_HEIGHT DISPLAY_HEIGHT      // vyska prvni roviny
#ifdef DISPLAY_MODE_MIXED
   #define DISPLAY_SAD2_START (DISPLAY_COLS * DISPLAY_ROWS)  // pocet znaku na strance
   #define DISPLAY_SAD3_START  0                             // nepouzito
   #define DISPLAY_SAD4_START  0                             // nepouzito
   #define DISPLAY_CGRAM_START 0xF000                        // zacatek znakoveho generatoru
#elif DISPLAY_MODE_GRAPHICS2
   #define DISPLAY_SAD2_START (DISPLAY_X_BYTE * DISPLAY_HEIGHT) // pocet bytu na strance
   #define DISPLAY_SAD3_START  0                                // nepouzito
   #define DISPLAY_SAD4_START  0                                // nepouzito
#elif DISPLAY_MODE_GRAPHICS3
   #define DISPLAY_SAD2_START (DISPLAY_X_BYTE * DISPLAY_HEIGHT)      // pocet bytu na strance
   #define DISPLAY_SAD3_START (2 * DISPLAY_X_BYTE * DISPLAY_HEIGHT)  // pocet bytu na predchozich strankach
   #define DISPLAY_SAD4_START  0                                     // nepouzito
#else
   #error "Undefined graphics mode (DISPLAY_MODE_...)"
#endif
#define DISPLAY_SAD2_HEIGHT DISPLAY_HEIGHT

// Lokalni promenne :
#ifdef DISPLAY_MODE_MIXED
   byte row;   // aktualni pozice v textove obrazovce
   byte col;
#endif

// Kontextove promenne pro kresleni :
bit    _plane;                         // cislo graficke roviny
static word   _offset;                 // ofset do videopameti, odpovidajici <x,y,plane>
static TFontDescriptor code *_font;    // pracovni font
static bit plane1_on;
static bit plane2_on;

// Lokalni funkce :

#define WriteData( d) DisplayWriteData( d)
// zapise do displeje data

static void ClearRAM( void);
// Smaze vsechny roviny RAM

static void VideoRAMSet( word start, word length, char ch);
// Nastavi oblast video RAM na zadanou hodnotu. POZOR, pred volanim musi byt
// nastaven smysl pohybu kurzoru

//-----------------------------------------------------------------------------
// Implementacni poznamky :
// - Vodic A0 je trvale drzen v 0 (datovy mod), jen po dobu vyslani prikazu
//   se nastavuje do 1. Bude-li vadit, muze se shozeni A0 schovat do makra
//   DisplayEnable() a nahozeni do DisplayDisable()
// - Vyberovy vodic /CS displeje je trvale v 1 (neaktivni), po dobu volani
//   funkce se aktivuje
// - Implicitni pohyb kurzoru je vpravo, pokud jej funkce zmeni, je treba jej
//   vratit

//-----------------------------------------------------------------------------
// Inicializace displeje - podle Swordfish
//-----------------------------------------------------------------------------

/*#define cmdSystem 0x40          // general system settings
#define cmdSleep 0x53           // enter into standy mode
#define cmdDisplayOff 0x58      // turn the display off
#define cmdDisplayOn 0x59       // turn the display on
#define cmdScroll 0x44          // setup text and graphics address regions
#define cmdCsrForm 0x5D         // set cursor size
#define cmdCsrDirRight 0x4C     // cursor moves right after write to display memory
#define cmdCsrDirLeft 0x4D      // cursor moves left after write to display memory
#define cmdCsrDirUp 0x4E        // cursor moves up after write to display memory
#define cmdCsrDirDown 0x4F      // cursor moves down after write to display memory
#define cmdCGRAMAddress 0x5C    // configure character generator RAM address
#define cmdHDotScroll 0x5A      // set horizontal scroll rate
#define cmdOverlay 0x5B         // configure how layers overlay
#define cmdSetCsrAddress 0x46   // set the cursor address
#define cmdGetCsrAddress 0x47   // read the cursor address
#define cmdDisplayWrite 0x42    // write to display memory
#define cmdDisplayRead 0x43     // read from display memory

static void CsrMemPosition(word pMemStart) {
   DisplayWriteCommand(cmdSetCsrAddress);  // command - next two bytes are low & high bytes of memory start
   WriteData(pMemStart & 0x00FF);   // low byte
   WriteData(pMemStart << 8);   // high byte
}

void DisplayInit( void)
// inicializuje display
{
   word w;

   DisplayRES = 1;
   DisplayWR = 1;
   DisplayA0 = 0;
   DisplayCS = 1;
   SysDelay(100);         // start up delay, allow GLCD to settle
   DisplayRES = 0;                    // RST = 0 to reset
   SysDelay(5);                 // 1 ms minimum, 5ms for safety
   DisplayRES = 1;
   SysDelay(5);                 // 3 ms minimum, 5ms for safety
   DisplayCS = 1;

   SysDelay(100);
   DisplayWriteCommand(cmdSystem);         // execute 'system set' instruction
   WriteData(0x30);               // control byte sets internal char gen (see DS);
   SysDelay(100);

   DisplayWriteCommand(cmdSystem);         // execute 'system set' instruction
   SysDelay(1);
   WriteData(0x30);               // control byte sets internal char gen (see DS);
   WriteData(0x87);               // FX - horizontal char size - 1 (see DS);
   WriteData(0x07);               // FY - vertical char size - 1
   WriteData(0x27);               // C/R - bytes per line horizintally - 1
   WriteData(0x42);               // TC/R - provides overscan idle time (see DS);
   WriteData(0xEF);               // LF - height of frame in lines - 1 (0xEF = 239);
   WriteData(0x28);               // AP[L] - low byte of AP (horizontal address of virtual screen);
   WriteData(0x00);               // AP[H] - high byte of AP (horizontal address of virtual screen);
   DisplayWriteCommand(cmdScroll);         // sets scroll address and lines per scroll block
   WriteData(0x00);               // graphics layer 1 start[L] (0x0000);
   WriteData(0x00);               // graphics layer 1 start[H]
   WriteData(0xEF);               // graphics layer 1 lines per block - 1 (0xEF = 239);
   WriteData(0x80);               // graphics layer 2 start[L] (0x2580 = 9600 = 320 x 240 / 8);
   WriteData(0x25);               // graphics layer 2 start[H]
   WriteData(0xEF);               // graphics layer 2 lines per block - 1 (0xEF = 239);
   WriteData(0x00);               // graphics layer 3 start[L] (0x4B00 = 19200 = 9600 + 320 x 240 / 8);
   WriteData(0x4B);               // graphics layer 3 start[H]
   WriteData(0x00);               // no graphics layer 4
   WriteData(0x00);
   DisplayWriteCommand(cmdHDotScroll);     // allow screen to scroll by pixels
   WriteData(0x00);               // set to zero pixels
   DisplayWriteCommand(cmdOverlay);        // controls how graphics layers are combined
   WriteData(0x1C);               // set to priority OR L1 > L2 > L3, all graphics layers
   DisplayWriteCommand(cmdDisplayOff);     // command for screen off...
   WriteData(0x54);               // but sets screens 1 - 3 for turning on later
   DisplayWriteCommand(cmdCsrForm);        // select cursor size and shape
   WriteData(0x00);               // cursor width - 1
   WriteData(0x80);               // cursor height - 1 (plus block style);

//   DisplayWriteCommand(0x60);         // SED1335_CMD_GRAYSCALE
//   WriteData(0x00);        // 1 bpp


   DisplayWriteCommand(cmdCsrDirRight);    // set cursor to increment to right
   CsrMemPosition(0);          // cursor at start of graphics layer 1
   DisplayWriteCommand(cmdDisplayWrite);   // set to write to display memory
   for (w = 0; w < 28800; w++) {         // clear layers 1 - 3 of display memory
      WriteData(0x01);
   }
   CsrMemPosition(0);          // cursor at start of graphics layer 1
   DisplayWriteCommand(cmdDisplayOn);      // command for screen on...
   WriteData(0x54);               // for screens 1 - 3

   SysDelay(50000);

   // Nastaveni implicitniho grafickeho fontu :
   ClearRAM();
   DisplaySetFont( 0);
   DisplaySetPlane( DISPLAY_PLANE2);
   DisplayPlane( YES);
   DisplaySetPlane( DISPLAY_PLANE1);
   DisplayPlane( YES);
   // zapnuty obe roviny :
   plane1_on = 1;
   plane2_on = 1;
}*/

//-----------------------------------------------------------------------------
// Inicializace displeje
//-----------------------------------------------------------------------------

void DisplayInit( void)
// inicializuje display
{
   // Default nastavim na data
   DisplayAddressData();

   // Reset
   DisplayRun();
   SysDelay(100);       // start up delay, allow GLCD to settle
   DisplayReset();      // RST = 0 to reset
   SysDelay(5);         // 1 ms minimum, 5ms for safety
   DisplayRun();
   SysDelay(5);         // 3 ms minimum, 5ms for safety

   // Probuzeni radice S1D3700 - zapis SYSTEM_SET + P1 = opusteni Sleep modu
   SysDelay(100);
   DisplayWriteCommand(DISPLAY_CMD_SYSTEM_SET);
   WriteData( DISPLAY_MASK_P1 |
              DISPLAY_MASK_IV
            );         // P1 - Internal ROM, 64 CG RAM, Height 8, single-panel,
                       // no top-line correction, LCD mode, no additional cycles
   SysDelay(100);

   //------------- SYSTEM_SET :
   DisplayWriteCommand( DISPLAY_CMD_SYSTEM_SET);
   WriteData( DISPLAY_MASK_P1 |
              DISPLAY_MASK_IV
            );         // P1 - Internal ROM, 64 CG RAM, Height 8, single-panel,
                       // no top-line correction, LCD mode, no additional cycles
   WriteData( DISPLAY_MASK_WF |
             (DISPLAY_MASK_FX  & (DISPLAY_CHAR_WIDTH - 1))
            );         // P2 - sirka znaku, two frame waveform period
   WriteData( DISPLAY_MASK_FY  & (DISPLAY_CHAR_HEIGHT - 1)
            );         // P3 - vyska znaku
   WriteData( DISPLAY_MASK_CR  & (DISPLAY_X_BYTE - 1));
                       // P4 - sirka radku v bytech - 1
   WriteData( DISPLAY_MASK_TCR & (DISPLAY_X_BYTE + DISPLAY_CR_SPACE - 1));
                       // P5 - celkova sirka radku vcetne zatemneni
   WriteData( DISPLAY_MASK_LF  & (DISPLAY_HEIGHT - 1));
                       // P6 - vyska displeje v pixelech - 1
   WriteData( DISPLAY_MASK_APL & DISPLAY_X_BYTE);
                       // P7 - LSB sirka virtualniho radku v bytech
   WriteData( DISPLAY_MASK_APH & (DISPLAY_X_BYTE >> 8));
                       // P8 - MSB -"-
   //------------- SCROLL :
   DisplayWriteCommand( DISPLAY_CMD_SCROLL);
   WriteData( DISPLAY_MASK_SAD1L & DISPLAY_SAD1_START);
                       // P1 - SAD1L
   WriteData( DISPLAY_MASK_SAD1H & (DISPLAY_SAD1_START >> 8));
                       // P2 - SAD1H
   WriteData( DISPLAY_MASK_SL1   & (DISPLAY_SAD1_HEIGHT - 1));
                       // P3 - SL1
   WriteData( DISPLAY_MASK_SAD2L & DISPLAY_SAD2_START);
                       // P4 - SAD2L
   WriteData( DISPLAY_MASK_SAD2H & (DISPLAY_SAD2_START >> 8));
                       // P5 - SAD2H
   WriteData( DISPLAY_MASK_SL2   & (DISPLAY_SAD2_HEIGHT - 1));
                       // P6 - SL2
   WriteData( DISPLAY_MASK_SAD3L & DISPLAY_SAD3_START);
                       // P7 - SAD3L
   WriteData( DISPLAY_MASK_SAD3H & (DISPLAY_SAD3_START >> 8));
                       // P8 - SAD3H
   WriteData( DISPLAY_MASK_SAD4L & DISPLAY_SAD4_START);
                       // P9 - SAD4L
   WriteData( DISPLAY_MASK_SAD4H & (DISPLAY_SAD4_START >> 8));
                       // P10 - SAD4H
   //------------- HDOT_SCR :
   DisplayWriteCommand( DISPLAY_CMD_HDOT_SCR);
   WriteData( DISPLAY_MASK_HDOT_PIXEL & 0);
                        // P1 - pocet pixelu horizontalniho posunu
   #ifdef DISPLAY_MODE_MIXED
      //------------- OVLAY :
      DisplayWriteCommand( DISPLAY_CMD_OVLAY);
      WriteData( DISPLAY_MASK_TEXT |
                 DISPLAY_MASK_XOR);
                                 // P1 - SAD1 textovy, grafika XORovana s textem, 2 roviny
      //------------- CGRAM ADR :
      DisplayWriteCommand( DISPLAY_CMD_CGRAM_ADR);
      WriteData( DISPLAY_MASK_SAGL & DISPLAY_CGRAM_START);
      WriteData( DISPLAY_MASK_SAGH & (DISPLAY_CGRAM_START >> 8));
                                 // nastaveni baze uzivatelskych znaku
   #elif DISPLAY_MODE_GRAPHICS2
      //------------- OVLAY :
      DisplayWriteCommand( DISPLAY_CMD_OVLAY);
      WriteData( DISPLAY_MASK_GRAPHICS |
                 DISPLAY_MASK_XOR);
                                 // P1 - SAD1 grafika, grafika XORovana se SAD1, 2 roviny
   #elif DISPLAY_MODE_GRAPHICS3
      //------------- OVLAY :
      DisplayWriteCommand( DISPLAY_CMD_OVLAY);
      WriteData( DISPLAY_MASK_GRAPHICS3 |
                 DISPLAY_MASK_XOR);
                                 // P1 - SAD1 grafika, grafika XORovana se SAD1, 3 roviny
   #endif
   //------------- GRAYSCALE
   DisplayWriteCommand(DISPLAY_CMD_GRAYSCALE);  // SED1335_CMD_GRAYSCALE
   WriteData(0x00);             // 1 bpp
   //------------- DISP_OFF
   DisplayWriteCommand( DISPLAY_CMD_DISP_OFF);
   //------------- CSRDIR :
   DisplayWriteCommand(DISPLAY_CMD_CSR_DOWN);     // Pohyb kurzoru dolu
//   DisplayWriteCommand( DISPLAY_CMD_CSR_RIGHT);
   ClearRAM();
   #ifdef DISPLAY_MODE_MIXED
      //------------- CSRFORM :
      DisplayWriteCommand( DISPLAY_CMD_CSRFORM);
      WriteData( DISPLAY_MASK_CRX & (DISPLAY_X_FONT - 1));
                                 // P1 - sirka kurzoru v pixelech-1
      WriteData( DISPLAY_MASK_CM |
                 DISPLAY_MASK_CRY & (DISPLAY_Y_FONT - 1));
                                 // P2 - vyska kurzoru v pixelech - 1, blokovy kurzor
   #endif
   #ifdef DISPLAY_MODE_MIXED
      //------------- DISP_ON :
      DisplayWriteCommand( DISPLAY_CMD_DISP_ON);
      WriteData( DISPLAY_MASK_CURSOR_OFF |
                 DISPLAY_MASK_SAD1_ON  |
                 DISPLAY_MASK_SAD2_ON  |
                 DISPLAY_MASK_SAD3_OFF
               );
                                 // P1 - Vypnuty kurzor, zapnuty roviny SAD1, SAD2
   #elif DISPLAY_MODE_GRAPHICS2
      //------------- DISP_ON :
      DisplayWriteCommand( DISPLAY_CMD_DISP_ON);
      WriteData( DISPLAY_MASK_CURSOR_OFF |
                 DISPLAY_MASK_SAD1_ON  |
                 DISPLAY_MASK_SAD2_ON  |
                 DISPLAY_MASK_SAD3_OFF
               );
                                 // P1 - Vypnuty kurzor, zapnuty roviny SAD1, SAD2
   #elif DISPLAY_MODE_GRAPHICS3
      //------------- DISP_ON :
      DisplayWriteCommand( DISPLAY_CMD_DISP_ON);
      WriteData( DISPLAY_MASK_CURSOR_OFF |
                 DISPLAY_MASK_SAD1_ON  |
                 DISPLAY_MASK_SAD2_ON  |
                 DISPLAY_MASK_SAD3_ON
               );
                                 // P1 - Vypnuty kurzor, zapnuty roviny SAD1, SAD2, SAD3
   #endif

   #ifdef DISPLAY_MODE_MIXED
      // kurzor na prvni textovou pozici
      row = 0;
      col = 0;
   #endif

   // Nastaveni implicitniho grafickeho fontu :
   DisplaySetFont( 0);
   DisplaySetPlane( DISPLAY_PLANE2);
   DisplayPlane( YES);
   DisplaySetPlane( DISPLAY_PLANE1);
   DisplayPlane( YES);
   // zapnuty obe roviny :
   plane1_on = 1;
   plane2_on = 1;
} // DisplayInit

//-----------------------------------------------------------------------------
// Offset adresy podle souradnic
//-----------------------------------------------------------------------------

#ifdef DISPLAY_OFFSET

word DisplayOffset(byte X, byte Y) {
  // Offset adresy podle souradnic
  return (word)(X + (_plane ? DISPLAY_SAD2_START : DISPLAY_SAD1_START) + Y * DISPLAY_X_BYTE);
}

#endif

//-----------------------------------------------------------------------------
// Nastaveni roviny
//-----------------------------------------------------------------------------

void DisplaySetPlane( bit plane)
// nastavi pracovni zobrazovaci rovinu
{
   _plane = plane;
} // DisplaySetPlane


//-----------------------------------------------------------------------------
// Nastaveni fontu
//-----------------------------------------------------------------------------

void DisplaySetFont( byte font)
// nastavi pracovni font
{
//   _font = &Font[ font];
   switch (font) {
#ifdef DISPLAY_FONT_SYSTEM
     case FONT_SYSTEM: {
       _font = &FontSystem;
       break;
     }
#endif
#ifdef DISPLAY_MODE_MIXED
     case FONT_FRAME_8x8: {
       _font = &FontFrame;
       break;
     }
#endif
#ifdef DISPLAY_FONT_NUM_MYRIAD40
     case FONT_NUM_MYRIAD40: {
       _font = &FontNumericMyriad40;
       break;
     }
#endif
#ifdef DISPLAY_FONT_NUM_MYRIAD32
     case FONT_NUM_MYRIAD32: {
       _font = &FontNumericMyriad32;
       break;
     }
#endif
#ifdef DISPLAY_FONT_NUM_TAHOMA18
     case FONT_NUM_TAHOMA18: {
       _font = &FontNumericTahoma18;
       break;
     }
#endif
   }//switch
} // DisplaySetFont

//-----------------------------------------------------------------------------
// Nastaveni pozice
//-----------------------------------------------------------------------------
#ifdef DISPLAY_MOVE_TO

void DisplayMoveTo( byte x, byte y)
// presun pozice kurzoru <x> je pocet osmic pixelu, <y> je pocet pixelu
{
#ifdef DISPLAY_MODE_MIXED
   _offset  = DISPLAY_SAD2_START + y * DISPLAY_X_BYTE;          // zacatek radku y
   _offset += x;                                                // sloupec x
#elif DISPLAY_MODE_GRAPHICS2
   _offset  = (plane ? DISPLAY_SAD2_START : DISPLAY_SAD1_START) // zacatek roviny
               + y * DISPLAY_X_BYTE;                            // zacatek radku y
   _offset += x;                                                // sloupec x
#elif DISPLAY_MODE_GRAPHICS3
#endif
} // DisplayMoveTo

#endif // DISPLAY_MOVE_TO

//-----------------------------------------------------------------------------
// Mazani roviny
//-----------------------------------------------------------------------------
#ifdef DISPLAY_CLEAR

void DisplayClear( void)
// Smaze nastavenou rovinu
{
//   DisplayEnable();
#ifdef DISPLAY_MODE_MIXED
   if( !plane){
      // nastaveni textove roviny na mezery :
      VideoRAMSet( DISPLAY_SAD1_START, DISPLAY_ROWS * DISPLAY_COLS, ' ');
   } else {
      // nastaveni graficke roviny SAD2 na 0 :
      VideoRAMSet( DISPLAY_SAD2_START, DISPLAY_X_BYTE * DISPLAY_HEIGHT, 0);
   }
   // kurzor na prvni textovou pozici :
   row = 0;
   col = 0;
#elif DISPLAY_MODE_GRAPHICS2
      // Maze obe roviny!
      // nastaveni graficke roviny SAD1 na 0 :
      VideoRAMSet( DISPLAY_SAD1_START, DISPLAY_X_BYTE * DISPLAY_HEIGHT, 0);
      // nastaveni graficke roviny SAD2 na 0 :
      VideoRAMSet( DISPLAY_SAD2_START, DISPLAY_X_BYTE * DISPLAY_HEIGHT, 0);
#elif DISPLAY_MODE_GRAPHICS3
#endif
//   DisplayDisable();
} // DisplayClear
#endif  // DISPLAY_CLEAR

//-----------------------------------------------------------------------------
// Zobrazeni roviny
//-----------------------------------------------------------------------------
#ifdef DISPLAY_PLANE

void DisplayPlane( bit on)
// Podle promenne <on> zapne/vypne nastavenou rovinu
{

//   DisplayEnable();
   if( !plane){
      plane1_on = on;
   } else {
      plane2_on = on;
   }
   //------------- DISP_ON :
   DisplayWriteCommand( DISPLAY_CMD_DISP_ON);
#ifdef DISPLAY_MODE_MIXED
   WriteData( DISPLAY_MASK_CURSOR_FLASH2 |      // v textu zapnuty
              (plane1_on ? DISPLAY_MASK_SAD1_ON : DISPLAY_MASK_SAD1_OFF) |
              (plane2_on ? DISPLAY_MASK_SAD2_ON : DISPLAY_MASK_SAD2_OFF) |
              DISPLAY_MASK_SAD3_OFF
            );
#else
   WriteData( DISPLAY_MASK_CURSOR_OFF |         // v grafice vypnuty
              (plane1_on ? DISPLAY_MASK_SAD1_ON : DISPLAY_MASK_SAD1_OFF) |
              (plane2_on ? DISPLAY_MASK_SAD2_ON : DISPLAY_MASK_SAD2_OFF) |
              DISPLAY_MASK_SAD3_OFF
            );
#endif
//   DisplayDisable();
} // DisplayPlane

#endif // DISPLAY_PLANE

//-----------------------------------------------------------------------------
// Obdelnik
//-----------------------------------------------------------------------------
#ifdef DISPLAY_FILL_BOX

void DisplayFillBox( byte w, byte h, byte value)
// Nakresli obdelnik do nastavene roviny od nastavene pozice.
// <w> je sirka v osmicich pixelu, <h> je vyska v pixelech, <value>
// je hodnota osmice
{
byte i, j;

//   DisplayWriteCommand( DISPLAY_CMD_CSR_DOWN);                 // pohyb kurzoru dolu
   for( j = 0; j < w; j++){
      // jeden sloupec
      SetCursor( _offset);
      DisplayWriteCommand( DISPLAY_CMD_MWRITE);
      for( i = 0; i < h; i++){
         WriteData( value);
      }
      _offset++;                                         // dalsi sloupec
   }
//   DisplayWriteCommand( DISPLAY_CMD_CSR_RIGHT);                 // standardni smer pohybu
} // DisplayBox

#endif // DISPLAY_FILL_BOX

//-----------------------------------------------------------------------------
// Zapis obdelnikoveho vzoru
//-----------------------------------------------------------------------------
#ifdef DISPLAY_PATTERN

void DisplayPattern( byte code *pattern, byte w, byte h)
// Nakresli obdelnik vyplneny vzorem z adresy <pattern>
// do nastavene roviny od nastavene pozice.
// <w> je sirka v osmicich pixelu, <h> je vyska v pixelech
{
byte i, j;

//   DisplayEnable();
//   DisplayWriteCommand( DISPLAY_CMD_CSR_DOWN);                 // pohyb kurzoru dolu
   for( j = 0; j < w; j++){
      // jeden sloupec
      SetCursor( _offset);
      DisplayWriteCommand( DISPLAY_CMD_MWRITE);
      for( i = 0; i < h; i++){
         WriteData( *pattern);
/*         if (i % 2) {
           WriteData(0xFF);
         } else {
           WriteData(0);
         }*/
         pattern++;
      }
      _offset++;                                         // dalsi sloupec
   }
//   DisplayWriteCommand( DISPLAY_CMD_CSR_RIGHT);                 // standardni smer pohybu
//   DisplayDisable();
} // DisplayPattern

#endif // DISPLAY_PATTERN

//-----------------------------------------------------------------------------
// Pozicovani kurzoru
//-----------------------------------------------------------------------------
#ifdef DISPLAY_GOTO_RC

void DisplayGotoRC( byte r, byte c)
// presune kurzor na zadanou textovou pozici <r> radek <c> sloupec
// Hodnota DISPLAY_CURRENT znamena beze zmeny
{
#ifdef DISPLAY_MODE_MIXED
   if( r != DISPLAY_CURRENT){
      row = r;
   }
   if( c != DISPLAY_CURRENT){
      col = c;
   }
#else
   DisplayMoveTo( c, r * font->height);
#endif
} // DisplayGotoRC

#endif // DISPLAY_GOTO_RC

//-----------------------------------------------------------------------------
// Zapis znaku
//-----------------------------------------------------------------------------
#ifdef DISPLAY_PUTCHAR

char putchar( char c)
// standardni zapis znaku
{
word index;
byte bwidth;
#ifdef DISPLAY_MODE_MIXED
word start;

   if( !plane){
      if( c == '\n'){
         // (ponekud atypicky) presun na prvni pozici noveho radku
         row++;
         col = 0;
         return( c);
      }
      if( c == '\r'){
         // (ponekud atypicky) presun na predchozi pozici noveho radku
         row++;
         col--;
         return( c);
      }
      if( c == CH_NULL){
         // mezera bez prepsani podkladu
         col++;
         return( c);
      }
      DisplayEnable();
      start  = DISPLAY_SAD1_START + row * DISPLAY_COLS;
      start += col;
      SetCursor( start);

      //------------- MWRITE
      DisplayWriteCommand( DISPLAY_CMD_MWRITE);
      WriteData( c);
      col++;
      if( col >= DISPLAY_COLS){
         col = 0;
         row++;
      }
      DisplayDisable();
      return( c);
   } else {
      // plane == 1 je grafika
#endif
   if( c == '\n'){
      // (ponekud atypicky) presun na prvni pozici noveho radku
      _offset /= DISPLAY_X_BYTE;
      _offset *= DISPLAY_X_BYTE;                 // zacatek radku
      _offset += font->height * DISPLAY_X_BYTE;  // novy radek
      return( c);
   }
   bwidth = font->width / 8;                     // bytova sirka fontu
   if( c == '\r'){
      // (ponekud atypicky) presun na predchozi pozici noveho radku
      _offset -= bwidth;
      _offset += font->height * DISPLAY_X_BYTE;  // novy radek
      return( c);
   }
   if( c == CH_NULL){
      // mezera bez prepsani podkladu
      _offset += bwidth;                      // presun vpravo na nasledujici znak
      return( c);
   }
   // Nahrada znaku za spravnou pozici
   switch (c) {
     case '-': c = font->minus; break;
     case ' ': c = font->space; break;
     case '.': c = font->dot; break;
     case ':': c = font->colon; break;
     default:  c -= font->offset;             // Normalni znak, pole znaku zacina od znaku 'offset'
   }//else
   index  = c * (bwidth * font->height);      // zacatek vybraneho znaku
   DisplayPattern( &font->array[ index], bwidth, font->height);
   return( c);
#ifdef DISPLAY_MODE_MIXED
   }    // vetev plane == 1
#endif
} // putchar

#endif // DISPLAY_PUTCHAR

//-----------------------------------------------------------------------------
// Zapis prikazu
//-----------------------------------------------------------------------------

/*void DisplayWriteCommand( byte cmd)
// zapise do displeje prikaz
{
   DisplayAddressCommand();                    // nahodit A0
   DisplayWriteData( cmd);
   DisplayAddressData();                       // shodit A0, nasleduji parametry
} // WriteCommand*/

//-----------------------------------------------------------------------------
// Mazani RAM
//-----------------------------------------------------------------------------

static void ClearRAM( void)
// Smaze vsechny roviny RAM
{
   #ifdef DISPLAY_MODE_MIXED
      // nastaveni textove roviny na mezery :
      VideoRAMSet( DISPLAY_SAD1_START, DISPLAY_ROWS * DISPLAY_COLS, ' ');
      // nastaveni graficke roviny SAD2 na 0 :
      VideoRAMSet( DISPLAY_SAD2_START, DISPLAY_X_BYTE * DISPLAY_HEIGHT, 0);
   #elif DISPLAY_MODE_GRAPHICS2
      // nastaveni graficke roviny SAD1 na 0 :
      VideoRAMSet( DISPLAY_SAD1_START, DISPLAY_X_BYTE * DISPLAY_HEIGHT, 0);
      // nastaveni graficke roviny SAD2 na 0 :
      VideoRAMSet( DISPLAY_SAD2_START, DISPLAY_X_BYTE * DISPLAY_HEIGHT, 0);
   #elif DISPLAY_MODE_GRAPHICS3
      // nastaveni graficke roviny SAD1 na 0 :
      VideoRAMSet( DISPLAY_SAD1_START, DISPLAY_X_BYTE * DISPLAY_HEIGHT, 0);
      // nastaveni graficke roviny SAD2 na 0 :
      VideoRAMSet( DISPLAY_SAD2_START, DISPLAY_X_BYTE * DISPLAY_HEIGHT, 0);
      // nastaveni graficke roviny SAD3 na 0 :
      VideoRAMSet( DISPLAY_SAD3_START, DISPLAY_X_BYTE * DISPLAY_HEIGHT, 0);
   #endif
} // ClearRAM

//-----------------------------------------------------------------------------
// Nastaveni ukazatele RAM
//-----------------------------------------------------------------------------

void SetCursor( word address)
// Nastavi adresu do video RAM
{
   DisplayWriteCommand( DISPLAY_CMD_CSRW);
   WriteData( DISPLAY_MASK_CSRL & address);
   WriteData( DISPLAY_MASK_CSRH & (address >> 8));
} // SetCursor

//-----------------------------------------------------------------------------
// Nastaveni RAM na hodnotu
//-----------------------------------------------------------------------------

static void VideoRAMSet( word start, word length, char ch)
// Nastavi oblast video RAM na zadanou hodnotu. POZOR, pred volanim musi byt
// nastaven smysl pohybu kurzoru
{
word i;

   DisplayWriteCommand(DISPLAY_CMD_CSR_RIGHT);
   SetCursor( start);
   //------- MWRITE
   DisplayWriteCommand( DISPLAY_CMD_MWRITE);
   for( i = 0; i < length; i++){
      WriteData( ch);
   }
   DisplayWriteCommand(DISPLAY_CMD_CSR_DOWN);
} // VideoRAMSet

#ifdef DISPLAY_MODE_MIXED
//-----------------------------------------------------------------------------
// Zobrazeni kurzoru
//-----------------------------------------------------------------------------
#ifdef DISPLAY_CURSOR

void DisplayCursor( bool on)
// je-li <on>=NO zhasne kurzor, jinak rozsviti
{
word Start;

   DisplayEnable();
   if( on){
      // pozice kurzoru :
      Start  = DISPLAY_SAD1_START + row * DISPLAY_COLS;
      Start += col;
      SetCursor( Start);
      // zapnuti kurzoru :
      DisplayWriteCommand( DISPLAY_CMD_DISP_ON);
      WriteData( DISPLAY_MASK_CURSOR_FLASH2 |
                 DISPLAY_MASK_SAD1_ON  |
                 DISPLAY_MASK_SAD2_ON  |
                 DISPLAY_MASK_SAD3_OFF
               );
                                 // P1 - Blikajici kurzor, zapnuty roviny SAD1, SAD2
   } else {
      // zhasnuti kurzoru :
      DisplayWriteCommand( DISPLAY_CMD_DISP_ON);
      WriteData( DISPLAY_MASK_CURSOR_OFF |
                 DISPLAY_MASK_SAD1_ON  |
                 DISPLAY_MASK_SAD2_ON  |
                 DISPLAY_MASK_SAD3_OFF
               );
                                 // P1 - vypnuty kurzor, zapnuty roviny SAD1, SAD2
   }
   DisplayDisable();
} // DisplayCursor

#endif // DISPLAY_CURSOR
//-----------------------------------------------------------------------------
// Mazani radku
//-----------------------------------------------------------------------------
#ifdef DISPLAY_CLR_EOL

void DisplayClrEol( void)
// smaze az do konce radku, nemeni pozici kurzoru
{
word Start;

   DisplayEnable();
   Start  = DISPLAY_SAD1_START + row * DISPLAY_COLS;
   Start += col;
   VideoRAMSet( Start, DISPLAY_COLS - col, ' ');
   DisplayDisable();
} // DisplayClrEol
#endif // DISPLAY_CLR_EOL

#endif // DISPLAY_MODE_MIXED
