//*****************************************************************************
//
//    FArray.c - FIFO array module
//    Version 1.1, (c) Vymos
//
//*****************************************************************************


/*

Verze 1.1:
  - 7.6.2005: Pridan watchdog do fce FarReset(), pri count = 50 a size = 4 se to resetovalo, protoze ED2 nema strankovy zapis, tj. 200*10ms = 2sec.

*/

#include "Fifo\FArray.h"
#include "Iep.h"

#ifndef NO_DIESEL
#define FAR_INVALID_POSITION      0xFF               // neplatna pozice

// Promenna <address> udava zacatek pole hodnot,
// promenna <count> udava pocet prvku pole
// promenna <size> je velikost prvku v bytech

// Lokani funkce :

static byte FindMarker( word address, byte count, byte size);
// Vrati pozici markeru

static TYesNo WriteMarker( word address, byte size);
// Zapise znacku na pozici <address>

//-----------------------------------------------------------------------------
// Zapis
//-----------------------------------------------------------------------------

TYesNo FarWrite( dword value, word address, byte count, byte size)
// Zapise hodnotu do interni EEPROM
{
byte     i;
byte     pos;
byte     offset;
TFarData un;

   pos = FindMarker( address, count, size);
   if( pos == FAR_INVALID_POSITION){
      pos = 0;     // zkus zapis od prvni pozice
   }
   // nova pozice markeru :
   if( pos + 1 == count){
      // posledni pozice, marker na zacatek :
      WriteMarker( address, size);
   } else {
      // marker na nasledujici pozici
      offset = (pos + 1) * size;
      WriteMarker( address + offset, size);
   }
   // zapis dat :
   offset = pos * size;
   un.dw  = value;
   if( !IepPageWriteStart()){
      return( NO);
   }
   for( i = 0; i < size; i++){
      IepPageWriteData( address + (offset + i), un.array[ i]);
   }
   IepPageWritePerform();
   return( YES);      // zapis markeru na pozici
} // FarWrite

//-----------------------------------------------------------------------------
// Cteni
//-----------------------------------------------------------------------------

dword FarRead( word address, byte count, byte size)
// Precte hodnotu z interni EEPROM
{
byte     pos;
byte     offset;
byte     i;
TFarData un;

   pos = FindMarker( address, count, size);
   if( pos == FAR_INVALID_POSITION){
      return( 0);     // nenalezena znacka, vrat cokoli
   }
   if( pos == 0){
      // znacka na prvni pozici, data na posledni
      pos = count - 1;
   } else {
      // platna hodnota je pred znackou
      pos--;
   }
   offset = pos * size;
   un.dw  = 0;
   for( i = 0; i < size; i++){
      un.array[ i] = IepRead( address + (offset + i));
   }
   return( un.dw);
} // FarRead

#ifdef FAR_RESET
//-----------------------------------------------------------------------------
// Mazani obsahu
//-----------------------------------------------------------------------------

TYesNo FarReset( word address, byte count, byte size)
// Vymaze celou oblast, zapise marker na prvni pozici
{
byte i;
byte length;

   // mazani cele oblasti :
   length = count * size;
   if( !IepPageWriteStart()){
      return( NO);
   }
   for( i = 0; i < length; i++){
      IepPageWriteData( address + i, 0);
      WatchDog();       // Pridano 7.6.2005: pri count = 50 a size = 4 se to resetovalo, protoze ED2 nema strankovy zapis, tj. 200*10ms = 2sec.
   }
   IepPageWritePerform();
   // zapis markeru :
   return( WriteMarker( address, size));
} // FarReset
#endif

//-----------------------------------------------------------------------------
// Hledani znacky
//-----------------------------------------------------------------------------

static byte FindMarker( word address, byte count, byte size)
// Vrati pozici markeru
{
byte i, j;

   // znacka je ulozena jako FAR_MARKER 0 0 0 ...
   for( i = 0; i < count; i++, address += size){
      if( IepRead( address) != FAR_MARKER){
         continue;         // neni zacatek markeru
      }
      // na prvni pozici je MARKER, dalsi pozice :
      for( j = 1; j < size; j++){
          if( IepRead( address + j) != 0){
             goto next;
          }
      }
      return( i);          // nasli jsme
next :;                    // nesedi nektera z dalsich pozic
   }
   return( FAR_INVALID_POSITION);
} // FindMarker

//-----------------------------------------------------------------------------
// Zapis znacky
//-----------------------------------------------------------------------------

static TYesNo WriteMarker( word address, byte size)
// Zapise znacku na pozici <address>
{
byte i;

   if( !IepPageWriteStart()){
      return( NO);
   }
   IepPageWriteData( address, FAR_MARKER);
   for( i = 1; i < size; i++){
      IepPageWriteData( address + i, 0);
   }
   IepPageWritePerform();
   return( YES);
} // WriteMarker

#endif