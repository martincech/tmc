//*****************************************************************************
//
//    Ads7846.c -  Touch screen controller ADS 7846
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

// Testuje ustaleni souradnic po prvnim dotyku a po nabehu
// Obsahuje synchronni cekani

#include "Ads7846\Ads7846.h"
#include "System.h"            // SysDelay

#define __DEBUG__

#ifdef __DEBUG__
   #include "Sed1335\Sed1335.h"    // display
   #include "conio.h"      // jednoduchy display
#endif


TTouch Touch;

#define IsPen() (!TouchPENIRQ)                             // dotek panelu
#define delta( x, y)   ((x) > (y) ? (x) - (y) : (y) - (x)) // absolutni diference
#define TimerTicks( ms) ((ms) / TIMER0_PERIOD)             // prepocet [ms] na tiky casovace

//-----------------------------------------------------------------------------
// Kontext
//-----------------------------------------------------------------------------

volatile byte _touch_counter;         // citac prodlevy
static   byte _touch_context;         // kontext automatu

#define TOUCH_IDLE         0x00       // cekame na udalost
#define TOUCH_WAIT_REPEAT  0x01       // stisknuto, ceka se na dalsi repeat

//-----------------------------------------------------------------------------
// Ridici priznaky
//-----------------------------------------------------------------------------

// adresy (vcetne startbitu) :
#define TOUCH_TEMP0         0x80  // A0-2 SER mereni teploty TEMP0
#define TOUCH_Y             0x90  // A0-2 DFR mereni vstupu X
#define TOUCH_VBAT          0xA0  // A0-2 SER mereni baterie
#define TOUCH_Z1            0xB0  // A0-2 DFR mereni tlaku Z1
#define TOUCH_Z2            0xC0  // A0-2 DFR mereni tlaku Z2
#define TOUCH_X             0xD0  // A0-2 DFR mereni vstupu Y
#define TOUCH_AUX           0xE0  // A0-2 SER mereni vstupu AUX
#define TOUCH_TEMP1         0xF0  // A0-2 SER mereni teploty TEMP1

// ostatni priznaky :
#define TOUCH_8BITS         0x08  // MODE 8 bitovy prevod
#define TOUCH_SER           0x04  // SER single ended (0 - differential)

#define TOUCH_POWER_DOWN    0x00  // PD0-1 Power Down mode
#define TOUCH_ADC_ON        0x01  // PD0-1 ADC on
#define TOUCH_REF_ON        0x02  // PD0-1 REFERENCE on
#define TOUCH_POWER_ON      (TOUCH_ADC_ON | TOUCH_REF_ON)     // trvale zapnuto

#if TOUCH_DATA_SIZE == 8
   #define TOUCH_RESOLUTION  TOUCH_8BITS
   #if (TOUCH_X_RANGE < 255) && (TOUCH_Y_RANGE < 255)
      typedef byte  TData;         // zakladni presnost
      typedef word  TDData;        // dvojnasobna presnost (RANGE * TData)
   #else
      typedef byte  TData;         // zakladni presnost
      typedef dword TDData;        // dvojnasobna presnost (RANGE * TData)
   #endif

#elif TOUCH_DATA_SIZE == 12
   #define TOUCH_RESOLUTION  0x00
   typedef word  TData;            // zakladni presnost
   typedef dword TDData;           // dvojnasobna presnost (RANGE * TData)

#else
   #error "Unknown TOUCH_DATA_SIZE"
#endif

// Maximalni hodnota prevodniku (jen pro otoceni X, Y) :
#define TOUCH_MAX_VALUE         ((1 << TOUCH_DATA_SIZE) - 1)

// Lokalni funkce :

static TData ReadConversion( byte control);
// Cteni prevodniku, <control> je ridici byte prevodniku

static TYesNo ReadCoordinate( byte control, TData data *coord);
// Opakovane cte souradnici az do ustaleni

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

TYesNo TouchInit( void)
// Inicializace Touch screen
{
byte i;

   TouchDeselect();
   TouchDCLK   = 0;
   TouchDIN    = 0;
   TouchDOUT   = 1;    // vstup
#ifdef TouchBUSY
   TouchBUSY   = 1;    // vstup
#endif
   TouchPENIRQ = 1;    // vstup
   TouchSelect();
   // odstran stara data :
   for( i = 0; i < 18; i++){
      TouchDCLK = 1;
      TouchDCLK = 0;
   }
   // power down a deselect :
   ReadConversion( TOUCH_X | TOUCH_RESOLUTION | TOUCH_POWER_DOWN);
   _touch_context = TOUCH_IDLE;
   return( YES);
} // TouchInit

//-----------------------------------------------------------------------------
// Testovani dotyku
//-----------------------------------------------------------------------------

TYesNo TouchGet( void)
// Je-li stisknuto, vraci YES a souradnice
{
static TData x1, y1;
TData        x2, y2;

   switch( _touch_context){
      case TOUCH_IDLE :
         if( !IsPen()){
            return( NO);                              // cekame na stisknuti
         }
         TouchX( &x1);                                // zapamatuj souradnice prvniho dotyku
         TouchY( &y1);
         SysDelay( TOUCH_PEN_RISE);                   // synchronni cekani na ustaleni
         if( !IsPen() || !TouchX( &x2) || !TouchY( &y2)){
            // odpadl nebo neustalena souradnice
            return( NO);
         }
         if( (delta( x2, x1) > TOUCH_DIFFERENCE) || (delta( y2, y1) > TOUCH_DIFFERENCE)){
            // souradnice prvniho dotyku se lisi od aktualni
            return( NO);
         }
         _touch_context = TOUCH_WAIT_REPEAT;          // zahajeni autorepeat
         _touch_counter = TimerTicks( TOUCH_AUTOREPEAT_START); // prodleva po zahajeni
         Touch.X = x2;
         Touch.Y = y2;
         Touch.Repeat = NO;
         return( YES);                                // platny dotek

      case TOUCH_WAIT_REPEAT:
         if( !IsPen()){
            _touch_context = TOUCH_IDLE;              // odpadl
            return( NO);
         }
         if( _touch_counter != 0){
            return( NO);                              // stale cekame
         }
         if( !TouchX( &x2) || !TouchY( &y2)){
            // neustalena souradnice
            _touch_context = TOUCH_IDLE;              // odpadl
            return( NO);
         }
         _touch_context = TOUCH_WAIT_REPEAT;          // autorepeat
         _touch_counter = TimerTicks( TOUCH_AUTOREPEAT_SPEED); // prodleva mezi opakovanim
         Touch.X = x2;
         Touch.Y = y2;
         Touch.Repeat = YES;
         return( YES);                                // platny dotek
   }//switch
} // TouchGet

//-----------------------------------------------------------------------------
// Cteni souradnice
//-----------------------------------------------------------------------------

TYesNo TouchX( word data *x)
// Vraci souradnici X dotyku
{
#if TOUCH_X_RANGE == 0
   return( ReadCoordinate( TOUCH_X | TOUCH_RESOLUTION, x));
#else
TData value;
byte  ok;

   ok = ReadCoordinate( TOUCH_X | TOUCH_RESOLUTION, &value);

#ifdef TOUCH_READ_RAW
  Touch.RawX = value;
#endif

   #if TOUCH_XL < TOUCH_XR
      // vlevo je nizsi souradnice
      if( value <= TOUCH_XL){
         *x = 0;
         return( ok);
      }
      if( value >= TOUCH_XR){
         *x = TOUCH_X_RANGE - 1;
         return( ok);
      }
      value -= TOUCH_XL;
      *x = ((TOUCH_X_RANGE - 1) * (TDData)value) / (TOUCH_XR - TOUCH_XL);
   #else
      // vpravo je nizsi souradnice
      if( value <= TOUCH_XR){
         *x = TOUCH_X_RANGE - 1;
         return( ok);
      }
      if( value >= TOUCH_XL){
         *x = 0;
         return( ok);
      }
      value -= TOUCH_XR;
      *x = TOUCH_X_RANGE - 1 - ((TOUCH_X_RANGE - 1) * (TDData)value) / (TOUCH_XL - TOUCH_XR);
   #endif
   return( ok);
#endif
} // TouchX

//-----------------------------------------------------------------------------
// Cteni souradnice
//-----------------------------------------------------------------------------

TYesNo TouchY( word data *y)
// Vraci souradnici Y dotyku
{
#if TOUCH_X_RANGE == 0
   return( ReadCoordinate( TOUCH_Y | TOUCH_RESOLUTION, y));
#else
TData value;
byte  ok;

   ok = ReadCoordinate( TOUCH_Y | TOUCH_RESOLUTION,  &value);

#ifdef TOUCH_READ_RAW
  Touch.RawY = value;
#endif

   #if TOUCH_YU < TOUCH_YD
      // nahore je mensi souradnice
      if( value <= TOUCH_YU){
         *y = 0;
         return( ok);
      }
      if( value >= TOUCH_YD){
         *y = TOUCH_Y_RANGE - 1;
         return( ok);
      }
      value -= TOUCH_YU;
      *y = ((TOUCH_Y_RANGE - 1) * (TDData)value) / (TOUCH_YD - TOUCH_YU);
   #else
      // dole je mensi souradnice
      if( value <= TOUCH_YD){
         *y = TOUCH_Y_RANGE - 1;
         return( ok);
      }
      if( value >= TOUCH_YU){
         *y = 0;
         return( ok);
      }
      value -= TOUCH_YD;
      *y = TOUCH_Y_RANGE - 1 - ((TOUCH_Y_RANGE - 1) * (TDData)value) / (TOUCH_YU - TOUCH_YD);
   #endif
   return( ok);
#endif
} // TouchY

//-----------------------------------------------------------------------------
// Cteni AUX
//-----------------------------------------------------------------------------
#ifdef TOUCH_READ_AUX

word TouchAux( void)
// Mereni napeti na vstupu AUX
{
TData value;

   ReadConversion( TOUCH_X | TOUCH_RESOLUTION | TOUCH_REF_ON);            // zapnuti reference
   value = ReadConversion( TOUCH_AUX | TOUCH_RESOLUTION | TOUCH_REF_ON | TOUCH_SER);
   ReadConversion( TOUCH_X | TOUCH_RESOLUTION | TOUCH_POWER_DOWN);        // navrat do Power Down
   return( value);
} // TouchAux

#endif // TOUCH_READ_AUX

//-----------------------------------------------------------------------------
// Cteni VBAT
//-----------------------------------------------------------------------------
#ifdef TOUCH_READ_VBAT

word TouchVbat( void)
// Mereni napeti na vstupu Vbat
{
TData value;

   ReadConversion( TOUCH_X | TOUCH_RESOLUTION | TOUCH_REF_ON);            // zapnuti reference
   value = ReadConversion( TOUCH_VBAT | TOUCH_RESOLUTION | TOUCH_REF_ON | TOUCH_SER);
   ReadConversion( TOUCH_X | TOUCH_RESOLUTION | TOUCH_POWER_DOWN);        // navrat do Power Down
   return( value);
} // TouchVbat

#endif // TOUCH_READ_VBAT

//-----------------------------------------------------------------------------
// Cteni prevodniku
//-----------------------------------------------------------------------------

static TData ReadConversion( byte control)
// Cteni prevodniku, <control> je ridici byte prevodniku
// Na konci vrati sdilene porty do H
{
byte  i;
TData value;

   TouchDCLK = 0;
   TouchDIN  = 0;
   TouchDOUT = 1;    // vstup
   TouchSelect();
   // nastaveni adresy MUX a prip. zapnuti buzeni :
   for( i = 0; i < 8; i++){
      TouchDIN  = control & 0x80;
      TouchDCLK = 1;
      control <<= 1;
      TouchDCLK = 0;
   }
   // priprava na cteni :
   TouchDIN  = 0;
   TouchDCLK = 1;
   _nop_();
   TouchDCLK = 0;     // sestupnou hranou pripravena data

   // cteni dat :
   value = 0;
   for( i = 0; i < TOUCH_DATA_SIZE; i++){
      value   <<= 1;
      TouchDCLK = 1;  // nabezna (neaktivni) hrana
      if( TouchDOUT){
         value |= 0x01;
      }
      TouchDCLK = 0;  // sestupna (aktivni) hrana
   }
   TouchDeselect();
   // Vratim sdilene porty na H
   TouchDCLK   = 1;
   TouchDIN    = 1;
   return( value);
} // ReadConversion

//-----------------------------------------------------------------------------
// Cteni souradnice
//-----------------------------------------------------------------------------

static TYesNo ReadCoordinate( byte control, TData data *coord)
// Opakovane cte souradnici az do ustaleni
{
byte  i;
TData c, old_c;
bool  done_ok;

   ReadConversion( control | TOUCH_POWER_ON);            // zapnuti napajeni
   old_c   = 0xFFFF;                                     // neplatna hodnota
   done_ok = NO;                                         // zatim neplatny vysledek
   for( i = 0; i < TOUCH_REPEAT_COUNT; i++){
      c = ReadConversion( control | TOUCH_POWER_ON);     // cteni souradnice
      if( delta( c, old_c) < TOUCH_REPEAT_RANGE){
         done_ok = YES;
         break;
      }
      old_c = c;
   }
   *coord = c;
   ReadConversion( control | TOUCH_POWER_DOWN);          // vypnuti napajeni
   return( done_ok);
} // ReadCoordinate
