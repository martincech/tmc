//*****************************************************************************
//
//    ComAsync.c   RS232 communication services, Rx/Tx asynchronous
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

/*
  - 7.1.2007: Pridana MARK parity (2 stop bity)

 */

#include "RS232\Com.h"
#include "Fifo\TinyFifo.h"           // FIFO pro buffery
#include "Rd2\Delays.h"             // Delay computing

#define ComDisableInt()   ES = 0
#define ComEnableInt()    ES = 1

//-- FIFO pro Rx -------------------------------------------------------------

#define COM_ISIZE          COM_RX_BUFFER_SIZE           // delka Rx fronty

static volatile    byte iread;                          // cteci ukazatel do Rx bufferu
static volatile    byte iwrite;                         // zapisovaci ukazatel do Rx bufferu
static __comdata__ byte ibuff[ COM_ISIZE];              // Rx fifo


#define ComRFifoInit()     TFifoInit(  iread, iwrite)                // inicializace
#define ComRFifoEmpty()    TFifoEmpty( iread, iwrite)                // zadne nactene znaky
#define ComRFifoRead()     TFifoRead(  ibuff, iread,  COM_ISIZE)     // cteni znaku
#define ComRFifoFlush()    TFifoFlush( iread);                       // preskok znaku
#define ComRFifoFull()     TFifoFull(  iread, iwrite, COM_ISIZE)     // zaplnene fifo
#define ComRFifoWrite( ch) TFifoWrite( ibuff, iwrite, COM_ISIZE, ch) // zapis znaku

//-- FIFO pro Tx -------------------------------------------------------------

#define COM_OSIZE          COM_TX_BUFFER_SIZE           // delka Tx fronty

volatile           byte _tx_running;                    // probiha vysilani

static volatile    byte oread;                          // cteci ukazatel do Tx bufferu
static volatile    byte owrite;                         // zapisovaci ukazatel do Tx bufferu
static __comdata__ byte obuff[ COM_OSIZE];              // Tx fifo


#define ComTFifoInit()     TFifoInit(  oread, owrite)                // inicializace
#define ComTFifoEmpty()    TFifoEmpty( oread, owrite)                // zadne nactene znaky
#define ComTFifoRead()     TFifoRead(  obuff, oread,  COM_OSIZE)     // cteni znaku
#define ComTFifoFlush()    TFifoFlush( oread);                       // preskok znaku
#define ComTFifoFull()     TFifoFull(  oread, owrite, COM_OSIZE)     // zaplnene fifo
#define ComTFifoWrite( ch) TFifoWrite( obuff, owrite, COM_OSIZE, ch) // zapis znaku

//-----------------------------------------------------------------------------
// Inicializace komunikace
//-----------------------------------------------------------------------------

#define ComSpecialInit()   ComRFifoInit();ComTFifoInit();_tx_running = NO;ComEnableInt();

void ComInit( void)
// Inicializace komunikace
{
   #ifdef ComTxDisable
      ComTxDisable();                  // RS485 disable Tx
   #endif
   #if DOUBLE_BAUDRATE_T1 == 2
      PCON |=  PCON_SMOD1;
   #else
      PCON &= ~PCON_SMOD1;
   #endif
   #ifdef COM_TIMER_1
      // Timer 1 jako baud generator :
      SetTimer1Baud( COM_BAUD);
      TMOD &= 0x0F;
      TMOD |= TMOD_M81;                // timer, mode 2, TR1 control
      TR1   = 1;                       // run timer 1
   #elif COM_TIMER_2
      // Timer 2 jako baud generator :
      SetTimer2Baud( COM_BAUD);
      T2CON  = T2CON_COM;              // TR2=1, RCLK, TCLK
   #else
      // ED2 ma baudrate timer, mozna casem doplnit
      #error "Neni definovan casovac pro RS232"
   #endif
   #if defined( COM_PARITY_EVEN) || defined( COM_PARITY_ODD) || defined( COM_PARITY_MARK)
      SCON = SCON_PARITY;              // serial MODE 3, enable Rx
   #else
      SCON = SCON_NORMAL;              // serial MODE 1, enable Rx
   #endif
   #ifdef ComSpecialInit
      ComSpecialInit();
   #endif
} // ComInit

//-----------------------------------------------------------------------------
// Vysilani
//-----------------------------------------------------------------------------

void ComTxChar( byte Char)
// Vyslani znaku
{
   if( ComTFifoFull()){
      return;                          // neni misto ve fronte
   }
   ComDisableInt();
   if( !_tx_running){
      // prazdne FIFO, vysilani je zastaveno
      _tx_running = YES;               // zahajeni vysilani
      #ifdef ComTxEnable
         ComTxEnable();                // RS485, povoleni vysilani
      #endif
      TI = 1;                          // vyvolej interrupt na vyslani prvniho znaku
   } // else probiha vysilani
   ComTFifoWrite( Char);
   ComEnableInt();
} // ComTxChar

//-----------------------------------------------------------------------------
// Prijem
//-----------------------------------------------------------------------------

TYesNo ComRxChar( byte data *c)
// Vraci prijaty znak, nebo NO pro timeout
{
word Timeout;

   Timeout = WDelayCount( COM_RX_TIMEOUT, 6);
   do {
      if( !ComRFifoEmpty()){
         // prijat znak
         ComDisableInt();
         *c = ComRFifoRead();
         ComEnableInt();
         return( YES);
      }
   } while( --Timeout);
   return( NO);
} // ComRxChar

#ifdef COM_RX_WAIT
//-----------------------------------------------------------------------------
// Cekani na Rx
//-----------------------------------------------------------------------------

TYesNo ComRxWait( byte Timeout)
// Ceka na znak az <Timeout> * 100 milisekund
{
word i;

   i = 1;
   Timeout++;
   do {
      WatchDog();
      do {
         if( !ComRFifoEmpty()){
            return( YES);                        // pripraven znak
         }
      } while( --i);
      i = WDelayCount( 100000, 6);
   } while( --Timeout);
   return( NO);
} // ComRxWait
#endif // COM_RX_WAIT

#ifdef COM_FLUSH_CHARS
//-----------------------------------------------------------------------------
// Preskok prijmu
//-----------------------------------------------------------------------------

void ComFlushChars( void)
// Vynecha vsechny znaky az do meziznakoveho timeoutu
{
word Timeout;

   Timeout = WDelayCount( COM_RX_TIMEOUT, 10);
   do {
      WatchDog();
      if( !ComRFifoEmpty()){
         // prijat znak
         ComDisableInt();
         ComRFifoFlush();
         ComEnableInt();
         Timeout = WDelayCount( COM_RX_TIMEOUT, 10);  // znovu timeout
         continue;
      }
   } while( --Timeout);
} // ComFlushChars

#endif // COM_FLUSH_CHARS

//-----------------------------------------------------------------------------
// Prerusovaci rutina
//-----------------------------------------------------------------------------

static void ComIsr( void) interrupt INT_SERIAL
{
byte c;

   if( RI){
      // preruseni od prijimace
      RI = 0;                          // zrus priznak preruseni
      c  = SBUF;                       // prevezmi znak
      #ifdef COM_PARITY_EVEN
         ACC = c;
         if( RB8 != P){
            goto _check_tx;            // chyba parity
         }
      #endif
      #ifdef COM_PARITY_ODD
         ACC = c;
         if( RB8 == P){
            goto _check_tx;            // chyba parity
         }
      #endif
      // uspesny prijem znaku
      if( ComRFifoFull()){
         ComRFifoFlush();              // zahod nejstarsi znak
      }
      ComRFifoWrite( c);               // zapis znak
   }
#if defined(COM_PARITY_EVEN) || defined(COM_PARITY_ODD)
_check_tx :
#endif
   if( TI){
      // preruseni od vysilace
      TI = 0;
      if( ComTFifoEmpty()){
         _tx_running = NO;             // dovysilano
         #ifdef ComTxDisable
            ComTxDisable();            // RS485, zakaz vysilani
         #endif
         return;                       // fronta je prazdna
      }
      c = ComTFifoRead();
      #ifdef COM_PARITY_EVEN
         ACC = c;
         TB8 = P;
      #endif
      #ifdef COM_PARITY_ODD
         ACC = c;
         TB8 = !P;
      #endif
      #ifdef COM_PARITY_MARK
         TB8 = 1;
      #endif
      SBUF = c;                        // vyslani
   }
} // ComIsr

