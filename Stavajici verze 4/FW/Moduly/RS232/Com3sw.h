//*****************************************************************************
//
//    Com3sw.h - RS232 software emulation services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Com3sw_H__
   #define __Com3sw_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void Com3TxChar( byte c);
// Vyslani znaku. Po timeoutu vraci NO

/* --- neimplementovano
TYesNo Com3RxChar( byte data *Char);
// Vraci prijaty znak. Po timeoutu vraci NO

TYesNo Com3RxWait( byte Timeout);
// Ceka na znak az <Timeout> * 100 milisekund

void Com3FlushChars( void);
// Vynecha vsechny znaky az do meziznakoveho timeoutu
*/

#endif

