$HW_VERSIONS=@(
  @(4),
  @(5),
  @(6),
  @(10),
  @(11),
  @(12)
)
$MAJOR=4
$MINOR=5
$BUILD=6
$VERSION="{0}.{1:d2}.{2}" -f $MAJOR,$MINOR,$BUILD

$MM_VERSION="{0}.{1:d2}" -f $MAJOR,$MINOR
$targetpath="x:\Programy HEX\Tmc4\"+$MM_VERSION+"\"
$languages = @(
  @("LANG_CZ","CZ"),
  @("LANG_EN", "EN"),
  @("LANG_GE", "DE"),
  @("LANG_HU", "HU"),
  @("LANG_PL", "PL"),
  @("LANG_RO", "RO"),
  @("LANG_RU", "RU"),  
  @("LANG_SW", "SW")  
)

$tmcs=@(
   "TMC",
   "TMCR"
)
#build library

For ($i=0; $i -lt $languages.Length; $i++) {
   For ($v=0; $v -lt $HW_VERSIONS.Length; $v++){
      For($t=0; $t -lt $tmcs.Length; $t++){
         $hw=$HW_VERSIONS[$v]
         $fpath = $tmcs[$t] + "\" + $languages[$i][1]
         $sourcefolder= ".\hexy\" + $fpath
         $file= $sourcefolder+"\" + $tmcs[$t] + " " + $VERSION + "-" +$hw + " " + $languages[$i][1] + ".HEX"
         $hex= $tmcs[$t] + ".HEX"
         $destfolder=$targetpath + $fpath      
         if(!(Test-Path -Path "$destfolder" )){
            New-Item -ItemType directory -Path $destfolder
         }
         Copy-Item $file  -Destination $destfolder        
      }
   }
}