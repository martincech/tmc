//*****************************************************************************
//
//    Tmcr.c - posilani prikazu do TMC
//    Version 1.0
//
//*****************************************************************************

#ifndef __Tmcr_H__
   #define __Tmcr_H__

#include "Hardware.h"     // pro podminenou kompilaci
#include "Packet\ComPkt.h"     // Packet interface


extern byte __xdata__ CommunicationError;   // YES/NO porucha komunikace
extern byte __xdata__ ReplyError;           // YES/NO po zaslani prikazu nedosla z TMC odpoved, prikaz se neprovedl

extern word __xdata__ PacketCounterTotalOk; // Pocet dobrych paketu celkem
extern word __xdata__ PacketCounterTotalErr;// Pocet vadnych paketu celkem

#define CFG_TARGET_TEMPERATURE  1
#define CFG_LOCALIZATION        2


void TmcrInit();
  // Nastavi vychozi data

TYesNo TmcrReadPeriodic();
  // Nacte periodicka data z TMC

TYesNo TmcrCfgSave(byte item);

#endif
