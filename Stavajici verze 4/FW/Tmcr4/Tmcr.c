//*****************************************************************************
//
//    Tmcr.c - posilani prikazu do TMC
//    Version 1.0
//
//*****************************************************************************

#include "Tmcr.h"

#include "RS232\Com.h"        // COM interface
#include "..\Tmc4\Ras.h"       // Prikazy do TMC
#include "System.h"     // "operacni system" - SysSetTimeout()

#include "..\Tmc4\Control.h"           // Automaticke rizeni
#include "..\Tmc4\FAir.h"              // Regulace cerstrveho vzduchu
#include "..\Tmc4\Time.h"              // Aktualni datum a cas
#include "..\Tmc4\Temp.h"              // Teploty
#include "..\Tmc4\FAir.h"              // Regulace cerstrveho vzduchu
#include "..\Tmc4\Heating.h"           // Regulace topeni
#include "..\Tmc4\Cooling.h"           // Regulace chlazeni
#include "..\Tmc4\Servo.h"             // Stav serv
#include "..\Tmc4\Accu.h"              // Napeti a proud akumulatoru
#include "..\Tmc4\Diesel.h"            // Diesel motor
#include "..\Tmc4\Charging.h"          // Dobijeni
#include "..\Tmc4\ElMotor.h"           // Elektromotor
#include "..\Tmc4\Fan.h"               // Ventilatory
#include "..\Tmc4\Control.h"           // Automaticke rizeni
#include "..\Tmc4\Alarm.h"             // Indikace poruch
#include "..\Tmc4\Menu.h"              // Prekresleni
#include "..\Tmc4\Co2.h"               // Koncentrace CO2
#include "..\Tmc4\Humidity.h"          // Relativni vlhkost
#include "..\Tmc4\AirPress.h"          // Tlak radiatoru a filtru
#include "..\Tmc4\Disinf.h"            // Dezinfekce


#define PACKET_REPEAT           3     // Pocet pokusu o zaslani prikazu a prijem odpovedi
#define PACKET_MAX_ERRORS       15    // Maximalni pocet chybne prijatych periodickych paketu pred vyhlasenim poruchy prijmu

byte __xdata__ CommunicationError;    // YES/NO porucha periodicke komunikace
byte __xdata__ ReplyError;            // YES/NO po zaslani prikazu nedosla z TMC odpoved, prikaz se neprovedl

byte __xdata__ PacketCounterErr;       // Pocet vadnych paketu od posledniho platneho prijmu
word __xdata__ PacketCounterTotalOk;   // Pocet dobrych paketu celkem
word __xdata__ PacketCounterTotalErr;  // Pocet vadnych paketu celkem

//#define __DEBUG__

#ifdef __DEBUG__
#include "conio.h"           // jednoduchy display

#define TRACE( s)          cputs( s);putchar('\n')
#define TRACECMD( s, c, d) cputs( s);cbyte( c);putchar( ' ');cdword( d);putchar('\n')
#else
#define TRACE( s)
#define TRACECMD( s, c, d)
#endif

//-----------------------------------------------------------------------------
// Promenne z neprilinkovanych modulu
//-----------------------------------------------------------------------------

byte __xdata__ Driver;                  // Cislo nalogovaneho ridice
static TRasData __xdata__ RasData;      // Periodicka data

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void TmcrInit () {
   // Nastavi vychozi data a komunikaci s TMC
   byte j;

   ComInit ();   // Komunikace s TMC

   // DIagnostika prenosu paketu
   CommunicationError = YES;
   PacketCounterErr = PACKET_MAX_ERRORS;
   PacketCounterTotalOk = 0;
   PacketCounterTotalErr = 0;

   // Datova struktura:
   Accu.Voltage = 0;
   Config.Localization = 0;

   Control.Mode = 0;
   Control.AutoMode = 0;
   Control.EmergencyMode = NO;
   Control.OldEmergencyMode = NO;
   Control.TempFaults[TEMP_FRONT].Fault = NO;
   Control.TempFaults[TEMP_MIDDLE].Fault = NO;
   Control.TempFaults[TEMP_REAR].Fault = NO;

   Cooling.On = NO;
   Cooling.Failure = NO;
   Diesel.On = NO;
   Diesel.Temperature = 0;

   ServoStatus[SERVO_FLOOR] = 0;
   Diesel.ChangeOil = NO;
   Diesel.Overheated = NO;
   Diesel.EcuError = NO;
   ElMotorOn = NO;
   Heating.Failure = NO;
   Heating.ServoFailure = NO;

   FreshAir.Mode = 0;
   FreshAir.Percent = 0;

   FreshAir.Position = 0;
   FreshAir.FloorFlap.Position = 0;

   FreshAir.FloorFlap.Mode = 0;
   FreshAir.FloorFlap.Percent = 0;

   Heating.Position = 0;
   ServoStatus[SERVO_RECIRCULATION] = 0;
   Heating.Flame1 = NO;

   Heating.Percent = 0;
   Fan.On = NO;

   ServoStatus[SERVO_INDUCTION1] = 0;
   ServoStatus[SERVO_INDUCTION2] = 0;
   Fan.AutoMode = YES;
   Fan.Failure = NO;

   Driver = 1;
   Accu.LowVoltage = NO;

   Humidity.Value = 0;
   FiltersAirPressure = 0;
   Heating.Flame2 = NO;

   Charging.On = NO;

   Disinfection.Mode = DISINFECTION_OFF;

   for (j = 0; j < TEMP_COUNT; j++) {
      Temp1C[j] = 0;
      Temp01C[j] = 0;
   }

   Time.Day = 1;
   Time.Month = 1;
   Time.Year = 2000;
   Time.Hour = 0;
   Time.Min = 0;

   Config.TargetTemperature = 0;

   Config.MaxOffsetAbove = 0;
   Alarm.ShowAlarm = NO;

   Config.MaxOffsetBelow = 0;
   Alarm.Quiet = NO;

   Charging.Failure = NO;
   FiltersHighAirPressure = NO;
   Co2.Value = 0;
   Co2.OverLimit = NO;
   Co2.Failure = NO;
   Config.TempUnits = UNITS_CELSIUS;

   Fan.Power = 100;
}

//-----------------------------------------------------------------------------
// Dekodovani periodickych dat
//-----------------------------------------------------------------------------

static void LeaveMenuAndRedraw () {
   SysSetTimeout ();            // Rucne zmenil rezim - pokud je v nejakem menu, vypadne do zakladniho zobrazeni, aby neslo ovladat obojim zaroven
   Redraw = REDRAW_ALL;        // Prekresli cely displej, jinak by se novy rezim neprekreslil korektne
}

static void Decode () {
   // Zkonvertuje prijatou strukturu RasData do lokalnich promennych
   // POZOR, musi odpovidat kodovani na strane TMC
   byte j;

   // Cislo verze TMC zatim nekontroluju. Vyssi cislo Minor nemusi nutne znamenat, ze se zmenila struktura RasData

   Accu.Voltage = RasData.AccuVoltage;
   if (Config.Localization != RasData.ConfigLocalization) {
      Redraw = REDRAW_ALL;        // Prekresli cely displej, jinak by se novy rezim neprekreslil korektne
   }
   Config.Localization = RasData.ConfigLocalization;

   if (Control.Mode != RasData.ControlMode) {
      LeaveMenuAndRedraw ();            // Rucne zmenil rezim - pokud je v nejakem menu, vypadne do zakladniho zobrazeni, aby neslo ovladat obojim zaroven
   }
   Control.Mode = RasData.ControlMode;
   Control.AutoMode = RasData.ControlAutoMode;
   Control.EmergencyMode = RasData.ControlEmergencyMode;
   Control.OldEmergencyMode = RasData.ControlOldEmergencyMode;
   Control.TempFaults[TEMP_FRONT].Fault = RasData.ControlFrontTempFault;
   Control.TempFaults[TEMP_MIDDLE].Fault = RasData.ControlMiddleTempFault;
   Control.TempFaults[TEMP_REAR].Fault = RasData.ControlRearTempFault;

   Cooling.On = RasData.CoolingOn;
   Cooling.Failure = RasData.CoolingFailure;
   Diesel.On = RasData.DieselOn;
   Diesel.Temperature = RasData.DieselTemperature;

   ServoStatus[SERVO_FLOOR] = RasData.ServoFloorServoStatus;
   Diesel.ChangeOil = RasData.DieselChangeOil;
   Diesel.Overheated = RasData.DieselOverheated;
   Diesel.EcuError = RasData.EcuError;
   ElMotorOn = RasData.ElMotorOn;
   Heating.Failure = RasData.HeatingFailure;
   Heating.ServoFailure = RasData.HeatingServoFailure;

   if (FreshAir.Mode != RasData.FreshAirMode) {
      SysSetTimeout ();            // Rucne zmenil rezim recirkulace - pokud je v nejakem menu, vypadne do zakladniho zobrazeni, aby neslo ovladat obojim zaroven
      Redraw = REDRAW_ALL;        // Prekresli cely displej, jinak by se novy rezim neprekreslil korektne
   }
   FreshAir.Mode = RasData.FreshAirMode;
   if (FreshAir.Mode == FAIR_MANUAL && FreshAir.Percent != RasData.FreshAirPercent) {
      SysSetTimeout ();            // Rucne zmenil recirkulaci - pokud je v nejakem menu, vypadne do zakladniho zobrazeni, aby neslo ovladat obojim zaroven
   }
   FreshAir.Percent = RasData.FreshAirPercent;

   FreshAir.Position = RasData.FreshAirPosition;
   FreshAir.FloorFlap.Position = RasData.FreshAirFloorFlapPosition;

   if (FreshAir.FloorFlap.Mode != RasData.FreshAirFloorFlapMode) {
      SysSetTimeout ();            // Rucne zmenil rezim predni klapky - pokud je v nejakem menu, vypadne do zakladniho zobrazeni, aby neslo ovladat obojim zaroven
      Redraw = REDRAW_ALL;        // Prekresli cely displej, jinak by se novy rezim neprekreslil korektne
   }

   FreshAir.FloorFlap.Mode = RasData.FreshAirFloorFlapMode;
   if (FreshAir.FloorFlap.Mode == FLAP_MANUAL && FreshAir.FloorFlap.Percent != RasData.FreshAirFloorFlapPercent) {
      SysSetTimeout ();            // Rucne zmenil predni klapky - pokud je v nejakem menu, vypadne do zakladniho zobrazeni, aby neslo ovladat obojim zaroven
   }
   FreshAir.FloorFlap.Percent = RasData.FreshAirFloorFlapPercent;

   Heating.Position = RasData.HeatingPosition;
   ServoStatus[SERVO_RECIRCULATION] = RasData.RecirculationServoStatus;
   Heating.Flame1 = RasData.HeatingFlame1;

   Heating.Percent = RasData.HeatingPercent;
   Fan.On = RasData.FanOn;

   ServoStatus[SERVO_INDUCTION1] = RasData.ServoTopInductionServoStatus;
   ServoStatus[SERVO_INDUCTION2] = RasData.ServoBottomInductionServoStatus;
   Fan.AutoMode = RasData.FanAutoMode;
   Fan.Failure = RasData.FanFailure;

   Driver = RasData.LogDriver;
   Accu.LowVoltage = RasData.AccuLowVoltage;

   Co2.Value = RasData.Co2;
   Humidity.Value = RasData.Humidity;
   FiltersAirPressure = RasData.FiltersAirPressure;
   Heating.Flame2 = RasData.HeatingFlame2;

   Disinfection.Mode = RasData.DisinfectionMode;
   if (DisplayMode != RasData.DisplayMode) {
      LeaveMenuAndRedraw ();            // Zmenilo se zobrazeni dezinfekce - pokud je v nejakem menu, vypadne do zakladniho zobrazeni, aby neslo ovladat obojim zaroven
   }
   DisplayMode = RasData.DisplayMode;
   Charging.On = RasData.ChargingOn;
   Disinfection.IsInstalled = RasData.DisinfectionIsInstalled;

   for (j = 0; j < TEMP_COUNT; j++) {
      Temp1C[j] = RasData.Temp1C[j];
      Temp01C[j] = RasData.Temp01C[j];
   }

   Time.Day = RasData.Time.Time.Day;
   Time.Month = RasData.Time.Time.Month;
   Time.Year = RasData.Time.Time.Year;
   Time.Hour = RasData.Time.Time.Hour;
   Time.Min = RasData.Time.Time.Min;

   if (Config.TargetTemperature != RasData.ConfigTargetTemperature) {
      Redraw = REDRAW_ALL;        // Prekresli cely displej, jinak by se novy rezim neprekreslil korektne
   }
   Config.TargetTemperature = RasData.ConfigTargetTemperature;

   Config.MaxOffsetAbove = RasData.ConfigMaxOffsetAbove;
   Alarm.ShowAlarm = RasData.AlarmShowAlarm;

   Config.MaxOffsetBelow = RasData.ConfigMaxOffsetBelow;
   if (Alarm.Quiet != RasData.AlarmQuiet) {
      SysSetTimeout ();            // Rucne zmenil alarm - pokud je v nejakem menu, vypadne do zakladniho zobrazeni, aby neslo ovladat obojim zaroven
   }
   Alarm.Quiet = RasData.AlarmQuiet;

   Charging.Failure = RasData.ChargingFailure;
   Heating.HeatingCircuitState = RasData.HeatingCircuitState;
   FiltersHighAirPressure = RasData.FiltersPressureOverLimit;
   Co2.OverLimit = RasData.Co2OverLimit;
   Co2.Failure = RasData.Co2Failure;
   Config.TempUnits = RasData.TempUnits;

   Fan.Power = RasData.FanPower;
}

//-----------------------------------------------------------------------------
// Nacteni periodickych dat z TMC
//----------------------------------------------------------------------------

static void SetPacketOk () {
   // Volat po uspesnem prijmu paketu
   PacketCounterTotalOk++;
   PacketCounterErr = 0;
   CommunicationError = NO;      // Porucha neni
}

static void SetPacketErr () {
   // Volat po neuspesnem prijmu paketu
   PacketCounterTotalErr++;
   if (PacketCounterErr < PACKET_MAX_ERRORS) {
      CommunicationError = NO;    // ZAtim chybu nevyhlasuju, jeste cekam
      PacketCounterErr++;
   }
   else {
      CommunicationError = YES;   // Prijal jsem moc chybnych paketu za sebou, vyhlasim poruchu komunikace
   }
}

TYesNo TmcrReadPeriodic () {
   // Nacte periodicka data z TMC, volat 1x za sekundu
   word Size;

   // Schvalne nezkousim vicekrat, protoze ComFlushChars() na vsechny znaky paketu (52 znaku) nekolikrat za sebou totalne zablokuje cinnost. V pripade
   // neusapechu nic nedelam a pokusim se zase na sekundu.
   ComFlushChars ();            // Pockam na prijem vsech znaku z pripadneho predchoziho paketu (nebo smeti)
   ComTxPacket (RAS_GET_DATA, 0);
   // Nactu odpoved
   Size = sizeof (RasData);
   if (!ComRxBlock (&RasData, Size)) {
      SetPacketErr ();
      return NO;  // Odpoved neprisla
   }
   if (Size != sizeof (RasData)) {
      SetPacketErr ();
      return NO;  // Nenacetl dost bajtu
   }
   Decode ();
   SetPacketOk ();
   return YES;
}

//-----------------------------------------------------------------------------
// Poslani prikazu do TMC
//-----------------------------------------------------------------------------

static TYesNo SendCommand (byte cmd, dword data *arg) {
   // Posle do TMC prikaz s argumentem, v <arg> vraci novou hodnotu argumentu
   byte ReplyCmd;
   dword ReplyArg;
   byte j;

   ReplyError = NO;

   for (j = 0; j < PACKET_REPEAT; j++) {
      ComFlushChars ();            // Pockam na prijem vsech znaku z pripadneho predchoziho paketu (nebo smeti)
      ComTxPacket (cmd, *arg);      // Poslu prikaz do TMC
      // Nactu odpoved
      if (!ComRxPacket (&ReplyCmd, &ReplyArg)) {
         continue;  // Odpoved neprisla
      }
      if (ReplyCmd != cmd) {
         continue;  // Jde o odpoved na jiny paket
      }
      // Prila platna odpoved, koncim
      *arg = ReplyArg;      // Vratim novou hodnotu
      return YES;
   }//for
   // Ani po PACKET_REPEAT pokusech neprisla platna odpoved
   ReplyError = YES;
   return NO;
}

//-----------------------------------------------------------------------------
// Konfigurace
//-----------------------------------------------------------------------------

TYesNo TmcrCfgSave (byte item) {
   // Posle parametr konfigurace do TMC
   // Vraci NO, nepovedl-li se zapis
   dword Arg;
   byte Command;

   switch (item) {
   case CFG_TARGET_TEMPERATURE:
      Command = RAS_CMD_SET_TARGET_TEMPERATURE;
      Arg = Config.TargetTemperature;
      break;
   case CFG_LOCALIZATION:
      Command = RAS_CMD_SET_LOCALIZATION;
      Arg = Config.Localization;
      break;
   }

   // Poslu prikaz do TMC
   if (!SendCommand (Command, &Arg)) {
      return NO;     // Nepodarilo se
   }
   return(YES);
} // CfgSave


//-----------------------------------------------------------------------------
// Rezim
//-----------------------------------------------------------------------------

void ControlSetNewMode (TControlMode Mode) {
   // Nastavi novy rezim
   dword Arg;

   Arg = Mode;
   if (!SendCommand (RAS_CMD_SET_MODE, &Arg)) {
      return;     // Nepodarilo se
   }
   // Nastavim lokalni promenne, aby se mod rovnou korektne prekreslil
   Control.Mode = Mode;
   Control.NewMode = YES;           // Flag, ze se zmenil rezim, nuluje se automaticky po zpracovani rezimu
}

//-----------------------------------------------------------------------------
// Cerstvy vzduch
//-----------------------------------------------------------------------------

void FAirNewMode (TFreshAirMode Mode) {
   // Nastavi novy rezim cerstveho vzduchu
   dword Arg;

   Arg = Mode;
   if (!SendCommand (RAS_CMD_SET_FRESH_AIR_MODE, &Arg)) {
      return;     // Nepodarilo se
   }
   FreshAir.Mode = Mode;
}

void FAirManualIncrease () {
   // Pridani cerstveho vzduchu v rucnim rezimu
   dword Arg;

   if (!SendCommand (RAS_CMD_FRESH_AIR_INCREASE, &Arg)) {
      return;     // Nepodarilo se
   }
   FreshAir.Percent = Arg;
}

void FAirManualDecrease () {
   // Ubrani cerstveho vzduchu v rucnim rezimu
   dword Arg;

   if (!SendCommand (RAS_CMD_FRESH_AIR_DECREASE, &Arg)) {
      return;     // Nepodarilo se
   }
   FreshAir.Percent = Arg;
}

#if defined(USE_FLOOR_FLAPS) && defined(USE_FLOOR_FLAPS_CONTROL)

void FAirNewFloorFlapMode (TFlapMode Mode) {
   // Nastavi novy rezim klapek v podlaze
   dword Arg;

   Arg = Mode;
   if (!SendCommand (RAS_CMD_SET_FLOOR_FLAP_MODE, &Arg)) {
      return;     // Nepodarilo se
   }
   FreshAir.FloorFlap.Mode = Mode;
}

void FAirFloorFlapManualIncrease () {
   // Pridani (otevreni) podlahove klapky v rucnim rezimu
   dword Arg;

   if (!SendCommand (RAS_CMD_FLOOR_FLAP_INCREASE, &Arg)) {
      return;     // Nepodarilo se
   }
   FreshAir.FloorFlap.Percent = Arg;
}

void FAirFloorFlapManualDecrease () {
   // Ubrani (zavreni) podlahove klapky s indexem <Index> v rucnim rezimu
   dword Arg;

   if (!SendCommand (RAS_CMD_FLOOR_FLAP_DECREASE, &Arg)) {
      return;     // Nepodarilo se
   }
   FreshAir.FloorFlap.Percent = Arg;
}

#endif // USE_FLOOR_FLAPS

//-----------------------------------------------------------------------------
// Alarm
//-----------------------------------------------------------------------------

void AlarmChange () {
   // Zapne / vypne hlaseni alarmu
   dword Arg;

   if (!SendCommand (RAS_CMD_SOUND, &Arg)) {
      return;     // Nepodarilo se
   }
   Alarm.Quiet = Arg;
}

//-----------------------------------------------------------------------------
// Datum a cas
//-----------------------------------------------------------------------------

void TimeSetNewTime (TTransferTime data *NewTime) {
   // Nastavi novy datum a cas zadany strukturou TTransferTime
   // POZOR, zmeni argument <NewTime>!
   if (!SendCommand (RAS_CMD_TIME, (dword data *)NewTime)) {
      return;     // Nepodarilo se
   }
}

//-----------------------------------------------------------------------------
// Dezinfekce kol
//-----------------------------------------------------------------------------

void DisinfectionWheelsStart () {
   // Zahaji dezinfekci kol
   dword Arg;

   if (!SendCommand (RAS_CMD_DISINFECTION_WHEELS, &Arg)) {
      return;     // Nepodarilo se
   }
}

//-----------------------------------------------------------------------------
// Dezinfekce skrine
//-----------------------------------------------------------------------------

void DisinfectionBodyStart () {
   // Zahaji dezinfekci skrine
   dword Arg;

   if (!SendCommand (RAS_CMD_DISINFECTION_BODY, &Arg)) {
      return;     // Nepodarilo se
   }
}
