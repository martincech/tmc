$MAJOR=4
$MINOR=5
$BUILD=6
$VERSION="{0}.{1:d2}.{2}" -f $MAJOR,$MINOR,$BUILD
$HW_VERSIONS_FROM=1
$HW_VERSIONS_TO=12

$languages = @(
  @("LANG_CZ","CZ"),
  @("LANG_GE", "DE"),
  @("LANG_EN", "EN"),
  @("LANG_HU", "HU"),
  @("LANG_PL", "PL"),
  @("LANG_RO", "RO"),
  @("LANG_RU", "RU"),  
  @("LANG_SW", "SW")  
)

$tmcs=@(
   "TMC",
   "TMCR"
)
#build library

Set-Location -Path "Moduly\Lib"
if((Test-Path -Path "mylib.lib" )){
   Remove-Item -Force "mylib.lib"
}   
kmake.exe -f Makefile -B
Set-Location ..\..

For ($i=0; $i -lt $languages.Length; $i++) {
   For ($v=$HW_VERSIONS_FROM; $v -le $HW_VERSIONS_TO; $v++){
      For($t=0; $t -lt $tmcs.Length; $t++){
         $folder= "..\hexy\" + $tmcs[$t] + "\" + $languages[$i][1]
         $file= $folder+"\" + $tmcs[$t] + " " + $VERSION + "-" +$v + " " + $languages[$i][1] + ".HEX"
         $hex= $tmcs[$t] + ".HEX"
         $buildFolder=$tmcs[$t]+"4"
         
         Set-Location -Path $buildFolder            
         if(!(Test-Path -Path "$folder" )){
            New-Item -ItemType directory -Path $folder
         }

         if(!(Test-Path -Path "$file" )){                     
            if((Test-Path -Path $hex )){
               Remove-Item -Force $hex
            }            
            $define="DF(HW=" + $v + "," + $languages[$i][0] + ",MAJ=" + $MAJOR + ",MIN="+$MINOR + ",BU="+$BUILD+")"
            kmake.exe -f Makefile -B DEFINES=$define
            if((Test-Path -Path $hex )){
               Copy-Item $hex  -Destination $file
            }            
            
         }
         Set-Location ..
      }
   }
}